package br.gov.se.setc.bean;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;
import org.primefaces.model.chart.PieChartModel;

import br.gov.se.setc.DAO.ManifestacaoDAO;
import br.gov.se.setc.model.DashboardInfo;
import br.gov.se.setc.model.Usuario;
import br.gov.se.setc.util.GeralUtil;

@Named
@ViewScoped
public class DashboardBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private ManifestacaoDAO manifestacaoDAO = ManifestacaoDAO.getInstance();
	
	private DashboardInfo infoCidadao;
	
	private DashboardInfo infoOuvidor;
	
	private DashboardInfo infoInformacao;
	private DashboardInfo infoSugestao;
	private DashboardInfo infoSolicitacao;
	private DashboardInfo infoReclamacao;
	private DashboardInfo infoElogio;
	private DashboardInfo infoDenuncia;
	
	private PieChartModel graficoDashboard;
	
	@PostConstruct
	public void init() {
//		System.out.println(":: init dashboardBean ::");
		
		String perfilLogado = (String) GeralUtil.recuperarDaSessao("perfilLogado");
		Usuario usuario = (Usuario) GeralUtil.recuperarDaSessao("usuarioLogin");
		
		if(perfilLogado != null) {
			if(perfilLogado.equals("cidadao")) {
				gerarDashboardCidadao(usuario.getCidadaos().iterator().next().getIdCidadao());
			} else if(perfilLogado.equals("ouvidorOrgao")) {
				gerarDashboardOuvidor(usuario.getResponsaveis().iterator().next().getEntidade().getIdEntidade());
			} else if (perfilLogado.equals("usciOrgao")){
				gerarDashboardGestorOrgao(usuario.getResponsaveis().iterator().next().getEntidade().getIdEntidade());
			} else if (perfilLogado.equals("coordenadorOGE") || perfilLogado.equals("ouvidorGeral") || perfilLogado.equals("admin")) {
				gerarDashboardGestor();
			}
		} else {
			System.out.println("Perfil logado não encontrado");
		}
	}
	
//	@PreDestroy
//	public void release() {
//		System.out.println(":: release dashboardBean");
//	}
	
	private void gerarDashboardCidadao(Integer idCidadao) {
//		System.out.println(":: gerarDashboardCidadao");
		try {
			infoCidadao = manifestacaoDAO.contarManifestacoesCidadao(idCidadao);
		}catch (Exception e) {
			System.out.println("Falha no dashboard de manifestações");
			e.printStackTrace();
		}
	}
	
	private void gerarDashboardOuvidor(Integer idEntidade) {
//		System.out.println(":: gerarDashboardOuvidor");
		
		infoOuvidor = manifestacaoDAO.contarManifestacoesOuvidor(idEntidade);
	}
	
	private void gerarDashboardGestorOrgao(Integer idEntidade) {
		System.out.println(":: gerarDashboardGestorOrgao");
		
		infoInformacao = manifestacaoDAO.contarManifestacoesPorTipo("Informação", idEntidade);
		infoSugestao = manifestacaoDAO.contarManifestacoesPorTipo("Sugestão", idEntidade);
		infoSolicitacao = manifestacaoDAO.contarManifestacoesPorTipo("Solicitação", idEntidade);
		infoReclamacao = manifestacaoDAO.contarManifestacoesPorTipo("Reclamação", idEntidade);
		infoElogio = manifestacaoDAO.contarManifestacoesPorTipo("Elogio", idEntidade);
		infoDenuncia = manifestacaoDAO.contarManifestacoesPorTipo("Denúncia", idEntidade);
		
		gerarGrafico(infoInformacao.getTotal(), infoSugestao.getTotal(), infoSolicitacao.getTotal(), infoReclamacao.getTotal(), infoElogio.getTotal(), infoDenuncia.getTotal());
	}
	
	private void gerarDashboardGestor() {
		System.out.println(":: gerarDashboardGestor");
		
		infoInformacao = manifestacaoDAO.contarManifestacoesPorTipo("Informação");
		infoSugestao = manifestacaoDAO.contarManifestacoesPorTipo("Sugestão");
		infoSolicitacao = manifestacaoDAO.contarManifestacoesPorTipo("Solicitação");
		infoReclamacao = manifestacaoDAO.contarManifestacoesPorTipo("Reclamação");
		infoElogio = manifestacaoDAO.contarManifestacoesPorTipo("Elogio");
		infoDenuncia = manifestacaoDAO.contarManifestacoesPorTipo("Denúncia");
		
		gerarGrafico(infoInformacao.getTotal(), infoSugestao.getTotal(), infoSolicitacao.getTotal(), infoReclamacao.getTotal(), infoElogio.getTotal(), infoDenuncia.getTotal());
	}
	
	private void gerarGrafico(int totalInfo, int totalSug, int totalSol, int totalRec, int totalElo, int totalDenu) {
		graficoDashboard  = new PieChartModel();
		
		graficoDashboard.set("Pedidos de Informação", totalInfo);
		graficoDashboard.set("Sugestões", totalSug);
		graficoDashboard.set("Solicitações", totalSol);
		graficoDashboard.set("Reclamações", totalRec);
		graficoDashboard.set("Elogios", totalElo);
		graficoDashboard.set("Denúncias", totalDenu);
		
		graficoDashboard.setDiameter(400);
		graficoDashboard.setFill(true);
		graficoDashboard.setShowDatatip(true);
		graficoDashboard.setShadow(false);
		graficoDashboard.setExtender("bigPieExt");
		graficoDashboard.setShowDataLabels(true);
		graficoDashboard.setSeriesColors("1295db, f0ce14, 2b9c1f, e69914, e0299d, dd1717");
		graficoDashboard.setSliceMargin(10);
	}
	
	
	public DashboardInfo getInfoCidadao() {
		return infoCidadao;
	}

	public void setInfoCidadao(DashboardInfo infoCidadao) {
		this.infoCidadao = infoCidadao;
	}

	public DashboardInfo getInfoOuvidor() {
		return infoOuvidor;
	}

	public void setInfoOuvidor(DashboardInfo infoOuvidor) {
		this.infoOuvidor = infoOuvidor;
	}

	public DashboardInfo getInfoInformacao() {
		return infoInformacao;
	}

	public void setInfoInformacao(DashboardInfo infoInformacao) {
		this.infoInformacao = infoInformacao;
	}

	public DashboardInfo getInfoSugestao() {
		return infoSugestao;
	}

	public void setInfoSugestao(DashboardInfo infoSugestao) {
		this.infoSugestao = infoSugestao;
	}

	public DashboardInfo getInfoSolicitacao() {
		return infoSolicitacao;
	}

	public void setInfoSolicitacao(DashboardInfo infoSolicitacao) {
		this.infoSolicitacao = infoSolicitacao;
	}

	public DashboardInfo getInfoReclamacao() {
		return infoReclamacao;
	}

	public void setInfoReclamacao(DashboardInfo infoReclamacao) {
		this.infoReclamacao = infoReclamacao;
	}

	public DashboardInfo getInfoElogio() {
		return infoElogio;
	}

	public void setInfoElogio(DashboardInfo infoElogio) {
		this.infoElogio = infoElogio;
	}

	public DashboardInfo getInfoDenuncia() {
		return infoDenuncia;
	}

	public void setInfoDenuncia(DashboardInfo infoDenuncia) {
		this.infoDenuncia = infoDenuncia;
	}

	public PieChartModel getGraficoDashboard() {
		return graficoDashboard;
	}

	public void setGraficoDashboard(PieChartModel graficoDashboard) {
		this.graficoDashboard = graficoDashboard;
	}


}
