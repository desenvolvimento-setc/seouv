package br.gov.se.setc.bean;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Faces;
import org.omnifaces.util.Messages;

import br.gov.se.setc.DAO.UsuarioDAO;
import br.gov.se.setc.model.Cidadao;
import br.gov.se.setc.model.Usuario;
import br.gov.se.setc.util.CriptografiaUtil;
import br.gov.se.setc.util.GeralUtil;

@Named
@ViewScoped
public class ConsultaUsuarioBean implements Serializable{

	private static final long serialVersionUID = 7531784703525992582L;

	private UsuarioDAO usuarioDAO = UsuarioDAO.getInstance();
	
	private Usuario usuario;
	
	private String emailAtual;
	private String CPFAtual;
	private String RGAtual;
	
	@PostConstruct
	public void init() {
//		System.out.println(":: init consultaUsuarioBean ::");
		
		try {
			usuario = (Usuario) GeralUtil.recuperarDaSessao("usuarioLogin");
			System.out.println(usuario);
			
			Cidadao cid = usuario.getCidadaos().iterator().next();
			
			emailAtual = cid.getEmail();
			CPFAtual = cid.getCpf();
			RGAtual = cid.getRg();
		} catch (Exception e) {
			e.printStackTrace();
			Messages.addGlobalError("Falha ao recuperar cidadão");
		}
	}
	
	public String salvar() {
		try {
			if(existeEmail() || existeCPF() || existeRG()) {
				return null;
			} else {
				if(usuario.getCidadaos().iterator().next().getNumero().isEmpty()) {
					usuario.getCidadaos().iterator().next().setNumero(null);
				}
				
				boolean salvou = usuarioDAO.saveOrUpdate(usuario);
				
				if(salvou) {
					System.out.println("Cidadão atualizado com sucesso");
					
					return "/index";
				} else {
					System.out.println("Falha ao atualizar cidadão");
					Messages.addGlobalError("Falha ao atualizar dados do cidadão");
					return null;
				}
			}
			
		} catch (Exception e) {
			System.out.println("Falha ao atualizar cidadão: " + e.getMessage());
			Messages.addGlobalError("Falha ao cadastrar cidadão");
			return null;
		}
	}
	
	private boolean existeEmail() {
		try {
			boolean existe = usuarioDAO.existeEmail(usuario.getCidadaos().iterator().next().getEmail(), emailAtual);
			
			if(existe) {
				System.out.println("Email já cadastrado no sistema");
				Messages.addGlobalWarn("Email já cadastrado no sistema");
				return existe;
			} else {
				System.out.println("Email liberado");
				return existe;
			}
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	private boolean existeCPF() {
		try {
			if(usuario.getCidadaos().iterator().next().getCpf() == null || usuario.getCidadaos().iterator().next().getCpf().isEmpty()) {
				return false;
			} else {
				boolean existe = usuarioDAO.existeCPF(usuario.getCidadaos().iterator().next().getCpf(), CPFAtual);
				
				if(existe) {
					System.out.println("CPF já cadastrado no sistema");
					Messages.addGlobalWarn("CPF já cadastrado no sistema");
					return existe;
				} else {
					System.out.println("CPF liberado");
					return existe;
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	private boolean existeRG() {
		try {
			if(usuario.getCidadaos().iterator().next().getRg() == null || usuario.getCidadaos().iterator().next().getRg().isEmpty()) {
				return false;
			} else {
				boolean existe = usuarioDAO.existeRG(usuario.getCidadaos().iterator().next().getRg(), RGAtual);
				
				if(existe) {
					System.out.println("RG já cadastrado no sistema");
					Messages.addGlobalWarn("RG já cadastrado no sistema");
					return existe;
				} else {
					System.out.println("RG liberado");
					return existe;
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	

}
