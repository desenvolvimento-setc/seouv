package br.gov.se.setc.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Messages;

import br.gov.se.setc.DAO.EntidadeDAO;
import br.gov.se.setc.DAO.ResponsavelDAO;
import br.gov.se.setc.model.Entidade;
import br.gov.se.setc.model.Responsavel;

@Named
@ViewScoped
public class RedeOuvidoriaBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private ResponsavelDAO responsavelDAO = ResponsavelDAO.getInstance();
	
	private List<Responsavel> listOuvidores;
	
	@PostConstruct
	public void init() {
//		System.out.println(":: init redeOuvidoriaBean ::");
		
		try {
			listOuvidores = responsavelDAO.listOuvidoresAtivos();
		} catch (Exception e) {
			System.out.println("Falha ao recuperar lista de ouvidores");
			Messages.addGlobalWarn("Falha ao recuperar lista de ouvidores");
		}
	}

	public List<Responsavel> getListOuvidores() {
		return listOuvidores;
	}

	public void setListOuvidores(List<Responsavel> listOuvidores) {
		this.listOuvidores = listOuvidores;
	}

}
