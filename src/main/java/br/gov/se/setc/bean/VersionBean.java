package br.gov.se.setc.bean;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServlet;

import org.apache.commons.lang3.StringUtils;


@Named
@ApplicationScoped
public class VersionBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String appVersion;
	
	@PostConstruct
	public void init() {
		appVersion = recuperarVersao();
	}
	
	private String recuperarVersao() {
		String version = "0.0.0";
		try {
			String path = this.getClass().getResource("/").getPath().toString().replaceAll("%23", "#");
			
			version = StringUtils.substringBefore(
					StringUtils.substringAfter(
							path,
							"##"),
					"/");
			
			System.out.println(":: Version: " + version);
		} catch (Exception e) {
			System.out.println(":: Falha ao recuperar versão da aplicação");
			e.printStackTrace();
		}
		
		return version;
	}

	public String getAppVersion() {
		return appVersion;
	}

}
