package br.gov.se.setc.bean;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;

import br.gov.se.setc.DAO.EntidadeDAO;
import br.gov.se.setc.DAO.RelatorioDAO;
import br.gov.se.setc.model.Entidade;


@ManagedBean(name = "relatorioLAI")
@SessionScoped
public class RelatorioLAI {

	public int idEntidade = 0;
	
	String[] meses = { "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" };

	private List<String[]> manifPorMes;
	private String[] columnManifPorMes;
	private String[] dataManifPorMes;
	
	private List<String[]> manifPorTema;
	private String[] columnManifPorTema;
	private String[] dataManifPorTema;
	
	private List<String[]> manifPorSituacao;
	private String[] columnManifPorSituacao;
	private String[] dataManifPorSituacao;
	
	private List<String[]> manifPorOrgao;
	private String[] columnManifPorOrgao;
	private String[] dataManifPorOrgao;
	
	private List<String[]> cidPorFaixaEtaria;
	private String[] columnCidPorFaixaEtaria;
	private String[] dataCidPorFaixaEtaria;
	
	private List<String[]> cidPorRenda;
	private String[] columnCidPorRenda;
	private String[] dataCidPorRenda;
	
	private List<String[]> cidPorSexo;
	private String[] columnCidPorSexo;
	private String[] dataCidPorSexo;
	
	private List<String[]> cidPorEscolaridade;
	private String[] columnCidPorEscolaridade;
	private String[] dataCidPorEscolaridade;
	
	private List<String[]> cidPorEstado;
	private String[] columnCidPorEstado;
	private String[] dataCidPorEstado;
	
	private Integer anoAtual;
	private Integer mesAtual;
	
	private Date dataInicialConsulta;
	private Date dataFinalConsulta;
	
	private String exibirOrgao;
	private String exibirPeriodo;
	
	//Download
	private String base64Str;
	private StreamedContent file;
	
	private RelatorioDAO relatorioDAO = RelatorioDAO.getInstance();
	private EntidadeDAO entidadeDAO = EntidadeDAO.getInstance();
	
	private List<Entidade> listEntidades;

	private String siglaOrgao;
	private String dataInicio;
	private String dataFim;
	
	@PostConstruct
	public void init() {
		
		Calendar c = Calendar.getInstance();
		
		anoAtual = c.get(Calendar.YEAR);
		String dataInicial = anoAtual+"/01/01";
		
		dataInicialConsulta = new Date(anoAtual+1900, 0, 1);
		
		mesAtual = c.get(Calendar.MONTH)+1;
		String dataFinal = c.get(Calendar.YEAR)+"/"+mesAtual+"/"+c.get(Calendar.DATE);
		
		dataFinalConsulta = new Date(anoAtual+1900, mesAtual-1, c.get(Calendar.DATE));
		
		exibirPeriodo = meses[dataInicialConsulta.getMonth()] + " a " +meses[dataFinalConsulta.getMonth()] + " de " + Integer.toString(anoAtual);
		
		manifPorMes = relatorioDAO.getManifLAIPorMes(dataInicial, dataFinal);
		columnManifPorMes = manifPorMes.get(0);
		dataManifPorMes = manifPorMes.get(1);
		
		manifPorTema = relatorioDAO.getManifLAIPorTema(dataInicial, dataFinal);
		columnManifPorTema = manifPorTema.get(0);
		dataManifPorTema = manifPorTema.get(1);
		
		manifPorSituacao = relatorioDAO.getManifLAIPorSituacao(dataInicial, dataFinal);
		columnManifPorSituacao = manifPorSituacao.get(0);
		dataManifPorSituacao = manifPorSituacao.get(1);
		
		manifPorOrgao = relatorioDAO.getManifLAIPorOrgao(dataInicial, dataFinal);
		columnManifPorOrgao = manifPorOrgao.get(0);
		dataManifPorOrgao = manifPorOrgao.get(1);
		
		cidPorFaixaEtaria = relatorioDAO.getCidLAIPorFaixaEtaria(dataInicial, dataFinal);
		columnCidPorFaixaEtaria = cidPorFaixaEtaria.get(0);
		dataCidPorFaixaEtaria = cidPorFaixaEtaria.get(1);
		
		cidPorRenda = relatorioDAO.getCidLAIPorRenda(dataInicial, dataFinal);
		columnCidPorRenda = cidPorRenda.get(0);
		dataCidPorRenda = cidPorRenda.get(1);
		
		cidPorSexo = relatorioDAO.getCidLAIPorSexo(dataInicial, dataFinal);
		columnCidPorSexo = cidPorSexo.get(0);
		dataCidPorSexo = cidPorSexo.get(1);
		
		cidPorEscolaridade = relatorioDAO.getCidLAIPorEscolaridade(dataInicial, dataFinal);
		columnCidPorEscolaridade = cidPorEscolaridade.get(0);
		dataCidPorEscolaridade = cidPorEscolaridade.get(1);
		
		cidPorEstado = relatorioDAO.getCidLAIPorEstado(dataInicial, dataFinal);
		columnCidPorEstado = cidPorEstado.get(0);
		dataCidPorEstado = cidPorEstado.get(1);
	}
	
	//Download
	public void submittedBase64Str() throws IOException{
	    if(base64Str.split(",").length > 1){
	        String encoded = base64Str.split(",")[1];
	        byte[] decoded = org.apache.commons.codec.binary.Base64.decodeBase64(encoded);
	            InputStream in = new ByteArrayInputStream(decoded);
	            file = new DefaultStreamedContent(in, "image/png", "chart.png");
	    }
	}
	
	public void loadParametros() {
		try {
			if(siglaOrgao != null) {
				System.out.println("== Órgão: " + siglaOrgao);
				if(siglaOrgao.equals("todos") || siglaOrgao == null)
					idEntidade = 0;
				else {
					idEntidade = entidadeDAO.getIdPorSigla(siglaOrgao);
					
				SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
				if(dataInicio != null && dataFim != null ) {
				
					dataInicialConsulta = formatter.parse(dataInicio);
					dataFinalConsulta = formatter.parse(dataFim);
				} else {
					int ano = Calendar.getInstance().get(Calendar.YEAR);
					
					dataInicialConsulta = formatter.parse("01-01-" + ano);
					dataFinalConsulta = formatter.parse("31-12-" + ano);
				}
					
					
					System.out.println("== Início: " + dataInicialConsulta.toString());
					System.out.println("== Fim: " + dataFinalConsulta.toString());
				}
				
				loadGraficos();
			} 
			else
				System.out.println("Parametro nulo");
		} catch (Exception e) {
			System.out.println("Falha ao recuperar parametros");
		}
	}
	
	
	@SuppressWarnings("deprecation")
	public void loadGraficos() {
		
		if(dataFinalConsulta.before(dataInicialConsulta)) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null,  new FacesMessage("Erro", "Período de pesquisa inválido"));
			
		}else {
			System.out.println("Periodo valido");
			if(idEntidade == 0) {
				exibirOrgao = null;
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				System.out.println(sdf.format(dataInicialConsulta) + " - " + sdf.format(dataFinalConsulta));
				String dataInicial = sdf.format(dataInicialConsulta);
				String dataFinal = sdf.format(dataFinalConsulta);
				
				manifPorMes = relatorioDAO.getManifLAIPorMes(dataInicial, dataFinal);
				columnManifPorMes = manifPorMes.get(0);
				dataManifPorMes = manifPorMes.get(1);
				
				manifPorTema = relatorioDAO.getManifLAIPorTema(dataInicial, dataFinal);
				columnManifPorTema = manifPorTema.get(0);
				dataManifPorTema = manifPorTema.get(1);
				
				manifPorSituacao = relatorioDAO.getManifLAIPorSituacao(dataInicial, dataFinal);
				columnManifPorSituacao = manifPorSituacao.get(0);
				dataManifPorSituacao = manifPorSituacao.get(1);
				
				manifPorOrgao = relatorioDAO.getManifLAIPorOrgao(dataInicial, dataFinal);
				columnManifPorOrgao = manifPorOrgao.get(0);
				dataManifPorOrgao = manifPorOrgao.get(1);
				
				cidPorFaixaEtaria = relatorioDAO.getCidLAIPorFaixaEtaria(dataInicial, dataFinal);
				columnCidPorFaixaEtaria = cidPorFaixaEtaria.get(0);
				dataCidPorFaixaEtaria = cidPorFaixaEtaria.get(1);
				
				cidPorRenda = relatorioDAO.getCidLAIPorRenda(dataInicial, dataFinal);
				columnCidPorRenda = cidPorRenda.get(0);
				dataCidPorRenda = cidPorRenda.get(1);
				
				cidPorSexo = relatorioDAO.getCidLAIPorSexo(dataInicial, dataFinal);
				columnCidPorSexo = cidPorSexo.get(0);
				dataCidPorSexo = cidPorSexo.get(1);
				
				cidPorEscolaridade = relatorioDAO.getCidLAIPorEscolaridade(dataInicial, dataFinal);
				columnCidPorEscolaridade = cidPorEscolaridade.get(0);
				dataCidPorEscolaridade = cidPorEscolaridade.get(1);
				
				cidPorEstado = relatorioDAO.getCidLAIPorEstado(dataInicial, dataFinal);
				columnCidPorEstado = cidPorEstado.get(0);
				dataCidPorEstado = cidPorEstado.get(1);
				
				if(dataInicialConsulta.getYear() != dataFinalConsulta.getYear()) {
					manifPorMes = relatorioDAO.getManifLAIPorAno(dataInicial, dataFinal);
					columnManifPorMes = manifPorMes.get(0);
					dataManifPorMes = manifPorMes.get(1);
					
					exibirPeriodo = meses[dataInicialConsulta.getMonth()] + " de " + Integer.toString(dataInicialConsulta.getYear()+1900) +
									" a " + meses[dataFinalConsulta.getMonth()] + " de " + Integer.toString(dataFinalConsulta.getYear()+1900);
					System.out.println(exibirPeriodo);
					System.out.println(Integer.toString(dataFinalConsulta.getYear()+1900));
				}
				else {
					exibirPeriodo = meses[dataInicialConsulta.getMonth()] + " a " +meses[dataFinalConsulta.getMonth()] + " de " + Integer.toString(dataInicialConsulta.getYear()+1900);
				}
			}else {
				exibirOrgao = entidadeDAO.getSigla(idEntidade);
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				System.out.println(sdf.format(dataInicialConsulta) + " - " + sdf.format(dataFinalConsulta));
				String dataInicial = sdf.format(dataInicialConsulta);
				String dataFinal = sdf.format(dataFinalConsulta);
				
				manifPorMes = relatorioDAO.getManifLAIPorMesEntidade(dataInicial, dataFinal, idEntidade);
				columnManifPorMes = manifPorMes.get(0);
				dataManifPorMes = manifPorMes.get(1);
				
				manifPorTema = relatorioDAO.getManifLAIPorTemaEntidade(dataInicial, dataFinal, idEntidade);
				columnManifPorTema = manifPorTema.get(0);
				dataManifPorTema = manifPorTema.get(1);
				
				manifPorSituacao = relatorioDAO.getManifLAIPorSituacaoEntidade(dataInicial, dataFinal, idEntidade);
				columnManifPorSituacao = manifPorSituacao.get(0);
				dataManifPorSituacao = manifPorSituacao.get(1);
				
				manifPorOrgao = relatorioDAO.getManifLAIPorOrgaoEntidade(dataInicial, dataFinal, idEntidade);
				columnManifPorOrgao = manifPorOrgao.get(0);
				dataManifPorOrgao = manifPorOrgao.get(1);
				
				cidPorFaixaEtaria = relatorioDAO.getCidLAIPorFaixaEtariaEntidade(dataInicial, dataFinal, idEntidade);
				columnCidPorFaixaEtaria = cidPorFaixaEtaria.get(0);
				dataCidPorFaixaEtaria = cidPorFaixaEtaria.get(1);
				
				cidPorRenda = relatorioDAO.getCidLAIPorRendaEntidade(dataInicial, dataFinal, idEntidade);
				columnCidPorRenda = cidPorRenda.get(0);
				dataCidPorRenda = cidPorRenda.get(1);
				
				cidPorSexo = relatorioDAO.getCidLAIPorSexoEntidade(dataInicial, dataFinal, idEntidade);
				columnCidPorSexo = cidPorSexo.get(0);
				dataCidPorSexo = cidPorSexo.get(1);
				
				cidPorEscolaridade = relatorioDAO.getCidLAIPorEscolaridadeEntidade(dataInicial, dataFinal, idEntidade);
				columnCidPorEscolaridade = cidPorEscolaridade.get(0);
				dataCidPorEscolaridade = cidPorEscolaridade.get(1);
				
				cidPorEstado = relatorioDAO.getCidLAIPorEstadoEntidade(dataInicial, dataFinal, idEntidade);
				columnCidPorEstado = cidPorEstado.get(0);
				dataCidPorEstado = cidPorEstado.get(1);
				
				//exibirPeriodo = meses[dataInicialConsulta.getMonth()] + " a " +meses[dataFinalConsulta.getMonth()];
				if(dataInicialConsulta.getYear() != dataFinalConsulta.getYear()) {
					manifPorMes = relatorioDAO.getManifLAIPorAnoEntidade(dataInicial, dataFinal, idEntidade);
					columnManifPorMes = manifPorMes.get(0);
					dataManifPorMes = manifPorMes.get(1);
					
					exibirPeriodo = meses[dataInicialConsulta.getMonth()] + " de " + Integer.toString(dataInicialConsulta.getYear()+1900) +
									" a " + meses[dataFinalConsulta.getMonth()] + " de " + Integer.toString(dataFinalConsulta.getYear()+1900);
				}
				else {
					exibirPeriodo = meses[dataInicialConsulta.getMonth()] + " a " +meses[dataFinalConsulta.getMonth()] + " de " + Integer.toString(dataInicialConsulta.getYear()+1900);
				}
			}
		}
	}
	
	public StreamedContent exportarDados() {
		StreamedContent zipFile;
	  
	    try {
	    	if(manifPorMes != null) {
	    		List<byte[]> filedataList = new ArrayList<byte[]>();
	    		
	    		String texto = "";
					if(exibirPeriodo != null)
						if(exibirOrgao.equals(""))
							texto = "-Geral-" + exibirPeriodo;
						else
							texto = "-" + exibirOrgao + "-" + exibirPeriodo;
	    		
	    		filedataList.add(gerarCVS("Data", "Manifestações", columnManifPorMes, dataManifPorMes));
	    		filedataList.add(gerarCVS("Tema", "Manifestações", columnManifPorTema, dataManifPorTema));
	    		filedataList.add(gerarCVS("Situação", "Manifestações", columnManifPorSituacao, dataManifPorSituacao));
	    		filedataList.add(gerarCVS("Órgão", "Manifestações", columnManifPorOrgao, dataManifPorOrgao));
	    		filedataList.add(gerarCVS("Faixa Etária", "Manifestações", columnCidPorFaixaEtaria, dataCidPorFaixaEtaria));
	    		filedataList.add(gerarCVS("Renda", "Manifestações", columnCidPorRenda, dataCidPorRenda));
	    		filedataList.add(gerarCVS("Gênero", "Manifestações", columnCidPorSexo, dataCidPorSexo));
	    		filedataList.add(gerarCVS("Escolaridade", "Manifestações", columnCidPorEscolaridade, dataCidPorEscolaridade));
	    		filedataList.add(gerarCVS("Estado", "Manifestações", columnCidPorEstado, dataCidPorEstado));
				
	    		ByteArrayOutputStream baos = new ByteArrayOutputStream();
	            ZipOutputStream zipOut = new ZipOutputStream(baos);
	            
	            for (int i = 0; i < filedataList.size(); i++) {
	            	String name = "Dados" + texto + ".csv";
	            	switch (i) {
					case 0:
						name = "Manifestacoes-por-Data" + texto + ".csv";
						break;
					case 1:
						name = "Manifestacoes-por-Tema" + texto + ".csv";
						break;
					case 2:
						name = "Manifestacoes-por-Situacao" + texto + ".csv";
						break;
					case 3:
						name = "Manifestacoes-por-Orgao" + texto + ".csv";
						break;
					case 4:
						name = "Manifestacoes-por-Faixa-Etaria" + texto + ".csv";
						break;
					case 5:
						name = "Manifestacoes-por-Renda" + texto + ".csv";
						break;
					case 6:
						name = "Manifestacoes-por-Genero" + texto + ".csv";
						break;
					case 7:
						name = "Manifestacoes-por-Escolaridade" + texto + ".csv";
						break;
					case 8:
						name = "Manifestacoes-por-Estado" + texto + ".csv";
						break;
					default:
						break;
					}

	                zipOut.putNextEntry(new ZipEntry(name));
	                zipOut.write(filedataList.get(i));
	                zipOut.closeEntry();
	                
	            }

	            zipOut.finish();
	            zipOut.close();
	            
	            zipFile = new DefaultStreamedContent(new ByteArrayInputStream(baos.toByteArray()), "text/csv", "Dados_LAI" + texto + ".zip");
	            		
				return zipFile;
	    	}
	    }
	    catch (Exception e) {
	    	System.out.println("Falha ao exportar dados");
	        e.printStackTrace();
	    }
	    
	    return null;
	}
	
	private byte[] gerarCVS(String coluna1, String coluna2, String[] dados1, String[] dados2) throws IOException {
		byte[] fileData;
		
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
	    OutputStreamWriter streamWriter = new OutputStreamWriter(stream);
	    CSVWriter writer = new CSVWriter(streamWriter);
	    
	    writer.writeNext(new String[]{coluna1, coluna2});
	    for(int i=0; i < dados1.length; i++) {
	    	writer.writeNext(new String[]{dados1[i], dados2[i]});
	    }
	    
	    writer.flush();
	    
		fileData = stream.toByteArray();
		
		writer.close();
		streamWriter.close();
		stream.close();
		
		return fileData;
	}
	
	public int getAnoAtual() {
		return anoAtual;
	}

	public String getMesAtual() {
		return meses[mesAtual-1];
	}

	public int getIdEntidade() {
		return idEntidade;
	}
	
	public void setIdEntidade(int idEntidade) {
		this.idEntidade = idEntidade;
	}

	public String[] getColumnManifPorMes() {
		return columnManifPorMes;
	}

	public void setColumnManifPorMes(String[] columnManifPorMes) {
		this.columnManifPorMes = columnManifPorMes;
	}

	public String[] getDataManifPorMes() {
		return dataManifPorMes;
	}

	public void setDataManifPorMes(String[] dataManifPorMes) {
		this.dataManifPorMes = dataManifPorMes;
	}

	public List<String[]> getManifPorTema() {
		return manifPorTema;
	}

	public void setManifPorTema(List<String[]> manifPorTema) {
		this.manifPorTema = manifPorTema;
	}

	public String[] getColumnManifPorTema() {
		return columnManifPorTema;
	}

	public void setColumnManifPorTema(String[] columnManifPorTema) {
		this.columnManifPorTema = columnManifPorTema;
	}

	public String[] getDataManifPorTema() {
		return dataManifPorTema;
	}

	public void setDataManifPorTema(String[] dataManifPorTema) {
		this.dataManifPorTema = dataManifPorTema;
	}

	public String[] getColumnManifPorSituacao() {
		return columnManifPorSituacao;
	}

	public void setColumnManifPorSituacao(String[] columnManifPorSituacao) {
		this.columnManifPorSituacao = columnManifPorSituacao;
	}

	public String[] getDataManifPorSituacao() {
		return dataManifPorSituacao;
	}

	public void setDataManifPorSituacao(String[] dataManifPorSituacao) {
		this.dataManifPorSituacao = dataManifPorSituacao;
	}

	public String[] getColumnManifPorOrgao() {
		return columnManifPorOrgao;
	}

	public void setColumnManifPorOrgao(String[] columnManifPorOrgao) {
		this.columnManifPorOrgao = columnManifPorOrgao;
	}

	public String[] getDataManifPorOrgao() {
		return dataManifPorOrgao;
	}

	public void setDataManifPorOrgao(String[] dataManifPorOrgao) {
		this.dataManifPorOrgao = dataManifPorOrgao;
	}

	public Date getDataInicialConsulta() {
		return dataInicialConsulta;
	}

	public void setDataInicialConsulta(Date dataInicialConsulta) {
		this.dataInicialConsulta = dataInicialConsulta;
	}

	public Date getDataFinalConsulta() {
		return dataFinalConsulta;
	}

	public void setDataFinalConsulta(Date dataFinalConsulta) {
		this.dataFinalConsulta = dataFinalConsulta;
	}

	public String getExibirOrgao() {
		return exibirOrgao;
	}

	public void setExibirOrgao(String exibirOrgao) {
		this.exibirOrgao = exibirOrgao;
	}

	public String getExibirPeriodo() {
		return exibirPeriodo;
	}

	public void setExibirPeriodo(String exibirPeriodo) {
		this.exibirPeriodo = exibirPeriodo;
	}

	public String getBase64Str() {
		return base64Str;
	}

	public void setBase64Str(String base64Str) {
		this.base64Str = base64Str;
	}

	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}

	public String[] getColumnCidPorFaixaEtaria() {
		return columnCidPorFaixaEtaria;
	}

	public void setColumnCidPorFaixaEtaria(String[] columnCidPorFaixaEtaria) {
		this.columnCidPorFaixaEtaria = columnCidPorFaixaEtaria;
	}

	public String[] getDataCidPorFaixaEtaria() {
		return dataCidPorFaixaEtaria;
	}

	public void setDataCidPorFaixaEtaria(String[] dataCidPorFaixaEtaria) {
		this.dataCidPorFaixaEtaria = dataCidPorFaixaEtaria;
	}

	public String[] getColumnCidPorRenda() {
		return columnCidPorRenda;
	}

	public void setColumnCidPorRenda(String[] columnCidPorRenda) {
		this.columnCidPorRenda = columnCidPorRenda;
	}

	public String[] getDataCidPorRenda() {
		return dataCidPorRenda;
	}

	public void setDataCidPorRenda(String[] dataCidPorRenda) {
		this.dataCidPorRenda = dataCidPorRenda;
	}

	public String[] getColumnCidPorSexo() {
		return columnCidPorSexo;
	}

	public void setColumnCidPorSexo(String[] columnCidPorSexo) {
		this.columnCidPorSexo = columnCidPorSexo;
	}

	public String[] getDataCidPorSexo() {
		return dataCidPorSexo;
	}

	public void setDataCidPorSexo(String[] dataCidPorSexo) {
		this.dataCidPorSexo = dataCidPorSexo;
	}

	public String[] getColumnCidPorEscolaridade() {
		return columnCidPorEscolaridade;
	}

	public void setColumnCidPorEscolaridade(String[] columnCidPorEscolaridade) {
		this.columnCidPorEscolaridade = columnCidPorEscolaridade;
	}

	public String[] getDataCidPorEscolaridade() {
		return dataCidPorEscolaridade;
	}

	public void setDataCidPorEscolaridade(String[] dataCidPorEscolaridade) {
		this.dataCidPorEscolaridade = dataCidPorEscolaridade;
	}

	public String[] getColumnCidPorEstado() {
		return columnCidPorEstado;
	}

	public void setColumnCidPorEstado(String[] columnCidPorEstado) {
		this.columnCidPorEstado = columnCidPorEstado;
	}

	public String[] getDataCidPorEstado() {
		return dataCidPorEstado;
	}

	public void setDataCidPorEstado(String[] dataCidPorEstado) {
		this.dataCidPorEstado = dataCidPorEstado;
	}

	public List<Entidade> getListEntidades() {
		return listEntidades;
	}

	public void setListEntidades(List<Entidade> listEntidades) {
		this.listEntidades = listEntidades;
	}

	public String getSiglaOrgao() {
		return siglaOrgao;
	}

	public void setSiglaOrgao(String siglaOrgao) {
		this.siglaOrgao = siglaOrgao;
	}

	public String getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(String dataInicio) {
		this.dataInicio = dataInicio;
	}

	public String getDataFim() {
		return dataFim;
	}

	public void setDataFim(String dataFim) {
		this.dataFim = dataFim;
	}

}