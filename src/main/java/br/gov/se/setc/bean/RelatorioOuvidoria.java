package br.gov.se.setc.bean;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import br.gov.se.setc.DAO.EntidadeDAO;
import br.gov.se.setc.DAO.RelatorioDAO;
import br.gov.se.setc.model.Entidade;

@ManagedBean(name = "relatorioOuvidoria")
@SessionScoped
public class RelatorioOuvidoria {

	public int idEntidade = 0;
	
	String[] meses = { "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" };
	
	private List<String[]> manifPorTipo;
	private String[] columnManifPorTipo;
	private String[] dataManifPorTipo;
	
	private List<String[]> manifPorMes;
	private String[] columnManifPorMes;
	private String[] dataManifPorMes;
	
	private List<String[]> manifPorTema;
	private String[] columnManifPorTema;
	private String[] dataManifPorTema;
	
	private List<String[]> manifPorSituacao;
	private String[] columnManifPorSituacao;
	private String[] dataManifPorSituacao;
	
	private List<String[]> manifPorOrgao;
	private String[] columnManifPorOrgao;
	private String[] dataManifPorOrgao;
	
	private List<String[]> manifPorCanal;
	private String[] columnManifPorCanal;
	private String[] dataManifPorCanal;
	
	private List<String[]> cidPorFaixaEtaria;
	private String[] columnCidPorFaixaEtaria;
	private String[] dataCidPorFaixaEtaria;
	
	private List<String[]> cidPorRenda;
	private String[] columnCidPorRenda;
	private String[] dataCidPorRenda;
	
	private List<String[]> cidPorSexo;
	private String[] columnCidPorSexo;
	private String[] dataCidPorSexo;
	
	private List<String[]> cidPorEscolaridade;
	private String[] columnCidPorEscolaridade;
	private String[] dataCidPorEscolaridade;
	
	private List<String[]> cidPorEstado;
	private String[] columnCidPorEstado;
	private String[] dataCidPorEstado;
	
	private Integer anoAtual;
	private Integer mesAtual;
	
	private Date dataInicialConsulta;
	private Date dataFinalConsulta;
	
	private String exibirOrgao;
	private String exibirPeriodo;
	private String exibirAno;
	
	public Entidade entidade;
	
	//Download
	private String base64Str;
	private StreamedContent file;
	
	private RelatorioDAO relatorioDAO = RelatorioDAO.getInstance();
	private EntidadeDAO entidadeDAO = EntidadeDAO.getInstance();
	
	private List<Entidade> listEntidades;
	
	@PostConstruct
	public void init() {
//		System.out.println(":: init relatorioOuvidoria");
		try {
			listEntidades = entidadeDAO.listEntidades();
			
			Calendar c = Calendar.getInstance();
			
			anoAtual = c.get(Calendar.YEAR);
			String dataInicial = anoAtual+"/01/01";
			
			dataInicialConsulta = new Date(anoAtual+1900, 0, 1);
			
			mesAtual = c.get(Calendar.MONTH)+1;
			String dataFinal = c.get(Calendar.YEAR)+"/"+mesAtual+"/"+c.get(Calendar.DATE);
			
			dataFinalConsulta = new Date(anoAtual+1900, mesAtual-1, c.get(Calendar.DATE));
			
			exibirAno = Integer.toString(anoAtual);
			
			exibirPeriodo = meses[dataInicialConsulta.getMonth()] + " a " +meses[dataFinalConsulta.getMonth()] + " de " + Integer.toString(anoAtual);
			
			manifPorTipo = relatorioDAO.getManifPorTipo(dataInicial, dataFinal);
			manifPorMes = relatorioDAO.getManifPorMes(dataInicial, dataFinal);
			manifPorTema = relatorioDAO.getManifPorTema(dataInicial, dataFinal);
			manifPorSituacao = relatorioDAO.getManifPorSituacao(dataInicial, dataFinal);
			manifPorOrgao = relatorioDAO.getManifPorOrgao(dataInicial, dataFinal);
			manifPorCanal = relatorioDAO.getManifPorCanal(dataInicial, dataFinal);
			
			columnManifPorTipo = manifPorTipo.get(0);
			dataManifPorTipo = manifPorTipo.get(1);
			
			columnManifPorMes = manifPorMes.get(0);
			dataManifPorMes = manifPorMes.get(1);
			
			columnManifPorTema = manifPorTema.get(0);
			dataManifPorTema = manifPorTema.get(1);
			
			columnManifPorSituacao = manifPorSituacao.get(0);
			dataManifPorSituacao = manifPorSituacao.get(1);
			
			columnManifPorOrgao = manifPorOrgao.get(0);
			dataManifPorOrgao = manifPorOrgao.get(1);
			
			columnManifPorCanal = manifPorCanal.get(0);
			dataManifPorCanal = manifPorCanal.get(1);
			
			cidPorFaixaEtaria = relatorioDAO.getCidPorFaixaEtaria(dataInicial, dataFinal);
			cidPorRenda = relatorioDAO.getCidPorRenda(dataInicial, dataFinal);
			cidPorSexo = relatorioDAO.getCidPorSexo(dataInicial, dataFinal);
			cidPorEscolaridade = relatorioDAO.getCidPorEscolaridade(dataInicial, dataFinal);
			cidPorEstado = relatorioDAO.getCidPorEstado(dataInicial, dataFinal);
			
			columnCidPorFaixaEtaria = cidPorFaixaEtaria.get(0);
			dataCidPorFaixaEtaria = cidPorFaixaEtaria.get(1);
			
			columnCidPorRenda = cidPorRenda.get(0);
			dataCidPorRenda = cidPorRenda.get(1);
			
			columnCidPorSexo = cidPorSexo.get(0);
			dataCidPorSexo = cidPorSexo.get(1);
			
			columnCidPorEscolaridade = cidPorEscolaridade.get(0);
			dataCidPorEscolaridade = cidPorEscolaridade.get(1);
			
			columnCidPorEstado = cidPorEstado.get(0);
			dataCidPorEstado = cidPorEstado.get(1);
		} catch (Exception e) {
			System.out.println("Falha ao inicializar relatório");
		}
	}
	
	//Download
	public void submittedBase64Str() throws IOException{
	    if(base64Str.split(",").length > 1){
	        String encoded = base64Str.split(",")[1];
	        byte[] decoded = org.apache.commons.codec.binary.Base64.decodeBase64(encoded);
	            InputStream in = new ByteArrayInputStream(decoded);
	            file = new DefaultStreamedContent(in, "image/png", "chart.png");
	    }
	}

	
	public void loadGraficos() {
		
		if(dataFinalConsulta.before(dataInicialConsulta)) {
			FacesContext context = FacesContext.getCurrentInstance();
			context.addMessage(null,  new FacesMessage("Erro", "Período de pesquisa inválido"));
			
		} else {
			System.out.println("Periodo valido");
			if(idEntidade == 0) {
				exibirOrgao = null;
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				System.out.println(sdf.format(dataInicialConsulta) + " - " + sdf.format(dataFinalConsulta));
				String dataInicial = sdf.format(dataInicialConsulta);
				String dataFinal = sdf.format(dataFinalConsulta);
				
				manifPorTipo = relatorioDAO.getManifPorTipo(dataInicial, dataFinal);
				columnManifPorTipo = manifPorTipo.get(0);
				dataManifPorTipo = manifPorTipo.get(1);
				
				manifPorMes = relatorioDAO.getManifPorMes(dataInicial, dataFinal);
				columnManifPorMes = manifPorMes.get(0);
				dataManifPorMes = manifPorMes.get(1);
				
				manifPorTema = relatorioDAO.getManifPorTema(dataInicial, dataFinal);
				columnManifPorTema = manifPorTema.get(0);
				dataManifPorTema = manifPorTema.get(1);
				
				manifPorSituacao = relatorioDAO.getManifPorSituacao(dataInicial, dataFinal);
				columnManifPorSituacao = manifPorSituacao.get(0);
				dataManifPorSituacao = manifPorSituacao.get(1);
				
				manifPorOrgao = relatorioDAO.getManifPorOrgao(dataInicial, dataFinal);
				columnManifPorOrgao = manifPorOrgao.get(0);
				dataManifPorOrgao = manifPorOrgao.get(1);
				
				manifPorCanal = relatorioDAO.getManifPorCanal(dataInicial, dataFinal);
				columnManifPorCanal = manifPorCanal.get(0);
				dataManifPorCanal = manifPorCanal.get(1);
				
				cidPorFaixaEtaria = relatorioDAO.getCidPorFaixaEtaria(dataInicial, dataFinal);
				columnCidPorFaixaEtaria = cidPorFaixaEtaria.get(0);
				dataCidPorFaixaEtaria = cidPorFaixaEtaria.get(1);
				
				cidPorRenda = relatorioDAO.getCidPorRenda(dataInicial, dataFinal);
				columnCidPorRenda = cidPorRenda.get(0);
				dataCidPorRenda = cidPorRenda.get(1);
				
				cidPorSexo = relatorioDAO.getCidPorSexo(dataInicial, dataFinal);
				columnCidPorSexo = cidPorSexo.get(0);
				dataCidPorSexo = cidPorSexo.get(1);
				
				cidPorEscolaridade = relatorioDAO.getCidPorEscolaridade(dataInicial, dataFinal);
				columnCidPorEscolaridade = cidPorEscolaridade.get(0);
				dataCidPorEscolaridade = cidPorEscolaridade.get(1);
				
				cidPorEstado = relatorioDAO.getCidPorEstado(dataInicial, dataFinal);
				columnCidPorEstado = cidPorEstado.get(0);
				dataCidPorEstado = cidPorEstado.get(1);
				
				if(dataInicialConsulta.getYear() != dataFinalConsulta.getYear()) {
					
					manifPorMes = relatorioDAO.getManifPorAno(dataInicial, dataFinal);
					columnManifPorMes = manifPorMes.get(0);
					dataManifPorMes = manifPorMes.get(1);
					
					exibirPeriodo = meses[dataInicialConsulta.getMonth()] + " de " + Integer.toString(dataInicialConsulta.getYear()+1900) +
									" a " + meses[dataFinalConsulta.getMonth()] + " de " + Integer.toString(dataFinalConsulta.getYear()+1900);
					System.out.println(exibirPeriodo);
					System.out.println(Integer.toString(dataFinalConsulta.getYear()+1900));
				} else {
					exibirPeriodo = meses[dataInicialConsulta.getMonth()] + " a " +meses[dataFinalConsulta.getMonth()] + " de " + Integer.toString(dataInicialConsulta.getYear()+1900);
				}
			} else {
				exibirOrgao = entidadeDAO.getSigla(idEntidade) + " - ";
				
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				System.out.println(sdf.format(dataInicialConsulta) + " - " + sdf.format(dataFinalConsulta));
				String dataInicial = sdf.format(dataInicialConsulta);
				String dataFinal = sdf.format(dataFinalConsulta);
				
				
				manifPorTipo = relatorioDAO.getManifPorTipoEntidade(dataInicial, dataFinal, idEntidade);
				columnManifPorTipo = manifPorTipo.get(0);
				dataManifPorTipo = manifPorTipo.get(1);
				
				manifPorMes = relatorioDAO.getManifPorMesEntidade(dataInicial, dataFinal, idEntidade);
				columnManifPorMes = manifPorMes.get(0);
				dataManifPorMes = manifPorMes.get(1);
				
				manifPorTema = relatorioDAO.getManifPorTemaEntidade(dataInicial, dataFinal, idEntidade);
				columnManifPorTema = manifPorTema.get(0);
				dataManifPorTema = manifPorTema.get(1);
				
				manifPorSituacao = relatorioDAO.getManifPorSituacaoEntidade(dataInicial, dataFinal, idEntidade);
				columnManifPorSituacao = manifPorSituacao.get(0);
				dataManifPorSituacao = manifPorSituacao.get(1);
				
				manifPorOrgao = relatorioDAO.getManifPorOrgaoEntidade(dataInicial, dataFinal, idEntidade);
				columnManifPorOrgao = manifPorOrgao.get(0);
				dataManifPorOrgao = manifPorOrgao.get(1);
				
				manifPorCanal = relatorioDAO.getManifPorCanal(dataInicial, dataFinal, idEntidade);
				columnManifPorCanal = manifPorCanal.get(0);
				dataManifPorCanal = manifPorCanal.get(1);
				
				cidPorFaixaEtaria = relatorioDAO.getCidPorFaixaEtariaEntidade(dataInicial, dataFinal, idEntidade);
				columnCidPorFaixaEtaria = cidPorFaixaEtaria.get(0);
				dataCidPorFaixaEtaria = cidPorFaixaEtaria.get(1);
				
				cidPorRenda = relatorioDAO.getCidPorRendaEntidade(dataInicial, dataFinal, idEntidade);
				columnCidPorRenda = cidPorRenda.get(0);
				dataCidPorRenda = cidPorRenda.get(1);
				
				cidPorSexo = relatorioDAO.getCidPorSexoEntidade(dataInicial, dataFinal, idEntidade);
				columnCidPorSexo = cidPorSexo.get(0);
				dataCidPorSexo = cidPorSexo.get(1);
				
				cidPorEscolaridade = relatorioDAO.getCidPorEscolaridadeEntidade(dataInicial, dataFinal, idEntidade);
				columnCidPorEscolaridade = cidPorEscolaridade.get(0);
				dataCidPorEscolaridade = cidPorEscolaridade.get(1);
				
				cidPorEstado = relatorioDAO.getCidPorEstadoEntidade(dataInicial, dataFinal, idEntidade);
				columnCidPorEstado = cidPorEstado.get(0);
				dataCidPorEstado = cidPorEstado.get(1);
				
				if(dataInicialConsulta.getYear() != dataFinalConsulta.getYear()) {
					manifPorMes = relatorioDAO.getManifPorAnoEntidade(dataInicial, dataFinal, idEntidade);
					columnManifPorMes = manifPorMes.get(0);
					dataManifPorMes = manifPorMes.get(1);
					
					exibirPeriodo = meses[dataInicialConsulta.getMonth()] + " de " + Integer.toString(dataInicialConsulta.getYear()+1900) +
									" a " + meses[dataFinalConsulta.getMonth()] + " de " + Integer.toString(dataFinalConsulta.getYear()+1900);
				}
				else {
					exibirPeriodo = meses[dataInicialConsulta.getMonth()] + " a " +meses[dataFinalConsulta.getMonth()] + " de " + Integer.toString(dataInicialConsulta.getYear()+1900);
				}
			}
			
			
		}
	}
	
	public int getAnoAtual() {
		return anoAtual;
	}

	public String getMesAtual() {
		//String[] meses = { "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" };
		
		return meses[mesAtual-1];
	}

	public int getIdEntidade() {
		return idEntidade;
	}
	
	public void setIdEntidade(int idEntidade) {
		this.idEntidade = idEntidade;
	}

	public List<String[]> getManifPorTipo() {
		return manifPorTipo;
	}

	public void setManifPorTipo(List<String[]> manifPorTipo) {
		this.manifPorTipo = manifPorTipo;
	}

	public String[] getColumnManifPorTipo() {
		return columnManifPorTipo;
	}

	public void setColumnManifPorTipo(String[] columnManifPorTipo) {
		this.columnManifPorTipo = columnManifPorTipo;
	}

	public String[] getDataManifPorTipo() {
		return dataManifPorTipo;
	}

	public void setDataManifPorTipo(String[] dataManifPorTipo) {
		this.dataManifPorTipo = dataManifPorTipo;
	}

	public String[] getColumnManifPorMes() {
		return columnManifPorMes;
	}

	public void setColumnManifPorMes(String[] columnManifPorMes) {
		this.columnManifPorMes = columnManifPorMes;
	}

	public String[] getDataManifPorMes() {
		return dataManifPorMes;
	}

	public void setDataManifPorMes(String[] dataManifPorMes) {
		this.dataManifPorMes = dataManifPorMes;
	}

	public List<String[]> getManifPorTema() {
		return manifPorTema;
	}

	public void setManifPorTema(List<String[]> manifPorTema) {
		this.manifPorTema = manifPorTema;
	}

	public String[] getColumnManifPorTema() {
		return columnManifPorTema;
	}

	public void setColumnManifPorTema(String[] columnManifPorTema) {
		this.columnManifPorTema = columnManifPorTema;
	}

	public String[] getDataManifPorTema() {
		return dataManifPorTema;
	}

	public void setDataManifPorTema(String[] dataManifPorTema) {
		this.dataManifPorTema = dataManifPorTema;
	}

	public String[] getColumnManifPorSituacao() {
		return columnManifPorSituacao;
	}

	public void setColumnManifPorSituacao(String[] columnManifPorSituacao) {
		this.columnManifPorSituacao = columnManifPorSituacao;
	}

	public String[] getDataManifPorSituacao() {
		return dataManifPorSituacao;
	}

	public void setDataManifPorSituacao(String[] dataManifPorSituacao) {
		this.dataManifPorSituacao = dataManifPorSituacao;
	}

	public String[] getColumnManifPorOrgao() {
		return columnManifPorOrgao;
	}

	public void setColumnManifPorOrgao(String[] columnManifPorOrgao) {
		this.columnManifPorOrgao = columnManifPorOrgao;
	}

	public String[] getDataManifPorOrgao() {
		return dataManifPorOrgao;
	}

	public void setDataManifPorOrgao(String[] dataManifPorOrgao) {
		this.dataManifPorOrgao = dataManifPorOrgao;
	}

	public Date getDataInicialConsulta() {
		return dataInicialConsulta;
	}

	public void setDataInicialConsulta(Date dataInicialConsulta) {
		this.dataInicialConsulta = dataInicialConsulta;
	}

	public Date getDataFinalConsulta() {
		return dataFinalConsulta;
	}

	public void setDataFinalConsulta(Date dataFinalConsulta) {
		this.dataFinalConsulta = dataFinalConsulta;
	}

	public String getExibirOrgao() {
		return exibirOrgao;
	}

	public void setExibirOrgao(String exibirOrgao) {
		this.exibirOrgao = exibirOrgao;
	}

	public String getExibirPeriodo() {
		return exibirPeriodo;
	}

	public void setExibirPeriodo(String exibirPeriodo) {
		this.exibirPeriodo = exibirPeriodo;
	}

	public String getBase64Str() {
		return base64Str;
	}

	public void setBase64Str(String base64Str) {
		this.base64Str = base64Str;
	}

	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}

	public String getExibirAno() {
		return exibirAno;
	}

	public void setExibirAno(String exibirAno) {
		this.exibirAno = exibirAno;
	}

	public String[] getColumnCidPorFaixaEtaria() {
		return columnCidPorFaixaEtaria;
	}

	public void setColumnCidPorFaixaEtaria(String[] columnCidPorFaixaEtaria) {
		this.columnCidPorFaixaEtaria = columnCidPorFaixaEtaria;
	}

	public String[] getDataCidPorFaixaEtaria() {
		return dataCidPorFaixaEtaria;
	}

	public void setDataCidPorFaixaEtaria(String[] dataCidPorFaixaEtaria) {
		this.dataCidPorFaixaEtaria = dataCidPorFaixaEtaria;
	}

	public String[] getColumnCidPorRenda() {
		return columnCidPorRenda;
	}

	public void setColumnCidPorRenda(String[] columnCidPorRenda) {
		this.columnCidPorRenda = columnCidPorRenda;
	}

	public String[] getDataCidPorRenda() {
		return dataCidPorRenda;
	}

	public void setDataCidPorRenda(String[] dataCidPorRenda) {
		this.dataCidPorRenda = dataCidPorRenda;
	}

	public String[] getColumnCidPorSexo() {
		return columnCidPorSexo;
	}

	public void setColumnCidPorSexo(String[] columnCidPorSexo) {
		this.columnCidPorSexo = columnCidPorSexo;
	}

	public String[] getDataCidPorSexo() {
		return dataCidPorSexo;
	}

	public void setDataCidPorSexo(String[] dataCidPorSexo) {
		this.dataCidPorSexo = dataCidPorSexo;
	}

	public String[] getColumnCidPorEscolaridade() {
		return columnCidPorEscolaridade;
	}

	public void setColumnCidPorEscolaridade(String[] columnCidPorEscolaridade) {
		this.columnCidPorEscolaridade = columnCidPorEscolaridade;
	}

	public String[] getDataCidPorEscolaridade() {
		return dataCidPorEscolaridade;
	}

	public void setDataCidPorEscolaridade(String[] dataCidPorEscolaridade) {
		this.dataCidPorEscolaridade = dataCidPorEscolaridade;
	}

	public String[] getColumnCidPorEstado() {
		return columnCidPorEstado;
	}

	public void setColumnCidPorEstado(String[] columnCidPorEstado) {
		this.columnCidPorEstado = columnCidPorEstado;
	}

	public String[] getDataCidPorEstado() {
		return dataCidPorEstado;
	}

	public void setDataCidPorEstado(String[] dataCidPorEstado) {
		this.dataCidPorEstado = dataCidPorEstado;
	}

	public List<String[]> getManifPorCanal() {
		return manifPorCanal;
	}

	public void setManifPorCanal(List<String[]> manifPorCanal) {
		this.manifPorCanal = manifPorCanal;
	}

	public String[] getColumnManifPorCanal() {
		return columnManifPorCanal;
	}

	public void setColumnManifPorCanal(String[] columnManifPorCanal) {
		this.columnManifPorCanal = columnManifPorCanal;
	}

	public String[] getDataManifPorCanal() {
		return dataManifPorCanal;
	}

	public void setDataManifPorCanal(String[] dataManifPorCanal) {
		this.dataManifPorCanal = dataManifPorCanal;
	}

	public List<Entidade> getListEntidades() {
		return listEntidades;
	}

	public void setListEntidades(List<Entidade> listEntidades) {
		this.listEntidades = listEntidades;
	}

}