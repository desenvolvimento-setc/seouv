package br.gov.se.setc.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Messages;

import br.gov.se.setc.DAO.EntidadeDAO;
import br.gov.se.setc.DAO.ResponsavelDAO;
import br.gov.se.setc.DAO.UsuarioDAO;
import br.gov.se.setc.model.Entidade;
import br.gov.se.setc.model.Responsavel;
import br.gov.se.setc.model.Usuario;
import br.gov.se.setc.util.CriptografiaUtil;
import br.gov.se.setc.util.EmailUtil;
import br.gov.se.setc.util.GeralUtil;

@Named
@ViewScoped
public class CadResponsavelBean implements Serializable {

	private static final long serialVersionUID = -568903469058784542L;

	private UsuarioDAO usuarioDAO = UsuarioDAO.getInstance();
	
	private EntidadeDAO entidadeDAO = EntidadeDAO.getInstance();
	private ResponsavelDAO responsavelDAO = ResponsavelDAO.getInstance();;
	
	private boolean cadastrarUsuario;
	
	private String usuarioSelecionado;
	private Usuario usuarioCadastro;
	
	private Responsavel responsavelCadastro;
	private Integer idEntidade;
	
	private List<Entidade> listEntidadesAtivas;

	@PostConstruct
	public void init() {
//		System.out.println(":: init cadResponsavelBean ::");
		
		cadastrarUsuario = true;
		
		usuarioCadastro = new Usuario();
		responsavelCadastro = new Responsavel();
		
		listEntidadesAtivas = entidadeDAO.listEntidadesAtivas();
	}
	
	public String salvar() {
		try {
			if(existeEmail()) {
				return null;
			} else {
				if(cadastrarUsuario) {
					System.out.println("- Salvando responsável com novo usuário");
					
					usuarioCadastro.setSenha(CriptografiaUtil.Criptografar(usuarioCadastro.getSenha()));
					
					usuarioCadastro.setPerfil((short) 2);
					
					
					responsavelCadastro.setEntidade(entidadeDAO.getEntidade(idEntidade));
					responsavelCadastro.setAtivo(true);
					
					usuarioCadastro.addResponsavel(responsavelCadastro);
					
					boolean salvou = usuarioDAO.saveOrUpdate(usuarioCadastro);
					
					if(salvou) {
						System.out.println("Responsável salvo com sucesso");
						Messages.addGlobalInfo("Responsável salvo com sucesso");
						return "/consulta/responsaveis.xhtml?faces-redirect=true";
					} else {
						Messages.addGlobalError("Falha ao cadastrar responsável");
						return null;
					} 
					
				} else {
					System.out.println("- Salvando responsável com usuário selecionado");
					
					Usuario usuario = usuarioDAO.getUsuarioPorNick(usuarioSelecionado);
					
					if(!responsavelCadastrado(usuario.getIdUsuario(), idEntidade)) {
						usuario.setPerfil((short) 2);
						
						responsavelCadastro.setEntidade(entidadeDAO.getEntidade(idEntidade));
						responsavelCadastro.setAtivo(true);
						
						usuario.addResponsavel(responsavelCadastro);
						
						boolean salvou = usuarioDAO.saveOrUpdate(usuario);
						
						if(salvou) {
							System.out.println("Responsável salvo com sucesso");
							Messages.addGlobalInfo("Responsável salvo com sucesso");
							return "/consulta/responsaveis.xhtml?faces-redirect=true";
						} else {
							Messages.addGlobalError("Falha ao cadastrar responsável");
							return null;
						} 
						
					} else {
						Messages.addGlobalWarn("Responsável já alocado nesta entidade");
						return null;
					}
				}
				
			}
		} catch (Exception e) {
			System.out.println(":: Falha ao cadastrar usuário");
			e.printStackTrace();
			Messages.addGlobalError("Falha ao cadastrar responsável");
			return null;
		} finally {
			cadastrarUsuario = false;
			
			usuarioCadastro = new Usuario();
			usuarioSelecionado = "";
			responsavelCadastro = new Responsavel();
		}
	}
	
	public void gerarNick() {
		
		try {
			String nomeSobrenome = GeralUtil.nomeSobrenome(usuarioCadastro.getNome());
			String nick = GeralUtil.removerAcentos(nomeSobrenome).replace(" ", ".");
				
			if (!GeralUtil.checarPalavrasReservadas(nomeSobrenome)) {
				Long count = usuarioDAO.countNick(nick);
				
				if(count >= 1) {
					usuarioCadastro.setNick(nick + (count+1));
				} else {
					usuarioCadastro.setNick(nick);
				}
			} else {
				Messages.addGlobalWarn("Digite um nome válido.");
				usuarioCadastro.setNick(null);
			}
			
		} catch (Exception e) {
			Messages.addGlobalError("Falha ao gerar nome de usuário");
			e.printStackTrace();
		}
		
	}
	
	private boolean existeEmail() {
		try {
			boolean existe = usuarioDAO.existeEmail(responsavelCadastro.getEmail());
			
			if(existe) {
//				System.out.println("Email já cadastrado no sistema");
				Messages.addGlobalWarn("Email já cadastrado no sistema");
				return existe;
			} else {
//				System.out.println("Email liberado");
				return existe;
			}
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public List<String> completeUsuario (String prefix) {
		List<String> usuarios = new ArrayList<String>();
		
		try {
			usuarios = usuarioDAO.listUsuariosPorNick(prefix);
		}catch (Exception e) {
			System.out.println(":: completeUsuario - Erro ao consultar usuários");
			e.printStackTrace();
		}
		
		return usuarios;
	}
	
	
	public boolean responsavelCadastrado(Integer idUsuario, Integer idEntidade) {
		try{
			List<Responsavel> responsaveis = responsavelDAO.listResponsaveisUsuario(idUsuario);
			
			if(responsaveis.stream().anyMatch(o-> o.getEntidade().getIdEntidade() == idEntidade)) {
				return true;
			} else {
				return false;
			}
		}catch (Exception e) {
			return false;
		}
	}

	
	
	public Usuario getUsuarioCadastro() {
		return usuarioCadastro;
	}

	public void setUsuarioCadastro(Usuario usuarioCadastro) {
		this.usuarioCadastro = usuarioCadastro;
	}

	public boolean isCadastrarUsuario() {
		return cadastrarUsuario;
	}

	public void setCadastrarUsuario(boolean cadastrarUsuario) {
		this.cadastrarUsuario = cadastrarUsuario;
	}

	public Responsavel getResponsavelCadastro() {
		return responsavelCadastro;
	}

	public void setResponsavelCadastro(Responsavel responsavelCadastro) {
		this.responsavelCadastro = responsavelCadastro;
	}

	public String getUsuarioSelecionado() {
		return usuarioSelecionado;
	}

	public void setUsuarioSelecionado(String usuarioSelecionado) {
		this.usuarioSelecionado = usuarioSelecionado;
	}

	public Integer getIdEntidade() {
		return idEntidade;
	}

	public void setIdEntidade(Integer idEntidade) {
		this.idEntidade = idEntidade;
	}

	public List<Entidade> getListEntidadesAtivas() {
		return listEntidadesAtivas;
	}

	public void setListEntidadesAtivas(List<Entidade> listEntidadesAtivas) {
		this.listEntidadesAtivas = listEntidadesAtivas;
	}


}
