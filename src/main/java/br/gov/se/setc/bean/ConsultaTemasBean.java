package br.gov.se.setc.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Faces;
import org.omnifaces.util.FacesLocal;
import org.omnifaces.util.Messages;
import org.primefaces.PrimeFaces;

import br.gov.se.setc.DAO.CompetenciaDAO;
import br.gov.se.setc.DAO.TemaDAO;
import br.gov.se.setc.model.Competencia;
import br.gov.se.setc.model.Tema;
import br.gov.se.setc.util.GeralUtil;

@Named
@ViewScoped
public class ConsultaTemasBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private CompetenciaDAO competenciaDAO = CompetenciaDAO.getInstance();;
	private TemaDAO temaDAO = TemaDAO.getInstance();;
	
	private String perfilLogado;
	
	private List<Tema> listTemasCadastrados;
	private List<Tema> listTemasPendentes;
	private List<Tema> listTemasCadastradosFilter;
	private List<Tema> listTemasPendentesFilter;
	
	private Tema novoTema;
	private Tema editTema;
	
//	private Tema originalTema;

	@PostConstruct
	public void init() {
		System.out.println(":: init consultaTemasBean ::");
		
		// Inicializar objetos
		novoTema = new Tema();
		editTema = new Tema();
		
		// Verifica se perfil logado tem autorização na página e popula listas
		try {
			perfilLogado = (String) GeralUtil.recuperarDaSessao("perfilLogado");
			
			if(perfilLogado != null && !perfilLogado.equals("logoff")) {
				if(perfilLogado.equals("coordenadorOGE") || perfilLogado.equals("ouvidorGeral") || perfilLogado.equals("admin")) {
					listTemasCadastrados = temaDAO.listTemasCadastrados();
					listTemasPendentes = temaDAO.listTemasPendentes();
				} else {
					System.out.println("Acesso Negado");
					Messages.addGlobalWarn("Acesso Negado", "");
					PrimeFaces.current().ajax().update("growl");
					FacesLocal.redirect(Faces.getContext(), "../pagina/acessoNegado.xhtml");
				}
			} else {
				System.out.println("Acesso Negado");
				Messages.addGlobalWarn("Acesso Negado", "Realize login");
				FacesLocal.redirect(Faces.getContext(), "../index.xhtml");
			}
			
		} catch (Exception e) {
			System.out.println(":: Erro ao popular lista de entidades");
			e.printStackTrace();
		}
	}
	
	public void salvar() {
		try {
			if(perfilLogado.equals("coordenadorOGE") || perfilLogado.equals("ouvidorGeral") || perfilLogado.equals("admin")) {
				if(!listTemasCadastrados.stream().anyMatch(o-> o.getTitulo().equals(novoTema.getTitulo()))) {
					novoTema.setStatus("Não-vinculada");
					boolean salvou = temaDAO.saveOrUpdate(novoTema);
					
					if(salvou) listTemasCadastrados.add(novoTema);
				} else {
					Messages.addGlobalWarn("Tema '"+ novoTema.getTitulo() + "' já cadastrado no sistema.");
				}
			} else {
				Messages.addGlobalWarn("Usuário sem permissão");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			novoTema = new Tema();
		}
	}
	
	public void abrirEditarTema(Tema tema) {
		try {
			editTema = tema.copy();
			
			PrimeFaces.current().executeScript("PF('editDialog').show();");
		} catch (Exception e) {
			e.printStackTrace();
			Messages.addGlobalWarn("", "Falha ao abrir dialog");
		}
	}
	
	public void editar() {
		try {
			if(perfilLogado.equals("coordenadorOGE") || perfilLogado.equals("ouvidorGeral") || perfilLogado.equals("admin")) {
				if(!listTemasCadastrados.stream().filter(i -> i.getIdTema() != editTema.getIdTema()).anyMatch(o-> o.getTitulo().equals(editTema.getTitulo()))) {
//					System.out.println("editTema: " + editTema.getTitulo() + " originalTema: " + originalTema.getTitulo());
					temaDAO.saveOrUpdate(editTema);
				} else {
//					System.out.println("editTema: " + editTema.getTitulo() + " originalTema: " + originalTema.getTitulo());
//					editTema = originalTema;
					Messages.addGlobalWarn("Tema '"+ editTema.getTitulo() + "' já cadastrado no sistema.");
				}
			} else {
				Messages.addGlobalWarn("Usuário sem permissão");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			listTemasCadastrados = temaDAO.listTemasCadastrados();
			editTema = new Tema();
//			originalTema = new Tema();
		}
	}

	
	public void aprovarTema(Tema tema) {
		try {
			tema.setStatus("Vinculada");
			
			boolean salvou = temaDAO.saveOrUpdate(tema);
			listTemasPendentes.remove(tema);
			listTemasCadastrados.add(tema);
			
			boolean atualizouComp = false;
			
			if (salvou) {
				List<Competencia> listComp = competenciaDAO.listCompetenciasPorTema(tema.getIdTema());
				
				for (Competencia comp : listComp) {
					comp.setAtiva(true);
					atualizouComp = competenciaDAO.saveOrUpdate(comp);
				}
			}
			
			if (!atualizouComp) {
				System.out.println("Falha ao ativar competência relacionada");
				Messages.addGlobalWarn("", "Falha ao ativar competência relacionada");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Messages.addGlobalWarn("", "Falha ao aprovar tema");
		}
	}
	
	public void recusarTema(Tema tema) {
		try {
			listTemasPendentes.remove(tema);
			TemaDAO.delete(tema);
		} catch (Exception e) {
			e.printStackTrace();
			Messages.addGlobalWarn("", "Falha ao recusar tema");
		}
	}
	
	
	public Tema getEditTema() {
		return editTema;
	}

	public void setEditTema(Tema editTema) {
		this.editTema = editTema;
	}

	public List<Tema> getListTemasCadastrados() {
		return listTemasCadastrados;
	}

	public void setListTemasCadastrados(List<Tema> listTemasCadastrados) {
		this.listTemasCadastrados = listTemasCadastrados;
	}

	public List<Tema> getListTemasPendentes() {
		return listTemasPendentes;
	}

	public void setListTemasPendentes(List<Tema> listTemasPendentes) {
		this.listTemasPendentes = listTemasPendentes;
	}

	public List<Tema> getListTemasCadastradosFilter() {
		return listTemasCadastradosFilter;
	}

	public void setListTemasCadastradosFilter(List<Tema> listTemasCadastradosFilter) {
		this.listTemasCadastradosFilter = listTemasCadastradosFilter;
	}

	public List<Tema> getListTemasPendentesFilter() {
		return listTemasPendentesFilter;
	}


	public void setListTemasPendentesFilter(List<Tema> listTemasPendentesFilter) {
		this.listTemasPendentesFilter = listTemasPendentesFilter;
	}

	public Tema getNovoTema() {
		return novoTema;
	}

	public void setNovoTema(Tema novoTema) {
		this.novoTema = novoTema;
	}

}
