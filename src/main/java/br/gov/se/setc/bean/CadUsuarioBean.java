package br.gov.se.setc.bean;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Faces;
import org.omnifaces.util.Messages;

import br.gov.se.setc.DAO.UsuarioDAO;
import br.gov.se.setc.model.Cidadao;
import br.gov.se.setc.model.Usuario;
import br.gov.se.setc.util.CriptografiaUtil;
import br.gov.se.setc.util.EmailUtil;
import br.gov.se.setc.util.GeralUtil;

@Named
@ViewScoped
public class CadUsuarioBean implements Serializable{

	private static final long serialVersionUID = 7531784703525992582L;

	private UsuarioDAO usuarioDAO = UsuarioDAO.getInstance();
	
	private Usuario usuarioCadastro;
	private Cidadao cidadaoCadastro;
	
	private boolean isPessoaFisica;

	@PostConstruct
	public void init() {
//		System.out.println(":: init cadUsuarioBean ::");
		
		usuarioCadastro = new Usuario();
		cidadaoCadastro = new Cidadao();
		cidadaoCadastro.setTipo(true);
	}
	
	public String salvar() {
		try {
			if(existeEmail() || existeCPF() || existeRG()) {
				return null;
			} else {
				String psswOriginal = usuarioCadastro.getSenha();
				usuarioCadastro.setSenha(CriptografiaUtil.Criptografar(psswOriginal));
				
				usuarioCadastro.setPerfil((short) 3);
				
				if(this.cidadaoCadastro.getNumero().isEmpty()) {
					this.cidadaoCadastro.setNumero(null);
				}
				
				usuarioCadastro.addCidadao(cidadaoCadastro);
				
				boolean salvou = usuarioDAO.saveOrUpdate(usuarioCadastro);
				
				if(salvou) {
					System.out.println("Cidadão salvo com sucesso");
					EmailUtil.enviarEmailCadastroCid(cidadaoCadastro);
					ELContext elContext = Faces.getContext().getELContext();
					
					LoginBean loginBean = (LoginBean) FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(elContext, null, "loginBean");
					loginBean.setUsername(usuarioCadastro.getNick());
					loginBean.setSenha(psswOriginal);
					String redirect = loginBean.login();
					
					return redirect;
				} else {
					System.out.println("Falha ao cadastrar cidadão");
					Messages.addGlobalError("Falha ao cadastrar cidadão");
					return null;
				}
			}
			
		} catch (Exception e) {
			System.out.println("Falha ao cadastrar cidadão: " + e.getMessage());
			Messages.addGlobalError("Falha ao cadastrar cidadão");
			return null;
		}
	}
	
	public void gerarNick() {
		
		try {
			String nomeSobrenome = GeralUtil.nomeSobrenome(usuarioCadastro.getNome());
			String nick = GeralUtil.removerAcentos(nomeSobrenome).replace(" ", ".").toLowerCase();
				
			if (!GeralUtil.checarPalavrasReservadas(nomeSobrenome)) {
				Long count = usuarioDAO.countNick(nick);
				
				if(count >= 1) {
					usuarioCadastro.setNick(nick + (count+1));
				} else {
					usuarioCadastro.setNick(nick);
				}
			} else {
				Messages.addGlobalWarn("Digite um nome válido.");
				usuarioCadastro.setNick(null);
			}
			
		} catch (Exception e) {
			Messages.addGlobalError("Falha ao gerar nome de usuário");
			e.printStackTrace();
		}
		
	}
	
	private boolean existeEmail() {
		try {
			boolean existe = usuarioDAO.existeEmail(cidadaoCadastro.getEmail());
			
			if(existe) {
//				System.out.println("Email já cadastrado no sistema");
				Messages.addGlobalWarn("Email já cadastrado no sistema");
				return existe;
			} else {
//				System.out.println("Email liberado");
				return existe;
			}
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	private boolean existeCPF() {
		try {
			if(cidadaoCadastro.getCpf() == null || cidadaoCadastro.getCpf().isEmpty()) {
				return false;
			} else {
				boolean existe = usuarioDAO.existeCPF(cidadaoCadastro.getCpf());
				
				if(existe) {
//					System.out.println("CPF já cadastrado no sistema");
					Messages.addGlobalWarn("CPF já cadastrado no sistema");
					return existe;
				} else {
//					System.out.println("CPF liberado");
					return existe;
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	private boolean existeRG() {
		try {
			if(cidadaoCadastro.getRg() == null || cidadaoCadastro.getRg().isEmpty()) {
				return false;
			} else {
				boolean existe = usuarioDAO.existeRG(cidadaoCadastro.getRg());
				
				if(existe) {
//					System.out.println("RG já cadastrado no sistema");
					Messages.addGlobalWarn("RG já cadastrado no sistema");
					return existe;
				} else {
//					System.out.println("RG liberado");
					return existe;
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	

	public Usuario getUsuarioCadastro() {
		return usuarioCadastro;
	}

	public void setUsuarioCadastro(Usuario usuarioCadastro) {
		this.usuarioCadastro = usuarioCadastro;
	}

	public Cidadao getCidadaoCadastro() {
		return cidadaoCadastro;
	}

	public void setCidadaoCadastro(Cidadao cidadaoCadastro) {
		this.cidadaoCadastro = cidadaoCadastro;
	}

	public boolean isPessoaFisica() {
		return isPessoaFisica;
	}

	public void setPessoaFisica(boolean isPessoaFisica) {
		this.isPessoaFisica = isPessoaFisica;
	}
	

}
