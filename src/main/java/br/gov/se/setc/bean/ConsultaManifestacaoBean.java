package br.gov.se.setc.bean;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.event.ValueChangeEvent;
import javax.faces.event.ValueChangeListener;
import javax.inject.Named;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.apache.commons.net.ftp.FTPFile;
import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Messages;
import org.primefaces.PrimeFaces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import br.gov.se.setc.DAO.EntidadeDAO;
import br.gov.se.setc.DAO.ManifestacaoDAO;
import br.gov.se.setc.DAO.MensagemDAO;
import br.gov.se.setc.DAO.UsuarioDAO;
import br.gov.se.setc.model.Anexo;
import br.gov.se.setc.model.AnexoUpload;
import br.gov.se.setc.model.DashboardInfo2;
import br.gov.se.setc.model.Entidade;
import br.gov.se.setc.model.Manifestacao;
import br.gov.se.setc.model.Mensagem;
import br.gov.se.setc.model.Responsavel;
import br.gov.se.setc.model.Usuario;
import br.gov.se.setc.util.AnexoUtil;
import br.gov.se.setc.util.EmailUtil;
import br.gov.se.setc.util.GeralUtil;
import br.gov.se.setc.util.PrazosUtil;

@Named
@ViewScoped
public class ConsultaManifestacaoBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private ManifestacaoDAO manifestacaoDAO = ManifestacaoDAO.getInstance();
	private UsuarioDAO usuarioDAO = UsuarioDAO.getInstance();
	
	private MensagemDAO mensagemDAO = MensagemDAO.getInstance();
//	private ResponsavelDAO responsavelDAO = ResponsavelDAO.getInstance();
	private EntidadeDAO entidadeDAO = EntidadeDAO.getInstance();
	
	private List<Manifestacao> listManifestacoes; 
	
	private LazyDataModel<Manifestacao> dataModel;
	
	private Manifestacao maniDetalhe;
	
	// Texto das mensagens nas manifestaçoes
	private String mensagem_1;
	private String mensagem_2;
	
	// Arquivo e Lista de anexos usados nas respostas das manifestações
	private List<AnexoUpload> anexosList;
	
	// Tipo de manifestação da função de Alterar tipo de manifestação
	private String tipoSelecionado;
	
	// Nota da função de Avaliar manifestação
    private int nota;
	
	// Lista de Entidades Ativas e Entidade Selecionada para reencaminhamento
	private List<Entidade> entidadesAtivas;
	private int idEntReencaminhar;
	
	// Lista de todas as Entidades para filtro de tabela
	private List<Entidade> listEntidades;
	
	private String perfilLogado;
	private Usuario usuarioLogado;
	private Responsavel responsavel;
	
	private boolean liberarAnexo;
	
	private short filtro;
	
	private DashboardInfo2 dashboard;
	
	@PostConstruct
	public void init() {
		System.out.println(":: init consultaManifestacaoBean ::");
		
		filtro = 0;
		dashboard = new DashboardInfo2();
		
		perfilLogado = (String) GeralUtil.recuperarDaSessao("perfilLogado");
		usuarioLogado = (Usuario) GeralUtil.recuperarDaSessao("usuarioLogin");
		
		try {
			if(perfilLogado != null && !perfilLogado.equals("logoff")) {
				lazyLoad();
				anexosList = new ArrayList<AnexoUpload>();
				
				entidadesAtivas = entidadeDAO.listEntidadesAtivas();
				
				if(usuarioLogado.getPerfil() == 2) {
					responsavel = usuarioLogado.getResponsaveis().iterator().next();
					
					if (responsavel.getEntidade().getIdEntidade() == 116) {
						listEntidades = entidadeDAO.listEntidadesAgrese();
					} else {
						listEntidades = entidadeDAO.listEntidades();
					}
				} else {
					listEntidades = entidadeDAO.listEntidades();
				}
			} else {
				Messages.addGlobalWarn("Acesso Negado - Realize Login");
			}
		} catch (Exception e) {
			System.out.println(":: Erro ao popular lista de entidades");
			e.printStackTrace();
		}
		
		try {
			if(perfilLogado.equals("ouvidorOrgao")) {
				dashboard = manifestacaoDAO.contarManifestacoesSituacaoVencimentoOrgao(responsavel.getEntidade().getIdEntidade());
			} else if(perfilLogado.equals("coordenadorOGE") || perfilLogado.equals("ouvidorGeral") || perfilLogado.equals("admin")) {
				dashboard = manifestacaoDAO.contarManifestacoesSituacaoVencimento();
			}
		} catch (Exception e) {
			System.out.println(":: Erro ao popular dashboard");
			e.printStackTrace();
		}
		
		testConexaoFTP();
	}
	
//	@PreDestroy
//	public void release() {
//		System.out.println(":: release consultaManifestacaoBean");
//	}
	
	public void reloadManifestacoes() {
		System.out.println("Filtrando: " + filtro);
		lazyLoad();
	}
	
	public void reloadManifestacoes(short filtroSelecionado) {
		filtro = filtroSelecionado;
		System.out.println("Filtrando: " + filtro);
		lazyLoad();
	}
	
	public void refreshDashboard() {
		try {
			if(perfilLogado.equals("ouvidorOrgao")) {
				dashboard = manifestacaoDAO.contarManifestacoesSituacaoVencimentoOrgao(responsavel.getEntidade().getIdEntidade());
			} else if(perfilLogado.equals("coordenadorOGE") || perfilLogado.equals("ouvidorGeral") || perfilLogado.equals("admin")) {
				dashboard = manifestacaoDAO.contarManifestacoesSituacaoVencimento();
			}
		} catch (Exception e) {
			System.out.println(":: Erro ao atualizar dashboard");
			e.printStackTrace();
		}
	}
	
	private void lazyLoad() {
		if(usuarioLogado != null) {
			dataModel = new LazyDataModel<Manifestacao>() {
				
				private static final long serialVersionUID = 1L;
				
				@Override
				public List<Manifestacao> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters){
					setRowCount(manifestacaoDAO.countManifestacoes(usuarioLogado, filters, filtro));
					
					return manifestacaoDAO.listManifestacoes(usuarioLogado, first, pageSize, sortField, sortOrder, filters, filtro);
				}
			};
		}
	}
	
	public void abrirManifestacao(Manifestacao manifestacao) {
		try {
			
			maniDetalhe = manifestacao;
			
			Usuario usuarioLogado = (Usuario) GeralUtil.recuperarDaSessao("usuarioLogin");
			
			String perfilLogado = (String) GeralUtil.recuperarDaSessao("perfilLogado");
			
			if (perfilLogado.equals("ouvidorOrgao") || perfilLogado.equals("responsavelOrgao")) {
				if ((!maniDetalhe.isVisualizada() && maniDetalhe.getStatus().equals("Aberta")) || maniDetalhe.getStatus().equals("Reencaminhada")) {
					Mensagem mensagem = new Mensagem();
					mensagem.setTexto("Manifestação " + maniDetalhe.getProtocolo() + " foi visualizada por " + usuarioLogado.getNome() + ".");
					mensagem.setData(GeralUtil.getDataAtual());
					mensagem.setUsuario(usuarioDAO.getUsuario(2));
					mensagem.setTipo((short) 4);
					
					maniDetalhe.addHistorico(mensagem);
					maniDetalhe.setVisualizada(true);
					maniDetalhe.setStatus("Tramitando");
					
					boolean salvou = manifestacaoDAO.saveOrUpdate(maniDetalhe);
					
					if (salvou)
						System.out.println("Manifestação " + maniDetalhe.getProtocolo() + " foi visualizada por " + usuarioLogado.getNome() + ".");
					
				} 
			}
			
//			for(Mensagem msg: maniDetalhe.getMensagens()) {
//				if(!msg.getAnexos().isEmpty()) {
//					for(Anexo anx : msg.getAnexos()) {
//						System.out.println("Anexo: " + anx.getIdAnexo() + " - " + anx.getNome());
//					}
//					
//				}
//				
//			}
			
			limparResposta();
			
			PrimeFaces.current().executeScript("PF('detalheDialog').show();");
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("Manifestação não carregada");
			Messages.addGlobalError("Falha ao carregar manifestação");
		}
	}
	
	
	public void limparResposta() {
		anexosList = new ArrayList<AnexoUpload>();
		mensagem_1 = "";
		mensagem_2 = "";
		System.out.println("limparResposta");
	}
	
	// Ações manifestação
	public void responder() {
		String diretorioAnexos = gerarNomeDiretorio();
		
		try {
			Date dataAtual = GeralUtil.getDataAtual(manifestacaoDAO);
			
			Usuario usuarioLogado = (Usuario) GeralUtil.recuperarDaSessao("usuarioLogin");
			
			Mensagem mensagem = new Mensagem();
			
			mensagem.setTexto(mensagem_1);
			mensagem.setData(dataAtual);
			mensagem.setUsuario(usuarioLogado);
			mensagem.setTipo((short) 2);
			mensagem.setManifestacao(maniDetalhe);
			
			if(anexosList.size() > 0) {
				List<Anexo> anexos = salvarAnexos(diretorioAnexos);
				
				mensagem.setAnexos(anexos);
			}
			
			Mensagem mensagemSistema = GeralUtil.gerarMensagem("Manifestação "+ maniDetalhe.getProtocolo() +" recebeu resposta no sistema.", usuarioDAO.getUsuario(2), (short) 4);
			mensagemSistema.setManifestacao(maniDetalhe);
			
			maniDetalhe.setStatus("Atendida");
			maniDetalhe.addMensagem(mensagem);
			maniDetalhe.addHistorico(mensagemSistema);
			
			boolean salvou = manifestacaoDAO.saveOrUpdate(maniDetalhe);
			
			if(salvou) {
				EmailUtil.enviarEmailNotificacaoCidadao(maniDetalhe, mensagem);
				System.out.println("Manifestação " + maniDetalhe.getProtocolo() + " respondida por " + usuarioLogado.getNome() + ".");				
			}
			
			limparResposta();
		} catch(Exception e) {
			e.printStackTrace();
			Messages.addGlobalError("Falha ao responder manifestação");
			limparAnexos(diretorioAnexos);
		}
	}
	
	public void solicitarReformulacao() {
		try {
			Usuario usuarioLogado = (Usuario) GeralUtil.recuperarDaSessao("usuarioLogin");
			
			Mensagem mensagem = new Mensagem();
			
			mensagem.setTexto(mensagem_1);
			mensagem.setData(GeralUtil.getDataAtual());
			mensagem.setUsuario(usuarioLogado);
			mensagem.setTipo((short) 7);
			mensagem.setManifestacao(maniDetalhe);
			
			Mensagem mensagemSistema = GeralUtil.gerarMensagem("Solicitação de reformulação de manifestação realizado por "+ GeralUtil.nomeSobrenome(usuarioLogado.getNome()) +".", usuarioDAO.getUsuario(2), (short) 4);
			mensagemSistema.setManifestacao(maniDetalhe);
			
			boolean salvou = mensagemDAO.saveOrUpdate(mensagem);
			
			if (maniDetalhe.getTipo().equals("Informação"))
				maniDetalhe.setDataLimite(PrazosUtil.gerarPrazoDiaUtilLimite(new Date(System.currentTimeMillis()), "Esic","Reformulação"));
			else
				maniDetalhe.setDataLimite(PrazosUtil.gerarPrazoDiaUtilLimite(new Date(System.currentTimeMillis()), "Ouv", "Reformulação"));
			
			maniDetalhe.setStatus("Reformulação");
			maniDetalhe.addMensagem(mensagem);
			maniDetalhe.addHistorico(mensagemSistema);
			
			manifestacaoDAO.saveOrUpdate(maniDetalhe);
			
			// Gera data limite
			if (salvou) {
				EmailUtil.enviarEmailNotificacaoCidadao(maniDetalhe, mensagem);
				System.out.println("Manifestação " + maniDetalhe.getProtocolo() + " teve reformulação solicitada por " + usuarioLogado.getNome() + ".");
			}
			
			limparResposta();
		} catch(Exception e) {
			limparResposta();
			Messages.addGlobalError( "Falha ao solicitar reformulação da manifestação");
			e.printStackTrace();
		}
	}
	
	public void reformular() {
		String diretorioAnexos = gerarNomeDiretorio();
		
		try {
			Usuario usuarioLogado = (Usuario) GeralUtil.recuperarDaSessao("usuarioLogin");
			
			Mensagem mensagem = new Mensagem();
			
			mensagem.setTexto(mensagem_1);
			mensagem.setData(GeralUtil.getDataAtual());
			mensagem.setUsuario(usuarioLogado);
			mensagem.setTipo((short) 1);
			
			if(anexosList.size() > 0) {
				List<Anexo> anexos = salvarAnexos(diretorioAnexos);
				
				mensagem.setAnexos(anexos);
			}
			
			Mensagem mensagemSistema = GeralUtil.gerarMensagem("Pedido de reformulação atendido pelo manifestante.", usuarioDAO.getUsuario(2), (short) 4);
			
			if (maniDetalhe.getTipo().equals("Informação"))
				maniDetalhe.setDataLimite(PrazosUtil.gerarPrazoDiaUtilLimite(new Date(System.currentTimeMillis()), "Esic","Aberta"));
			else
				maniDetalhe.setDataLimite(PrazosUtil.gerarPrazoDiaUtilLimite(new Date(System.currentTimeMillis()), "Ouv", "Aberta"));
			
			maniDetalhe.setStatus("Tramitando");
			maniDetalhe.addMensagem(mensagem);
			maniDetalhe.addHistorico(mensagemSistema);
			
			boolean salvou = manifestacaoDAO.saveOrUpdate(maniDetalhe);
			
			if(salvou) {
				EmailUtil.enviarEmailNotificacaoCidadao(maniDetalhe, mensagem);
				EmailUtil.enviarEmailRespostaReformulacao(maniDetalhe);
				System.out.println("Manifestação " + maniDetalhe.getProtocolo() + " reformulada.");
			}
			
			limparResposta();
		} catch(Exception e) {
			Messages.addGlobalError("Falha ao responder manifestação");
			limparAnexos(diretorioAnexos);
			e.printStackTrace();
		}
	}
	
	public void alterarTipo() {
		try {
			String tipoAtual = maniDetalhe.getTipo();
			String tipoFinal = tipoSelecionado;
			
			Usuario usuarioLogado = (Usuario) GeralUtil.recuperarDaSessao("usuarioLogin");
			
			Mensagem mensagem = GeralUtil.gerarMensagem(mensagem_1, usuarioLogado, (short) 7);
			
			Mensagem mensagemSistema = GeralUtil.gerarMensagem("Tipo de manifestação alterada de " + tipoAtual + " para " + tipoFinal + " por " + usuarioLogado.getNome(), usuarioDAO.getUsuario(2), (short) 4);
			
			maniDetalhe.setTipo(tipoFinal);
			maniDetalhe.addMensagem(mensagem);
			maniDetalhe.addHistorico(mensagemSistema);
			
			if (maniDetalhe.getTipo().equals("Informação"))
				maniDetalhe.setDataLimite(PrazosUtil.gerarPrazoDiaUtilLimite(maniDetalhe.getDataIni(), "Esic","Aberta"));
			else
				maniDetalhe.setDataLimite(PrazosUtil.gerarPrazoDiaUtilLimite(maniDetalhe.getDataIni(), "Ouv", "Aberta"));
			
			
			boolean salvou = manifestacaoDAO.saveOrUpdate(maniDetalhe);
			
			if(salvou) {
				EmailUtil.enviarEmailNotificacaoCidadao(maniDetalhe, mensagem);
				System.out.println("Manifestação " + maniDetalhe.getProtocolo() + " teve tipo alterado de " + tipoAtual + " para " + tipoFinal + " por " + usuarioLogado.getNome() + ".");
			}
			
			limparResposta();
		} catch (Exception e) {
			Messages.addGlobalError( "Falha ao alterar tipo da manifestação");
			e.printStackTrace();
		}
	}
	
	public void negar() {
		try {
			Usuario usuarioLogado = (Usuario) GeralUtil.recuperarDaSessao("usuarioLogin");
			
			Mensagem mensagem = new Mensagem();
			
			mensagem.setTexto(mensagem_1);
			mensagem.setData(GeralUtil.getDataAtual());
			mensagem.setUsuario(usuarioLogado);
			mensagem.setTipo((short) 7);
			
			Mensagem mensagemSistema = GeralUtil.gerarMensagem("Manifestação "+ maniDetalhe.getProtocolo() +" foi negada no sistema.", usuarioDAO.getUsuario(2), (short) 4);
			
			// Gera data limite
			if (maniDetalhe.getTipo().equals("Informação"))
				maniDetalhe.setDataLimite(PrazosUtil.gerarPrazoDiaUtilLimite(new Date(System.currentTimeMillis()), "Esic","Recurso"));
			else
				maniDetalhe.setDataLimite(PrazosUtil.gerarPrazoDiaUtilLimite(new Date(System.currentTimeMillis()), "Ouv", "Recurso"));
			
			maniDetalhe.setStatus("Negada");
			maniDetalhe.addMensagem(mensagem);
			maniDetalhe.addHistorico(mensagemSistema);
			
			boolean salvou = manifestacaoDAO.saveOrUpdate(maniDetalhe);
			
			if(salvou) {
				EmailUtil.enviarEmailNotificacaoCidadao(maniDetalhe, mensagem);
				System.out.println("Manifestação " + maniDetalhe.getProtocolo() + " negada por " + usuarioLogado.getNome() + ".");				
			}
			
			limparResposta();
		} catch(Exception e) {
			Messages.addGlobalError( "Falha ao negar manifestação");
			e.printStackTrace();
		}
	}
	
	public void prorrogar() {
		try {
			Usuario usuarioLogado = (Usuario) GeralUtil.recuperarDaSessao("usuarioLogin");
			
			Mensagem mensagem = new Mensagem();
			
			mensagem.setTexto(mensagem_1);
			mensagem.setData(GeralUtil.getDataAtual());
			mensagem.setUsuario(usuarioLogado);
			mensagem.setTipo((short) 7);
			
			Mensagem mensagemSistema = GeralUtil.gerarMensagem("Manifestação "+ maniDetalhe.getProtocolo() +" foi prorrogada no sistema.", usuarioDAO.getUsuario(2), (short) 8);
			
			if (maniDetalhe.getTipo().equals("Informação"))
				maniDetalhe.setDataLimite(PrazosUtil.gerarPrazoDiaUtilLimite(maniDetalhe.getDataLimite(), "Esic","Prorrogada"));
			else
				maniDetalhe.setDataLimite(PrazosUtil.gerarPrazoDiaUtilLimite(maniDetalhe.getDataLimite(), "Ouv", "Prorrogada"));
			
			maniDetalhe.setStatus("Prorrogada");
			maniDetalhe.addMensagem(mensagem);
			maniDetalhe.addHistorico(mensagemSistema);
			
			boolean salvou = manifestacaoDAO.saveOrUpdate(maniDetalhe);
			
			if(salvou) {
				EmailUtil.enviarEmailNotificacaoCidadao(maniDetalhe, mensagem);
				System.out.println("Manifestação " + maniDetalhe.getProtocolo() + " prorrogada por " + usuarioLogado.getNome() + ".");
			}
			
			limparResposta();
		} catch(Exception e) {
			Messages.addGlobalError( "Falha ao prorrogar manifestação");
			e.printStackTrace();
		}
	}
	
	public void recurso() {
		try {
			Usuario usuarioLogado = (Usuario) GeralUtil.recuperarDaSessao("usuarioLogin");
			
			Mensagem mensagem = new Mensagem();
			
			mensagem.setTexto(mensagem_1);
			mensagem.setData(GeralUtil.getDataAtual());
			mensagem.setUsuario(usuarioLogado);
			mensagem.setTipo((short) 1);
			
			short novaIntancia = (short) (maniDetalhe.getInstancia() + 1);
			
			Mensagem mensagemSistema = GeralUtil.gerarMensagem("Manifestação "+ maniDetalhe.getProtocolo() +" entrou no "+ novaIntancia +"º recurso no sistema.", usuarioDAO.getUsuario(2), (short) 3);
			
			if (maniDetalhe.getTipo().equals("Informação"))
				maniDetalhe.setDataLimite(PrazosUtil.gerarPrazoDiaUtilLimite(new Date(System.currentTimeMillis()), "Esic","Recurso"));
			else
				maniDetalhe.setDataLimite(PrazosUtil.gerarPrazoDiaUtilLimite(new Date(System.currentTimeMillis()), "Ouv", "Recurso"));
			
			maniDetalhe.setInstancia(novaIntancia); 
			maniDetalhe.setStatus("Recurso");
			maniDetalhe.addMensagem(mensagem);
			maniDetalhe.addHistorico(mensagemSistema);
			
			if(!maniDetalhe.getEntidade().isAtiva()) {
				maniDetalhe.setEntidade(entidadeDAO.getEntidade(166));
				
				Mensagem mensagemSistema2 = GeralUtil.gerarMensagem("Manifestação "+ maniDetalhe.getProtocolo() +" alocada em órgão inativo sendo encaminhada a Ouvidoria Geral.", usuarioDAO.getUsuario(2), (short) 4);
				maniDetalhe.addTramites(mensagemSistema2);
			}
			
			boolean salvou = manifestacaoDAO.saveOrUpdate(maniDetalhe);
			
			if(salvou) {
				EmailUtil.enviarEmailNotificacaoRecurso(maniDetalhe);
				System.out.println("Manifestação " + maniDetalhe.getProtocolo() + " entrou em recurso.");
			}
			
			limparResposta();
		} catch(Exception e) {
			Messages.addGlobalError( "Falha ao prorrogar manifestação");
			e.printStackTrace();
		}
	}
	
	public void encaminharSetorial() {
		try {
			Date dataAtual = GeralUtil.getDataAtual();
			
			Usuario usuarioLogado = (Usuario) GeralUtil.recuperarDaSessao("usuarioLogin");
			
			Entidade entAntiga = maniDetalhe.getEntidade();
			Entidade entReencaminhar = entidadeDAO.getEntidade(166);
			
			Responsavel respRemetente = usuarioLogado.getResponsaveis().iterator().next();
			
			Mensagem justCidadao = new Mensagem();
			
			justCidadao.setTexto(mensagem_1);
			justCidadao.setData(dataAtual);
			justCidadao.setUsuario(usuarioLogado);
			justCidadao.setTipo((short) 7);

			Mensagem justInterna = new Mensagem();
			
			justInterna.setTexto(mensagem_2);
			justInterna.setData(dataAtual);
			justInterna.setUsuario(usuarioLogado);
			justInterna.setTipo((short) 5);
			
			Mensagem mensagemSistema = GeralUtil.gerarMensagem("Manifestação "+ maniDetalhe.getProtocolo() +" foi encaminhada no sistema da entidade "+ entAntiga.getSigla() +" para "+ entReencaminhar.getSigla() +".", usuarioDAO.getUsuario(2), (short) 4);
			mensagemSistema.setManifestacao(maniDetalhe);
			
			maniDetalhe.setEntidade(entReencaminhar);
			maniDetalhe.setEncaminhada(true);
			maniDetalhe.setStatus("Reencaminhada");
			maniDetalhe.setVisualizada(false);
			maniDetalhe.addMensagem(justCidadao);
			maniDetalhe.addTramites(justInterna);
			maniDetalhe.addHistorico(mensagemSistema);
			
			boolean salvou = manifestacaoDAO.saveOrUpdate(maniDetalhe);
			
			if(salvou) {
				EmailUtil.enviarEmailNotificacaoCidadao(maniDetalhe, justCidadao);
				EmailUtil.enviarEmailTramites(maniDetalhe, mensagem_2, respRemetente);
				
				System.out.println("Manifestação " + maniDetalhe.getProtocolo() + " encaminhada de " + entAntiga.getSigla() + " para " + entReencaminhar.getSigla() + " por " + usuarioLogado.getNome() + ".");
			}
			
			limparResposta();
		} catch(Exception e) {
			Messages.addGlobalError( "Falha ao prorrogar manifestação");
			e.printStackTrace();
		}
	}
	
	public void encaminharOuvidoriaGeral() {
		try {
			Date dataAtual = GeralUtil.getDataAtual();
			
			Usuario usuarioLogado = (Usuario) GeralUtil.recuperarDaSessao("usuarioLogin");
			
			Entidade entAntiga = maniDetalhe.getEntidade();
			Entidade entReencaminhar = entidadeDAO.getEntidade(idEntReencaminhar);
			
			Responsavel respRemetente = usuarioLogado.getResponsaveis().iterator().next();
			
			Mensagem justCidadao = new Mensagem();
			
			justCidadao.setTexto(mensagem_1);
			justCidadao.setData(dataAtual);
			justCidadao.setUsuario(usuarioLogado);
			justCidadao.setTipo((short) 7);

			Mensagem justInterna = new Mensagem();
			
			justInterna.setTexto(mensagem_2);
			justInterna.setData(dataAtual);
			justInterna.setUsuario(usuarioLogado);
			justInterna.setTipo((short) 5);
			
			Mensagem mensagemSistema = GeralUtil.gerarMensagem("Manifestação "+ maniDetalhe.getProtocolo() +" foi encaminhada no sistema da entidade "+ entAntiga.getSigla() +" para "+ entReencaminhar.getSigla() +".", usuarioDAO.getUsuario(2), (short) 4);
			
			
			maniDetalhe.setEntidade(entReencaminhar);
			maniDetalhe.setEncaminhada(true);
			maniDetalhe.setStatus("Reencaminhada");
			maniDetalhe.setVisualizada(false);
			maniDetalhe.addMensagem(justCidadao);
			maniDetalhe.addTramites(justInterna);
			maniDetalhe.addHistorico(mensagemSistema);
			
			boolean salvou = manifestacaoDAO.saveOrUpdate(maniDetalhe);
			
			if(salvou) {
				EmailUtil.enviarEmailNotificacaoCidadao(maniDetalhe, justCidadao);
				EmailUtil.enviarEmailTramites(maniDetalhe, mensagem_2, respRemetente);
				
				System.out.println("Manifestação " + maniDetalhe.getProtocolo() + " encaminhada de " + entAntiga.getSigla() + " para " + entReencaminhar.getSigla() + " por " + usuarioLogado.getNome() + "."); 
			}
			
			limparResposta();
		} catch(Exception e) {
			Messages.addGlobalError( "Falha ao prorrogar manifestação");
			e.printStackTrace();
		}
	}
	
	public void avaliar() {
        try {
            Usuario usuarioLogado = (Usuario) GeralUtil.recuperarDaSessao("usuarioLogin");
            
            Mensagem mensagem = GeralUtil.gerarMensagem(mensagem_1, usuarioLogado, (short) 6);
            mensagem.setNota(nota);
            
            Mensagem mensagemSistema = GeralUtil.gerarMensagem("Manifestação "+ maniDetalhe.getProtocolo() +" recebeu avaliação de nota " + nota +" pelo cidadão.", usuarioDAO.getUsuario(2), (short) 4);
            
            List<Mensagem> listMensagensAvaliadas = (List<Mensagem>) maniDetalhe.getMensagens().stream().filter(o-> o.getTipo() == 6).collect(Collectors.toList());
            
            float somaNotas = nota;
            int qntdAvaliacoes = 1;
            
            for(Mensagem msg : listMensagensAvaliadas) {
                somaNotas += msg.getNota();
                qntdAvaliacoes++;
            }
            
            maniDetalhe.setAvaliacao((Float) (somaNotas/qntdAvaliacoes));
            maniDetalhe.addMensagem(mensagem);
            maniDetalhe.addHistorico(mensagemSistema);
            
            boolean salvou = manifestacaoDAO.saveOrUpdate(maniDetalhe);
            
            if(salvou) {
            	EmailUtil.enviarNotificacaoEntidade(maniDetalhe, "Manifestação " + maniDetalhe.getProtocolo() + " foi avaliada pelo manifestante");
            	System.out.println("Manifestação " + maniDetalhe.getProtocolo() + " avaliada por " + usuarioLogado.getNome() + ".");
            }
            
            limparResposta();
        } catch (Exception e) {
            Messages.addGlobalError( "Falha ao avaliar manifestação");
            e.printStackTrace();
        } 
	}
	
	public void reabrirManifestacao() {
		try {
			Usuario usuarioLogado = (Usuario) GeralUtil.recuperarDaSessao("usuarioLogin");
		
			Mensagem msgTramite = new Mensagem();
		
			msgTramite.setTexto("Manifestação reaberta por " + usuarioLogado.getNome() + ".");
			msgTramite.setTipo((short) 4);
			msgTramite.setData(GeralUtil.getDataAtual());
			msgTramite.setUsuario(usuarioDAO.getUsuario(2));
			
			maniDetalhe.addHistorico(msgTramite);
			maniDetalhe.setStatus("Tramitando");
			
			if (maniDetalhe.getTipo().equals("Informação"))
				maniDetalhe.setDataLimite(PrazosUtil.gerarPrazoDiaUtilLimite(new Date(System.currentTimeMillis()), "Esic","Reabrir"));
			else
				maniDetalhe.setDataLimite(PrazosUtil.gerarPrazoDiaUtilLimite(new Date(System.currentTimeMillis()), "Ouv", "Reabrir"));
			
			maniDetalhe.setDatafim(null);
			
			if(!maniDetalhe.getEntidade().isAtiva()) {
				maniDetalhe.setEntidade(entidadeDAO.getEntidade(166));
				
				Mensagem mensagemSistema2 = GeralUtil.gerarMensagem("Manifestação "+ maniDetalhe.getProtocolo() +" alocada em órgão inativo sendo encaminhada a Ouvidoria Geral.", usuarioDAO.getUsuario(2), (short) 4);
				maniDetalhe.addTramites(mensagemSistema2);
			}
			
			boolean salvou = manifestacaoDAO.saveOrUpdate(maniDetalhe);
			
			if (salvou) {
				System.out.println("Manifestação " + maniDetalhe.getProtocolo() + " reaberta por " + usuarioLogado.getNome() + ".");
			}
			
		} catch (Exception e) {
			Messages.addGlobalError("Falha ao reabrir manifestação");
			e.printStackTrace();
		}
	}
	
	
	public List<Anexo> salvarAnexos(String diretorioAnexos) {
		List<Anexo> anexos = new ArrayList<Anexo>();
		
		FTPClient client = new FTPClient();
		FTPClientConfig conf = new FTPClientConfig(FTPClientConfig.SYST_UNIX);
		
		try {
			client.setControlEncoding("ISO-8859-1");
			
			client.connect("187.17.2.187", 21);
			client.configure(conf);
			
			client.enterLocalPassiveMode();
			client.login("ftp_setc", "eB5QxROL");
			
			client.setBufferSize(15 * 1024 * 1024);
			client.setAutodetectUTF8(true);
			client.setFileType(FTP.BINARY_FILE_TYPE);
			
			client.changeWorkingDirectory("/SE-Ouv/" + GeralUtil.caminhoAnexos);
			
			if (client.isConnected()) {
				client.makeDirectory(diretorioAnexos);
				client.changeWorkingDirectory(diretorioAnexos);
				
				boolean salvou = true;
				int contadorAnexo = 1;
				// Salva arquivos no servidor de arquivos
				for(AnexoUpload arqv : anexosList) {
					InputStream inputStream = arqv.getStream();
					salvou = salvou & client.storeFile("Anexo-" + contadorAnexo + "." + StringUtils.substringAfterLast(arqv.getNome(), "."), inputStream);
					inputStream.close();
					
					if (salvou) {
						contadorAnexo++;
						System.out.println(":: Arquivo \"" + arqv.getNome() + "\" salvo no servidor");
					}
					else 
						System.out.println(":: Falha ao salvar arquivo \"" + arqv.getNome() + "\" no servidor");
						
				}
				
				System.out.println("Salvou todos os arquivos? " + salvou);
				
				// Salva registro dos arquivos no banco de dados
				
				if (salvou) {
					
					FTPFile[] ftpFiles = client.listFiles();
					for (FTPFile ftpFile : ftpFiles) {
						// Check if FTPFile is a regular file
						if (ftpFile.getType() == FTPFile.FILE_TYPE) {
							Anexo novoAnexo = new Anexo();
							
							novoAnexo.setNome(ftpFile.getName());
							novoAnexo.setTamanho(ftpFile.getSize());
							novoAnexo.setTipo(StringUtils.substringAfterLast(ftpFile.getName(), "."));
							novoAnexo.setLocal("http://arquivos.setc.se.gov.br/SE-Ouv/" + GeralUtil.caminhoAnexos + "/" + diretorioAnexos + "/" + ftpFile.getName());
							
							anexos.add(novoAnexo);
						}
					}
				 
				} else {
					limparAnexos(diretorioAnexos);
					throw new IOException();
				}
				 
			}
		} catch (Throwable e) {
			System.out.println("Erro salvarAnexos");
			e.printStackTrace();
		}
		
		return anexos;
	}
	
	public void fileUploadListener(FileUploadEvent e) {
//		System.out.println("fileUploadListener");
		
//		List<Anexo> fileList = this.anexoList;
		
		UploadedFile arquivo = e.getFile();
		
		try {
			AnexoUpload novoAnexo = new AnexoUpload(limiteNomeArquivo(arquivo.getFileName()), arquivo.getContentType(), arquivo.getSize(), arquivo.getInputstream());
			
			if(!anexosList.contains(novoAnexo)) {
				if (arquivo.getSize() < 15728640) {
					anexosList.add(novoAnexo);
					System.out.println(":: Arquivo adicionado: "+ novoAnexo.getNome() +" | Tamanho : "+ novoAnexo.getTamanho());
				} else {
					System.out.println(":: Anexo excede limite de 15 MB");
					Messages.addGlobalWarn( "Anexo excede limite de 15 MB");
					return;
				}
			} else {
				System.out.println(":: Arquivo já adicionado a lista");
				Messages.addGlobalWarn( "Arquivo já adicionado a lista");
			}
		} catch (Exception e1) {
			System.out.println(":: Falha ao salvar anexo");
			e1.printStackTrace();
		} 
	}
	
	public void removerAnexo(AnexoUpload rmvFile) {
		System.out.println(": Removendo arquivo da lista: " + rmvFile.getNome());
		try {
			anexosList.remove(rmvFile);
		} catch (Exception e) {
			System.out.println(":: Falha ao remover arquivo");
			e.printStackTrace();
		} 
		
	}
	
	public void limparAnexos(String diretorioAnexos) {
		FTPClient client = new FTPClient();
		FTPClientConfig conf = new FTPClientConfig(FTPClientConfig.SYST_UNIX);
		
		try {
			client.setControlEncoding("ISO-8859-1");
			
			client.connect("187.17.2.187", 21);
			client.configure(conf);
			
			client.enterLocalActiveMode();
			client.login("ftp_setc", "eB5QxROL");
			
			client.setBufferSize(1024 * 1024);
			client.setAutodetectUTF8(true);
			client.setFileType(FTP.BINARY_FILE_TYPE);
			
			client.changeWorkingDirectory("/SE-Ouv/" + GeralUtil.caminhoAnexos + "/" + diretorioAnexos);
			
			 if (client.isConnected()) {
				 FTPFile[] subFiles = client.listFiles();
				 
				 for (FTPFile aFile : subFiles) {
				 	if(aFile.isFile())
				 		client.deleteFile(aFile.getName());
	             }
				 
				 client.changeToParentDirectory();
				 client.removeDirectory(diretorioAnexos);
			 }
				 
			 anexosList.clear();
			 System.out.println(":: Lista de anexos esvaziada");
		} catch (Exception e) {
			System.out.println(":: Falha ao limpar lista de anexos");
			e.printStackTrace();
		} finally {
			try {
				client.logout();
				client.disconnect();
			} catch (IOException e1) {
				// do nothing
			}
		}
	}
	
	private String limiteNomeArquivo(String nome) {
		String nomeFinal = "";
		
		if(nome.length() > 150) {
			String extensao = StringUtils.substringAfterLast(nome, ".");
			System.out.println(extensao);
			
			nomeFinal = nome.substring(0, 145).concat(".").concat(extensao);
		} else {
			nomeFinal = nome;
		}
		
//		nomeFinal.replaceAll("\\(\\)\\#\\$\\+\\%\\<\\>\\!\\`\\&\\*\\'\\|\\{\\}\\?\\\"\\=\\/\\:\\@", "-");
		
		System.out.println(" : Nome arquivo = " + nomeFinal);
		return nomeFinal;
	}
	
	public String anexoSizeMB(String size) {
		Double tamanho = Double.parseDouble(size);
		
		if (tamanho <= 100000) {
			return String.format("%.2f", (tamanho / 1024)).concat(" kB");
		} else {
			return String.format("%.2f", (tamanho / (1024 * 1024))).concat(" MB");
		}
		
	}
	
	public String gerarNomeDiretorio() {

	    String generatedString = RandomStringUtils.random(10, true, true);
	    
	    System.out.println("String gerada: " + generatedString);
	    
	    return generatedString;
	}
	
	private void testConexaoFTP() {
		try 
		{
			FTPClient client = new FTPClient();
			FTPClientConfig conf = new FTPClientConfig(FTPClientConfig.SYST_UNIX);
			
			client.connect("187.17.2.187", 21);
			client.configure(conf);
			
			client.enterLocalActiveMode();
			client.login("ftp_setc", "eB5QxROL");
			
			boolean answer = client.sendNoOp();
			if(answer){
				liberarAnexo = true;
			}
			else {
				System.out.println("Server connection failed!");
				liberarAnexo = false;
			}
		}
		catch (Exception e) 
		{
			System.out.println("Server connection is closed!");
			liberarAnexo = false;
		}
	}
	
	
	// Checa botões habilitados
	public boolean disResposta() {
		try {
			if(usuarioLogado.getPerfil() == 2 && responsavel.getEntidade().getIdEntidade() == 116 && responsavel.getEntidade().getIdEntidade() != maniDetalhe.getEntidade().getIdEntidade()) {
				return true;
			} else {
				if (maniDetalhe.getStatus().equals("Aberta") || maniDetalhe.getStatus().equals("Tramitando") || maniDetalhe.getStatus().equals("Recurso") || maniDetalhe.getStatus().equals("Prorrogada") || maniDetalhe.getStatus().equals("Reencaminhada")) {
					return false;
				} else
					return true;
			}
		} catch (Exception e) {
			return true;
		}
	}
	
	public boolean disReformula() {
		try {
			if(usuarioLogado.getPerfil() == 2 && responsavel.getEntidade().getIdEntidade() == 116 && responsavel.getEntidade().getIdEntidade() != maniDetalhe.getEntidade().getIdEntidade()) {
				return true;
			} else {
				if ((this.maniDetalhe.getStatus().equals("Aberta") || maniDetalhe.getStatus().equals("Tramitando") || this.maniDetalhe.getStatus().equals("Recurso") || this.maniDetalhe.getStatus().equals("Prorrogada")) && maniDetalhe.getSigilo() != 2)
					return false;
				else
					return true;
			}
		} catch (Exception e) {
			return true;
		}
	}
	
	public boolean disMudar() {
		try {
			if(usuarioLogado.getPerfil() == 2 && responsavel.getEntidade().getIdEntidade() == 116 && responsavel.getEntidade().getIdEntidade() != maniDetalhe.getEntidade().getIdEntidade()) {
				return true;
			} else {
				if (maniDetalhe.getStatus().equals("Finalizada") || maniDetalhe.getStatus().equals("Sem Resposta"))
					return true;
				else
					return false;
			}
		} catch (Exception e) {
			return true;
		}
	}
	
	public boolean disNegar() {
		try {
			if(usuarioLogado.getPerfil() == 2 && responsavel.getEntidade().getIdEntidade() == 116 && responsavel.getEntidade().getIdEntidade() != maniDetalhe.getEntidade().getIdEntidade()) {
				return true;
			} else {
				if (maniDetalhe.getStatus().equals("Aberta") || maniDetalhe.getStatus().equals("Tramitando") || maniDetalhe.getStatus().equals("Recurso") || maniDetalhe.getStatus().equals("Prorrogada"))
					return false;
				else
					return true;
			}
		} catch (Exception e) {
			return true;
		}
	}
	
	public boolean disProrrogar() {
		// ATIVAR- DATA PRAZO
		try {
			if(usuarioLogado.getPerfil() == 2 && responsavel.getEntidade().getIdEntidade() == 116 && responsavel.getEntidade().getIdEntidade() != maniDetalhe.getEntidade().getIdEntidade()) {
				return true;
			} else {
				if (maniDetalhe.getDataLimite() != null) {
					
					Calendar hoje = Calendar.getInstance();
	
					Calendar limiteMin = Calendar.getInstance();
					limiteMin.setTime(maniDetalhe.getDataLimite());
					limiteMin.add(Calendar.DATE, -5);
	
					Calendar limite = Calendar.getInstance();
					limite.setTime(maniDetalhe.getDataLimite());
					limite.add(Calendar.DAY_OF_MONTH, 1);
	
					if (!foiProrrogada(maniDetalhe) && hoje.after(limiteMin) && hoje.before(limite)) {
						return false;
					} else {
						return true;
					}
				} else {
					return true;
				}
			}
		} catch (Exception e) {
			return true;
		}
	}
	
	private boolean foiProrrogada(Manifestacao manifestacao) {
		return manifestacao.getHistorico().stream().anyMatch(o-> o.getTipo() == 8);
	}
	
	public boolean disRecurso() {
		try {
			if (recursavel(maniDetalhe) && prazoRecurso(maniDetalhe))
				return false;
			else
				return true;
		} catch (Exception e) {
			return true;
		}
	}
	
	private boolean recursavel(Manifestacao manifestacao) {
		int count = 0;
		
		count = (int) manifestacao.getHistorico().stream().filter(o-> o.getTipo() == 3).count();
		
		if (count >= 2)
			return false; 
		else
			return true;
	}
	
	private boolean prazoRecurso(Manifestacao manifestacao) {
		try {
			if(manifestacao.getStatus().equals("Sem Resposta")) {
				Date limite = manifestacao.getDatafim();
				
				Calendar c = Calendar.getInstance(); 
				
				Date hoje = c.getTime();
				
				c.setTime(limite); 
				c.add(Calendar.DATE, 5);
				limite = c.getTime();
				
				if(hoje.before(limite)) {
					return true;
				} else {
					return false;
				}
			} else if(manifestacao.getStatus().equals("Atendida") || manifestacao.getStatus().equals("Negada") || manifestacao.getStatus().equals("Finalizada")) {
				
				List<Mensagem> mensagens = (List<Mensagem>) manifestacao.getMensagens().stream().filter(o-> o.getTipo() == 2 || o.getTipo() == 7).collect(Collectors.toList());
				
				if (!mensagens.isEmpty()) {
					
					Date limite = mensagens.get(mensagens.size()-1).getData();
					
					Calendar c = Calendar.getInstance(); 
					
					Date hoje = c.getTime();
					
					c.setTime(limite); 
					c.add(Calendar.DATE, 5);
					limite = c.getTime();
					
					if(hoje.before(limite)) {
						return true;
					} else {
						return false;
					}
				} else {
					return false;
				}
				
			} else {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean disEncaminhar() {
		try {
			if(usuarioLogado.getPerfil() == 2 && responsavel.getEntidade().getIdEntidade() == 116 && responsavel.getEntidade().getIdEntidade() != maniDetalhe.getEntidade().getIdEntidade()) {
				return true;
			} else {
				if (encaminhavel(maniDetalhe) && (maniDetalhe.getStatus().equals("Aberta") || maniDetalhe.getStatus().equals("Tramitando")))
					return false;
				else
					return true;
			}
		} catch (Exception e) {
			return true;
		}
	}
	
	private boolean encaminhavel(Manifestacao manifestacao) {
		Calendar hoje = Calendar.getInstance();
		Calendar limite = Calendar.getInstance();
		
		limite.setTime(PrazosUtil.gerarPrazoDiaUtilLimite(manifestacao.getDataIni(), manifestacao.getTipo(), "Encaminhar"));
		
		if (hoje.before(limite)) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean disEncaminharOGE() {
		try {
			if (maniDetalhe.getStatus().equals("Finalizada") || maniDetalhe.getStatus().equals("Sem Resposta") || maniDetalhe.getStatus().equals("Atendida"))
				return true;
			else
				return false;
		} catch (Exception e) {
			return true;
		}
	}
	
	public boolean disReabrir() {
		try {
			if (maniDetalhe.getStatus().equals("Atendida") || maniDetalhe.getStatus().equals("Reformulação") || maniDetalhe.getStatus().equals("Finalizada") || maniDetalhe.getStatus().equals("Sem Resposta"))
				return false;
			else 
				return true;
		} catch (Exception e) {
			return true;
		}
	}
	
	public boolean disAvaliar() {
		try {
			if (maniDetalhe.getStatus().equals("Aberta") || maniDetalhe.getStatus().equals("Tramitando") || maniDetalhe.getStatus().equals("Reformulação") || maniDetalhe.getStatus().equals("Reencaminhada") || !avaliavel(maniDetalhe)) {
				return true;
			} else
				return false;
		} catch (Exception e) {
			return true;
		}
	}
	
	private boolean avaliavel(Manifestacao manifestacao) {
		
		int countRespostas = 0, countAvaliacoes = 0;
		
		countRespostas = (int) manifestacao.getMensagens().stream().filter(o-> o.getTipo() == 2).count();
		
		countAvaliacoes = (int) manifestacao.getMensagens().stream().filter(o-> o.getTipo() == 6).count();
		
//		System.out.println("Respostas / Avaliações: " + countRespostas + "/" + countAvaliacoes);
		
		
		if (countRespostas > countAvaliacoes)
			return true;
		else
			return false;
	}
	
	public StreamedContent download(Anexo anexo) {
		try {
			return AnexoUtil.download(anexo);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	// =================
	// GETTERS E SETTERS
	// =================
	
	public List<Manifestacao> getListManifestacoes() {
		return listManifestacoes;
	}

	public void setListManifestacoes(List<Manifestacao> listManifestacoes) {
		this.listManifestacoes = listManifestacoes;
	}

	
	public LazyDataModel<Manifestacao> getDataModel() {
		return dataModel;
	}

	public void setDataModel(LazyDataModel<Manifestacao> dataModel) {
		this.dataModel = dataModel;
	}

	public Manifestacao getManiDetalhe() {
		return maniDetalhe;
	}

	public void setManiDetalhe(Manifestacao maniDetalhe) {
		this.maniDetalhe = maniDetalhe;
	}

	public String getMensagem_1() {
		return mensagem_1;
	}

	public void setMensagem_1(String mensagem_1) {
		this.mensagem_1 = mensagem_1;
	}

	public String getMensagem_2() {
		return mensagem_2;
	}

	public void setMensagem_2(String mensagem_2) {
		this.mensagem_2 = mensagem_2;
	}

	public String getTipoSelecionado() {
		return tipoSelecionado;
	}

	public void setTipoSelecionado(String tipoSelecionado) {
		this.tipoSelecionado = tipoSelecionado;
	}

	public List<Entidade> getEntidadesAtivas() {
		return entidadesAtivas;
	}

	public void setEntidadesAtivas(List<Entidade> entidadesAtivas) {
		this.entidadesAtivas = entidadesAtivas;
	}

	public int getIdEntReencaminhar() {
		return idEntReencaminhar;
	}

	public void setIdEntReencaminhar(int idEntReencaminhar) {
		this.idEntReencaminhar = idEntReencaminhar;
	}

	public int getNota() {
		return nota;
	}

	public void setNota(int nota) {
		this.nota = nota;
	}

	public List<Entidade> getListEntidades() {
		return listEntidades;
	}

	public void setListEntidades(List<Entidade> todasEntidades) {
		this.listEntidades = todasEntidades;
	}

	public List<AnexoUpload> getAnexosList() {
		return anexosList;
	}

	public void setAnexosList(List<AnexoUpload> anexosList) {
		this.anexosList = anexosList;
	}

	public boolean isLiberarAnexo() {
		return liberarAnexo;
	}

	public void setLiberarAnexo(boolean liberarAnexo) {
		this.liberarAnexo = liberarAnexo;
	}
	
	public short getFiltro() {
		return filtro;
	}

	public void setFiltro(short filtro) {
		this.filtro = filtro;
	}

	public DashboardInfo2 getDashboard() {
		return dashboard;
	}

	public void setDashboard(DashboardInfo2 dashboard) {
		this.dashboard = dashboard;
	}

	public String getPerfilLogado() {
		return perfilLogado;
	}

	public void setPerfilLogado(String perfilLogado) {
		this.perfilLogado = perfilLogado;
	}
	
}
