package br.gov.se.setc.bean;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.apache.commons.net.ftp.FTPFile;
import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Faces;
import org.omnifaces.util.FacesLocal;
import org.omnifaces.util.Messages;
import org.primefaces.PrimeFaces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FlowEvent;
import org.primefaces.model.UploadedFile;

import br.gov.se.setc.DAO.CidadaoDAO;
import br.gov.se.setc.DAO.EntidadeDAO;
import br.gov.se.setc.DAO.ManifestacaoDAO;
import br.gov.se.setc.DAO.TemaDAO;
import br.gov.se.setc.DAO.UsuarioDAO;
import br.gov.se.setc.model.Anexo;
import br.gov.se.setc.model.AnexoUpload;
import br.gov.se.setc.model.Cidadao;
import br.gov.se.setc.model.Entidade;
import br.gov.se.setc.model.Manifestacao;
import br.gov.se.setc.model.Mensagem;
import br.gov.se.setc.model.Tema;
import br.gov.se.setc.model.Usuario;
import br.gov.se.setc.util.EmailUtil;
import br.gov.se.setc.util.GeralUtil;
import br.gov.se.setc.util.PrazosUtil;

@Named
@ViewScoped
public class CadManifestacaoBean implements Serializable {
	
	private static final long serialVersionUID = 6996964647580149765L;
	
	private ManifestacaoDAO manifestacaoDAO = ManifestacaoDAO.getInstance();
	private CidadaoDAO cidadaoDAO = CidadaoDAO.getInstance();
	private UsuarioDAO usuarioDAO = UsuarioDAO.getInstance();
	
	private TemaDAO temaDAO = TemaDAO.getInstance();
	private EntidadeDAO entidadeDAO = EntidadeDAO.getInstance();
	
	private Manifestacao manifestacaoCadastro;
	private Mensagem mensagemCadastro;
	
	private Integer idTema;
	private Integer idEntidade;
	
	private boolean selecionaOrgao;
	
	private List<Tema> listTemas;
	private List<Tema> listTemasFiltrados;
	
	private List<Entidade> listEntidades;
	private List<Entidade> listEntidadesFiltradas;
	
	private UploadedFile file;

	private List<AnexoUpload> anexosList;
	
	private boolean renderProtocoloAgrese;
	private boolean renderConteudoManifestacao;
	
	private boolean liberarAnexo;
	
	@PostConstruct
	public void init() {
		System.out.println(":: init cadManifestacaoBean ::");
		manifestacaoCadastro = new Manifestacao();
		mensagemCadastro =  new Mensagem();
		renderConteudoManifestacao = true;
		
		anexosList = new ArrayList<AnexoUpload>();
		
		// Popula Listas
		try {
			listTemas = temaDAO.listTemasEntidadesAtivas();
			listEntidades = entidadeDAO.listEntidadesAtivas();
			listEntidadesFiltradas = new ArrayList<Entidade>();
		} catch (Exception e) {
			System.out.println(":: Erro ao popular listas");
			e.printStackTrace();
		}
		
		// Recupera parâmetros da URL para redirecionar aba inicial
		try {
			HttpServletRequest request = (HttpServletRequest) FacesLocal.getRequest(Faces.getContext());
			
			String tipo = request.getParameter("tipo");
			
			if(tipo != null && !tipo.isEmpty()) {
				int tipoInt = Integer.valueOf(tipo);
				
				switch (tipoInt) {
				case 1:
					manifestacaoCadastro.setTipo("Informação");
					break;
				case 2:
					manifestacaoCadastro.setTipo("Solicitação");
					break;
				case 3:
					manifestacaoCadastro.setTipo("Sugestão");
					break;
				case 4:
					manifestacaoCadastro.setTipo("Reclamação");
					break;
				case 5:
					manifestacaoCadastro.setTipo("Elogio");
					break;

				default:
					manifestacaoCadastro.setTipo("Denúncia");
					break;
				}
			}
			
			String sigilo = request.getParameter("sigilo");
			
			if(sigilo != null && !sigilo.isEmpty()) {
				manifestacaoCadastro.setSigilo(Short.valueOf(sigilo));
			}
			
			
			String tema = request.getParameter("tema");
			
			if(tema != null && !tema.isEmpty()) {
				manifestacaoCadastro.setTema(new Tema(Integer.valueOf(tema)));
				idTema = Integer.valueOf(tema);
				filtrarEntidades();
			}
			
			
			String primeiraPagina = request.getParameter("tab");
			
			if(primeiraPagina != null && !primeiraPagina.isEmpty()) {
				System.out.println("Executar redirecionamento");
				PrimeFaces.current().executeScript("PF('wizardW').loadStep('" + primeiraPagina + "', true);");
			}
			
		} catch (Exception e) {
			System.out.println(":: Falha ao recuperar parâmetros");
			e.printStackTrace();
		}
		
		try {
			String perfilLogado = (String) GeralUtil.recuperarDaSessao("perfilLogado");
			
			if(perfilLogado == null || perfilLogado.isEmpty() || perfilLogado.equals("logoff")) {
				manifestacaoCadastro.setSigilo((short) 2);
				System.out.println(":: inicializando Nível de Sigilo Anonimo");
			} else {
				manifestacaoCadastro.setSigilo((short) 0);
				System.out.println(":: inicializando Nível de Sigilo Identificando");
			}
			
		} catch (Exception e) {
			manifestacaoCadastro.setSigilo((short) 2);
			System.out.println(":: Falha ao recuperar usuário logado");
			e.printStackTrace();
		}
		
		testConexaoFTP();
		
	}
	
//	@PreDestroy
//	public void release() {
//		System.out.println(":: release cadManifestacaoBean ::");
//	}
	
	public String salvar() {
		String diretorioAnexos = gerarNomeDiretorio();
		
		try {
			Date dataAtual = GeralUtil.getDataAtual();
			
			// Seta manifestação
			if (manifestacaoCadastro.getTipo() == null || manifestacaoCadastro.getTipo().isEmpty())
				manifestacaoCadastro.setTipo("Solicitação");
			
			String protocolo = gerarProtocolo(manifestacaoCadastro);
			
			manifestacaoCadastro.setProtocolo(protocolo);
			
			manifestacaoCadastro.setInstancia((short) 0);
			manifestacaoCadastro.setCanalEntrada((short) 0);
			manifestacaoCadastro.setLiberaDenuncia(true);
			manifestacaoCadastro.setStatus("Aberta");
			manifestacaoCadastro.setDataIni(dataAtual);
			
			// Gera data limite
			if (manifestacaoCadastro.getTipo().equals("Informação"))
				manifestacaoCadastro.setDataLimite(PrazosUtil.gerarPrazoDiaUtilLimite(dataAtual, "Esic","Aberta"));
			else
				manifestacaoCadastro.setDataLimite(PrazosUtil.gerarPrazoDiaUtilLimite(dataAtual, "Ouv", "Aberta"));
			
			
			// Seta cidadão por nível de sigilo
			Usuario usuarioLogado = (Usuario) GeralUtil.recuperarDaSessao("usuarioLogin");
			
			if(manifestacaoCadastro.getSigilo() == 0 || manifestacaoCadastro.getSigilo() == 1) {
				if (usuarioLogado.getPerfil() == 3 && !usuarioLogado.getCidadaos().isEmpty()) {
					manifestacaoCadastro.setCidadao(usuarioLogado.getCidadaos().iterator().next());
					mensagemCadastro.setUsuario(usuarioLogado);
				} else {
					Messages.addGlobalError("Falha ao recuperar usuário");
				}
			} else if(manifestacaoCadastro.getSigilo() == 2) {
				Cidadao cidadaoAnonimo = cidadaoDAO.getCidadao(1);
				manifestacaoCadastro.setCidadao(cidadaoAnonimo);
				mensagemCadastro.setUsuario(cidadaoAnonimo.getUsuario());
			}
			//
			
			// Seta mensagem do usuário
			mensagemCadastro.setData(GeralUtil.getDataAtual());
			mensagemCadastro.setTipo((short) 1);
			
			if(anexosList.size() > 0) {
				List<Anexo> anexos = salvarAnexos(diretorioAnexos);
				
				mensagemCadastro.setAnexos(anexos);
			}
			
			manifestacaoCadastro.addMensagem(mensagemCadastro);
			
			Mensagem mensagemSistema = new Mensagem();
			
			if(manifestacaoCadastro.getProtocolo() == null) {
				System.out.println("Protocolo == null");
			}
			if(manifestacaoCadastro.getEntidade().getNome() == null) {
				System.out.println("Entidade.nome == null");
			}
			mensagemSistema.setTexto("Manifestação "+ manifestacaoCadastro.getProtocolo() +" foi recebida no sistema da entidade "+ manifestacaoCadastro.getEntidade().getNome() +".");
			mensagemSistema.setData(dataAtual);
			mensagemSistema.setTipo((short) 4);
			mensagemSistema.setUsuario(usuarioDAO.getUsuario(2));
			
			manifestacaoCadastro.addHistorico(mensagemSistema);
			
			if(manifestacaoCadastro.getIdManifestacao() == null) {
				boolean salvou = manifestacaoDAO.saveOrUpdate(manifestacaoCadastro);
				
				if(salvou) { 
					System.out.println("Salvou manifestação");
					Messages.addGlobalInfo("Manifestação salva");
					
					EmailUtil.enviarEmailNovaManifestacaoCidadao(manifestacaoCadastro);
					EmailUtil.enviarEmailNovaManifestacaoResp(manifestacaoCadastro);
					
					String url = "/cadastro/confirmaManifestacao.xhtml?faces-redirect=true&manifestacao=" + manifestacaoCadastro.getProtocolo();
//				System.out.println(url);
					return url;
				}
				else {
					System.out.println("Não salvou manifestação");
					Messages.addGlobalWarn("Erro", "Falha ao salvar manifestação");
					return null;
				}
			} else {
				System.out.println("Manifestação já salva");
//				Messages.addGlobalWarn("Erro", "Falha ao salvar manifestação");
				return null;
			}
		} catch (Exception e) {
			System.out.println("Erro ao salvar manifestação");
			Messages.addGlobalWarn("Erro", "Falha ao salvar manifestação");
			limparAnexos(diretorioAnexos);
			e.printStackTrace();
            return null;
		}
		
	}
	
	public void setarEntidadeTema() {
		System.out.println("setarEntidadeTema");
		manifestacaoCadastro.setEntidade(entidadeDAO.getEntidade(idEntidade));
		manifestacaoCadastro.setTema(temaDAO.getTema(idTema));
	}
	
	public String gerarProtocolo(Manifestacao manifestacao) throws NullPointerException {
		String protocolo = String.valueOf(manifestacaoDAO.getLastId() + 1);
		
		for (int i = 0; i<(5-protocolo.length()); i++ ) {
			protocolo = "0"+protocolo;
		}
		
		Date now = new Date(System.currentTimeMillis());
		SimpleDateFormat ft = new SimpleDateFormat("yy");
		protocolo += "/"+ft.format(now);
		
		switch (manifestacao.getTipo()) {
			case "Reclamação":
				protocolo += "-1";
				break;
			case "Denúncia":
				protocolo += "-2";
				break;
			case "Informação":
				protocolo += "-3";
				break;
			case "Solicitação":
				protocolo += "-4";
				break;
			case "Sugestão":
				protocolo += "-5";
				break;
			case "Elogio":
				protocolo += "-6";
				break;
			}
		
		return protocolo;
	}
	
	public void filtrarEntidades() {
		listEntidadesFiltradas = entidadeDAO.listEntidadesPorTema(idTema);
	}
	
	public void filtrarTemas() {
		listTemasFiltrados = temaDAO.listTemasPorEntidade(idEntidade);
	}
	
	
	public void printTipoManifestacao() {
		System.out.println(manifestacaoCadastro);
	}
	
	public List<Anexo> salvarAnexos(String diretorioAnexos) {
		List<Anexo> anexos = new ArrayList<Anexo>();
		
		FTPClient client = new FTPClient();
		FTPClientConfig conf = new FTPClientConfig(FTPClientConfig.SYST_UNIX);
		
		try {
			client.setControlEncoding("ISO-8859-1");
			
			client.connect("187.17.2.187", 21);
			client.configure(conf);
			
			client.enterLocalPassiveMode();
			client.login("ftp_setc", "eB5QxROL");
			
			client.setBufferSize(15 * 1024 * 1024);
			client.setAutodetectUTF8(true);
			client.setFileType(FTP.BINARY_FILE_TYPE);
			
			client.changeWorkingDirectory("/SE-Ouv/" + GeralUtil.caminhoAnexos);
			
			if (client.isConnected()) {
				client.makeDirectory(diretorioAnexos);
				client.changeWorkingDirectory(diretorioAnexos);
				
				boolean salvou = true;
				int contadorAnexo = 1;
				
				// Salva arquivos no servidor de arquivos
				for(AnexoUpload arqv : anexosList) {
					InputStream inputStream = arqv.getStream();
					salvou = salvou & client.storeFile("Anexo-" + contadorAnexo + "." + StringUtils.substringAfterLast(arqv.getNome(), "."), inputStream);
					inputStream.close();
					
					if (salvou) {
						contadorAnexo++;
						System.out.println(":: Arquivo \"" + arqv.getNome() + "\" salvo no servidor");
					}
					else 
						System.out.println(":: Falha ao salvar arquivo \"" + arqv.getNome() + "\" no servidor");
				}
				
				System.out.println("Salvou todos os arquivos? " + salvou);
				
				// Salva registro dos arquivos no banco de dados
				
				if (salvou) {
					
					FTPFile[] ftpFiles = client.listFiles();
					for (FTPFile ftpFile : ftpFiles) {
						// Check if FTPFile is a regular file
						if (ftpFile.getType() == FTPFile.FILE_TYPE) {
							Anexo novoAnexo = new Anexo();
							
							novoAnexo.setNome(ftpFile.getName());
							novoAnexo.setTamanho(ftpFile.getSize());
							novoAnexo.setTipo(StringUtils.substringAfterLast(ftpFile.getName(), "."));
							novoAnexo.setLocal("http://arquivos.setc.se.gov.br/SE-Ouv/" + GeralUtil.caminhoAnexos + "/" + diretorioAnexos + "/" + ftpFile.getName());
							
							anexos.add(novoAnexo);
						}
					}
				 
				} else {
					limparAnexos(diretorioAnexos);
					throw new IOException();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				client.logout();
				client.disconnect();
			} catch (IOException e1) {
				// do nothing
			}
		}
		
		return anexos;
	}
	
	public void fileUploadListener(FileUploadEvent e) {
//		System.out.println("fileUploadListener");
		
//		List<Anexo> fileList = this.anexoList;
		
		UploadedFile arquivo = e.getFile();
		
		try {
			AnexoUpload novoAnexo = new AnexoUpload(limiteNomeArquivo(arquivo.getFileName()), arquivo.getContentType(), arquivo.getSize(), arquivo.getInputstream());
			
			if(!anexosList.contains(novoAnexo)) {
				if (arquivo.getSize() < 15728640) {
					anexosList.add(novoAnexo);
					System.out.println(":: Arquivo adicionado: "+ novoAnexo.getNome() +" | Tamanho : "+ novoAnexo.getTamanho());
				} else {
					System.out.println(":: Anexo excede limite de 15 MB");
					Messages.addGlobalWarn( "Anexo excede limite de 15 MB");
					return;
				}
			} else {
				System.out.println(":: Arquivo já adicionado a lista");
				Messages.addGlobalWarn( "Arquivo já adicionado a lista");
			}
		} catch (Exception e1) {
			System.out.println(":: Falha ao salvar anexo");
			e1.printStackTrace();
		} 
	}
	
	public void removerAnexo(AnexoUpload rmvFile) {
		System.out.println(": Removendo arquivo da lista: " + rmvFile.getNome());
		try {
			anexosList.remove(rmvFile);
		} catch (Exception e) {
			System.out.println(":: Falha ao remover arquivo");
			e.printStackTrace();
		} 
		
	}
	
	public void limparAnexos(String diretorioAnexos) {
		System.out.println("::  limparAnexos");
		
		FTPClient client = new FTPClient();
		FTPClientConfig conf = new FTPClientConfig(FTPClientConfig.SYST_UNIX);
		
		try {
			client.setControlEncoding("ISO-8859-1");
			
			client.connect("187.17.2.187", 21);
			client.configure(conf);
			
			client.enterLocalActiveMode();
			client.login("ftp_setc", "eB5QxROL");
			
			client.setBufferSize(1024 * 1024);
			client.setAutodetectUTF8(true);
			client.setFileType(FTP.BINARY_FILE_TYPE);
			
			client.changeWorkingDirectory("/SE-Ouv/" + GeralUtil.caminhoAnexos + diretorioAnexos);
			
			 if (client.isConnected()) {
				 FTPFile[] subFiles = client.listFiles();
				 
				 for (FTPFile aFile : subFiles) {
				 	if(aFile.isFile())
				 		client.deleteFile(aFile.getName());
	             }
		         
				 client.changeToParentDirectory();
				 client.removeDirectory(diretorioAnexos);
			 }
				 
			 anexosList.clear();
			 System.out.println(":: Lista de anexos esvaziada");
		} catch (Exception e) {
			System.out.println(":: Falha ao limpar lista de anexos");
			e.printStackTrace();
		} finally {
			try {
				client.logout();
				client.disconnect();
			} catch (IOException e1) {
				// do nothing
			}
		}
	}
	
	private String limiteNomeArquivo(String nome) {
		String nomeFinal = "";
		
		if(nome.length() > 150) {
			String extensao = StringUtils.substringAfterLast(nome, ".");
			System.out.println(extensao);
			
			nomeFinal = nome.substring(0, 145).concat(".").concat(extensao);
		} else {
			nomeFinal = nome;
		}
		
		nomeFinal.replaceAll("\\(\\)\\#\\$\\+\\%\\<\\>\\!\\`\\&\\*\\'\\|\\{\\}\\?\\\"\\=\\/\\:\\@", "-");
		
		System.out.println(" : Nome arquivo = " + nomeFinal);
		return nomeFinal;
	}
	
	public String anexoSizeMB(String size) {
		Double tamanho = Double.parseDouble(size);
		
		if (tamanho <= 10000) {
			return String.format("%.2f", (tamanho / 1024)).concat(" kB");
		} else {
			return String.format("%.2f", (tamanho / (1024 * 1024))).concat(" MB");
		}
		
	}
	
	public String gerarNomeDiretorio() {

	    String generatedString = RandomStringUtils.random(10, true, true);
	    
	    System.out.println("String gerada: " + generatedString);
	    
	    return generatedString;
	}
	
	private void testConexaoFTP() {
		try 
		{
			FTPClient client = new FTPClient();
			FTPClientConfig conf = new FTPClientConfig(FTPClientConfig.SYST_UNIX);
			
			client.connect("187.17.2.187", 21);
			client.configure(conf);
			
			client.enterLocalActiveMode();
			client.login("ftp_setc", "eB5QxROL");
			
			boolean answer = client.sendNoOp();
			if(answer){
				liberarAnexo = true;
			}
			else {
				System.out.println("Server connection failed!");
				liberarAnexo = false;
			}
		}
		catch (Exception e) 
		{
			System.out.println("Server connection is closed!");
			liberarAnexo = false;
		}
	}
	
	public String onFlowProcess(FlowEvent event) {
		String perfilLogado = (String) GeralUtil.recuperarDaSessao("perfilLogado");
		
		if (manifestacaoCadastro.getTipo() == null || manifestacaoCadastro.getTipo().isEmpty()) {
			Messages.addGlobalWarn("Selecione o Tipo da manifestação", "Selecione o Tipo da manifestação");
			System.out.println("Tipo não selecionado");
			return "tipo";
		} else if(manifestacaoCadastro.getTipo().equals("Informação") && (perfilLogado == null || perfilLogado.isEmpty() || perfilLogado.equals("logoff"))) {
			Messages.addGlobalWarn("Acesse o sistema para realizar Pedido de Informação", "Acesse o sistema para realizar Pedido de Informação");
			System.out.println("Acesse o sistema para realizar Pedido de Informação");
			return "tipo";
		} else if(event.getNewStep().equals("manifestacao")) {
			if (idTema != 0 && idEntidade != 0) {
				try {
					setarEntidadeTema();

					// Tratamento para abrir popup da AGRESE
					if(manifestacaoCadastro.getEntidade().getIdEntidade() == 116 && (manifestacaoCadastro.getTema().getIdTema() >= 550 && manifestacaoCadastro.getTema().getIdTema() <= 553)) {
						renderProtocoloAgrese = true;
						
				        PrimeFaces.current().executeScript("PF('confirmDialog').show();");
				        
//						System.out.println("-- Ativa Protocolo AGRESE");
					} else {
						renderProtocoloAgrese = false;
//						System.out.println("-- Inativa Protocolo AGRESE");
					}
					
					// Tratamento para abrir popup do BANESE
					if(manifestacaoCadastro.getEntidade().getIdEntidade() == 144) {
						renderConteudoManifestacao = false;
						
						PrimeFaces.current().executeScript("PF('confirmDialogBanese').show();");
					}
					
					return event.getNewStep();
                } catch (Exception e) {
                    Messages.addGlobalWarn("Selecione Órgão e Tema da manifestação", "Selecione Órgão e Tema da manifestação");
                    System.out.println("Falha ao setar Órgão e Tema da manifestação");
                    return "orgao";
                }
			} else {
				Messages.addGlobalWarn("Selecione Órgão e Tema da manifestação", "Selecione Órgão e Tema da manifestação");
				System.out.println("Selecione Órgão e Tema da manifestação");
				return "orgao";
			}
		} else {
			return event.getNewStep();
		}
		
		 
	}
	
	
	// =================
	// GETTERS E SETTERS
	// =================

	public Manifestacao getManifestacaoCadastro() {
		return manifestacaoCadastro;
	}

	public void setManifestacaoCadastro(Manifestacao manifestacaoCadastro) {
		this.manifestacaoCadastro = manifestacaoCadastro;
	}

	public Mensagem getMensagemCadastro() {
		return mensagemCadastro;
	}

	public void setMensagemCadastro(Mensagem mensagemCadastro) {
		this.mensagemCadastro = mensagemCadastro;
	}

	public boolean isSelecionaOrgao() {
		return selecionaOrgao;
	}

	public void setSelecionaOrgao(boolean selecionaOrgao) {
		this.selecionaOrgao = selecionaOrgao;
	}

	public List<Tema> getListTemas() {
		return listTemas;
	}

	public void setListTemas(List<Tema> listTemas) {
		this.listTemas = listTemas;
	}

	public Integer getIdTema() {
		return idTema;
	}

	public void setIdTema(Integer idTema) {
		this.idTema = idTema;
	}

	public Integer getIdEntidade() {
		return idEntidade;
	}

	public void setIdEntidade(Integer idEntidade) {
		this.idEntidade = idEntidade;
	}

	public List<Entidade> getListEntidadesFiltradas() {
		return listEntidadesFiltradas;
	}

	public void setListEntidadesFiltradas(List<Entidade> listEntidadesFiltradas) {
		this.listEntidadesFiltradas = listEntidadesFiltradas;
	}

	public List<Entidade> getListEntidades() {
		return listEntidades;
	}

	public void setListEntidades(List<Entidade> listEntidades) {
		this.listEntidades = listEntidades;
	}

	public List<Tema> getListTemasFiltrados() {
		return listTemasFiltrados;
	}

	public void setListTemasFiltrados(List<Tema> listTemasFiltrados) {
		this.listTemasFiltrados = listTemasFiltrados;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public List<AnexoUpload> getAnexosList() {
		return anexosList;
	}

	public void setAnexosList(List<AnexoUpload> anexosList) {
		this.anexosList = anexosList;
	}

	public boolean isRenderProtocoloAgrese() {
		return renderProtocoloAgrese;
	}

	public void setRenderProtocoloAgrese(boolean renderProtocoloAgrese) {
		this.renderProtocoloAgrese = renderProtocoloAgrese;
	}

	public boolean isLiberarAnexo() {
		return liberarAnexo;
	}

	public void setLiberarAnexo(boolean liberarAnexo) {
		this.liberarAnexo = liberarAnexo;
	}

	public boolean isRenderConteudoManifestacao() {
		return renderConteudoManifestacao;
	}

	public void setRenderConteudoManifestacao(boolean renderConteudoManifestacao) {
		this.renderConteudoManifestacao = renderConteudoManifestacao;
	}

}
