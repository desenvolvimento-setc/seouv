package br.gov.se.setc.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.Random;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import br.gov.se.setc.DAO.UsuarioDAO;
import br.gov.se.setc.model.Cidadao;
import br.gov.se.setc.model.Responsavel;
import br.gov.se.setc.model.Usuario;
import br.gov.se.setc.util.CriptografiaUtil;
import br.gov.se.setc.util.GeralUtil;

@SessionScoped
@ManagedBean(name = "loginBean")
public class LoginBean implements Serializable{
 
	private static final long serialVersionUID = 1L;
	
	private Usuario usuarioLogado;
	private String perfilLogado;
	
	private String nomeCompleto;

	private Cidadao cidadao;
	private Responsavel responsavel;

	private String username;
	private String senha;

	// Checagem para redirecionamento
	private boolean veioDeSolicitacao;
	private boolean veioDeCadastro;
	private boolean veioDeCovid;

	private UsuarioDAO usuarioDAO = UsuarioDAO.getInstance();

	@PostConstruct
	public void init() {
//		System.out.println(":: init LoginBean ::");
		
//		System.out.println("Perfil logado: " + GeralUtil.recuperarDaSessao("perfilLogado"));
		
		try {
			if (GeralUtil.recuperarDaSessao("perfilLogado") == null)
				perfilLogado = "logoff";
			else
				perfilLogado = (String) GeralUtil.recuperarDaSessao("perfilLogado");
		} catch (Exception e) {
			perfilLogado = "logoff";
			e.printStackTrace();
		}
	}
	
	public String login() {
		
		try {
			Usuario usuarioLogin = usuarioDAO.getUsuarioPorNick(username);
			
			if (usuarioLogin != null) {
				System.out.println("Nome de usuário: \n" + username + " -- Usuario realizando login: " + usuarioLogin);
				if(CriptografiaUtil.Comparar(CriptografiaUtil.Criptografar(senha), usuarioLogin.getSenha())) {
	                
					usuarioLogin.setLastLogged(new Date());
					usuarioLogin.setSessionId(generateSessionId());
					try {
						usuarioDAO.saveOrUpdate(usuarioLogin);
					} catch (Exception e) {
						FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERRO", "Falha atualizar usuário"));
					}
					
					GeralUtil.salvarNaSessao("usuarioLogin", usuarioLogin);
					
					this.usuarioLogado = usuarioLogin;
					salvaPerfilLogado(usuarioLogado);
					gerarNomeCompleto(usuarioLogado.getNome());
					
					if(usuarioLogado.getPerfil() == 3) {
						cidadao = usuarioLogado.getCidadaos().iterator().next();
					} else if(usuarioLogado.getPerfil() == 2) {
						responsavel = usuarioLogado.getResponsaveis().iterator().next();
					}
					
//					System.out.println("Usuário logado");
					
				} else {
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Usuário ou senha incorretos", "Tente novamente"));
//					System.out.println("Usuário ou senha incorretos");
				}
			} else {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Usuário ou senha incorretos", "Tente novamente"));
//				System.out.println("Usuário não encontrado");
			}
		} catch (Exception e) {
			System.out.println("Falha ao realizar login");
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERRO", "Falha ao realizar login"));
		}
		
		return "/index.xhtml";
	}
	
	public void logout() {
		try {
			this.usuarioLogado = null;
			this.setNomeCompleto("");
			FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
			
			FacesContext.getCurrentInstance().getExternalContext().redirect("/index.xhtml");
		} catch (IOException e) {
			System.out.println("Falha ao realizar logout");
			e.printStackTrace();
		}
	}
	
	private void gerarNomeCompleto(String nome) {
		if (nome != null) {
			String[] nomeSobrenome = nome.split(" ");
			if (nomeSobrenome.length > 1) {
				this.nomeCompleto = nomeSobrenome[0] + " " + nomeSobrenome[nomeSobrenome.length - 1];
			} else {
				this.nomeCompleto = nomeSobrenome[0];
			}
		}
	}
	
	
	public void salvaPerfilLogado(Usuario usuario) {
		System.out.print(":: salvaPerfilLogado = ");
		
		if(usuario != null) {
			
			if (usuario.getPerfil() == 3 && !usuario.getCidadaos().isEmpty()) {
				System.out.println("Perfil logado: cidadao");
				perfilLogado = "cidadao";
				GeralUtil.salvarNaSessao("perfilLogado", perfilLogado);
			} else if(usuario.getPerfil() == 2 && !usuario.getResponsaveis().isEmpty()) {
				
				for(Responsavel resp : usuario.getResponsaveis()) {
//					System.out.println(resp);
					if(resp.isAtivo()) {
						if (resp.getEntidade().getSigla().equals("OGE")) {
							if(resp.getNivel() ==  1) {
								System.out.println("Perfil logado: coordenadorOGE");
								perfilLogado = "coordenadorOGE";
								GeralUtil.salvarNaSessao("perfilLogado", perfilLogado);
							} else if (resp.getNivel() ==  2) {
								System.out.println("Perfil logado: atendenteOGE");
								perfilLogado = "atendenteOGE";
								GeralUtil.salvarNaSessao("perfilLogado", perfilLogado);
							} else if (resp.getNivel() ==  3) {
								System.out.println("Perfil logado: ouvidorGeral");
								perfilLogado = "ouvidorGeral";
								GeralUtil.salvarNaSessao("perfilLogado", perfilLogado);
							} else {
								System.out.println("Perfil logado: responsavelOGE");
								perfilLogado = "responsavelOGE";
								GeralUtil.salvarNaSessao("perfilLogado", perfilLogado);
							}
						} else {
							if(resp.getNivel() ==  1) {
								System.out.println("Perfil logado: ouvidorOrgao");
								perfilLogado = "ouvidorOrgao";
								GeralUtil.salvarNaSessao("perfilLogado", perfilLogado);
							} else if (resp.getNivel() ==  2) {
								System.out.println("Perfil logado: atendenteOrgao");
								perfilLogado = "atendenteOrgao";
								GeralUtil.salvarNaSessao("perfilLogado", perfilLogado);
							} else if (resp.getNivel() ==  4) {
								System.out.println("Perfil logado: usciOrgao");
								perfilLogado = "usciOrgao";
								GeralUtil.salvarNaSessao("perfilLogado", perfilLogado);
							} else {
								System.out.println("Perfil logado: responsavelOrgao");
								perfilLogado = "responsavelOrgao";
								GeralUtil.salvarNaSessao("perfilLogado", perfilLogado);
							}
						}
					}
				}
						
			} else if(usuario.getPerfil() == 6) {
				System.out.println("admin");
				perfilLogado = "admin";
				GeralUtil.salvarNaSessao("perfilLogado", perfilLogado);
			} else if(usuario.getPerfil() == 1) {
				System.out.println("usuario");
				perfilLogado = "usuario";
				GeralUtil.salvarNaSessao("perfilLogado", perfilLogado);
			}
		} else {
			perfilLogado = "logoff";
			GeralUtil.salvarNaSessao("perfilLogado", perfilLogado);
			System.out.println("logoff");
		}
	}
	
	public String perfilNome() {
		if(perfilLogado.equals("cidadao")) {
			return "Cidadão";
		} else if(perfilLogado.equals("ouvidorOrgao")) {
			return "Ouvidor Setorial";
		} else if(perfilLogado.equals("usciOrgao")) {
			return "USCI";
		} else if(perfilLogado.equals("coordenadorOGE")) {
			return "Coordenador Ouvidoria";
		} else if (perfilLogado.equals("atendenteOGE") || perfilLogado.equals("atendenteOrgao")) {
			return "Atendente";
		} else if (perfilLogado.equals("ouvidorGeral")) {
			return "Ouvidor Geral";
		} else if (perfilLogado.equals("admin")) {
			return "Administrador";
		} else {
			return "Usuário";
		}
	} 
	
	public void printTime() {
		GeralUtil.getDataAtual();
	}
	
//	public boolean isUsuarioCidadao() {
//		Usuario userSessao = (Usuario) RecuperarDaSessao("usuarioLogin");
//		
//		if(userSessao != null) {
//			if (userSessao.getPerfil() == 1 && !userSessao.getCidadaos().isEmpty()) {
//				return true;
//			}
//		}
//		
//		return false;
//		
//	}
//	
//	public String getPerfilUsuario() {
//		Usuario userSessao = (Usuario) RecuperarDaSessao("usuarioLogin");
//		
//		if(userSessao != null) {
//			if (userSessao.getPerfil() == 1 && !userSessao.getCidadaos().isEmpty()) {
//				return "Cidadão";
//			} else if(userSessao.getPerfil() == 2 && !userSessao.getResponsaveis().isEmpty()) {
//				for(Responsavel resp : userSessao.getResponsaveis()) 
//					if(resp.isAtivo()) {
//						switch (resp.getNivel()) {
//							case 1:
//								return "Ouvidor Geral";
//							case 2:
//								return "Atendente " + resp.getEntidade().getSigla();
//							default:
//								return "Responsável Órgão";
//						}
//					}
//						
//			}
//		}
//		
//		return "Usuário";
//	}
	
	public void printUsuarioDaSessao() {
		Usuario userPrint = (Usuario) GeralUtil.recuperarDaSessao("usuarioLogin");
		
		System.out.println("Perfil logado: " + perfilLogado);
		
		if(userPrint != null) {
			System.out.println("Usuário logado: " + userPrint);
			if(userPrint.getPerfil() == 3) {
				System.out.print("Cidadão: ");
				if(!userPrint.getCidadaos().isEmpty()) {
					for(Cidadao cid : userPrint.getCidadaos())
						System.out.println(cid);
				}
			} else if(userPrint.getPerfil() == 2) {
				System.out.println("Responsáveis: ");
				if(!userPrint.getResponsaveis().isEmpty()) {
					for(Responsavel resp : userPrint.getResponsaveis())
						System.out.println(resp);
				}
			} else {
				System.out.println("Perfil de Cidadão ou Responsável não encontrados");
			}
		} else {
			System.out.println("Usuario não carregado devidamente");
		}
	}
	
	private String generateSessionId() {
		int len = 45;
		String charsCaps = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String Chars = "abcdefghijklmnopqrstuvwxyz";
		String nums = "0123456789";
		String passSymbols = charsCaps + Chars + nums;
		Random rnd = new Random();
		String sessionId = "";

		for (int i = 0; i < len; i++) {
			sessionId += passSymbols.charAt(rnd.nextInt(passSymbols.length()));
		}
		return sessionId;
	}
	
	public Usuario getUsuarioLogado() {
		if(this.usuarioLogado == null) {
			this.usuarioLogado = (Usuario) GeralUtil.recuperarDaSessao("usuarioLogin");
		}
		
		return usuarioLogado;
	}
	

	public void setUsuarioLogado(Usuario usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}

	public Cidadao getCidadao() {
		return cidadao;
	}

	public void setCidadao(Cidadao cidadao) {
		this.cidadao = cidadao;
	}

	public Responsavel getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(Responsavel responsavel) {
		this.responsavel = responsavel;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public boolean isVeioDeSolicitacao() {
		return veioDeSolicitacao;
	}

	public void setVeioDeSolicitacao(boolean veioDeSolicitacao) {
		this.veioDeSolicitacao = veioDeSolicitacao;
	}

	public boolean isVeioDeCadastro() {
		return veioDeCadastro;
	}

	public void setVeioDeCadastro(boolean veioDeCadastro) {
		this.veioDeCadastro = veioDeCadastro;
	}

	public boolean isVeioDeCovid() {
		return veioDeCovid;
	}

	public void setVeioDeCovid(boolean veioDeCovid) {
		this.veioDeCovid = veioDeCovid;
	}

	public String getPerfilLogado() {
		if(this.perfilLogado == null) {
			this.perfilLogado = (String) GeralUtil.recuperarDaSessao("perfilLogado");
		}
		
		return perfilLogado;
	}
	
	public void setPerfilLogado(String perfilLogado) {
		this.perfilLogado = perfilLogado;
	}

	public String getNomeCompleto() {
		return nomeCompleto;
	}

	public void setNomeCompleto(String nomeCompleto) {
		this.nomeCompleto = nomeCompleto;
	}
	
}
