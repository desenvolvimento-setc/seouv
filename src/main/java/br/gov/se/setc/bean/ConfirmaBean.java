package br.gov.se.setc.bean;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.omnifaces.cdi.Param;
import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Ajax;
import org.omnifaces.util.Faces;
import org.omnifaces.util.FacesLocal;
import org.omnifaces.util.Messages;

import br.gov.se.setc.DAO.ManifestacaoDAO;
import br.gov.se.setc.model.Manifestacao;

@Named
@ViewScoped
public class ConfirmaBean implements Serializable {
	
	private static final long serialVersionUID = -4471638179132742750L;

	private ManifestacaoDAO manifestacaoDAO = ManifestacaoDAO.getInstance();
	
	private Manifestacao manifestacao;
	
	@PostConstruct
	public void init() {
//		System.out.println(":: init confirmaBean ::");
		
		// Seta Manifestacao a partir do idManifestacao como parametro na URL
			try {
				HttpServletRequest request = (HttpServletRequest) FacesLocal.getRequest(Faces.getContext());
				
				String protocolo = request.getParameter("manifestacao");
				
				if(protocolo != null && !protocolo.isEmpty()) {
					System.out.println("Manifestação: " + protocolo);
					manifestacao = manifestacaoDAO.getManifestacaoProtocolo(protocolo);
					Messages.addGlobalInfo("Manifestação realizada com sucesso!");
					
					if(manifestacao.equals(null)) {
						System.out.println("Falha ao recuperar Manifestação");
						Messages.addGlobalWarn("Falha ao recuperar Manifestacao");
						FacesLocal.redirect(Faces.getContext(), "/index.xhtml");
					} 
					else
						Messages.addGlobalInfo("Manifestação recuperada");
				} else {
					System.out.println("Falha ao recuperar Manifestação");
					Messages.addGlobalWarn("Falha ao recuperar Manifestação");
					FacesLocal.redirect(Faces.getContext(), "/index.xhtml");
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Falha ao recuperar parametro");
				Messages.addGlobalWarn("Falha ao recuperar Manifestacao");
				try {
					FacesLocal.redirect(Faces.getContext(), "../index.xhtml");
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
	}

	
	public Manifestacao getManifestacao() {
		return manifestacao;
	}

	public void setManifestacao(Manifestacao manifestacao) {
		this.manifestacao = manifestacao;
	}
	
}
