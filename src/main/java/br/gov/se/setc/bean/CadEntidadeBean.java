package br.gov.se.setc.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Messages;

import br.gov.se.setc.DAO.EntidadeDAO;
import br.gov.se.setc.DAO.TemaDAO;
import br.gov.se.setc.model.Competencia;
import br.gov.se.setc.model.Entidade;

@Named
@ViewScoped
public class CadEntidadeBean implements Serializable {

	private static final long serialVersionUID = -568903469058784542L;

	private EntidadeDAO entidadeDAO = EntidadeDAO.getInstance();
	private TemaDAO temaDAO = TemaDAO.getInstance();
	
	private Entidade entidadeCadastro;
	private int idSuperior;
	
	private List<Entidade> listEntidadesAtivas;

	@PostConstruct
	public void init() {
//		System.out.println(":: init cadEntidadeBean ::");
		
		entidadeCadastro = new Entidade();
		entidadeCadastro.setOrgao(true);
		listEntidadesAtivas = entidadeDAO.listEntidadesAtivas();
	}
	
	public String salvar() {
		try {
			
			if(entidadeCadastro.isOrgao()) {
				entidadeCadastro.setIdSuperior(0);
			} 
			
			Competencia comp = new Competencia();
			comp.setTema(temaDAO.getTema(22));
			comp.setAtiva(true);
			
			entidadeCadastro.addCompetencia(comp);
			
			entidadeCadastro.setAtiva(true);
			
			boolean salvou = entidadeDAO.saveOrUpdate(entidadeCadastro);
			
			if(salvou)
				return "/consulta/entidades.xhtml";
			else 
				return null;
		} catch (Exception e) {
			System.out.println(":: Falha ao cadastrar entidade");
			e.printStackTrace();
			Messages.addGlobalError("Falha ao cadastrar entidade");
			return null;
		} finally {
			entidadeCadastro = new Entidade();
		}
	}

	
	public Entidade getEntidadeCadastro() {
		return entidadeCadastro;
	}

	public void setEntidadeCadastro(Entidade entidadeCadastro) {
		this.entidadeCadastro = entidadeCadastro;
	}

	public List<Entidade> getListEntidadesAtivas() {
		return listEntidadesAtivas;
	}

	public void setListEntidadesAtivas(List<Entidade> listEntidadesAtivas) {
		this.listEntidadesAtivas = listEntidadesAtivas;
	}

	public int getIdSuperior() {
		return idSuperior;
	}

	public void setIdSuperior(int idSuperior) {
		this.idSuperior = idSuperior;
	}


}
