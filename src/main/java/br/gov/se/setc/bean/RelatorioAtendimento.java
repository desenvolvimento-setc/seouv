package br.gov.se.setc.bean;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import br.gov.se.setc.DAO.EntidadeDAO;
import br.gov.se.setc.DAO.RelatorioDAO;
import br.gov.se.setc.model.Entidade;

@ManagedBean(name = "relatorioAtendimento")
@SessionScoped
public class RelatorioAtendimento {

	public int idEntidade = 0;
	
	String[] meses = { "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" };
	
	private List<String[]> manifPorMes;
	private String[] columnManifPorMes;
	private String[] dataManifPorMes;
	
	private List<String[]> manifPorCanal;
	private String[] columnManifPorCanal;
	private String[] dataManifPorCanal;
	
	private List<String[]> manifPorOrgao;
	private String[] columnManifPorOrgao;
	private String[] dataManifPorOrgao;
	
	private List<String[]> manifPorSituacao;
	private String[] columnManifPorSituacao;
	private String[] dataManifPorSituacao;
	
	private Integer anoAtual;
	private Integer mesAtual;
	
	private Date dataInicialConsulta;
	private Date dataFinalConsulta;
	
	private String exibirOrgao;
	private String exibirPeriodo;
	private String exibirAno;
	
	public Entidade entidade;
	
	//Download
	private String base64Str;
	private StreamedContent file;
	
	private RelatorioDAO relatorioDAO = RelatorioDAO.getInstance();
	private EntidadeDAO entidadeDAO = EntidadeDAO.getInstance();
	
	private List<Entidade> listEntidades;
	
	@PostConstruct
	public void init() {
//		System.out.println(":: init relatorioOuvidoria");
		try {
//			listEntidades = new ArrayList<Entidade>();
//			listEntidades.add(entidadeDAO.getEntidade(116));
			
			idEntidade = 116;
			exibirOrgao = "AGRESE";
			
			Calendar c = Calendar.getInstance();
			
			anoAtual = c.get(Calendar.YEAR);
			String dataInicial = anoAtual+"/01/01";
			
			dataInicialConsulta = new Date(anoAtual+1900, 0, 1);
			
			mesAtual = c.get(Calendar.MONTH)+1;
			String dataFinal = c.get(Calendar.YEAR)+"/"+mesAtual+"/"+c.get(Calendar.DATE);
			
			dataFinalConsulta = new Date(anoAtual+1900, mesAtual-1, c.get(Calendar.DATE));
			
			exibirAno = Integer.toString(anoAtual);
			
			exibirPeriodo = meses[dataInicialConsulta.getMonth()] + " a " +meses[dataFinalConsulta.getMonth()] + " de " + Integer.toString(anoAtual);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Falha ao inicializar relatório");
		}
	}
	
	//Download
	public void submittedBase64Str() throws IOException{
	    if(base64Str.split(",").length > 1){
	        String encoded = base64Str.split(",")[1];
	        byte[] decoded = org.apache.commons.codec.binary.Base64.decodeBase64(encoded);
	            InputStream in = new ByteArrayInputStream(decoded);
	            file = new DefaultStreamedContent(in, "image/png", "chart.png");
	    }
	}

	
	public void loadGraficos() {
		try {
			if(dataFinalConsulta.before(dataInicialConsulta)) {
				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null,  new FacesMessage("Erro", "Período de pesquisa inválido"));
				
			} else {
				System.out.println("Periodo valido");
				if(idEntidade > 0) {
//					exibirOrgao = entidadeDAO.getSigla(idEntidade) + " - ";
					
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
					String dataInicial = sdf.format(dataInicialConsulta);
					String dataFinal = sdf.format(dataFinalConsulta);
					
					manifPorMes = relatorioDAO.getAtendimentoPorMesEntidade(dataInicial, dataFinal, idEntidade);
					columnManifPorMes = manifPorMes.get(0);
					dataManifPorMes = manifPorMes.get(1);
					
					manifPorCanal = relatorioDAO.getAtendimentoPorCanalEntidade(dataInicial, dataFinal, idEntidade);
					columnManifPorCanal = manifPorCanal.get(0);
					dataManifPorCanal = manifPorCanal.get(1);
					
					manifPorOrgao = relatorioDAO.getAtendimentoPorOrgaoEntidade(dataInicial, dataFinal, idEntidade);
					columnManifPorOrgao = manifPorOrgao.get(0);
					dataManifPorOrgao = manifPorOrgao.get(1);
					
					manifPorSituacao = relatorioDAO.getAtendimentoPorSituacaoEntidade(dataInicial, dataFinal, idEntidade);
					columnManifPorSituacao = manifPorSituacao.get(0);
					dataManifPorSituacao = manifPorSituacao.get(1);
					
					
					if(dataInicialConsulta.getYear() != dataFinalConsulta.getYear()) {
						manifPorMes = relatorioDAO.getAtendimentoPorAnoEntidade(dataInicial, dataFinal, idEntidade);
						columnManifPorMes = manifPorMes.get(0);
						dataManifPorMes = manifPorMes.get(1);
						
						exibirPeriodo = meses[dataInicialConsulta.getMonth()] + " de " + Integer.toString(dataInicialConsulta.getYear()+1900) +
								" a " + meses[dataFinalConsulta.getMonth()] + " de " + Integer.toString(dataFinalConsulta.getYear()+1900);
					}
					else {
						exibirPeriodo = meses[dataInicialConsulta.getMonth()] + " a " +meses[dataFinalConsulta.getMonth()] + " de " + Integer.toString(dataInicialConsulta.getYear()+1900);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public int getAnoAtual() {
		return anoAtual;
	}

	public String getMesAtual() {
		//String[] meses = { "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" };
		
		return meses[mesAtual-1];
	}

	public int getIdEntidade() {
		return idEntidade;
	}
	
	public void setIdEntidade(int idEntidade) {
		this.idEntidade = idEntidade;
	}

	public String[] getColumnManifPorMes() {
		return columnManifPorMes;
	}

	public void setColumnManifPorMes(String[] columnManifPorMes) {
		this.columnManifPorMes = columnManifPorMes;
	}

	public String[] getDataManifPorMes() {
		return dataManifPorMes;
	}

	public void setDataManifPorMes(String[] dataManifPorMes) {
		this.dataManifPorMes = dataManifPorMes;
	}

	public String[] getColumnManifPorOrgao() {
		return columnManifPorOrgao;
	}

	public void setColumnManifPorOrgao(String[] columnManifPorOrgao) {
		this.columnManifPorOrgao = columnManifPorOrgao;
	}

	public String[] getDataManifPorOrgao() {
		return dataManifPorOrgao;
	}

	public void setDataManifPorOrgao(String[] dataManifPorOrgao) {
		this.dataManifPorOrgao = dataManifPorOrgao;
	}

	public Date getDataInicialConsulta() {
		return dataInicialConsulta;
	}

	public void setDataInicialConsulta(Date dataInicialConsulta) {
		this.dataInicialConsulta = dataInicialConsulta;
	}

	public Date getDataFinalConsulta() {
		return dataFinalConsulta;
	}

	public void setDataFinalConsulta(Date dataFinalConsulta) {
		this.dataFinalConsulta = dataFinalConsulta;
	}

	public String getExibirOrgao() {
		return exibirOrgao;
	}

	public void setExibirOrgao(String exibirOrgao) {
		this.exibirOrgao = exibirOrgao;
	}

	public String getExibirPeriodo() {
		return exibirPeriodo;
	}

	public void setExibirPeriodo(String exibirPeriodo) {
		this.exibirPeriodo = exibirPeriodo;
	}

	public String getBase64Str() {
		return base64Str;
	}

	public void setBase64Str(String base64Str) {
		this.base64Str = base64Str;
	}

	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}

	public String getExibirAno() {
		return exibirAno;
	}

	public void setExibirAno(String exibirAno) {
		this.exibirAno = exibirAno;
	}

	public List<String[]> getManifPorCanal() {
		return manifPorCanal;
	}

	public void setManifPorCanal(List<String[]> manifPorCanal) {
		this.manifPorCanal = manifPorCanal;
	}

	public String[] getColumnManifPorCanal() {
		return columnManifPorCanal;
	}

	public void setColumnManifPorCanal(String[] columnManifPorCanal) {
		this.columnManifPorCanal = columnManifPorCanal;
	}

	public String[] getDataManifPorCanal() {
		return dataManifPorCanal;
	}

	public void setDataManifPorCanal(String[] dataManifPorCanal) {
		this.dataManifPorCanal = dataManifPorCanal;
	}

	public List<Entidade> getListEntidades() {
		return listEntidades;
	}

	public void setListEntidades(List<Entidade> listEntidades) {
		this.listEntidades = listEntidades;
	}

	public List<String[]> getManifPorSituacao() {
		return manifPorSituacao;
	}

	public void setManifPorSituacao(List<String[]> manifPorSituacao) {
		this.manifPorSituacao = manifPorSituacao;
	}

	public String[] getColumnManifPorSituacao() {
		return columnManifPorSituacao;
	}

	public void setColumnManifPorSituacao(String[] columnManifPorSituacao) {
		this.columnManifPorSituacao = columnManifPorSituacao;
	}

	public String[] getDataManifPorSituacao() {
		return dataManifPorSituacao;
	}

	public void setDataManifPorSituacao(String[] dataManifPorSituacao) {
		this.dataManifPorSituacao = dataManifPorSituacao;
	}

}