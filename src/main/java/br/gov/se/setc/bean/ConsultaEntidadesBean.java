package br.gov.se.setc.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Faces;
import org.omnifaces.util.FacesLocal;
import org.omnifaces.util.Messages;
import org.primefaces.PrimeFaces;

import br.gov.se.setc.DAO.EntidadeDAO;
import br.gov.se.setc.model.Entidade;
import br.gov.se.setc.model.Usuario;
import br.gov.se.setc.util.GeralUtil;

@Named
@ViewScoped
public class ConsultaEntidadesBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private EntidadeDAO entidadeDAO = EntidadeDAO.getInstance();
	
	private Usuario usuarioLogado;
	private String perfilLogado;
	
	private List<Entidade> listEntidades;
	private List<Entidade> listEntidadesFiltradas;
	
	private Entidade editEntidade;
	
	private int qtdManifestacoesPendentes;

	@PostConstruct
	public void init() {
//		System.out.println(":: init consultaEntidadesBean ::");
		
		try {
			usuarioLogado = (Usuario) GeralUtil.recuperarDaSessao("usuarioLogin");
			perfilLogado = (String) GeralUtil.recuperarDaSessao("perfilLogado");
			
			if(perfilLogado != null && !perfilLogado.equals("logoff")) {
				if(perfilLogado.equals("ouvidorOrgao")) {
					listEntidades = new ArrayList<Entidade>();
					
					listEntidades.add(usuarioLogado.getResponsaveis().iterator().next().getEntidade());
				} else if (perfilLogado.equals("coordenadorOGE") || perfilLogado.equals("ouvidorGeral") || perfilLogado.equals("admin")) {
					listEntidades = entidadeDAO.listEntidades();
				}
			} else {
				System.out.println("Acesso Negado");
				Messages.addGlobalWarn("Acesso Negado");
				FacesLocal.redirect(Faces.getContext(), "../pagina/acessoNegado.xhtml");
			}
			
			qtdManifestacoesPendentes = 0;
		} catch (Exception e) {
			System.out.println(":: Erro ao popular lista de entidades");
			e.printStackTrace();
		}
	}
	
	public void abrirEntidade(Entidade entidade) {
		try {
			editEntidade = entidade;
			
			PrimeFaces.current().executeScript("PF('editDialog').show();");
		} catch (Exception e) {
			e.printStackTrace();
			Messages.addGlobalWarn("Falha ao abrir Entidade");
			PrimeFaces.current().ajax().update("growl");
		}
	}
	
	public void editarEntidade() {
		try {
			if(!perfilLogado.equals("admin")) {
				editEntidade.setAtualizado_em(GeralUtil.getDataAtual());
				editEntidade.setAtualizado_por(usuarioLogado.getResponsaveis().iterator().next());
			}
			
			entidadeDAO.saveOrUpdate(editEntidade);
			editEntidade = new Entidade();
		} catch (Exception e) {
			e.printStackTrace();
			Messages.addGlobalWarn("Falha ao editar Entidade");
			PrimeFaces.current().ajax().update("growl");
		}
	}
	
	public void confirmaInativacao(Entidade entidade) {
		try {
			editEntidade = entidade;
			
			qtdManifestacoesPendentes = entidadeDAO.countManifestacoesPendentes(entidade.getIdEntidade());
			
			PrimeFaces.current().executeScript("PF('inativaDialog').show();");
		} catch (Exception e) {
			e.printStackTrace();
			Messages.addGlobalWarn("Falha ao abrir Entidade");
			PrimeFaces.current().ajax().update("growl");
		}
	}
	
	public void inativarEntidade() {
		try {
			if(!perfilLogado.equals("admin")) {
				editEntidade.setAtualizado_em(GeralUtil.getDataAtual());
				editEntidade.setAtualizado_por(usuarioLogado.getResponsaveis().iterator().next());
			}
			
			editEntidade.setAtiva(false);
			entidadeDAO.saveOrUpdate(editEntidade);
			editEntidade = new Entidade();
		} catch (Exception e) {
			e.printStackTrace();
			Messages.addGlobalWarn("Falha ao editar Entidade");
			PrimeFaces.current().ajax().update("growl");
		}
	}
	
	public void ativarEntidade(Entidade ent) {
		try {
			if(!perfilLogado.equals("admin")) {
				ent.setAtualizado_em(GeralUtil.getDataAtual());
				ent.setAtualizado_por(usuarioLogado.getResponsaveis().iterator().next());
			}
			
			ent.setAtiva(true);
			entidadeDAO.saveOrUpdate(ent);
		} catch (Exception e) {
			e.printStackTrace();
			Messages.addGlobalWarn("Falha ao editar Entidade");
			PrimeFaces.current().ajax().update("growl");
		}
	}

	public List<Entidade> getListEntidades() {
		return listEntidades;
	}

	public void setListEntidades(List<Entidade> listEntidades) {
		this.listEntidades = listEntidades;
	}

	public Entidade getEditEntidade() {
		return editEntidade;
	}

	public void setEditEntidade(Entidade editEntidade) {
		this.editEntidade = editEntidade;
	}

	public List<Entidade> getListEntidadesFiltradas() {
		return listEntidadesFiltradas;
	}

	public void setListEntidadesFiltradas(List<Entidade> listEntidadesFiltradas) {
		this.listEntidadesFiltradas = listEntidadesFiltradas;
	}

	public int getQtdManifestacoesPendentes() {
		return qtdManifestacoesPendentes;
	}

	public void setQtdManifestacoesPendentes(int qtdManifestacoesPendentes) {
		this.qtdManifestacoesPendentes = qtdManifestacoesPendentes;
	}

}
