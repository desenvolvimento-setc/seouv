package br.gov.se.setc.bean;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.inject.Named;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPClientConfig;
import org.apache.commons.net.ftp.FTPFile;
import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Faces;
import org.omnifaces.util.FacesLocal;
import org.omnifaces.util.Messages;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import br.gov.se.setc.DAO.CidadaoDAO;
import br.gov.se.setc.DAO.EntidadeDAO;
import br.gov.se.setc.DAO.ManifestacaoDAO;
import br.gov.se.setc.DAO.TemaDAO;
import br.gov.se.setc.DAO.UsuarioDAO;
import br.gov.se.setc.model.Anexo;
import br.gov.se.setc.model.AnexoUpload;
import br.gov.se.setc.model.Cidadao;
import br.gov.se.setc.model.Entidade;
import br.gov.se.setc.model.Manifestacao;
import br.gov.se.setc.model.Mensagem;
import br.gov.se.setc.model.Responsavel;
import br.gov.se.setc.model.Tema;
import br.gov.se.setc.model.Usuario;
import br.gov.se.setc.util.EmailUtil;
import br.gov.se.setc.util.GeralUtil;
import br.gov.se.setc.util.PrazosUtil;

@Named
@ViewScoped
public class CadAtendimentoBean implements Serializable{

	private static final long serialVersionUID = -6187099514338119860L;
	
	private ManifestacaoDAO manifestacaoDAO = ManifestacaoDAO.getInstance();
	
	private UsuarioDAO usuarioDAO = UsuarioDAO.getInstance();
	
	private TemaDAO temaDAO = TemaDAO.getInstance();
	private EntidadeDAO entidadeDAO = EntidadeDAO.getInstance();
	private CidadaoDAO cidadaoDAO = CidadaoDAO.getInstance();
	
	private Manifestacao manifestacaoCadastro;
	private Mensagem mensagemCadastro;
	
	private Integer idTema;
	private Integer idEntidade;
	
	private String primeiraPagina;
	
	private List<Entidade> listEntidades;
	private List<Tema> listTemasFiltrados;
	
	private UploadedFile file;

	private List<AnexoUpload> anexosList;
	
	private Cidadao cidadao;
	private Usuario usuario;
	
	private Usuario usuarioLogado;
	
	private boolean renderProtocoloAgrese;
	
	private boolean liberarAnexo;
	
	
	@PostConstruct
	public void init() {
		System.out.println(":: init cadAtendimentoBean ::");
		manifestacaoCadastro = new Manifestacao();
		mensagemCadastro =  new Mensagem();
		cidadao = new Cidadao();
		cidadao.setTipo(true);
		usuario = new Usuario();
		renderProtocoloAgrese = false;
		
		anexosList = new ArrayList<AnexoUpload>();
		
		try {
			usuarioLogado = (Usuario) GeralUtil.recuperarDaSessao("usuarioLogin");
			String perfilLogado = (String) GeralUtil.recuperarDaSessao("perfilLogado");
			
			if(perfilLogado != null && !perfilLogado.equals("logoff")) {
				if(perfilLogado.equals("ouvidorOrgao") || perfilLogado.equals("atendenteOrgao")) {
					listEntidades = new ArrayList<Entidade>();
					
					if(!usuarioLogado.getResponsaveis().isEmpty())
						listEntidades.add(usuarioLogado.getResponsaveis().iterator().next().getEntidade());
				} else if (perfilLogado.equals("coordenadorOGE") || perfilLogado.equals("atendenteOGE")) {
					listEntidades = entidadeDAO.listEntidadesAtivas();
				}
			} else {
				Messages.addGlobalWarn("Acesso Negado - Realize Login");
				FacesLocal.redirect(Faces.getContext(), "/pagina/acessoNegado.xhtml");
			}
			
		} catch (Exception e) {
			System.out.println(":: Erro ao popular lista de entidades");
			e.printStackTrace();
		}
		
		testConexaoFTP();
	}
	
	public String salvar() {
		String diretorioAnexos = gerarNomeDiretorio();
		
		try {
			Date dataAtual = GeralUtil.getDataAtual();
			
			Usuario usuarioLogado = (Usuario) GeralUtil.recuperarDaSessao("usuarioLogin");
			
			String perfilLogado = (String) GeralUtil.recuperarDaSessao("perfilLogado");
			
			if (perfilLogado.equals("ouvidorOrgao") || perfilLogado.equals("atendenteOrgao") || perfilLogado.equals("responsavelOrgao") || perfilLogado.equals("coordenadorOGE") ||  perfilLogado.equals("atendenteOGE")) {
				Responsavel responsavel = usuarioLogado.getResponsaveis().iterator().next();
				manifestacaoCadastro.setResponsavel(responsavel);
			}
			
			// Seta cidadão por nível de sigilo
			if (manifestacaoCadastro.getSigilo() != 2) {
				//Salvar Usuário
				usuario.setNick("cidAtendente");
				usuario.setSenha("sU19FfjoU5wD94ElRNJI2CE06TdKG3");
				usuario.setPerfil((short) 3);
				
				
				if(cidadao.getNumero().isEmpty()) {
					cidadao.setNumero(null);
				}
				
				cidadao.setUsuario(usuario);
				
				usuario.addCidadao(cidadao);
				
				boolean salvouUsuario = usuarioDAO.saveOrUpdate(usuario);
				
				if (salvouUsuario) {
					manifestacaoCadastro.setCidadao(cidadao);
					mensagemCadastro.setUsuario(usuario);
				}
			} else {
				Cidadao cidadaoAnonimo = cidadaoDAO.getCidadao(1);
				manifestacaoCadastro.setCidadao(cidadaoAnonimo);
				mensagemCadastro.setUsuario(cidadaoAnonimo.getUsuario());
			}
			
			manifestacaoCadastro.setInstancia((short) 0);
			manifestacaoCadastro.setProtocolo(gerarProtocolo(manifestacaoCadastro));
			manifestacaoCadastro.setLiberaDenuncia(true);
			manifestacaoCadastro.setStatus("Aberta");
			manifestacaoCadastro.setDataIni(dataAtual);
			
			
			// Gera data limite
			if (manifestacaoCadastro.getTipo().equals("Informação"))
				manifestacaoCadastro.setDataLimite(PrazosUtil.gerarPrazoDiaUtilLimite(dataAtual, "Esic","Aberta"));
			else
				manifestacaoCadastro.setDataLimite(PrazosUtil.gerarPrazoDiaUtilLimite(dataAtual, "Ouv", "Aberta"));
			
			
			mensagemCadastro.setData(dataAtual);
			mensagemCadastro.setTipo((short) 1);
			
			if(anexosList.size() > 0) {
				List<Anexo> anexos = salvarAnexos(diretorioAnexos);
				
				mensagemCadastro.setAnexos(anexos);
			}
			
			manifestacaoCadastro.addMensagem(mensagemCadastro);
			
			Mensagem mensagemSistema = new Mensagem();
			
			mensagemSistema.setTexto("Manifestação "+ manifestacaoCadastro.getProtocolo() +" foi recebida no sistema da entidade "+ manifestacaoCadastro.getEntidade().getNome() +".");
			mensagemSistema.setData(dataAtual);
			mensagemSistema.setTipo((short) 4);
			mensagemSistema.setUsuario(new Usuario(2));
			
			manifestacaoCadastro.addHistorico(mensagemSistema);
			
			if(manifestacaoCadastro.getIdManifestacao() == null) {
				boolean salvou = manifestacaoDAO.saveOrUpdate(manifestacaoCadastro);
				
				if(salvou) { 
					System.out.println("Salvou manifestação");
					Messages.addGlobalInfo("Manifestação salva com sucesso!");
					
					if(cidadao.getEmail() != null && !cidadao.getEmail().isEmpty())
						EmailUtil.enviarEmailNovaManifestacaoCidadao(manifestacaoCadastro);
					
					EmailUtil.enviarEmailNovaManifestacaoResp(manifestacaoCadastro);
					
					String url = "/cadastro/confirmaManifestacao.xhtml?faces-redirect=true&manifestacao=" + manifestacaoCadastro.getProtocolo();
	                return url;
				}
				else {
					System.out.println("Não salvou manifestação");
					Messages.addGlobalError("Falha ao salvar manifestação");
	//				Messages.create("Falha ao salvar manifestação").error().add();
					return "";
				}
			} else {
				System.out.println("Manifestação já salva");
//					Messages.addGlobalWarn("Erro", "Falha ao salvar manifestação");
				return null;
			}
		} catch (Exception e) {
			System.out.println("Erro ao salvar manifestação");
			Messages.addGlobalError("Falha ao salvar manifestação");
			limparAnexos(diretorioAnexos);
			e.printStackTrace();
			return "";
		}
		
	}
	
	public void setarEntidadeTema() {
		System.out.println("setarEntidadeTema");
		manifestacaoCadastro.setEntidade(entidadeDAO.getEntidade(idEntidade));
		manifestacaoCadastro.setTema(temaDAO.getTema(idTema));

		if(manifestacaoCadastro.getEntidade().getIdEntidade() == 116 && (manifestacaoCadastro.getTema().getIdTema() >= 550 && manifestacaoCadastro.getTema().getIdTema() <= 553)) {
			renderProtocoloAgrese = true;
		} else {
			renderProtocoloAgrese = false;
		}
	}
	
	public String gerarProtocolo(Manifestacao manifestacao) throws NullPointerException {
		String protocolo = String.valueOf(manifestacaoDAO.getLastId() + 1);
		
		for (int i = 0; i<(5-protocolo.length()); i++ ) {
			protocolo = "0"+protocolo;
		}
		
		Date now = new Date(System.currentTimeMillis());
		SimpleDateFormat ft = new SimpleDateFormat("yy");
		protocolo += "/"+ft.format(now);
		
		switch (manifestacao.getTipo()) {
			case "Reclamação":
				protocolo += "-1";
				break;
			case "Denúncia":
				protocolo += "-2";
				break;
			case "Informação":
				protocolo += "-3";
				break;
			case "Solicitação":
				protocolo += "-4";
				break;
			case "Sugestão":
				protocolo += "-5";
				break;
			case "Elogio":
				protocolo += "-6";
				break;
			}
		
		return protocolo;
	}
	
	public void filtrarTemas() {
		listTemasFiltrados = temaDAO.listTemasPorEntidade(idEntidade);
	}
	
	public List<Anexo> salvarAnexos(String diretorioAnexos) {
		List<Anexo> anexos = new ArrayList<Anexo>();
		
		FTPClient client = new FTPClient();
		FTPClientConfig conf = new FTPClientConfig(FTPClientConfig.SYST_UNIX);
		
		try {
			client.setControlEncoding("ISO-8859-1");
			
			client.connect("187.17.2.187", 21);
			client.configure(conf);
			
			client.enterLocalActiveMode();
			client.login("ftp_setc", "eB5QxROL");
			
			client.setBufferSize(15 * 1024 * 1024);
			client.setAutodetectUTF8(true);
			client.setFileType(FTP.BINARY_FILE_TYPE);
			
			client.changeWorkingDirectory("/SE-Ouv/" + GeralUtil.caminhoAnexos);
			
			if (client.isConnected()) {
				client.makeDirectory(diretorioAnexos);
				client.changeWorkingDirectory(diretorioAnexos);
				
				boolean salvou = true;
				int contadorAnexo = 1;
				
				// Salva arquivos no servidor de arquivos
				for(AnexoUpload arqv : anexosList) {
					InputStream inputStream = arqv.getStream();
					salvou = salvou & client.storeFile("Anexo-" + contadorAnexo + "." + StringUtils.substringAfterLast(arqv.getNome(), "."), inputStream);
					inputStream.close();
					
					if (salvou) {
						contadorAnexo++;
						System.out.println(":: Arquivo \"" + arqv.getNome() + "\" salvo no servidor");
					}
					else 
						System.out.println(":: Falha ao salvar arquivo \"" + arqv.getNome() + "\" no servidor");
				}
				
				System.out.println("Salvou todos os arquivos? " + salvou);
				
				// Salva registro dos arquivos no banco de dados
				
				if (salvou) {
					
					FTPFile[] ftpFiles = client.listFiles();
					for (FTPFile ftpFile : ftpFiles) {
						// Check if FTPFile is a regular file
						if (ftpFile.getType() == FTPFile.FILE_TYPE) {
							Anexo novoAnexo = new Anexo();
							
							novoAnexo.setNome(ftpFile.getName());
							novoAnexo.setTamanho(ftpFile.getSize());
							novoAnexo.setTipo(StringUtils.substringAfterLast(ftpFile.getName(), "."));
							novoAnexo.setLocal("http://arquivos.setc.se.gov.br/SE-Ouv/" + GeralUtil.caminhoAnexos + "/" + diretorioAnexos + "/" + ftpFile.getName());
							
							anexos.add(novoAnexo);
						}
					}
				 
				} else {
					limparAnexos(diretorioAnexos);
					throw new IOException();
				}
				 
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return anexos;
	}
	
	public void fileUploadListener(FileUploadEvent e) {
		System.out.println("fileUploadListener");
		
//		List<Anexo> fileList = this.anexoList;
		
		UploadedFile arquivo = e.getFile();
		
		try {
			AnexoUpload novoAnexo = new AnexoUpload(limiteNomeArquivo(arquivo.getFileName()), arquivo.getContentType(), arquivo.getSize(), arquivo.getInputstream());
			
			if(!anexosList.contains(novoAnexo)) {
				if (arquivo.getSize() < 15728640) {
					anexosList.add(novoAnexo);
					System.out.println(":: Arquivo adicionado: "+ novoAnexo.getNome() +" | Tamanho : "+ novoAnexo.getTamanho());
				} else {
					System.out.println(":: Anexo excede limite de 15 MB");
					Messages.addGlobalWarn( "Anexo excede limite de 15 MB");
					return;
				}
			} else {
				System.out.println(":: Arquivo já adicionado a lista");
				Messages.addGlobalWarn( "Arquivo já adicionado a lista");
			}
		} catch (Exception e1) {
			System.out.println(":: Falha ao salvar anexo");
			e1.printStackTrace();
		} 
	}
	
	public void removerAnexo(AnexoUpload rmvFile) {
		System.out.println(": Removendo arquivo da lista: " + rmvFile.getNome());
		try {
			anexosList.remove(rmvFile);
		} catch (Exception e) {
			System.out.println(":: Falha ao remover arquivo");
			e.printStackTrace();
		} 
		
	}
	
	public void limparAnexos(String diretorioAnexos) {
		FTPClient client = new FTPClient();
		FTPClientConfig conf = new FTPClientConfig(FTPClientConfig.SYST_UNIX);
		
		try {
			client.setControlEncoding("ISO-8859-1");
			
			client.connect("187.17.2.187", 21);
			client.configure(conf);
			
			client.enterLocalActiveMode();
			client.login("ftp_setc", "eB5QxROL");
			
			client.setBufferSize(1024 * 1024);
			client.setAutodetectUTF8(true);
			client.setFileType(FTP.BINARY_FILE_TYPE);
			
			client.changeWorkingDirectory("/SE-Ouv/" + GeralUtil.caminhoAnexos + diretorioAnexos);
			
			 if (client.isConnected()) {
				 FTPFile[] subFiles = client.listFiles();
				 
				 for (FTPFile aFile : subFiles) {
				 	if(aFile.isFile())
				 		client.deleteFile(aFile.getName());
	             }
		         
				 client.changeToParentDirectory();
				 client.removeDirectory(diretorioAnexos);
			 }
				 
			 anexosList.clear();
			 System.out.println(":: Lista de anexos esvaziada");
		} catch (Exception e) {
			System.out.println(":: Falha ao limpar lista de anexos");
			e.printStackTrace();
		} finally {
			try {
				client.logout();
				client.disconnect();
			} catch (IOException e1) {
				// do nothing
			}
		}
	}
	
	private String limiteNomeArquivo(String nome) {
		String nomeFinal = "";
		
		if(nome.length() > 150) {
			String extensao = StringUtils.substringAfterLast(nome, ".");
			System.out.println(extensao);
			
			nomeFinal = nome.substring(0, 145).concat(".").concat(extensao);
		} else {
			nomeFinal = nome;
		}
		
		System.out.println(" : Nome arquivo = " + nomeFinal);
		return nomeFinal;
	}
	
	public String anexoSizeMB(String size) {
		Double tamanho = Double.parseDouble(size);
		
		if (tamanho <= 10000) {
			return String.format("%.2f", (tamanho / 1024)).concat(" kB");
		} else {
			return String.format("%.2f", (tamanho / (1024 * 1024))).concat(" MB");
		}
		
	}
	
	public String gerarNomeDiretorio() {

	    String generatedString = RandomStringUtils.random(10, true, true);
	    
	    System.out.println("String gerada: " + generatedString);
	    
	    return generatedString;
	}
	
	private void testConexaoFTP() {
		try 
		{
			FTPClient client = new FTPClient();
			FTPClientConfig conf = new FTPClientConfig(FTPClientConfig.SYST_UNIX);
			
			client.connect("187.17.2.187", 21);
			client.configure(conf);
			
			client.enterLocalActiveMode();
			client.login("ftp_setc", "eB5QxROL");
			
			boolean answer = client.sendNoOp();
			if(answer){
				liberarAnexo = true;
			}
			else {
				System.out.println("Server connection failed!");
				liberarAnexo = false;
			}
		}
		catch (Exception e) 
		{
			System.out.println("Server connection is closed!");
			liberarAnexo = false;
		}
	}

	
	// =================
	// GETTERS E SETTERS
	// =================

	public Manifestacao getManifestacaoCadastro() {
		return manifestacaoCadastro;
	}

	public void setManifestacaoCadastro(Manifestacao manifestacaoCadastro) {
		this.manifestacaoCadastro = manifestacaoCadastro;
	}

	public Mensagem getMensagemCadastro() {
		return mensagemCadastro;
	}

	public void setMensagemCadastro(Mensagem mensagemCadastro) {
		this.mensagemCadastro = mensagemCadastro;
	}

	public String getPrimeiraPagina() {
		return primeiraPagina;
	}

	public void setPrimeiraPagina(String primeiraPagina) {
		this.primeiraPagina = primeiraPagina;
	}

	public Integer getIdTema() {
		return idTema;
	}

	public void setIdTema(Integer idTema) {
		this.idTema = idTema;
	}

	public Integer getIdEntidade() {
		return idEntidade;
	}

	public void setIdEntidade(Integer idEntidade) {
		this.idEntidade = idEntidade;
	}

	public List<Entidade> getListEntidades() {
		return listEntidades;
	}

	public void setListEntidades(List<Entidade> listEntidades) {
		this.listEntidades = listEntidades;
	}

	public List<Tema> getListTemasFiltrados() {
		return listTemasFiltrados;
	}

	public void setListTemasFiltrados(List<Tema> listTemasFiltrados) {
		this.listTemasFiltrados = listTemasFiltrados;
	}

	public UploadedFile getFile() {
		return file;
	}

	public void setFile(UploadedFile file) {
		this.file = file;
	}

	public Cidadao getCidadao() {
		return cidadao;
	}

	public void setCidadao(Cidadao cidadao) {
		this.cidadao = cidadao;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<AnexoUpload> getAnexosList() {
		return anexosList;
	}

	public void setAnexosList(List<AnexoUpload> anexosList) {
		this.anexosList = anexosList;
	}

	public boolean isRenderProtocoloAgrese() {
		return renderProtocoloAgrese;
	}

	public void setRenderProtocoloAgrese(boolean renderProtocoloAgrese) {
		this.renderProtocoloAgrese = renderProtocoloAgrese;
	}

	public boolean isLiberarAnexo() {
		return liberarAnexo;
	}

	public void setLiberarAnexo(boolean liberarAnexo) {
		this.liberarAnexo = liberarAnexo;
	}

}
