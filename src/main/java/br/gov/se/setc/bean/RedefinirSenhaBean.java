package br.gov.se.setc.bean;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Faces;
import org.omnifaces.util.FacesLocal;
import org.omnifaces.util.Messages;

import br.gov.se.setc.DAO.UsuarioDAO;
import br.gov.se.setc.model.Usuario;
import br.gov.se.setc.util.AutenticacaoUtil;
import br.gov.se.setc.util.CriptografiaUtil;
import br.gov.se.setc.util.EmailUtil;
import br.gov.se.setc.util.GeralUtil;

@Named
@ViewScoped
public class RedefinirSenhaBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private UsuarioDAO usuarioDAO = UsuarioDAO.getInstance();

	private String email;
	
	private String novaSenha;
	
	private boolean linkValido;
	
	private String key;
	
	@PostConstruct
	public void init() {
//		System.out.println(":: init redefinirSenhaBean ::");
		
		// Recupera acess_key da url
		try {
			
			Usuario usuarioLogado = (Usuario) GeralUtil.recuperarDaSessao("usuarioLogin");
			
			if(usuarioLogado == null) {
				System.out.println("-- Usuario logado não encontrado");
				HttpServletRequest request = (HttpServletRequest) FacesLocal.getRequest(Faces.getContext());
				
				key = request.getParameter("access_key");
				
				if(key != null && !key.isEmpty())
					linkValido = linkValido(key);
				else
					linkValido = false;
			} else {
				System.out.println("-- Usuario logado encontrado");
				key = usuarioLogado.getSessionId();
				linkValido = true;
			}
			
			
		} catch (Exception e) {
			System.out.println(":: Falha ao recuperar parâmetro (redefinirSenhaBean)");
			Messages.addGlobalWarn("Falha ao recuperar chave do link");
		}
		
	}
	
	private boolean linkValido(String key) {
		try {
			Usuario usuario = usuarioDAO.getUsuarioPorSession(key);
			
			if(usuario.equals(new Usuario())) {
//				System.out.println("Link para redefinição inválido (Usuário não encontrado)");
				return false;
			} else {
				Calendar hoje = Calendar.getInstance();
				Calendar limite = Calendar.getInstance();
				
				limite.setTime(hoje.getTime());
				limite.add(Calendar.DAY_OF_WEEK, +1);
				
				if(hoje.before(limite)) {
//					System.out.println("Link válido");
					return true;
				} else {
//					System.out.println("Link expirado");
					return false;
				}
			} 
		} catch (Exception e) {
			System.out.println("Link para redefinição inválido (Exception)");
			e.printStackTrace();
			return false;
		}
	}
	
	public String emailRedefinirSenha() {
		try {
			List<Usuario> cidadaos = usuarioDAO.listUsuariosPorEmailCidadao(email);
			List<Usuario> responsaveis = usuarioDAO.listUsuariosPorEmailResponsavel(email);
			
			if (cidadaos.size() > 0 || responsaveis.size() > 0) {
//				System.out.println("Enviando email de cidadão");
				for(Usuario usr : cidadaos) {
					usr.setLastLogged(new Date());
					usr.setSessionId(GeralUtil.generateSessionId());
					boolean salvou = usuarioDAO.saveOrUpdate(usr);
					
					if (salvou) {
						String link = AutenticacaoUtil.getEndereco()+"pagina/redefinirSenha.xhtml?access_key="+usr.getSessionId();
						System.out.println("	link: " + link);
						
						EmailUtil.enviarEmailRedefinicaoSenha(link, email, usr.getNome(), usr.getNick());
					}
				}
				
//				System.out.println("Enviando email de responsáveis");
				for(Usuario usr : responsaveis) {
					usr.setLastLogged(new Date());
					usr.setSessionId(GeralUtil.generateSessionId());
					boolean salvou = usuarioDAO.saveOrUpdate(usr);
					
					if (salvou) {
						String link = AutenticacaoUtil.getEndereco()+"pagina/redefinirSenha.xhtml?access_key="+usr.getSessionId();
						System.out.println("	link: " + link);
						
						EmailUtil.enviarEmailRedefinicaoSenha(link, email, usr.getNome(), usr.getNick());
					}
				}
				
				Messages.addGlobalInfo( "Email enviado com sucesso, verifique sua caixa de mensagens");
				return "/index.xhtml";
			} else {
//				System.out.println("Nenhum usuário encontrado para o email informado");
				Messages.addGlobalWarn("Nenhum usuário encontrado para o email informado");
				return null;
			}
			
		} catch (Exception e) {
			System.out.println("Falha ao recuperar usuários");
			e.printStackTrace();
			Messages.addGlobalError("Ocorreu um problema ao buscar usuário");
			return null;
		}
		
	}
	
	public String redefinirSenha() {
		try {
			Usuario usuario = usuarioDAO.getUsuarioPorSession(key);
			
			if(!usuario.isEmpty()) {
				String senhaCrip = CriptografiaUtil.Criptografar(novaSenha);
				
				usuario.setSenha(senhaCrip);
				boolean salvou = usuarioDAO.saveOrUpdate(usuario);
				
				if(salvou) {
					if(usuario.getPerfil() == 2) {
						EmailUtil.enviarEmailHTML(usuario.getResponsaveis().iterator().next().getEmail(), "Senha redefinida", "Sua senha para o usuário "+ usuario.getNick() +" foi alterada.");
					} else if (usuario.getPerfil() == 3) {
						EmailUtil.enviarEmailHTML(usuario.getCidadaos().iterator().next().getEmail(), "Senha redefinida", "Sua senha para o usuário "+ usuario.getNick() +" foi alterada.");
					}
					
					Messages.addGlobalInfo("Senha redefinida com sucesso");
					
					return "/index.xhtml?faces-redirect=true";
				} else {
					Messages.addGlobalError("Ocorreu um problema ao redefinir senha");
					return null;
				}
			} else {
				Messages.addGlobalError("Não foi possível identificar usuário");
				return null;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			Messages.addGlobalError("Ocorreu um problema ao redefinir senha");
			return null;
		}
		
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNovaSenha() {
		return novaSenha;
	}

	public void setNovaSenha(String novaSenha) {
		this.novaSenha = novaSenha;
	}

	public boolean isLinkValido() {
		return linkValido;
	}

	public void setLinkValido(boolean linkValido) {
		this.linkValido = linkValido;
	}

}
