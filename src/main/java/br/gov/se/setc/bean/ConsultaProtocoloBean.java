package br.gov.se.setc.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Messages;
import org.primefaces.PrimeFaces;

import br.gov.se.setc.DAO.ManifestacaoDAO;
import br.gov.se.setc.model.Manifestacao;
import br.gov.se.setc.model.Usuario;
import br.gov.se.setc.util.GeralUtil;

@Named
@ViewScoped
public class ConsultaProtocoloBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private ManifestacaoDAO manifestacaoDAO = ManifestacaoDAO.getInstance();
	
	private List<Manifestacao> listManifestacoes; 
	
	private Manifestacao maniDetalhe;
	
	private String campoBusca;
	private boolean buscarManifestante;
	
	private String perfilLogado;
	
	private Integer entidadeLogado;
	
	@PostConstruct
	public void init() {
//		System.out.println(":: init consultaProtocoloBean ::");
		
		listManifestacoes = new ArrayList<Manifestacao>();
		
		try {
			perfilLogado = (String) GeralUtil.recuperarDaSessao("perfilLogado");
			Usuario usuarioLogado = (Usuario) GeralUtil.recuperarDaSessao("usuarioLogin");
			
			entidadeLogado = usuarioLogado.getResponsaveis().iterator().next().getEntidade().getIdEntidade();
			
			if(perfilLogado != null && !perfilLogado.equals("logoff")) {
				
			} else {
				Messages.addGlobalWarn("Acesso Negado - Realize Login");
				FacesContext.getCurrentInstance().getExternalContext().redirect("/pagina/acessoNegado.xhtml");
			}
			
		} catch (Exception e) {
			System.out.println(":: Erro ao verificar pefil logado");
			e.printStackTrace();
		}
	}
	
	public void abrirManifestacao(Manifestacao manifestacao) {
		try {
			maniDetalhe = manifestacao;
			
			PrimeFaces.current().executeScript("PF('detalheDialog').show();");
		} catch(Exception e) {
			e.printStackTrace();
			System.out.println("Manifestação não carregada");
			Messages.addGlobalError("Falha ao carregar manifestação");
		}
	}
	
	public void buscarManifestacoes() {
		try {
			if(!buscarManifestante) {
				if(perfilLogado.equals("atendenteOGE")) {
					listManifestacoes = manifestacaoDAO.listPorProtocolo(campoBusca);
				} else if(perfilLogado.equals("atendenteOrgao")) {
					listManifestacoes = manifestacaoDAO.listPorProtocoloEntidade(campoBusca, entidadeLogado);
				}
			} else {
				if(perfilLogado.equals("atendenteOGE")) {
					listManifestacoes = manifestacaoDAO.listPorManifestante(campoBusca);
				} else if(perfilLogado.equals("atendenteOrgao")) {
					listManifestacoes = manifestacaoDAO.listPorManifestanteEntidade(campoBusca, entidadeLogado);
				}
			}
		} catch (Exception e) {
			System.out.println(":: Falha ao pesquisar manifestações por protocolo");
			e.printStackTrace();
		}
	}
	
	public String anexoSizeMB(String size) {
		Double tamanho = Double.parseDouble(size);
		
		if (tamanho <= 100000) {
			return String.format("%.2f", (tamanho / 1024)).concat(" kB");
		} else {
			return String.format("%.2f", (tamanho / (1024 * 1024))).concat(" MB");
		}
		
	}
	
	// =================
	// GETTERS E SETTERS
	// =================
	
	public List<Manifestacao> getListManifestacoes() {
		return listManifestacoes;
	}

	public void setListManifestacoes(List<Manifestacao> listManifestacoes) {
		this.listManifestacoes = listManifestacoes;
	}

	public Manifestacao getManiDetalhe() {
		return maniDetalhe;
	}

	public void setManiDetalhe(Manifestacao maniDetalhe) {
		this.maniDetalhe = maniDetalhe;
	}

	public String getCampoBusca() {
		return campoBusca;
	}

	public void setCampoBusca(String campoBusca) {
		this.campoBusca = campoBusca;
	}

	public boolean isBuscarManifestante() {
		return buscarManifestante;
	}

	public void setBuscarManifestante(boolean buscarManifestante) {
		this.buscarManifestante = buscarManifestante;
	}

}
