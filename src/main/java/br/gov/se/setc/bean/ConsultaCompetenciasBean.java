package br.gov.se.setc.bean;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Faces;
import org.omnifaces.util.FacesLocal;
import org.omnifaces.util.Messages;
import org.primefaces.PrimeFaces;

import br.gov.se.setc.DAO.CompetenciaDAO;
import br.gov.se.setc.DAO.EntidadeDAO;
import br.gov.se.setc.DAO.TemaDAO;
import br.gov.se.setc.model.Competencia;
import br.gov.se.setc.model.Entidade;
import br.gov.se.setc.model.Tema;
import br.gov.se.setc.model.Usuario;
import br.gov.se.setc.util.GeralUtil;

@Named
@ViewScoped
public class ConsultaCompetenciasBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private EntidadeDAO entidadeDAO = EntidadeDAO.getInstance();
	private CompetenciaDAO competenciaDAO = CompetenciaDAO.getInstance();
	private TemaDAO temaDAO = TemaDAO.getInstance();
	
	private Usuario usuarioLogado;
	private String perfilLogado;
	
	private String idEntidade;
	private Entidade entidade;
	
	private List<Competencia> listCompetenciasAtivas;
	private List<Competencia> listCompetenciasInativas;
	
	private Competencia editCompetencia;
	
	private List<Tema> listTemas;
	private Integer idTemaSelecionado;
	private Competencia novaCompetencia;
	
	private Tema novoTema;

	@PostConstruct
	public void init() {
//		System.out.println(":: init consultaCompetenciasBean ::");
		
		// Inicializar objetos
		novaCompetencia = new Competencia();
		novoTema = new Tema();
		
		
		// Seta Entidade a partir do idEntidade como parametro na URL
		try {
			HttpServletRequest request = (HttpServletRequest) FacesLocal.getRequest(Faces.getContext());
			idEntidade = request.getParameter("idEntidade");
			
			entidade = entidadeDAO.getEntidade(Integer.parseInt(idEntidade));
			
			System.out.println("Nome:" + entidade.getNome());
		} catch (Exception e) {
			System.out.println("Falha ao recuperar Entidade");
			Messages.addGlobalWarn("Ocorreu um problema", "Falha ao recuperar Entidade");
			PrimeFaces.current().ajax().update("growl");
			try {
				FacesLocal.redirect(Faces.getContext(), "../consulta/entidades.xhtml");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		
		// Verifica se perfil logado tem autorização na página e popula listas
		try {
			usuarioLogado = (Usuario) GeralUtil.recuperarDaSessao("usuarioLogin");
			perfilLogado = (String) GeralUtil.recuperarDaSessao("perfilLogado");
			
			if(perfilLogado != null && !perfilLogado.equals("logoff")) {
				if(perfilLogado.equals("ouvidorOrgao") || perfilLogado.equals("coordenadorOGE") || perfilLogado.equals("ouvidorGeral") || perfilLogado.equals("admin")) {
					listCompetenciasAtivas = competenciaDAO.listCompetenciasAtivas(Integer.parseInt(idEntidade));
					listCompetenciasInativas = competenciaDAO.listCompetenciasInativas(Integer.parseInt(idEntidade));
					
					listTemas = temaDAO.listTemas();
				} else {
					System.out.println("Acesso Negado");
					Messages.addGlobalWarn("Acesso Negado", "");
					PrimeFaces.current().ajax().update("growl");
					FacesLocal.redirect(Faces.getContext(), "../pagina/acessoNegado.xhtml");
				}
			} else {
				System.out.println("Acesso Negado");
				Messages.addGlobalWarn("Acesso Negado", "Realize login");
				FacesLocal.redirect(Faces.getContext(), "../index.xhtml");
			}
			
		} catch (Exception e) {
			System.out.println(":: Erro ao popular lista de entidades");
			e.printStackTrace();
		}
	}
	
	public void salvar() {
		try {
			if (idTemaSelecionado != null) {
				novaCompetencia.setTema(temaDAO.getTema(idTemaSelecionado));
				novaCompetencia.setEntidade(entidade);
				novaCompetencia.setAtiva(true);
				
				if(!perfilLogado.equals("admin")) {
					novaCompetencia.setAtualizado_em(GeralUtil.getDataAtual());
					novaCompetencia.setAtualizado_por(usuarioLogado.getResponsaveis().iterator().next());
				}
				
				boolean salvou = competenciaDAO.saveOrUpdate(novaCompetencia);
				
				if (salvou) {
					listCompetenciasAtivas.add(novaCompetencia);
					
					try {
						Tema tema = temaDAO.getTema(idTemaSelecionado);
						
						if(tema.getStatus().equalsIgnoreCase("Não-Vinculada")) {
							tema.setStatus("Vinculada");
							temaDAO.saveOrUpdate(tema);
						}
					} catch (Exception e) {
						e.printStackTrace();
						Messages.addGlobalWarn("Ocorreu um problema", "Falha ao atualizar tema");
					}
				}
			} else {
				Messages.addGlobalWarn("Ocorreu um problema", "Tema da competência não selecionado");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Messages.addGlobalWarn("Ocorreu um problema", "Falha ao salvar competência");
		} finally {
			novaCompetencia = new Competencia();
		}
		
	}
	
	public void abrirAtivarCompetencia(Competencia competencia) {
		try {
			editCompetencia = competencia;
			
			PrimeFaces.current().executeScript("PF('ativaDialog').show();");
		} catch (Exception e) {
			e.printStackTrace();
			Messages.addGlobalWarn("", "Falha ao abrir dialog");
		}
	}
	
	public void abrirInativarCompetencia(Competencia competencia) {
		try {
			editCompetencia = competencia;
			
			PrimeFaces.current().executeScript("PF('inativaDialog').show();");
		} catch (Exception e) {
			e.printStackTrace();
			Messages.addGlobalWarn("", "Falha ao abrir dialog");
		}
	}
	
	public void ativarCompetencia() {
		try {
			editCompetencia.setAtiva(true);
			
			if(!perfilLogado.equals("admin")) {
				editCompetencia.setAtualizado_em(GeralUtil.getDataAtual());
				editCompetencia.setAtualizado_por(usuarioLogado.getResponsaveis().iterator().next());
			}
			
			competenciaDAO.saveOrUpdate(editCompetencia);
			
			listCompetenciasAtivas.add(editCompetencia);
			listCompetenciasInativas.remove(editCompetencia);
			
		} catch (Exception e) {
			e.printStackTrace();
			Messages.addGlobalWarn("", "Falha ao ativar competência");
		} finally {
			editCompetencia = new Competencia();
		}
	}
	
	public void inativarCompetencia() {
		try {
			editCompetencia.setAtiva(false);
			
			if(!perfilLogado.equals("admin")) {
				editCompetencia.setAtualizado_em(GeralUtil.getDataAtual());
				editCompetencia.setAtualizado_por(usuarioLogado.getResponsaveis().iterator().next());
			}
			
			competenciaDAO.saveOrUpdate(editCompetencia);
			
			listCompetenciasInativas.add(editCompetencia);
			listCompetenciasAtivas.remove(editCompetencia);
			editCompetencia = new Competencia();
			
		} catch (Exception e) {
			e.printStackTrace();
			Messages.addGlobalWarn("", "Falha ao inativar competência");
		} finally {
			editCompetencia = new Competencia();
		}
	}
	
	public void salvarTema() {
		try {
			if(perfilLogado.equals("coordenadorOGE") || perfilLogado.equals("ouvidorGeral") || perfilLogado.equals("admin")) {
				if(!listTemas.stream().anyMatch(o-> o.getTitulo().equals(novoTema.getTitulo()))) {
					novoTema.setStatus("Não-vinculada");
					boolean salvou = temaDAO.saveOrUpdate(novoTema);
					
					if(salvou) listTemas.add(novoTema);
				} else {
					Messages.addGlobalWarn("Falha no cadastro do Tema", "Tema '"+ novoTema.getTitulo() + "' já cadastrado no sistema.");
				}
			} else {
				Messages.addGlobalWarn("", "Usuário sem permissão");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			novoTema = new Tema();
		}
	}
	
	public void solicitarTema() {
		try {
			if(perfilLogado.equals("ouvidorOrgao")) {
				if(!listTemas.stream().anyMatch(o-> o.getTitulo().equals(novoTema.getTitulo()))) {
					novoTema.setStatus("Pendente");
					boolean salvou = temaDAO.saveOrUpdate(novoTema);
					
					if (salvou) {
						Competencia competencia = new Competencia();
						competencia.setTema(novoTema);
						competencia.setEntidade(entidade);
						competencia.setAtiva(false);
						
						competencia.setAtualizado_em(GeralUtil.getDataAtual());
						competencia.setAtualizado_por(usuarioLogado.getResponsaveis().iterator().next());
						
						competenciaDAO.saveOrUpdate(competencia);
					}
				} else {
					Messages.addGlobalWarn("Falha no cadastro do Tema", "Tema '"+ novoTema.getTitulo() + "' já cadastrado no sistema.");
				}
			} else {
				Messages.addGlobalWarn("Usuário sem permissão");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			novoTema = new Tema();
		}
	}
	
	

	public String getIdEntidade() {
		return idEntidade;
	}

	public void setIdEntidade(String idEntidade) {
		this.idEntidade = idEntidade;
	}

	public Entidade getEntidade() {
		return entidade;
	}

	public void setEntidade(Entidade entidade) {
		this.entidade = entidade;
	}

	public List<Competencia> getListCompetenciasAtivas() {
		return listCompetenciasAtivas;
	}

	public void setListCompetenciasAtivas(List<Competencia> listCompetenciasAtivas) {
		this.listCompetenciasAtivas = listCompetenciasAtivas;
	}

	public List<Competencia> getListCompetenciasInativas() {
		return listCompetenciasInativas;
	}

	public void setListCompetenciasInativas(List<Competencia> listCompetenciasInativas) {
		this.listCompetenciasInativas = listCompetenciasInativas;
	}

	public Competencia getEditCompetencia() {
		return editCompetencia;
	}

	public void setEditCompetencia(Competencia editCompetencia) {
		this.editCompetencia = editCompetencia;
	}

	public Competencia getNovaCompetencia() {
		return novaCompetencia;
	}

	public void setNovaCompetencia(Competencia novaCompetencia) {
		this.novaCompetencia = novaCompetencia;
	}

	public Integer getIdTemaSelecionado() {
		return idTemaSelecionado;
	}

	public void setIdTemaSelecionado(Integer idTemaSelecionado) {
		this.idTemaSelecionado = idTemaSelecionado;
	}

	public List<Tema> getListTemas() {
		return listTemas;
	}

	public void setListTemas(List<Tema> listTemas) {
		this.listTemas = listTemas;
	}

	public Tema getNovoTema() {
		return novoTema;
	}

	public void setNovoTema(Tema novoTema) {
		this.novoTema = novoTema;
	}
	
	

}
