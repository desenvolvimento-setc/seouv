package br.gov.se.setc.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Named;

import org.omnifaces.cdi.ViewScoped;
import org.omnifaces.util.Faces;
import org.omnifaces.util.FacesLocal;
import org.omnifaces.util.Messages;
import org.primefaces.PrimeFaces;

import br.gov.se.setc.DAO.EntidadeDAO;
import br.gov.se.setc.DAO.ResponsavelDAO;
import br.gov.se.setc.model.Entidade;
import br.gov.se.setc.model.Responsavel;
import br.gov.se.setc.util.GeralUtil;

@Named
@ViewScoped
public class ConsultaResponsaveisBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private ResponsavelDAO responsavelDAO = ResponsavelDAO.getInstance();
	private EntidadeDAO entidadeDAO = EntidadeDAO.getInstance();
	
	private String perfilLogado;
	
	private List<Responsavel> listResponsaveis;
	private List<Responsavel> listResponsaveisFiltro;
	
	private Responsavel novoResponsavel;
	private Responsavel editResponsavel;
	private Integer idEntidade;
	
	private List<Entidade> listEntidadesAtivas;
	
	@PostConstruct
	public void init() {
//		System.out.println(":: init consultaCompetenciasBean ::");
		
		// Inicializar objetos
		novoResponsavel = new Responsavel();
		editResponsavel = new Responsavel();
		
		// Verifica se perfil logado tem autorização na página e popula listas
		try {
			perfilLogado = (String) GeralUtil.recuperarDaSessao("perfilLogado");
			
			if(perfilLogado != null && !perfilLogado.equals("logoff")) {
				if(perfilLogado.equals("coordenadorOGE") || perfilLogado.equals("ouvidorGeral") || perfilLogado.equals("admin")) {
					listResponsaveis = responsavelDAO.listResponsaveis();
					listEntidadesAtivas = entidadeDAO.listEntidadesAtivas();
				} else {
					System.out.println("Acesso Negado");
					Messages.addGlobalWarn("Acesso Negado, realize login");
					PrimeFaces.current().ajax().update("growl");
					FacesLocal.redirect(Faces.getContext(), "/pagina/acessoNegado.xhtml");
				}
			} else {
				System.out.println("Acesso Negado");
				Messages.addGlobalWarn("Acesso Negado, realize login");
				FacesLocal.redirect(Faces.getContext(), "/index.xhtml");
			}
			
		} catch (Exception e) {
			System.out.println(":: Erro ao recuperar lista de entidades");
			e.printStackTrace();
		}
	}
	
	
	public void editar() {
		try {
			if(perfilLogado.equals("coordenadorOGE") || perfilLogado.equals("ouvidorGeral") || perfilLogado.equals("admin")) {
				
				if(idEntidade != editResponsavel.getEntidade().getIdEntidade()) {
					editResponsavel.setEntidade(entidadeDAO.getEntidade(idEntidade));
				}
				
				responsavelDAO.saveOrUpdate(editResponsavel);
			} else {
				Messages.addGlobalWarn("Usuário sem permissão");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Messages.addGlobalError("Falha ao cadastrar responsável");
		} finally {
			editResponsavel = new Responsavel();
			idEntidade = 0;
		}
	}


	public void abrirEditarResponsavel(Responsavel responsavel) {
		try {
			editResponsavel = responsavel;
			idEntidade = responsavel.getEntidade().getIdEntidade();
			
			PrimeFaces.current().executeScript("PF('editDialog').show();");
		} catch (Exception e) {
			e.printStackTrace();
			Messages.addGlobalWarn("Falha ao abrir dialog");
		}
	}


	public Responsavel getNovoResponsavel() {
		return novoResponsavel;
	}


	public void setNovoResponsavel(Responsavel novoResponsavel) {
		this.novoResponsavel = novoResponsavel;
	}


	public Responsavel getEditResponsavel() {
		return editResponsavel;
	}


	public void setEditResponsavel(Responsavel editResposavel) {
		this.editResponsavel = editResposavel;
	}


	public List<Responsavel> getListResponsaveis() {
		return listResponsaveis;
	}


	public void setListResponsaveis(List<Responsavel> listResponsaveis) {
		this.listResponsaveis = listResponsaveis;
	}


	public Integer getIdEntidade() {
		return idEntidade;
	}


	public void setIdEntidade(Integer idEntidade) {
		this.idEntidade = idEntidade;
	}


	public List<Entidade> getListEntidadesAtivas() {
		return listEntidadesAtivas;
	}


	public void setListEntidadesAtivas(List<Entidade> listEntidadesAtivas) {
		this.listEntidadesAtivas = listEntidadesAtivas;
	}


	public List<Responsavel> getListResponsaveisFiltro() {
		return listResponsaveisFiltro;
	}


	public void setListResponsaveisFiltro(List<Responsavel> listResponsaveisFiltro) {
		this.listResponsaveisFiltro = listResponsaveisFiltro;
	}
	
	

}
