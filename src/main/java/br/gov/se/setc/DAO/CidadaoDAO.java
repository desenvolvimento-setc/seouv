package br.gov.se.setc.DAO;

import javax.persistence.EntityManager;

import br.gov.se.setc.model.Cidadao;
import br.gov.se.setc.util.HibernateUtil;

public class CidadaoDAO {
	
	private static CidadaoDAO instance;

	private CidadaoDAO(){}

	public static CidadaoDAO getInstance(){
		if( instance == null )
			synchronized( CidadaoDAO.class ){
				if( instance == null )
					instance = new CidadaoDAO();
			}

		return instance;
	}
	
	private static EntityManager em = HibernateUtil.getEntityManager();
	
	public void saveOrUpdate(Cidadao cidadao) {
	}
	
	public Cidadao getCidadao(Integer id) {
		try {
			Cidadao cidadao = em.find(Cidadao.class, id);
			
			return cidadao;
		} catch (Exception e) {
			System.out.println(":: Falha ao pesquisar Cidadão");
			e.getCause().getStackTrace();
			e.printStackTrace();
			
			return null;
		}
	}
	
	
}
