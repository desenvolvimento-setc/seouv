package br.gov.se.setc.DAO;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import br.gov.se.setc.util.GeralUtil;
import br.gov.se.setc.util.HibernateUtil;

public class RelatorioDAO {
	
	private static RelatorioDAO instance;

	private RelatorioDAO(){}

	public static RelatorioDAO getInstance(){
		if( instance == null )
			synchronized( RelatorioDAO.class ){
				if( instance == null )
					instance = new RelatorioDAO();
			}

		return instance;
	}

	private static EntityManager em = HibernateUtil.getEntityManager();

	@SuppressWarnings("unchecked")
	public List<Integer> listAnos() {
		List<Integer> list = new ArrayList<Integer>();
		
		String query = "SELECT DISTINCT EXTRACT(YEAR FROM mani.dataIni) FROM Manifestacao as mani ORDER BY EXTRACT(YEAR FROM mani.dataIni) ASC";
		
		list  = em.createQuery(query).getResultList();
		
		return list;
	}

//	=====================================
//	===== RELATÓRIO OUVIDORIA GERAL =====
//	=====================================
	
	@SuppressWarnings("unchecked")
	public List<String[]> getManifPorTipo(String dataIni, String dataFim) {

		List<String[]> manifPorTipo = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		try {
			String query = "SELECT " + 
					"sol.tipo as Tipo, " + 
					"count(sol.idSolicitacao) as Manifestações " + 
					"FROM " + 
					"solicitacao sol " + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim " + 
					"GROUP by " + 
					"sol.tipo ORDER BY count(sol.idSolicitacao) DESC";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.getResultList();
			
			String[] temp1 = new String[manifestacoes.size()]; 
			String[] temp2 = new String[manifestacoes.size()];
			
			for(int i = 0; i < manifestacoes.size(); i++) {
				temp1[i] = manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[1].toString();
			}
			
			manifPorTipo.add(temp1);
			manifPorTipo.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
			
		return manifPorTipo;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getManifPorMes(String dataIni, String dataFim) {

		List<String[]> manifPorMes = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		try {
			String query = "Select b.Ano, b.Mes, IFNULL(a.Manifestacoes, 0) as Manifestações from ( " + 
					"SELECT " + 
					"YEAR(sol.dataIni) as Ano, " + 
					"MONTH(sol.dataIni) as Mes, " + 
					"count(sol.idSolicitacao) as Manifestacoes " + 
					"FROM " + 
					"solicitacao sol " + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim " + 
					"GROUP by " + 
					"YEAR(sol.dataIni), MONTH(sol.dataIni) " + 
					") a " + 
					"RIGHT JOIN " + 
					"(select " + 
					"DATE_FORMAT(m1, '%Y') Ano, " + 
					"DATE_FORMAT(m1, '%m') Mes, " + 
					"0 as Manifestações " + 
					"from " + 
					"( " + 
					"select " + 
					"(:dataIni - INTERVAL DAYOFMONTH(:dataIni)-1 DAY) + INTERVAL m MONTH as m1 " + 
					"from " + 
					"( " + 
					"select " + 
					"@rownum \\:= @rownum + 1 as m " + 
					"from " + 
					"(select 1 union select 2 union select 3 union select 4) t1, " + 
					"(select 1 union select 2 union select 3 union select 4) t2, " + 
					"(select 1 union select 2 union select 3 union select 4) t3, " + 
					"(select 1 union select 2 union select 3 union select 4) t4, " + 
					"(select @rownum\\:=-1) t0 " + 
					") d1 " + 
					") d2 " + 
					"where " + 
					"m1 <= :dataFim " + 
					"order by " + 
					"m1) b on a.Ano = b.Ano and a.Mes = b.Mes ";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.getResultList();
			
			String[] temp = new String[manifestacoes.size()];
			String[] temp2 = new String[manifestacoes.size()];
			
			for (int i = 0; i < manifestacoes.size(); i++) {
				temp[i] = GeralUtil.meses[Integer.parseInt(manifestacoes.get(i)[1].toString())] + " " + manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[2].toString();
			}
			
			manifPorMes.add(temp);
			manifPorMes.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return manifPorMes;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getManifPorTema(String dataIni, String dataFim) {

		List<String[]> manifPorTema = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		try {
			String query = "SELECT " + 
					"tema.titulo as 'Órgão', " + 
					"count(sol.idSolicitacao) as Manifestações " + 
					"FROM " + 
					"solicitacao sol " + 
					"INNER JOIN acoes tema on tema.idAcoes = sol.idAcao " + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim " + 
					"GROUP by " + 
					"sol.idAcao " + 
					"ORDER BY Manifestações desc, tema.titulo asc " + 
					"LIMIT 5";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.getResultList();
			
			String[] temp = new String[manifestacoes.size()];
			String[] temp2 = new String[manifestacoes.size()];
			
			for (int i = 0; i < manifestacoes.size(); i++) {
				temp[i] = manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[1].toString();
			}
			
			manifPorTema.add(temp);
			manifPorTema.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return manifPorTema;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getManifPorSituacao(String dataIni, String dataFim) {

		List<String[]> manifPorSituacao = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		try {
			String query = "SELECT 'Atendidas' as 'Status', COUNT(distinct sol.idSolicitacao) as 'Manifestações'\r\n" + 
					"	FROM solicitacao sol\r\n" + 
					"	where sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"	AND (sol.status like 'Atendida' or sol.status like 'Finalizada')\r\n" + 
					"		UNION\r\n" + 
					"SELECT 'Sem resposta', COUNT(distinct sol.idSolicitacao)\r\n" + 
					"	FROM solicitacao sol\r\n" + 
					"	where sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"	AND (sol.status like 'Sem resposta' or sol.status like 'Negada')\r\n" + 
					"		UNION\r\n" + 
					"SELECT 'Em trâmite', COUNT(distinct sol.idSolicitacao)\r\n" + 
					"	FROM solicitacao as sol\r\n" + 
					"	where sol.dataIni BETWEEN :dataIni AND :dataFim \r\n" + 
					"	AND sol.visualizada = 1\r\n" + 
					"	AND (sol.status like 'Aberta' or sol.status like 'Reencaminhada' or sol.status like 'Prorrogada' or sol.status like 'Recurso' or sol.status like 'Reformulação' or sol.status like 'Tramitando')\r\n" + 
					"		UNION\r\n" + 
					"SELECT 'Não visualizadas', COUNT(distinct sol.idSolicitacao) \r\n" + 
					"	FROM solicitacao as sol\r\n" + 
					"	where sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"	AND sol.visualizada = 0 AND sol.status like 'Aberta'";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.getResultList();
			
			String[] temp = new String[manifestacoes.size()];
			String[] temp2 = new String[manifestacoes.size()];
			
			for (int i = 0; i < manifestacoes.size(); i++) {
				temp[i] = manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[1].toString();
			}
			
			manifPorSituacao.add(temp);
			manifPorSituacao.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return manifPorSituacao;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getManifPorOrgao(String dataIni, String dataFim) {

		List<String[]> manifPorOrgao = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		try {
			String query = "SELECT\r\n" + 
					"	ent.sigla as 'Órgão',\r\n" + 
					"	count(sol.idSolicitacao) as Manifestações\r\n" + 
					"FROM\r\n" + 
					"	solicitacao sol\r\n" + 
					"INNER JOIN entidades ent on ent.idEntidades = sol.idEntidades\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"GROUP by\r\n" + 
					"	sol.idEntidades\r\n" + 
					"ORDER BY Manifestações desc, ent.sigla asc\r\n" + 
					"LIMIT 5";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.getResultList();
			
			String[] temp = new String[manifestacoes.size()];
			String[] temp2 = new String[manifestacoes.size()];
			
			for (int i = 0; i < manifestacoes.size(); i++) {
				temp[i] = manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[1].toString();
			}
			
			manifPorOrgao.add(temp);
			manifPorOrgao.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return manifPorOrgao;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getManifPorCanal(String dataIni, String dataFim) {

		List<String[]> manifPorCanal = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		try {
			String query = "SELECT 'Sistema' as 'Canal', COUNT(distinct sol.idSolicitacao) as 'Manifestações' " + 
					"FROM solicitacao sol " + 
					"where sol.dataIni BETWEEN :dataIni AND :dataFim " + 
					"AND sol.canalEntrada = 0 " + 
					"UNION " + 
					"SELECT 'Telefone', COUNT(distinct sol.idSolicitacao) " + 
					"FROM solicitacao sol " + 
					"where sol.dataIni BETWEEN :dataIni AND :dataFim " + 
					"AND sol.canalEntrada = 1 " + 
					"UNION " + 
					"SELECT 'Presencial', COUNT(distinct sol.idSolicitacao) " + 
					"FROM solicitacao as sol " + 
					"where sol.dataIni BETWEEN :dataIni AND :dataFim " + 
					"AND sol.canalEntrada = 2 " + 
					"UNION " + 
					"SELECT 'Postal', COUNT(distinct sol.idSolicitacao) " + 
					"FROM solicitacao as sol " + 
					"where sol.dataIni BETWEEN :dataIni AND :dataFim " + 
					"AND sol.canalEntrada = 3 ";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.getResultList();
			
			String[] temp = new String[manifestacoes.size()];
			String[] temp2 = new String[manifestacoes.size()];
			
			for (int i = 0; i < manifestacoes.size(); i++) {
				temp[i] = manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[1].toString();
			}
			
			manifPorCanal.add(temp);
			manifPorCanal.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return manifPorCanal;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getCidPorFaixaEtaria(String dataIni, String dataFim) {

		List<String[]> cidPorFaixa = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		try {
			String query = "SELECT '0 a 18 ANOS' as Faixa ,COUNT(*) as Quantidade\r\n" + 
					"FROM \r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, TIMESTAMPDIFF(YEAR,cid.datanasc,CURDATE()) Idade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim) temp\r\n" + 
					"WHERE Idade BETWEEN 0 AND 18\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT '19 a 25 ANOS' as Faixa ,COUNT(*) as Quantidade\r\n" + 
					"FROM \r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, TIMESTAMPDIFF(YEAR,cid.datanasc,CURDATE()) Idade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim) temp\r\n" + 
					"WHERE Idade BETWEEN 19 AND 25\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT '26 a 40 ANOS' as Faixa ,COUNT(*) as Quantidade\r\n" + 
					"FROM \r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, TIMESTAMPDIFF(YEAR,cid.datanasc,CURDATE()) Idade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim) temp\r\n" + 
					"WHERE Idade BETWEEN 26 AND 40\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT '41 a 60 ANOS' as Faixa ,COUNT(*) as Quantidade\r\n" + 
					"FROM \r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, TIMESTAMPDIFF(YEAR,cid.datanasc,CURDATE()) Idade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim) temp\r\n" + 
					"WHERE Idade BETWEEN 41 AND 60\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Acima de 60 ANOS' as Faixa ,COUNT(*) as Quantidade\r\n" + 
					"FROM \r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, TIMESTAMPDIFF(YEAR,cid.datanasc,CURDATE()) Idade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim) temp\r\n" + 
					"WHERE Idade >= 61\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Não Informaram' as Faixa ,COUNT(*) as Quantidade\r\n" + 
					"FROM \r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, TIMESTAMPDIFF(YEAR,cid.datanasc,CURDATE()) Idade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim) temp\r\n" + 
					"WHERE Idade IS NULL OR Idade < 0\r\n";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.getResultList();
			
			String[] temp1 = new String[manifestacoes.size()]; 
			String[] temp2 = new String[manifestacoes.size()];
			
			for(int i = 0; i < manifestacoes.size(); i++) {
				temp1[i] = manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[1].toString();
			}
			
			cidPorFaixa.add(temp1);
			cidPorFaixa.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cidPorFaixa;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getCidPorRenda(String dataIni, String dataFim) {

		List<String[]> cidPorRenda = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		try {
			String query = "SELECT 'Sem renda' as Categoria ,COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.renda\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim) temp\r\n" + 
					"WHERE renda = 0\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Até 2 SM' as Categoria ,COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.renda\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim) temp\r\n" + 
					"WHERE renda = 1\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT '2 a 4 SM' as Categoria ,COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.renda\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim) temp\r\n" + 
					"WHERE renda = 2\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT '4 a 10 SM' as Categoria ,COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.renda\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim) temp\r\n" + 
					"WHERE renda = 3\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT '10 a 20 SM' as Categoria ,COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.renda\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim) temp\r\n" + 
					"WHERE renda = 4\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Acima de 20 SM' as Categoria ,COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.renda\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim) temp\r\n" + 
					"WHERE renda = 5";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.getResultList();
			
			String[] temp1 = new String[manifestacoes.size()]; 
			String[] temp2 = new String[manifestacoes.size()];
			
			for(int i = 0; i < manifestacoes.size(); i++) {
				temp1[i] = manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[1].toString();
			}
			
			cidPorRenda.add(temp1);
			cidPorRenda.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cidPorRenda;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getCidPorSexo(String dataIni, String dataFim) {

		List<String[]> cidPorSexo = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		try {
			String query = "SELECT 'Masculino' as Sexo, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.sexo\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim) temp\r\n" + 
					"WHERE sexo = 'M'\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Feminino' as Sexo, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.sexo\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim) temp\r\n" + 
					"WHERE sexo = 'F'\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Outro' as Sexo, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.sexo\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim) temp\r\n" + 
					"WHERE sexo = 'O'\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Não Informado' as Sexo, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.sexo\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim) temp\r\n" + 
					"WHERE sexo = '' OR sexo IS NULL";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.getResultList();
			
			String[] temp1 = new String[manifestacoes.size()]; 
			String[] temp2 = new String[manifestacoes.size()];
			
			for(int i = 0; i < manifestacoes.size(); i++) {
				temp1[i] = manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[1].toString();
			}
			
			cidPorSexo.add(temp1);
			cidPorSexo.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cidPorSexo;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getCidPorEscolaridade(String dataIni, String dataFim) {

		List<String[]> cidPorEscolaridade = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		try {
			String query = "SELECT 'Analfabeto' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim) temp\r\n" + 
					"WHERE escolaridade = 1\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Semi-Analfabeto' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim) temp\r\n" + 
					"WHERE escolaridade = 2\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Fundamental' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim) temp\r\n" + 
					"WHERE escolaridade = 3\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Ensino Médio Incompleto' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim) temp\r\n" + 
					"WHERE escolaridade = 4\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Ensino Médio Completo' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim) temp\r\n" + 
					"WHERE escolaridade = 5\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Ensino Superior Incompleto' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim) temp\r\n" + 
					"WHERE escolaridade = 6\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Ensino Superior Completo' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim) temp\r\n" + 
					"WHERE escolaridade = 7\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Não Informado' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim) temp\r\n" + 
					"WHERE escolaridade IS NULL";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.getResultList();
			
			String[] temp1 = new String[manifestacoes.size()]; 
			String[] temp2 = new String[manifestacoes.size()];
			
			for(int i = 0; i < manifestacoes.size(); i++) {
				temp1[i] = manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[1].toString();
			}
			
			cidPorEscolaridade.add(temp1);
			cidPorEscolaridade.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cidPorEscolaridade;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getCidPorEstado(String dataIni, String dataFim) {

		List<String[]> cidPorEstado = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		try {
			String query = "SELECT IF(cid.estado=\"\", 'Não Informado', cid.estado) Estado, COUNT(*) Quantidade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"GROUP by cid.estado\r\n" + 
					"ORDER BY Quantidade DESC\r\n" + 
					"LIMIT 5";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.getResultList();
			
			String[] temp1 = new String[manifestacoes.size()]; 
			String[] temp2 = new String[manifestacoes.size()];
			
			for(int i = 0; i < manifestacoes.size(); i++) {
				temp1[i] = manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[1].toString();
			}
			
			cidPorEstado.add(temp1);
			cidPorEstado.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cidPorEstado;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getManifPorAno(String dataIni, String dataFim) {

		List<String[]> manifPorAno = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		try {
			String query = "SELECT\r\n" + 
					"	YEAR(sol.dataIni) as Ano,\r\n" + 
					"	count(sol.idSolicitacao) as Manifestações\r\n" + 
					"FROM\r\n" + 
					"	solicitacao sol\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"GROUP by\r\n" + 
					"	YEAR(sol.dataIni)";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.getResultList();
			
			String[] temp = new String[manifestacoes.size()];
			String[] temp2 = new String[manifestacoes.size()];
			
			for (int i = 0; i < manifestacoes.size(); i++) {
				temp[i] = manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[1].toString();
			}
			
			manifPorAno.add(temp);
			manifPorAno.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return manifPorAno;
	}

	
//	============================================
//	===== RELATÓRIO OUVIDORIA POR ENTIDADE =====
//	============================================
	
	@SuppressWarnings("unchecked")
	public List<String[]> getManifPorTipoEntidade(String dataIni, String dataFim, Integer idEntidade) {

		List<String[]> manifPorTipo = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		try {
			
			String query = "SELECT " + 
					"sol.tipo as Tipo, " + 
					"count(sol.idSolicitacao) as Manifestações " + 
					"FROM " + 
					"solicitacao sol " + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim " + 
					"AND sol.idEntidades = :idEntidade " + 
					"GROUP by " + 
					"sol.tipo " + 
					"ORDER BY count(sol.idSolicitacao) DESC";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.setParameter("idEntidade", idEntidade)
					.getResultList();
			
			String[] temp = new String[manifestacoes.size()];
			String[] temp2 = new String[manifestacoes.size()];
			
			for (int i = 0; i < manifestacoes.size(); i++) {
				temp[i] = manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[1].toString();
			}
			
			manifPorTipo.add(temp);
			manifPorTipo.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return manifPorTipo;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getManifPorMesEntidade(String dataIni, String dataFim, Integer idEntidade) {

		List<String[]> manifPorMes = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		String query = "Select b.Ano, b.Mes, IFNULL(a.Manifestacoes, 0) as Manifestações from ( " + 
				"SELECT " + 
				"YEAR(sol.dataIni) as Ano, " + 
				"MONTH(sol.dataIni) as Mes, " + 
				"count(sol.idSolicitacao) as Manifestacoes " + 
				"FROM " + 
				"solicitacao sol " + 
				"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim " + 
				"AND sol.idEntidades = :idEntidade " + 
				"GROUP by " + 
				"YEAR(sol.dataIni), MONTH(sol.dataIni) " + 
				") a " + 
				"RIGHT JOIN " + 
				"(select " + 
				"DATE_FORMAT(m1, '%Y') Ano, " + 
				"DATE_FORMAT(m1, '%m') Mes, " + 
				"0 as Manifestações " + 
				"from " + 
				"( " + 
				"select " + 
				"(:dataIni - INTERVAL DAYOFMONTH(:dataIni)-1 DAY) + INTERVAL m MONTH as m1 " + 
				"from " + 
				"( " + 
				"select " + 
				"@rownum \\:= @rownum + 1 as m " + 
				"from " + 
				"(select 1 union select 2 union select 3 union select 4) t1, " + 
				"(select 1 union select 2 union select 3 union select 4) t2, " + 
				"(select 1 union select 2 union select 3 union select 4) t3, " + 
				"(select 1 union select 2 union select 3 union select 4) t4, " + 
				"(select @rownum\\:=-1) t0 " + 
				") d1 " + 
				") d2 " + 
				"where " + 
				"m1 <= :dataFim " + 
				"order by " + 
				"m1) b on a.Ano = b.Ano and a.Mes = b.Mes ";
		
		manifestacoes = (List<Object[]>) em
				.createNativeQuery(query)
				.setParameter("dataIni", dataIni+" 00:00:00")
				.setParameter("dataFim", dataFim+" 23:59:59")
				.setParameter("idEntidade", idEntidade)
				.getResultList();
		
		String[] temp = new String[manifestacoes.size()];
		String[] temp2 = new String[manifestacoes.size()];
		
		for (int i = 0; i < manifestacoes.size(); i++) {
			temp[i] = GeralUtil.meses[Integer.parseInt(manifestacoes.get(i)[1].toString())] + " " + manifestacoes.get(i)[0].toString();
			temp2[i] = manifestacoes.get(i)[2].toString();
		}
		
		manifPorMes.add(temp);
		manifPorMes.add(temp2);
		
		return manifPorMes;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<String[]> getManifPorTemaEntidade(String dataIni, String dataFim, Integer idEntidade) {

		List<String[]> manifPorTema = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		String query = "SELECT " + 
				"tema.titulo as 'Órgão', " + 
				"count(sol.idSolicitacao) as Manifestações " + 
				"FROM " + 
				"solicitacao sol " + 
				"INNER JOIN acoes tema on tema.idAcoes = sol.idAcao " + 
				"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim " +
				"AND sol.idEntidades = :idEntidade " +
				"GROUP by " + 
				"sol.idAcao " + 
				"ORDER BY Manifestações desc, tema.titulo asc " + 
				"LIMIT 5";
		
		manifestacoes = (List<Object[]>) em
				.createNativeQuery(query)
				.setParameter("dataIni", dataIni+" 00:00:00")
				.setParameter("dataFim", dataFim+" 23:59:59")
				.setParameter("idEntidade", idEntidade)
				.getResultList();
		
		String[] temp = new String[manifestacoes.size()];
		String[] temp2 = new String[manifestacoes.size()];
		
		for (int i = 0; i < manifestacoes.size(); i++) {
			temp[i] = manifestacoes.get(i)[0].toString();
			temp2[i] = manifestacoes.get(i)[1].toString();
		}
		
		manifPorTema.add(temp);
		manifPorTema.add(temp2);
		
		return manifPorTema;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getManifPorSituacaoEntidade(String dataIni, String dataFim, Integer idEntidade) {

		List<String[]> manifPorSituacao = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		String query = "SELECT * \r\n" + 
				"FROM\r\n" + 
				"\r\n" + 
				"(SELECT 'Atendidas' as 'Status', COUNT(distinct sol.idSolicitacao) as 'Manifestações'\r\n" + 
				"	FROM solicitacao sol\r\n" + 
				"	where sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
				"	AND (sol.status like 'Atendida' or sol.status like 'Finalizada')\r\n" + 
				"	AND sol.idEntidades = :idEntidade\r\n" + 
				"		UNION\r\n" + 
				"SELECT 'Sem resposta', COUNT(distinct sol.idSolicitacao)\r\n" + 
				"	FROM solicitacao sol\r\n" + 
				"	where sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
				"	AND (sol.status like 'Sem resposta' or sol.status like 'Negada')\r\n" + 
				"	AND sol.idEntidades = :idEntidade\r\n" + 
				"		UNION\r\n" + 
				"SELECT 'Em trâmite', COUNT(distinct sol.idSolicitacao)\r\n" + 
				"	FROM solicitacao as sol\r\n" + 
				"	where sol.dataIni BETWEEN :dataIni AND :dataFim \r\n" + 
				"	AND sol.visualizada = 1\r\n" + 
				"	AND (sol.status like 'Aberta' or sol.status like 'Reencaminhada' or sol.status like 'Prorrogada' or sol.status like 'Recurso' or sol.status like 'Reformulação' or sol.status like 'Tramitando')\r\n" + 
				"	AND sol.idEntidades = :idEntidade\r\n" + 
				"		UNION\r\n" + 
				"SELECT 'Não visualizadas', COUNT(distinct sol.idSolicitacao) \r\n" + 
				"	FROM solicitacao as sol\r\n" + 
				"	where sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
				"	AND sol.visualizada = 0 AND sol.status like 'Aberta'\r\n" + 
				"	AND sol.idEntidades = :idEntidade) temp\r\n" + 
				"	\r\n" + 
				"ORDER BY Manifestações DESC";
		
		manifestacoes = (List<Object[]>) em
				.createNativeQuery(query)
				.setParameter("dataIni", dataIni+" 00:00:00")
				.setParameter("dataFim", dataFim+" 23:59:59")
				.setParameter("idEntidade", idEntidade)
				.getResultList();
		
		String[] temp = new String[manifestacoes.size()];
		String[] temp2 = new String[manifestacoes.size()];
		
		for (int i = 0; i < manifestacoes.size(); i++) {
			temp[i] = manifestacoes.get(i)[0].toString();
			temp2[i] = manifestacoes.get(i)[1].toString();
		}
		manifPorSituacao.add(temp);
		manifPorSituacao.add(temp2);
		
		return manifPorSituacao;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<String[]> getManifPorOrgaoEntidade(String dataIni, String dataFim, Integer idEntidade) {

		List<String[]> manifPorOrgao = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		String query = "SELECT\r\n" + 
				"	ent.sigla as 'Órgão',\r\n" + 
				"	count(sol.idSolicitacao) as Manifestações\r\n" + 
				"FROM\r\n" + 
				"	solicitacao sol\r\n" + 
				"INNER JOIN entidades ent on ent.idEntidades = sol.idEntidades\r\n" + 
				"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
				"AND sol.idEntidades = :idEntidade " + 
				"GROUP by\r\n" + 
				"	sol.idEntidades\r\n" + 
				"ORDER BY Manifestações desc, ent.sigla asc\r\n";
		
		manifestacoes = (List<Object[]>) em
				.createNativeQuery(query)
				.setParameter("dataIni", dataIni+" 00:00:00")
				.setParameter("dataFim", dataFim+" 23:59:59")
				.setParameter("idEntidade", idEntidade)
				.getResultList();
		
		String[] temp = new String[manifestacoes.size()];
		String[] temp2 = new String[manifestacoes.size()];
		
		for (int i = 0; i < manifestacoes.size(); i++) {
			temp[i] = manifestacoes.get(i)[0].toString();
			temp2[i] = manifestacoes.get(i)[1].toString();
		}
		
		manifPorOrgao.add(temp);
		manifPorOrgao.add(temp2);
		
		return manifPorOrgao;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getManifPorCanal(String dataIni, String dataFim, Integer idEntidade) {

		List<String[]> manifPorCanal = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		String query = "SELECT 'Sistema' as 'Canal', COUNT(distinct sol.idSolicitacao) as 'Manifestações' " + 
				"FROM solicitacao sol " + 
				"where sol.dataIni BETWEEN :dataIni AND :dataFim and sol.idEntidades = :idEntidade " + 
				"AND sol.canalEntrada = 0 " + 
				"UNION " + 
				"SELECT 'Telefone', COUNT(distinct sol.idSolicitacao) " + 
				"FROM solicitacao sol " + 
				"where sol.dataIni BETWEEN :dataIni AND :dataFim and sol.idEntidades = :idEntidade " + 
				"AND sol.canalEntrada = 1 " + 
				"UNION " + 
				"SELECT 'Presencial', COUNT(distinct sol.idSolicitacao) " + 
				"FROM solicitacao as sol " + 
				"where sol.dataIni BETWEEN :dataIni AND :dataFim and sol.idEntidades = :idEntidade " + 
				"AND sol.canalEntrada = 2 " + 
				"UNION " + 
				"SELECT 'Postal', COUNT(distinct sol.idSolicitacao) " + 
				"FROM solicitacao as sol " + 
				"where sol.dataIni BETWEEN :dataIni AND :dataFim and sol.idEntidades = :idEntidade " + 
				"AND sol.canalEntrada = 3 ";
		
		manifestacoes = (List<Object[]>) em
				.createNativeQuery(query)
				.setParameter("dataIni", dataIni+" 00:00:00")
				.setParameter("dataFim", dataFim+" 23:59:59")
				.setParameter("idEntidade", idEntidade)
				.getResultList();
		
		String[] temp = new String[manifestacoes.size()];
		String[] temp2 = new String[manifestacoes.size()];
		
		for (int i = 0; i < manifestacoes.size(); i++) {
			temp[i] = manifestacoes.get(i)[0].toString();
			temp2[i] = manifestacoes.get(i)[1].toString();
		}
		
		manifPorCanal.add(temp);
		manifPorCanal.add(temp2);
		
		return manifPorCanal;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getCidPorFaixaEtariaEntidade(String dataIni, String dataFim, Integer idEntidade) {

		List<String[]> cidPorFaixa = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		try {
			String query = "SELECT '0 a 18' as Faixa ,COUNT(*) as Quantidade\r\n" + 
					"FROM \r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, TIMESTAMPDIFF(YEAR,cid.datanasc,CURDATE()) Idade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE Idade BETWEEN 0 AND 18\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT '19 a 25' as Faixa ,COUNT(*) as Quantidade\r\n" + 
					"FROM \r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, TIMESTAMPDIFF(YEAR,cid.datanasc,CURDATE()) Idade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE Idade BETWEEN 19 AND 25\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT '26 a 40' as Faixa ,COUNT(*) as Quantidade\r\n" + 
					"FROM \r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, TIMESTAMPDIFF(YEAR,cid.datanasc,CURDATE()) Idade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE Idade BETWEEN 26 AND 40\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT '41 a 60' as Faixa ,COUNT(*) as Quantidade\r\n" + 
					"FROM \r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, TIMESTAMPDIFF(YEAR,cid.datanasc,CURDATE()) Idade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE Idade BETWEEN 41 AND 60\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Acima de 60' as Faixa ,COUNT(*) as Quantidade\r\n" + 
					"FROM \r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, TIMESTAMPDIFF(YEAR,cid.datanasc,CURDATE()) Idade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE Idade >= 61\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Não Informaram' as Faixa ,COUNT(*) as Quantidade\r\n" + 
					"FROM \r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, TIMESTAMPDIFF(YEAR,cid.datanasc,CURDATE()) Idade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE Idade IS NULL OR Idade < 0";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.setParameter("idEntidade", idEntidade)
					.getResultList();
			
			String[] temp = new String[manifestacoes.size()];
			String[] temp2 = new String[manifestacoes.size()];
			
			for (int i = 0; i < manifestacoes.size(); i++) {
				temp[i] = manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[1].toString();
			}
			
			cidPorFaixa.add(temp);
			cidPorFaixa.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cidPorFaixa;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getCidPorRendaEntidade(String dataIni, String dataFim, Integer idEntidade) {

		List<String[]> cidPorRenda = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		try {
			String query = "SELECT 'Sem renda' as Categoria ,COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.renda\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE renda = 0\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Até 2 SM' as Categoria ,COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.renda\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE renda = 1\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT '2 a 4 SM' as Categoria ,COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.renda\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE renda = 2\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT '4 a 10 SM' as Categoria ,COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.renda\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE renda = 3\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT '10 a 20 SM' as Categoria ,COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.renda\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE renda = 4\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Acima de 20 SM' as Categoria ,COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.renda\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE renda = 5";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.setParameter("idEntidade", idEntidade)
					.getResultList();
			
			String[] temp = new String[manifestacoes.size()];
			String[] temp2 = new String[manifestacoes.size()];
			
			for (int i = 0; i < manifestacoes.size(); i++) {
				temp[i] = manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[1].toString();
			}
			
			cidPorRenda.add(temp);
			cidPorRenda.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cidPorRenda;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getCidPorSexoEntidade(String dataIni, String dataFim, Integer idEntidade) {

		List<String[]> cidPorSexo = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		try {
			String query = "SELECT 'Masculino' as Sexo, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.sexo\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE sexo = 'M'\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Feminino' as Sexo, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.sexo\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE sexo = 'F'\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Outro' as Sexo, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.sexo\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE sexo = 'O'\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Não Informado' as Sexo, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.sexo\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE sexo = '' OR sexo IS NULL";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.setParameter("idEntidade", idEntidade)
					.getResultList();
			
			String[] temp = new String[manifestacoes.size()];
			String[] temp2 = new String[manifestacoes.size()];
			
			for (int i = 0; i < manifestacoes.size(); i++) {
				temp[i] = manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[1].toString();
			}
			
			cidPorSexo.add(temp);
			cidPorSexo.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cidPorSexo;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getCidPorEscolaridadeEntidade(String dataIni, String dataFim, Integer idEntidade) {

		List<String[]> cidPorEscolaridade = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		try {
			String query = "SELECT 'Analfabeto' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE escolaridade = 1\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Semi-Analfabeto' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE escolaridade = 2\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Fundamental' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE escolaridade = 3\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Ensino Médio Incompleto' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE escolaridade = 4\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Ensino Médio Completo' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE escolaridade = 5\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Ensino Superior Incompleto' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE escolaridade = 6\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Ensino Superior Completo' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE escolaridade = 7\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Não Informado' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE escolaridade IS NULL";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.setParameter("idEntidade", idEntidade)
					.getResultList();
			
			String[] temp = new String[manifestacoes.size()];
			String[] temp2 = new String[manifestacoes.size()];
			
			for (int i = 0; i < manifestacoes.size(); i++) {
				temp[i] = manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[1].toString();
			}
			
			cidPorEscolaridade.add(temp);
			cidPorEscolaridade.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cidPorEscolaridade;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getCidPorEstadoEntidade(String dataIni, String dataFim, Integer idEntidade) {

		List<String[]> cidPorEstado = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		try {
			String query = "SELECT IF(cid.estado=\"\", 'Não Informado', cid.estado) Estado, COUNT(*) Quantidade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"	AND sol.idEntidades = :idEntidade\r\n" + 
					"GROUP by cid.estado\r\n" + 
					"ORDER BY Quantidade DESC\r\n" + 
					"LIMIT 5\r\n";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.setParameter("idEntidade", idEntidade)
					.getResultList();
			
			String[] temp = new String[manifestacoes.size()];
			String[] temp2 = new String[manifestacoes.size()];
			
			for (int i = 0; i < manifestacoes.size(); i++) {
				temp[i] = manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[1].toString();
			}
			
			cidPorEstado.add(temp);
			cidPorEstado.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cidPorEstado;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getManifPorAnoEntidade(String dataIni, String dataFim, Integer idEntidade) {

		List<String[]> manifPorAno = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		String query = "SELECT \r\n" + 
				"YEAR(sol.dataIni) as Ano, \r\n" + 
				"count(sol.idSolicitacao) as Manifestações \r\n" + 
				"FROM \r\n" + 
				"solicitacao sol \r\n" + 
				"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
				"AND sol.idEntidades = :idEntidade \r\n" + 
				"GROUP by \r\n" + 
				"YEAR(sol.dataIni)";
		
		manifestacoes = (List<Object[]>) em
				.createNativeQuery(query)
				.setParameter("dataIni", dataIni+" 00:00:00")
				.setParameter("dataFim", dataFim+" 23:59:59")
				.setParameter("idEntidade", idEntidade)
				.getResultList();
		
		String[] temp = new String[manifestacoes.size()];
		String[] temp2 = new String[manifestacoes.size()];
		
		for (int i = 0; i < manifestacoes.size(); i++) {
			temp[i] = manifestacoes.get(i)[0].toString();
			temp2[i] = manifestacoes.get(i)[1].toString();
		}
		
		manifPorAno.add(temp);
		manifPorAno.add(temp2);
		
		return manifPorAno;
	}
	
	
//	=====================================
//	======== RELATÓRIO LAI GERAL ========
//	=====================================

	@SuppressWarnings("unchecked")
	public List<String[]> getManifLAIPorMes(String dataIni, String dataFim) {

		List<String[]> manifPorMes = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		String query = "Select b.Ano, b.Mes, IFNULL(a.Manifestacoes, 0) as Manifestações from ( " + 
				"SELECT " + 
				"YEAR(sol.dataIni) as Ano, " + 
				"MONTH(sol.dataIni) as MEs, " + 
				"count(sol.idSolicitacao) as Manifestacoes " + 
				"FROM " + 
				"solicitacao sol " + 
				"WHERE " + 
				"sol.dataIni BETWEEN :dataIni AND :dataFim " + 
				"AND sol.tipo = 'Informação' " + 
				"GROUP by " + 
				"YEAR(sol.dataIni), " + 
				"MONTH(sol.dataIni) " + 
				") a " + 
				"RIGHT JOIN " + 
				"(select " + 
				"DATE_FORMAT(m1, '%Y') Ano, " + 
				"DATE_FORMAT(m1, '%m') Mes, " + 
				"0 as Manifestações " + 
				"from " + 
				"( " + 
				"select " + 
				"(:dataIni - INTERVAL DAYOFMONTH(:dataIni)-1 DAY) + INTERVAL m MONTH as m1 " + 
				"from " + 
				"( " + 
				"select " + 
				"@rownum \\:= @rownum + 1 as m " + 
				"from " + 
				"(select 1 union select 2 union select 3 union select 4) t1, " + 
				"(select 1 union select 2 union select 3 union select 4) t2, " + 
				"(select 1 union select 2 union select 3 union select 4) t3, " + 
				"(select 1 union select 2 union select 3 union select 4) t4, " + 
				"(select @rownum\\:=-1) t0 " + 
				") d1 " + 
				") d2 " + 
				"where " + 
				"m1 <= :dataFim " + 
				"order by " + 
				"m1) b on a.Ano = b.Ano and a.Mes = b.Mes ";
		
		manifestacoes = (List<Object[]>) em
				.createNativeQuery(query)
				.setParameter("dataIni", dataIni+" 00:00:00")
				.setParameter("dataFim", dataFim+" 23:59:59")
				.getResultList();
		
		String[] temp = new String[manifestacoes.size()];
		String[] temp2 = new String[manifestacoes.size()];
		
		for (int i = 0; i < manifestacoes.size(); i++) {
			temp[i] = GeralUtil.meses[Integer.parseInt(manifestacoes.get(i)[1].toString())] + " " + manifestacoes.get(i)[0].toString();
			temp2[i] = manifestacoes.get(i)[2].toString();
		}
		manifPorMes.add(temp);
		manifPorMes.add(temp2);
		
		return manifPorMes;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getManifLAIPorTema(String dataIni, String dataFim) {

		List<String[]> manifPorTema = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		String query = "SELECT " + 
				"tema.titulo as 'Órgão', " + 
				"count(sol.idSolicitacao) as Manifestações " + 
				"FROM " + 
				"solicitacao sol " + 
				"INNER JOIN acoes tema on tema.idAcoes = sol.idAcao " + 
				"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim " + 
				"AND sol.tipo = 'Informação' " +
				"GROUP by " + 
				"sol.idAcao " + 
				"ORDER BY Manifestações desc, tema.titulo asc " + 
				"LIMIT 5";
		
		manifestacoes = (List<Object[]>) em
				.createNativeQuery(query)
				.setParameter("dataIni", dataIni+" 00:00:00")
				.setParameter("dataFim", dataFim+" 23:59:59")
				.getResultList();
		
		String[] temp = new String[manifestacoes.size()];
		String[] temp2 = new String[manifestacoes.size()];
		
		for (int i = 0; i < manifestacoes.size(); i++) {
			temp[i] = manifestacoes.get(i)[0].toString();
			temp2[i] = manifestacoes.get(i)[1].toString();
		}
		
		manifPorTema.add(temp);
		manifPorTema.add(temp2);
		
		return manifPorTema;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getManifLAIPorSituacao(String dataIni, String dataFim) {

		List<String[]> manifPorSituacao = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		String query = "SELECT 'Atendidas' as 'Status', COUNT(distinct sol.idSolicitacao) as 'Manifestações'\r\n" + 
				"	FROM solicitacao sol\r\n" + 
				"	where sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
				"	AND (sol.status like 'Atendida' or sol.status like 'Finalizada')\r\n" + 
				"   AND sol.tipo = 'Informação' " +
				"		UNION\r\n" + 
				"SELECT 'Sem resposta', COUNT(distinct sol.idSolicitacao)\r\n" + 
				"	FROM solicitacao sol\r\n" + 
				"	where sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
				"	AND (sol.status like 'Sem resposta' or sol.status like 'Negada')\r\n" +
				"   AND sol.tipo = 'Informação' " +
				"		UNION\r\n" + 
				"SELECT 'Em trâmite', COUNT(distinct sol.idSolicitacao)\r\n" + 
				"	FROM solicitacao as sol\r\n" + 
				"	where sol.dataIni BETWEEN :dataIni AND :dataFim \r\n" + 
				"	AND sol.visualizada = 1\r\n" + 
				"	AND (sol.status like 'Aberta' or sol.status like 'Reencaminhada' or sol.status like 'Prorrogada' or sol.status like 'Recurso' or sol.status like 'Reformulação' or sol.status like 'Tramitando')\r\n" + 
				"   AND sol.tipo = 'Informação' " +
				"		UNION\r\n" + 
				"SELECT 'Não visualizadas', COUNT(distinct sol.idSolicitacao) \r\n" + 
				"	FROM solicitacao as sol\r\n" + 
				"	where sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
				"	AND sol.visualizada = 0 AND sol.status like 'Aberta'" +
				"   AND sol.tipo = 'Informação' ";
		
		manifestacoes = (List<Object[]>) em
				.createNativeQuery(query)
				.setParameter("dataIni", dataIni+" 00:00:00")
				.setParameter("dataFim", dataFim+" 23:59:59")
				.getResultList();
		
		String[] temp = new String[manifestacoes.size()];
		String[] temp2 = new String[manifestacoes.size()];
		
		for (int i = 0; i < manifestacoes.size(); i++) {
			temp[i] = manifestacoes.get(i)[0].toString();
			temp2[i] = manifestacoes.get(i)[1].toString();
		}
		
		manifPorSituacao.add(temp);
		manifPorSituacao.add(temp2);
		
		return manifPorSituacao;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getManifLAIPorOrgao(String dataIni, String dataFim) {

		List<String[]> manifPorOrgao = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		String query = "SELECT\r\n" + 
				"	ent.sigla as 'Órgão',\r\n" + 
				"	count(sol.idSolicitacao) as Manifestações\r\n" + 
				"FROM\r\n" + 
				"	solicitacao sol\r\n" + 
				"INNER JOIN entidades ent on ent.idEntidades = sol.idEntidades\r\n" + 
				"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
				"AND sol.tipo = 'Informação' \r\n" +
				"GROUP by\r\n" + 
				"	sol.idEntidades\r\n" + 
				"ORDER BY Manifestações desc, ent.sigla asc\r\n" + 
				"LIMIT 5";
		
		manifestacoes = (List<Object[]>) em
				.createNativeQuery(query)
				.setParameter("dataIni", dataIni+" 00:00:00")
				.setParameter("dataFim", dataFim+" 23:59:59")
				.getResultList();
		
		String[] temp = new String[manifestacoes.size()];
		String[] temp2 = new String[manifestacoes.size()];
		
		for (int i = 0; i < manifestacoes.size(); i++) {
			temp[i] = manifestacoes.get(i)[0].toString();
			temp2[i] = manifestacoes.get(i)[1].toString();
		}
		
		manifPorOrgao.add(temp);
		manifPorOrgao.add(temp2);
		
		return manifPorOrgao;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getCidLAIPorFaixaEtaria(String dataIni, String dataFim) {

		List<String[]> cidPorFaixa = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		try {
			String query = "SELECT '0 a 18' as Faixa ,COUNT(*) as Quantidade\r\n" + 
					"FROM \r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, TIMESTAMPDIFF(YEAR,cid.datanasc,CURDATE()) Idade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação') temp\r\n" + 
					"WHERE Idade BETWEEN 0 AND 18\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT '19 a 25' as Faixa ,COUNT(*) as Quantidade\r\n" + 
					"FROM \r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, TIMESTAMPDIFF(YEAR,cid.datanasc,CURDATE()) Idade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação') temp\r\n" + 
					"WHERE Idade BETWEEN 19 AND 25\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT '26 a 40' as Faixa ,COUNT(*) as Quantidade\r\n" + 
					"FROM \r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, TIMESTAMPDIFF(YEAR,cid.datanasc,CURDATE()) Idade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação') temp\r\n" + 
					"WHERE Idade BETWEEN 26 AND 40\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT '41 a 60' as Faixa ,COUNT(*) as Quantidade\r\n" + 
					"FROM \r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, TIMESTAMPDIFF(YEAR,cid.datanasc,CURDATE()) Idade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação') temp\r\n" + 
					"WHERE Idade BETWEEN 41 AND 60\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Acima de 60' as Faixa ,COUNT(*) as Quantidade\r\n" + 
					"FROM \r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, TIMESTAMPDIFF(YEAR,cid.datanasc,CURDATE()) Idade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação') temp\r\n" + 
					"WHERE Idade >= 61\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Não Informaram' as Faixa ,COUNT(*) as Quantidade\r\n" + 
					"FROM \r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, TIMESTAMPDIFF(YEAR,cid.datanasc,CURDATE()) Idade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação') temp\r\n" + 
					"WHERE Idade IS NULL OR Idade < 0";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.getResultList();
			
			String[] temp = new String[manifestacoes.size()];
			String[] temp2 = new String[manifestacoes.size()];
			
			for (int i = 0; i < manifestacoes.size(); i++) {
				temp[i] = manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[1].toString();
			}
			
			cidPorFaixa.add(temp);
			cidPorFaixa.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cidPorFaixa;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getCidLAIPorRenda(String dataIni, String dataFim) {

		List<String[]> cidPorRenda = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		try {
			String query = "SELECT 'Sem renda' as Categoria ,COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.renda\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação') temp\r\n" + 
					"WHERE renda = 0\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Até 2 SM' as Categoria ,COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.renda\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação') temp\r\n" + 
					"WHERE renda = 1\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT '2 a 4 SM' as Categoria ,COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.renda\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação') temp\r\n" + 
					"WHERE renda = 2\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT '4 a 10 SM' as Categoria ,COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.renda\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação') temp\r\n" + 
					"WHERE renda = 3\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT '10 a 20 SM' as Categoria ,COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.renda\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação') temp\r\n" + 
					"WHERE renda = 4\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Acima de 20 SM' as Categoria ,COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.renda\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação') temp\r\n" + 
					"WHERE renda = 5";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.getResultList();
			
			String[] temp = new String[manifestacoes.size()];
			String[] temp2 = new String[manifestacoes.size()];
			
			for (int i = 0; i < manifestacoes.size(); i++) {
				temp[i] = manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[1].toString();
			}
			
			cidPorRenda.add(temp);
			cidPorRenda.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cidPorRenda;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getCidLAIPorSexo(String dataIni, String dataFim) {

		List<String[]> cidPorSexo = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		try {
			String query = "SELECT 'Masculino' as Sexo, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.sexo\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação') temp\r\n" + 
					"WHERE sexo = 'M'\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Feminino' as Sexo, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.sexo\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação') temp\r\n" + 
					"WHERE sexo = 'F'\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Outro' as Sexo, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.sexo\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação') temp\r\n" + 
					"WHERE sexo = 'O'\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Não Informado' as Sexo, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.sexo\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação') temp\r\n" + 
					"WHERE sexo = '' OR sexo IS NULL";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.getResultList();
			
			String[] temp = new String[manifestacoes.size()];
			String[] temp2 = new String[manifestacoes.size()];
			
			for (int i = 0; i < manifestacoes.size(); i++) {
				temp[i] = manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[1].toString();
			}
			
			cidPorSexo.add(temp);
			cidPorSexo.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cidPorSexo;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getCidLAIPorEscolaridade(String dataIni, String dataFim) {

		List<String[]> cidPorEscolaridade = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		try {
			String query = "SELECT 'Analfabeto' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação') temp\r\n" + 
					"WHERE escolaridade = 1\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Semi-Analfabeto' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação') temp\r\n" + 
					"WHERE escolaridade = 2\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Fundamental' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação') temp\r\n" + 
					"WHERE escolaridade = 3\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Ensino Médio Incompleto' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação') temp\r\n" + 
					"WHERE escolaridade = 4\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Ensino Médio Completo' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação') temp\r\n" + 
					"WHERE escolaridade = 5\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Ensino Superior Incompleto' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação') temp\r\n" + 
					"WHERE escolaridade = 6\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Ensino Superior Completo' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação') temp\r\n" + 
					"WHERE escolaridade = 7\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Não Informado' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação') temp\r\n" + 
					"WHERE escolaridade IS NULL";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.getResultList();
			
			String[] temp = new String[manifestacoes.size()];
			String[] temp2 = new String[manifestacoes.size()];
			
			for (int i = 0; i < manifestacoes.size(); i++) {
				temp[i] = manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[1].toString();
			}
			
			cidPorEscolaridade.add(temp);
			cidPorEscolaridade.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cidPorEscolaridade;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getCidLAIPorEstado(String dataIni, String dataFim) {

		List<String[]> cidPorEstado = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		try {
			String query = "SELECT IF(cid.estado=\"\", 'Não Informado', cid.estado) Estado, COUNT(*) Quantidade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"	AND sol.tipo = 'Informação'\r\n" + 
					"GROUP by cid.estado\r\n" + 
					"ORDER BY Quantidade DESC\r\n" + 
					"LIMIT 5";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.getResultList();
			
			String[] temp = new String[manifestacoes.size()];
			String[] temp2 = new String[manifestacoes.size()];
			
			for (int i = 0; i < manifestacoes.size(); i++) {
				temp[i] = manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[1].toString();
			}
			
			cidPorEstado.add(temp);
			cidPorEstado.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cidPorEstado;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getManifLAIPorAno(String dataIni, String dataFim) {

		List<String[]> manifPorMes = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		String query = "SELECT \r\n" + 
				"YEAR(sol.dataIni) as Ano,  \r\n" + 
				"count(sol.idSolicitacao) as Manifestações \r\n" + 
				"FROM \r\n" + 
				"solicitacao sol \r\n" + 
				"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
				"AND sol.tipo = 'Informação' \r\n" + 
				"GROUP by \r\n" + 
				"YEAR(sol.dataIni)";
		
		manifestacoes = (List<Object[]>) em
				.createNativeQuery(query)
				.setParameter("dataIni", dataIni+" 00:00:00")
				.setParameter("dataFim", dataFim+" 23:59:59")
				.getResultList();
		
		String[] temp = new String[manifestacoes.size()];
		String[] temp2 = new String[manifestacoes.size()];
		
		for (int i = 0; i < manifestacoes.size(); i++) {
			temp[i] = manifestacoes.get(i)[0].toString();
			temp2[i] = manifestacoes.get(i)[1].toString();
		}
		
		manifPorMes.add(temp);
		manifPorMes.add(temp2);
		
		return manifPorMes;
	}
	
//	============================================
//	===== RELATÓRIO LAI POR ENTIDADE =====
//	============================================
	
	@SuppressWarnings("unchecked")
	public List<String[]> getManifLAIPorMesEntidade(String dataIni, String dataFim, Integer idEntidade) {

		List<String[]> manifPorMes = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		String query = "Select b.Ano, b.Mes, IFNULL(a.Manifestacoes, 0) as Manifestações from ( " + 
				"SELECT " + 
				"YEAR(sol.dataIni) as Ano, " + 
				"MONTH(sol.dataIni) as MEs, " + 
				"count(sol.idSolicitacao) as Manifestacoes " + 
				"FROM " + 
				"solicitacao sol " + 
				"WHERE " + 
				"sol.dataIni BETWEEN :dataIni AND :dataFim " + 
				"AND sol.idEntidades = :idEntidade " + 
				"AND sol.tipo = 'Informação' " + 
				"GROUP by " + 
				"YEAR(sol.dataIni), " + 
				"MONTH(sol.dataIni) " + 
				") a " + 
				"RIGHT JOIN " + 
				"(select " + 
				"DATE_FORMAT(m1, '%Y') Ano, " + 
				"DATE_FORMAT(m1, '%m') Mes, " + 
				"0 as Manifestações " + 
				"from " + 
				"( " + 
				"select " + 
				"(:dataIni - INTERVAL DAYOFMONTH(:dataIni)-1 DAY) + INTERVAL m MONTH as m1 " + 
				"from " + 
				"( " + 
				"select " + 
				"@rownum \\:= @rownum + 1 as m " + 
				"from " + 
				"(select 1 union select 2 union select 3 union select 4) t1, " + 
				"(select 1 union select 2 union select 3 union select 4) t2, " + 
				"(select 1 union select 2 union select 3 union select 4) t3, " + 
				"(select 1 union select 2 union select 3 union select 4) t4, " + 
				"(select @rownum\\:=-1) t0 " + 
				") d1 " + 
				") d2 " + 
				"where " + 
				"m1 <= :dataFim " + 
				"order by " + 
				"m1) b on a.Ano = b.Ano and a.Mes = b.Mes ";
		
		manifestacoes = (List<Object[]>) em
				.createNativeQuery(query)
				.setParameter("dataIni", dataIni+" 00:00:00")
				.setParameter("dataFim", dataFim+" 23:59:59")
				.setParameter("idEntidade", idEntidade)
				.getResultList();
		
		String[] temp = new String[manifestacoes.size()];
		String[] temp2 = new String[manifestacoes.size()];
		
		for (int i = 0; i < manifestacoes.size(); i++) {
			temp[i] = GeralUtil.meses[Integer.parseInt(manifestacoes.get(i)[1].toString())] + " " + manifestacoes.get(i)[0].toString();
			temp2[i] = manifestacoes.get(i)[2].toString();
		}
		
		manifPorMes.add(temp);
		manifPorMes.add(temp2);
		
		return manifPorMes;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getManifLAIPorTemaEntidade(String dataIni, String dataFim, Integer idEntidade) {

		List<String[]> manifPorTema = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		String query = "SELECT " + 
				"tema.titulo as 'Órgão', " + 
				"count(sol.idSolicitacao) as Manifestações " + 
				"FROM " + 
				"solicitacao sol " + 
				"INNER JOIN acoes tema on tema.idAcoes = sol.idAcao " + 
				"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim " +
				"AND sol.idEntidades = :idEntidade " +
				"AND sol.tipo = 'Informação' " +
				"GROUP by " + 
				"sol.idAcao " + 
				"ORDER BY Manifestações desc, tema.titulo asc " + 
				"LIMIT 5";
		
		manifestacoes = (List<Object[]>) em
				.createNativeQuery(query)
				.setParameter("dataIni", dataIni+" 00:00:00")
				.setParameter("dataFim", dataFim+" 23:59:59")
				.setParameter("idEntidade", idEntidade)
				.getResultList();
		
		String[] temp = new String[manifestacoes.size()];
		String[] temp2 = new String[manifestacoes.size()];
		
		for (int i = 0; i < manifestacoes.size(); i++) {
			temp[i] = manifestacoes.get(i)[0].toString();
			temp2[i] = manifestacoes.get(i)[1].toString();
		}
		
		manifPorTema.add(temp);
		manifPorTema.add(temp2);
		
		return manifPorTema;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getManifLAIPorSituacaoEntidade(String dataIni, String dataFim, Integer idEntidade) {

		List<String[]> manifPorSituacao = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		String query = "SELECT * \r\n" + 
				"FROM\r\n" + 
				"\r\n" + 
				"(SELECT 'Atendidas' as 'Status', COUNT(distinct sol.idSolicitacao) as 'Manifestações'\r\n" + 
				"	FROM solicitacao sol\r\n" + 
				"	where sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
				"	AND (sol.status like 'Atendida' or sol.status like 'Finalizada')\r\n" + 
				"	AND sol.tipo = 'Informação' \r\n" +
				"	AND sol.idEntidades = :idEntidade\r\n" + 
				"		UNION\r\n" + 
				"SELECT 'Sem resposta', COUNT(distinct sol.idSolicitacao)\r\n" + 
				"	FROM solicitacao sol\r\n" + 
				"	where sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
				"	AND (sol.status like 'Sem resposta' or sol.status like 'Negada')\r\n" + 
				"	AND sol.tipo = 'Informação' \r\n" +
				"	AND sol.idEntidades = :idEntidade\r\n" + 
				"		UNION\r\n" + 
				"SELECT 'Em trâmite', COUNT(distinct sol.idSolicitacao)\r\n" + 
				"	FROM solicitacao as sol\r\n" + 
				"	where sol.dataIni BETWEEN :dataIni AND :dataFim \r\n" + 
				"	AND sol.visualizada = 1\r\n" + 
				"	AND (sol.status like 'Aberta' or sol.status like 'Reencaminhada' or sol.status like 'Prorrogada' or sol.status like 'Recurso' or sol.status like 'Reformulação' or sol.status like 'Tramitando')\r\n" + 
				"	AND sol.tipo = 'Informação' \r\n" +
				"	AND sol.idEntidades = :idEntidade\r\n" + 
				"		UNION\r\n" + 
				"SELECT 'Não visualizadas', COUNT(distinct sol.idSolicitacao) \r\n" + 
				"	FROM solicitacao as sol\r\n" + 
				"	where sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
				"	AND sol.visualizada = 0 AND sol.status like 'Aberta'\r\n" + 
				"	AND sol.tipo = 'Informação' \r\n" +
				"	AND sol.idEntidades = :idEntidade) temp\r\n" +
				"	\r\n" + 
				"ORDER BY Manifestações DESC";
		
		manifestacoes = (List<Object[]>) em
				.createNativeQuery(query)
				.setParameter("dataIni", dataIni+" 00:00:00")
				.setParameter("dataFim", dataFim+" 23:59:59")
				.setParameter("idEntidade", idEntidade)
				.getResultList();
		
		String[] temp = new String[manifestacoes.size()];
		String[] temp2 = new String[manifestacoes.size()];
		
		for (int i = 0; i < manifestacoes.size(); i++) {
			temp[i] = manifestacoes.get(i)[0].toString();
			temp2[i] = manifestacoes.get(i)[1].toString();
		}
		
		manifPorSituacao.add(temp);
		manifPorSituacao.add(temp2);
		
		return manifPorSituacao;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getManifLAIPorOrgaoEntidade(String dataIni, String dataFim, Integer idEntidade) {

		List<String[]> manifPorOrgao = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		String query = "SELECT\r\n" + 
				"	ent.sigla as 'Órgão',\r\n" + 
				"	count(sol.idSolicitacao) as Manifestações\r\n" + 
				"FROM\r\n" + 
				"	solicitacao sol\r\n" + 
				"INNER JOIN entidades ent on ent.idEntidades = sol.idEntidades\r\n" + 
				"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
				"AND sol.idEntidades = :idEntidade " + 
				"AND sol.tipo = 'Informação' \r\n" +
				"GROUP by\r\n" + 
				"	sol.idEntidades\r\n" + 
				"ORDER BY Manifestações desc, ent.sigla asc\r\n";
		
		manifestacoes = (List<Object[]>) em
				.createNativeQuery(query)
				.setParameter("dataIni", dataIni+" 00:00:00")
				.setParameter("dataFim", dataFim+" 23:59:59")
				.setParameter("idEntidade", idEntidade)
				.getResultList();
		
		String[] temp = new String[manifestacoes.size()];
		String[] temp2 = new String[manifestacoes.size()];
		
		for (int i = 0; i < manifestacoes.size(); i++) {
			temp[i] = manifestacoes.get(i)[0].toString();
			temp2[i] = manifestacoes.get(i)[1].toString();
		}
		
		manifPorOrgao.add(temp);
		manifPorOrgao.add(temp2);
		
		return manifPorOrgao;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getCidLAIPorFaixaEtariaEntidade(String dataIni, String dataFim, Integer idEntidade) {

		List<String[]> cidPorFaixa = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		try {
			String query = "SELECT '0 a 18' as Faixa ,COUNT(*) as Quantidade\r\n" + 
					"FROM \r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, TIMESTAMPDIFF(YEAR,cid.datanasc,CURDATE()) Idade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação'\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE Idade BETWEEN 0 AND 18\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT '19 a 25' as Faixa ,COUNT(*) as Quantidade\r\n" + 
					"FROM \r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, TIMESTAMPDIFF(YEAR,cid.datanasc,CURDATE()) Idade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação'\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE Idade BETWEEN 19 AND 25\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT '26 a 40' as Faixa ,COUNT(*) as Quantidade\r\n" + 
					"FROM \r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, TIMESTAMPDIFF(YEAR,cid.datanasc,CURDATE()) Idade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação'\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE Idade BETWEEN 26 AND 40\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT '41 a 60' as Faixa ,COUNT(*) as Quantidade\r\n" + 
					"FROM \r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, TIMESTAMPDIFF(YEAR,cid.datanasc,CURDATE()) Idade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação'\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE Idade BETWEEN 41 AND 60\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Acima de 60' as Faixa ,COUNT(*) as Quantidade\r\n" + 
					"FROM \r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, TIMESTAMPDIFF(YEAR,cid.datanasc,CURDATE()) Idade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação'\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE Idade >= 61\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Não Informaram' as Faixa ,COUNT(*) as Quantidade\r\n" + 
					"FROM \r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, TIMESTAMPDIFF(YEAR,cid.datanasc,CURDATE()) Idade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação'\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE Idade IS NULL OR Idade < 0\r\n";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.setParameter("idEntidade", idEntidade)
					.getResultList();
			
			String[] temp = new String[manifestacoes.size()];
			String[] temp2 = new String[manifestacoes.size()];
			
			for (int i = 0; i < manifestacoes.size(); i++) {
				temp[i] = manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[1].toString();
			}
			
			cidPorFaixa.add(temp);
			cidPorFaixa.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cidPorFaixa;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getCidLAIPorRendaEntidade(String dataIni, String dataFim, Integer idEntidade) {

		List<String[]> cidPorRenda = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		try {
			String query = "SELECT 'Sem renda' as Categoria ,COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.renda\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação'\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE renda = 0\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Até 2 SM' as Categoria ,COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.renda\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação'\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE renda = 1\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT '2 a 4 SM' as Categoria ,COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.renda\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação'\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE renda = 2\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT '4 a 10 SM' as Categoria ,COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.renda\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação'\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE renda = 3\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT '10 a 20 SM' as Categoria ,COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.renda\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação'\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE renda = 4\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Acima de 20 SM' as Categoria ,COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.renda\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação'\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE renda = 5";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.setParameter("idEntidade", idEntidade)
					.getResultList();
			
			String[] temp = new String[manifestacoes.size()];
			String[] temp2 = new String[manifestacoes.size()];
			
			for (int i = 0; i < manifestacoes.size(); i++) {
				temp[i] = manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[1].toString();
			}
			
			cidPorRenda.add(temp);
			cidPorRenda.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cidPorRenda;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getCidLAIPorSexoEntidade(String dataIni, String dataFim, Integer idEntidade) {

		List<String[]> cidPorSexo = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		try {
			String query = "SELECT 'Masculino' as Sexo, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.sexo\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação'\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE sexo = 'M'\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Feminino' as Sexo, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.sexo\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação'\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE sexo = 'F'\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Outro' as Sexo, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.sexo\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação'\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE sexo = 'O'\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Não Informado' as Sexo, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.sexo\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação'\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE sexo = '' OR sexo IS NULL";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.setParameter("idEntidade", idEntidade)
					.getResultList();
			
			String[] temp = new String[manifestacoes.size()];
			String[] temp2 = new String[manifestacoes.size()];
			
			for (int i = 0; i < manifestacoes.size(); i++) {
				temp[i] = manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[1].toString();
			}
			
			cidPorSexo.add(temp);
			cidPorSexo.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cidPorSexo;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getCidLAIPorEscolaridadeEntidade(String dataIni, String dataFim, Integer idEntidade) {

		List<String[]> cidPorEscolaridade = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		try {
			String query = "SELECT 'Analfabeto' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação'\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE escolaridade = 1\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Semi-Analfabeto' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação'\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE escolaridade = 2\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Fundamental' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação'\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE escolaridade = 3\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Ensino Médio Incompleto' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação'\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE escolaridade = 4\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Ensino Médio Completo' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação'\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE escolaridade = 5\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Ensino Superior Incompleto' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação'\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE escolaridade = 6\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Ensino Superior Completo' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação'\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE escolaridade = 7\r\n" + 
					"\r\n" + 
					"UNION\r\n" + 
					"\r\n" + 
					"SELECT 'Não Informado' as Escolaridade, COUNT(*) as Quantidade\r\n" + 
					"FROM\r\n" + 
					"(SELECT sol.idSolicitacao ,cid.idCidadao, cid.escolaridade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"AND sol.tipo = 'Informação'\r\n" + 
					"AND sol.idEntidades = :idEntidade) temp\r\n" + 
					"WHERE escolaridade IS NULL";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.setParameter("idEntidade", idEntidade)
					.getResultList();
			
			String[] temp = new String[manifestacoes.size()];
			String[] temp2 = new String[manifestacoes.size()];
			
			for (int i = 0; i < manifestacoes.size(); i++) {
				temp[i] = manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[1].toString();
			}
			
			cidPorEscolaridade.add(temp);
			cidPorEscolaridade.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cidPorEscolaridade;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getCidLAIPorEstadoEntidade(String dataIni, String dataFim, Integer idEntidade) {

		List<String[]> cidPorEstado = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		try {
			String query = "SELECT IF(cid.estado=\"\", 'Não Informado', cid.estado) Estado, COUNT(*) Quantidade\r\n" + 
					"FROM solicitacao sol\r\n" + 
					"	JOIN cidadao cid\r\n" + 
					"		ON sol.idCidadao = cid.idCidadao\r\n" + 
					"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
					"	AND sol.tipo = 'Informação'\r\n" + 
					"	AND sol.idEntidades = :idEntidade\r\n" + 
					"GROUP by cid.estado\r\n" + 
					"ORDER BY Quantidade DESC\r\n" + 
					"LIMIT 5";
			
			manifestacoes = (List<Object[]>) em
					.createNativeQuery(query)
					.setParameter("dataIni", dataIni+" 00:00:00")
					.setParameter("dataFim", dataFim+" 23:59:59")
					.setParameter("idEntidade", idEntidade)
					.getResultList();
			
			String[] temp = new String[manifestacoes.size()];
			String[] temp2 = new String[manifestacoes.size()];
			
			for (int i = 0; i < manifestacoes.size(); i++) {
				temp[i] = manifestacoes.get(i)[0].toString();
				temp2[i] = manifestacoes.get(i)[1].toString();
			}
			
			cidPorEstado.add(temp);
			cidPorEstado.add(temp2);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cidPorEstado;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getManifLAIPorAnoEntidade(String dataIni, String dataFim, Integer idEntidade) {

		List<String[]> manifPorMes = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		String query = "SELECT \r\n" + 
				"YEAR(sol.dataIni) as Ano, \r\n" + 
				"count(sol.idSolicitacao) as Manifestações \r\n" + 
				"FROM \r\n" + 
				"solicitacao sol \r\n" + 
				"WHERE sol.dataIni BETWEEN :dataIni AND :dataFim\r\n" + 
				"AND sol.idEntidades = :idEntidade \r\n" + 
				"AND sol.tipo = 'Informação' \r\n" + 
				"GROUP by \r\n" + 
				"YEAR(sol.dataIni)\r\n" + 
				"";
		
		manifestacoes = (List<Object[]>) em
				.createNativeQuery(query)
				.setParameter("dataIni", dataIni+" 00:00:00")
				.setParameter("dataFim", dataFim+" 23:59:59")
				.setParameter("idEntidade", idEntidade)
				.getResultList();
		
		String[] temp = new String[manifestacoes.size()];
		String[] temp2 = new String[manifestacoes.size()];
		
		for (int i = 0; i < manifestacoes.size(); i++) {
			temp[i] = manifestacoes.get(i)[0].toString();
			temp2[i] = manifestacoes.get(i)[1].toString();
		}
		
		manifPorMes.add(temp);
		manifPorMes.add(temp2);
		
		return manifPorMes;
	}
	
	
//	=================================
//	===== RELATÓRIO ATENDIMENTO =====
//	=================================
	
	@SuppressWarnings("unchecked")
	public List<String[]> getAtendimentoPorMesEntidade(String dataIni, String dataFim, Integer idEntidade) {

		List<String[]> manifPorMes = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		String query = "Select " + 
				"YEAR(mani.dataIni) as Ano, " + 
				"MONTH(mani.dataIni) as Mês, " + 
				"count(mani.idSolicitacao) as Manifestações " + 
				"from " + 
				"solicitacao mani " + 
				"left join responsavel resp on " + 
				"resp.idResponsavel = mani.idResponsavel " + 
				"where " + 
				"mani.idResponsavel is not null " + 
				"and resp.idEntidades = :idEntidade " + 
				"and mani.dataIni BETWEEN :dataIni AND :dataFim " + 
				"GROUP by " + 
				"YEAR(mani.dataIni), " + 
				"MONTH(mani.dataIni) ";
		
		manifestacoes = (List<Object[]>) em
				.createNativeQuery(query)
				.setParameter("dataIni", dataIni+" 00:00:00")
				.setParameter("dataFim", dataFim+" 23:59:59")
				.setParameter("idEntidade", idEntidade)
				.getResultList();
		
		String[] temp = new String[manifestacoes.size()];
		String[] temp2 = new String[manifestacoes.size()];
		
		for (int i = 0; i < manifestacoes.size(); i++) {
			temp[i] = manifestacoes.get(i)[0].toString() +" - " + GeralUtil.meses[Integer.parseInt(manifestacoes.get(i)[1].toString())];
			temp2[i] = manifestacoes.get(i)[2].toString();
		}
		
		manifPorMes.add(temp);
		manifPorMes.add(temp2);
		
		return manifPorMes;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getAtendimentoPorAnoEntidade(String dataIni, String dataFim, Integer idEntidade) {

		List<String[]> manifPorAno = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		String query = "Select " + 
				"YEAR(mani.dataIni) as Ano, " + 
				"count(mani.idSolicitacao) as Manifestações " + 
				"from " + 
				"solicitacao mani " + 
				"left join responsavel resp on " + 
				"resp.idResponsavel = mani.idResponsavel " + 
				"where " + 
				"mani.idResponsavel is not null " + 
				"and resp.idEntidades = :idEntidade " + 
				"and mani.dataIni BETWEEN :dataIni AND :dataFim " + 
				"GROUP by " + 
				"YEAR(mani.dataIni)";
		
		manifestacoes = (List<Object[]>) em
				.createNativeQuery(query)
				.setParameter("dataIni", dataIni+" 00:00:00")
				.setParameter("dataFim", dataFim+" 23:59:59")
				.setParameter("idEntidade", idEntidade)
				.getResultList();
		
		String[] temp = new String[manifestacoes.size()];
		String[] temp2 = new String[manifestacoes.size()];
		
		for (int i = 0; i < manifestacoes.size(); i++) {
			temp[i] = manifestacoes.get(i)[0].toString();
			temp2[i] = manifestacoes.get(i)[1].toString();
		}
		
		manifPorAno.add(temp);
		manifPorAno.add(temp2);
		
		return manifPorAno;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getAtendimentoPorOrgaoEntidade(String dataIni, String dataFim, Integer idEntidade) {

		List<String[]> manifPorOrgao = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		String query = "Select " + 
				"ent.sigla as 'Órgão', " + 
				"count(mani.idSolicitacao) as Manifestações " + 
				"from " + 
				"solicitacao mani " + 
				"left join responsavel resp on " + 
				"resp.idResponsavel = mani.idResponsavel " + 
				"INNER JOIN entidades ent on " + 
				"ent.idEntidades = mani.idEntidades " + 
				"where " + 
				"mani.idResponsavel is not null " + 
				"and resp.idEntidades = :idEntidade " + 
				"and mani.dataIni BETWEEN :dataIni AND :dataFim " + 
				"GROUP by " + 
				"mani.idEntidades " + 
				"ORDER BY " + 
				"Manifestações desc, " + 
				"ent.sigla " +
				"LIMIT 5";
		
		manifestacoes = (List<Object[]>) em
				.createNativeQuery(query)
				.setParameter("dataIni", dataIni+" 00:00:00")
				.setParameter("dataFim", dataFim+" 23:59:59")
				.setParameter("idEntidade", idEntidade)
				.getResultList();
		
		String[] temp = new String[manifestacoes.size()];
		String[] temp2 = new String[manifestacoes.size()];
		
		for (int i = 0; i < manifestacoes.size(); i++) {
			temp[i] = manifestacoes.get(i)[0].toString();
			temp2[i] = manifestacoes.get(i)[1].toString();
		}
		
		manifPorOrgao.add(temp);
		manifPorOrgao.add(temp2);
		
		return manifPorOrgao;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getAtendimentoPorCanalEntidade(String dataIni, String dataFim, Integer idEntidade) {

		List<String[]> manifPorCanal = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		String query = "Select " + 
				"Case " + 
				"when mani.canalEntrada = 0 then 'Sistema' " + 
				"when mani.canalEntrada = 1 then 'Telefone' " + 
				"when mani.canalEntrada = 2 then 'Presencial' " + 
				"when mani.canalEntrada = 3 then 'Postal' " + 
				"else 'Outro' " + 
				"END as Canal, " + 
				"count(mani.idSolicitacao) as Manifestações " + 
				"from " + 
				"solicitacao mani " + 
				"left join responsavel resp on " + 
				"resp.idResponsavel = mani.idResponsavel " + 
				"where " + 
				"mani.idResponsavel is not null " + 
				"and resp.idEntidades = :idEntidade " + 
				"and mani.dataIni BETWEEN :dataIni AND :dataFim " + 
				"GROUP by " + 
				"mani.canalEntrada ";
		
		manifestacoes = (List<Object[]>) em
				.createNativeQuery(query)
				.setParameter("dataIni", dataIni+" 00:00:00")
				.setParameter("dataFim", dataFim+" 23:59:59")
				.setParameter("idEntidade", idEntidade)
				.getResultList();
		
		String[] temp = new String[manifestacoes.size()];
		String[] temp2 = new String[manifestacoes.size()];
		
		for (int i = 0; i < manifestacoes.size(); i++) {
			temp[i] = manifestacoes.get(i)[0].toString();
			temp2[i] = manifestacoes.get(i)[1].toString();
		}
		
		manifPorCanal.add(temp);
		manifPorCanal.add(temp2);
		
		return manifPorCanal;
	}
	
	@SuppressWarnings("unchecked")
	public List<String[]> getAtendimentoPorSituacaoEntidade(String dataIni, String dataFim, Integer idEntidade) {
		
		List<String[]> manifPorCanal = new ArrayList<>();
		List<Object[]> manifestacoes = null;
		
		String query = "SELECT * " + 
				"FROM " + 
				"(SELECT 'Atendidas' as 'Status', COUNT(distinct sol.idSolicitacao) as 'Manifestações' " + 
				"FROM solicitacao sol " + 
				"left join responsavel resp on " + 
				"resp.idResponsavel = sol.idResponsavel " + 
				"where sol.dataIni BETWEEN :dataIni AND :dataFim " + 
				"AND (sol.status like 'Atendida' or sol.status like 'Finalizada') " + 
				"AND sol.idResponsavel is not null " + 
				"AND resp.idEntidades = :idEntidade " + 
				"UNION " + 
				"SELECT 'Sem resposta', COUNT(distinct sol.idSolicitacao) " + 
				"FROM solicitacao sol " + 
				"left join responsavel resp on " + 
				"resp.idResponsavel = sol.idResponsavel " + 
				"where sol.dataIni BETWEEN :dataIni AND :dataFim " + 
				"AND (sol.status like 'Sem resposta' or sol.status like 'Negada') " + 
				"AND sol.idResponsavel is not null " + 
				"AND resp.idEntidades = :idEntidade " + 
				"UNION " + 
				"SELECT 'Em trâmite', COUNT(distinct sol.idSolicitacao) " + 
				"FROM solicitacao as sol " + 
				"left join responsavel resp on " + 
				"resp.idResponsavel = sol.idResponsavel " + 
				"where sol.dataIni BETWEEN :dataIni AND :dataFim " + 
				"AND sol.visualizada = 1 " + 
				"AND (sol.status like 'Aberta' or sol.status like 'Reencaminhada' or sol.status like 'Prorrogada' or sol.status like 'Recurso' or sol.status like 'Reformulação' or sol.status like 'Tramitando') " + 
				"AND sol.idResponsavel is not null " + 
				"AND resp.idEntidades = :idEntidade " + 
				"UNION " + 
				"SELECT 'Não visualizadas', COUNT(distinct sol.idSolicitacao) " + 
				"FROM solicitacao as sol " + 
				"left join responsavel resp on " + 
				"resp.idResponsavel = sol.idResponsavel " + 
				"where sol.dataIni BETWEEN :dataIni AND :dataFim " + 
				"AND sol.visualizada = 0 AND sol.status like 'Aberta' " + 
				"AND sol.idResponsavel is not null " + 
				"AND resp.idEntidades = :idEntidade) temp " + 
				"ORDER BY Manifestações DESC ";
		
		manifestacoes = (List<Object[]>) em
				.createNativeQuery(query)
				.setParameter("dataIni", dataIni+" 00:00:00")
				.setParameter("dataFim", dataFim+" 23:59:59")
				.setParameter("idEntidade", idEntidade)
				.getResultList();
		
		String[] temp = new String[manifestacoes.size()];
		String[] temp2 = new String[manifestacoes.size()];
		
		for (int i = 0; i < manifestacoes.size(); i++) {
			temp[i] = manifestacoes.get(i)[0].toString();
			temp2[i] = manifestacoes.get(i)[1].toString();
		}
		
		manifPorCanal.add(temp);
		manifPorCanal.add(temp2);
		
		return manifPorCanal;
	}
	
}
