package br.gov.se.setc.DAO;

import java.util.List;

import javax.persistence.EntityManager;

import br.gov.se.setc.model.Entidade;
import br.gov.se.setc.util.HibernateUtil;

public class EntidadeDAO {
	
	private static EntidadeDAO instance;

	private EntidadeDAO(){}

	public static EntidadeDAO getInstance(){
		if( instance == null )
			synchronized( EntidadeDAO.class ){
				if( instance == null )
					instance = new EntidadeDAO();
			}

		return instance;
	}
	
	private static EntityManager em = HibernateUtil.getEntityManager();
	
	public boolean saveOrUpdate(Entidade entidade) {

		try {
			if(!em.getTransaction().isActive()) em.getTransaction().begin();
        	if(entidade.getIdEntidade() ==  null) {
        		em.persist(entidade);
    		}else {
    			em.merge(entidade);
    		}
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
        	em.getTransaction().rollback();
        	e.printStackTrace();
            return false;
        }
		
	}
	
	public Entidade getEntidade(Integer id) {
		
		Entidade entidade = em.find(Entidade.class, id);
		
		return entidade;
	}
	
	@SuppressWarnings("unchecked")
	public List<Entidade> listEntidades() {
		
		String query = "FROM Entidade ent ORDER BY ent.sigla ASC";
		
		List<Entidade> result = em.createQuery(query)
				.getResultList();
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Entidade> listEntidadesVerificacao() {
		
		String query = "FROM Entidade ent WHERE ent.idEntidade NOT IN (2, 156, 157, 164, 170) ORDER BY ent.sigla ASC";
		
		List<Entidade> result = em.createQuery(query)
				.getResultList();
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Entidade> listEntidadesPorTema(Integer idTema) {
		
		String query = "Select DISTINCT ent FROM Entidade ent "
				+ "INNER JOIN Competencia comp ON comp.entidade.idEntidade = ent.idEntidade "
				+ "WHERE comp.tema.idTema = :idTema "
				+ "AND comp.entidade.ativa = true "
				+ "AND comp.ativa = true "
				+ "ORDER BY comp.entidade.sigla ASC";
		
		List<Entidade> result = em.createQuery(query)
				.setParameter("idTema", idTema)
				.getResultList();
		
//		for(Entidade ent : result) {
//			System.out.println("	::" + ent.getSigla());
//		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Entidade> listEntidadesAtivas() {
		
		String query = "FROM Entidade ent WHERE ent.ativa = true ORDER BY ent.sigla ASC";
		
		List<Entidade> result = em.createQuery(query)
				.getResultList();
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Entidade> listEntidadesAtivasResponsaveis() {
		System.out.println("-- listEntidadesAtivasResponsaveis");
		
		String query = "FROM Entidade ent LEFT JOIN FETCH ent.ouvidores ouv WHERE ent.ativa = true and ouv.ativo = true and ouv.nivel = 1 ORDER BY ent.sigla ASC";
		
		List<Entidade> result = em.createQuery(query)
				.getResultList();
		
		return result;
	}
	
	public String getSigla(Integer idEntidade) {
		
		String query = "SELECT ent.sigla FROM Entidade ent WHERE ent.idEntidade = :idEntidade ";
		
		String result = (String) em.createQuery(query)
				.setParameter("idEntidade", idEntidade)
				.getSingleResult();
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Entidade> listEntidadesAgrese() {
		String query = "FROM Entidade ent WHERE ent.idEntidade in (116, 146, 148) ORDER BY ent.sigla ASC";
		
		List<Entidade> result = em.createQuery(query)
				.getResultList();
		
		return result;
	}

	public int getIdPorSigla(String sigla) {
		String query = "SELECT ent.idEntidade FROM Entidade ent WHERE ent.sigla = :sigla ";
		
		int result = (int) em.createQuery(query)
				.setParameter("sigla", sigla)
				.getSingleResult();
		
		return result;
	}
	
	public int countManifestacoesPendentes(Integer idEntidade) {
		EntityManager em = HibernateUtil.getEntityManager();
		Long result = Long.valueOf(0);
		try {
			String query = "Select count(DISTINCT mani.idManifestacao) FROM Manifestacao mani where mani.entidade.idEntidade = :idEntidade AND mani.status not in ('Finalizada', 'Sem Resposta', 'Atendida', 'Negada') ";
			
			result = (Long) em.createQuery(query)
					.setParameter("idEntidade", idEntidade)
					.getSingleResult();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			em.close();
		}
		
		System.out.println("Manifestações pendentes: " + result);
		
		return result.intValue();
	}
	
}
