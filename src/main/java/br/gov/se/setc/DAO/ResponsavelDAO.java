package br.gov.se.setc.DAO;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import br.gov.se.setc.model.Responsavel;
import br.gov.se.setc.util.HibernateUtil;

public class ResponsavelDAO {
	
	private static ResponsavelDAO instance;

	private ResponsavelDAO(){}

	public static ResponsavelDAO getInstance(){
		if( instance == null )
			synchronized( ResponsavelDAO.class ){
				if( instance == null )
					instance = new ResponsavelDAO();
			}

		return instance;
	}

	private static EntityManager em = HibernateUtil.getEntityManager();

	public boolean saveOrUpdate(Responsavel responsavel) {

		try {
			if(!em.getTransaction().isActive()) em.getTransaction().begin();
        	if(responsavel.getIdResponsavel() ==  null) {
        		em.persist(responsavel);
    		}else {
    			em.merge(responsavel);
    		}
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
        	em.getTransaction().rollback();
        	e.printStackTrace();
            return false;
        }
		
	}
	
	@SuppressWarnings("unchecked")
	public Responsavel getResponsavelAtivo(int idUsuario) {

		Responsavel responsavel = new Responsavel();
		
		List<Responsavel> result = new ArrayList<Responsavel>();
		
		String query = "Select resp FROM Responsavel as resp WHERE resp.usuario.idUsuario = :idUsuario and resp.ativo = true";
		
		result = (List<Responsavel>) em.createQuery(query)
				.setParameter("idUsuario", idUsuario)
				.getResultList();
		
		if(!result.isEmpty()) {
			responsavel =  result.get(0);
		}
		
		return responsavel;
	}
	
	@SuppressWarnings("unchecked")
	public List<Responsavel> listResponsaveis() {

		List<Responsavel> result = new ArrayList<Responsavel>();
		
		String query = "FROM Responsavel as resp";
		
		result = (List<Responsavel>) em.createQuery(query)
				.getResultList();
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Responsavel> listResponsaveisUsuario(Integer idUsuario) {

		List<Responsavel> result = new ArrayList<Responsavel>();
		
		String query = "FROM Responsavel as resp WHERE resp.usuario.idUsuario = :idUsuario";
		
		result = (List<Responsavel>) em.createQuery(query)
				.setParameter("idUsuario", idUsuario)
				.getResultList();
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<String> listEmailResponsaveisEntidade(Integer idEntidade) {

		List<String> result = new ArrayList<String>();
		
		String query = "Select resp.email FROM Responsavel as resp WHERE resp.ativo = true and resp.nivel = 1 and resp.entidade.idEntidade = :idEntidade";
		
		result = em.createQuery(query)
				.setParameter("idEntidade", idEntidade)
				.getResultList();
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Responsavel> listOuvidoresAtivos() {

		List<Responsavel> result = new ArrayList<Responsavel>();
		
		String query = "FROM Responsavel as resp WHERE resp.ativo = true and resp.nivel = 1";
		
		result = (List<Responsavel>) em.createQuery(query)
				.getResultList();
		
		return result;
	}
	
}
