package br.gov.se.setc.DAO;

import java.util.List;

import javax.persistence.EntityManager;

import br.gov.se.setc.model.Competencia;
import br.gov.se.setc.util.HibernateUtil;

public class CompetenciaDAO {
	
	private static CompetenciaDAO instance;

	private CompetenciaDAO(){}

	public static CompetenciaDAO getInstance(){
		if( instance == null )
			synchronized( CompetenciaDAO.class ){
				if( instance == null )
					instance = new CompetenciaDAO();
			}

		return instance;
	}
	
	private static EntityManager em = HibernateUtil.getEntityManager();
	
	public boolean saveOrUpdate(Competencia competencia) {

		try {
			if(!em.getTransaction().isActive()) em.getTransaction().begin();
        	if(competencia.getIdCompetencia() ==  null) {
        		em.persist(competencia);
    		}else {
    			em.merge(competencia);
    		}
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
        	em.getTransaction().rollback();
        	e.printStackTrace();
            return false;
        }
		
	}
	
	
	public Competencia getCompetencia(Integer id) {
	
		Competencia competencia = em.find(Competencia.class, id);
		
		return competencia;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Competencia> listCompetenciasAtivas(Integer idEntidade) {
		
		String query = "FROM Competencia comp WHERE comp.entidade.idEntidade = :idEntidade AND comp.ativa = true AND comp.tema.status != 'Pendente' ORDER BY comp.tema.titulo ASC";
		
		List<Competencia> result = em.createQuery(query)
				.setParameter("idEntidade", idEntidade)
				.getResultList();
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Competencia> listCompetenciasInativas(Integer idEntidade) {
		
		String query = "FROM Competencia comp WHERE comp.entidade.idEntidade = :idEntidade AND comp.ativa = false AND comp.tema.status != 'Pendente' ORDER BY comp.tema.titulo ASC";
		
		List<Competencia> result = em.createQuery(query).setParameter("idEntidade", idEntidade)
				.getResultList();
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Competencia> listCompetenciasPorTema(Integer idTema) {
		
		String query = "FROM Competencia comp WHERE comp.tema.idTema = :idTema";
		
		List<Competencia> result = em.createQuery(query)
				.setParameter("idTema", idTema)
				.getResultList();
		
		return result;
	}
	
}
