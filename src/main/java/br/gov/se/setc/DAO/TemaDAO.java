package br.gov.se.setc.DAO;

import java.util.List;

import javax.persistence.EntityManager;

import br.gov.se.setc.model.Tema;
import br.gov.se.setc.util.HibernateUtil;

public class TemaDAO {
	
	private static TemaDAO instance;

	private TemaDAO(){}

	public static TemaDAO getInstance(){
		if( instance == null )
			synchronized( TemaDAO.class ){
				if( instance == null )
					instance = new TemaDAO();
			}

		return instance;
	}

	private static EntityManager em = HibernateUtil.getEntityManager();

	public boolean saveOrUpdate(Tema tema) {

		try {
			if(!em.getTransaction().isActive()) em.getTransaction().begin();
        	if(tema.getIdTema() ==  null) {
        		em.persist(tema);
    		}else {
    			em.merge(tema);
    		}
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
        	em.getTransaction().rollback();
        	e.printStackTrace();
            return false;
        }
		
	}
	
	public static void delete(Tema tema) {        
        try {            
            if (tema == null) {
			}else {
				em.getTransaction().begin();
				em.remove(tema);
	            em.getTransaction().commit();
			}            
        } catch (Exception e) {
            em.getTransaction().rollback();
            e.printStackTrace();
        }
    }  
	
	public Tema getTema(Integer id) {

		Tema tema = em.find(Tema.class, id);
		
		return tema;
	}
	
	@SuppressWarnings("unchecked")
	public List<Tema> listTemas() {

		System.out.println(":: listTemasAtivos");
		
		String query = "FROM Tema t ORDER BY t.titulo ASC";
		
		List<Tema> result = em.createQuery(query)
				.getResultList();
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Tema> listTemasEntidadesAtivas() {

		System.out.println(":: listTemasEntidadesAtivas");
		
		String query = "Select distinct t FROM Tema t "
						+ "INNER JOIN Competencia c ON c.tema.idTema = t.idTema "
						+ "INNER JOIN Entidade e ON e.idEntidade = c.entidade.idEntidade "
						+ "WHERE e.ativa = true "
						+ "and c.ativa = true "
						+ "ORDER BY t.titulo ASC";
		
		List<Tema> result = em.createQuery(query)
				.getResultList();
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Tema> listTemasPorEntidade(Integer idEntidade) {

		String query = "Select distinct t FROM Tema t "
						+ "INNER JOIN Competencia c ON c.tema.idTema = t.idTema "
						+ "INNER JOIN Entidade e ON e.idEntidade = c.entidade.idEntidade "
						+ "WHERE e.idEntidade = :idEntidade AND e.ativa = true AND c.ativa = true "
						+ "ORDER BY t.titulo ASC";
		
		List<Tema> result = em.createQuery(query)
				.setParameter("idEntidade", idEntidade)
				.getResultList();
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Tema> listTemasCadastrados() {

		String query = "FROM Tema as t WHERE t.status IN ('Vinculada', 'Não-Vinculada') ORDER BY t.titulo ASC";
		
		List<Tema> result = em.createQuery(query)
				.getResultList();
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Tema> listTemasPendentes() {

		String query = "FROM Tema as t WHERE t.status = 'Pendente' ORDER BY t.titulo ASC";
		
		List<Tema> result = em.createQuery(query)
				.getResultList();
		
		return result;
	}

}
