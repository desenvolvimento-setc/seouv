package br.gov.se.setc.DAO;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import br.gov.se.setc.model.Usuario;
import br.gov.se.setc.util.HibernateUtil;

public class UsuarioDAO {
	
	 private static UsuarioDAO instance;
	 
	 private UsuarioDAO(){}
	 
	 public static UsuarioDAO getInstance(){
        if( instance == null )
            synchronized( UsuarioDAO.class ){
                if( instance == null )
                    instance = new UsuarioDAO();
            }

        return instance;
    }
	 
	
//	private EntityManager em = HibernateUtil.getEntityManager();

	public   boolean saveOrUpdate(Usuario usuario) {
		EntityManager em = HibernateUtil.getEntityManager();

		try {
			if(!em.getTransaction().isActive()) em.getTransaction().begin();
        	if(usuario.getIdUsuario() ==  null) {
        		em.persist(usuario);
    		}else {
    			em.merge(usuario);
    		}
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
        	em.getTransaction().rollback();
        	e.printStackTrace();
            return false;
        } finally {
			em.close();
		}
	}
	
	public   Usuario getUsuario(Integer id) {
		EntityManager em = HibernateUtil.getEntityManager();

		Usuario usuario = null ;
		
		try {
			usuario = em.find(Usuario.class, id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			em.close();
		} 
		
		return usuario;
	}
	
	@SuppressWarnings("unchecked")
	public Usuario getUsuarioPorNick(String nick) {
		EntityManager em = HibernateUtil.getEntityManager();
		
		Usuario usuario = new Usuario();
		
		try {
			
			String query = "Select usr from Usuario usr LEFT JOIN FETCH usr.cidadaos cid LEFT JOIN FETCH usr.responsaveis resp WHERE usr.nick = :nick";

			List<Usuario> usuarioList = em.createQuery(query)
					.setParameter("nick", nick)
					.getResultList();
			
			if(!usuarioList.isEmpty()) {
				usuario = usuarioList.get(0);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			em.close();
		}
		
		return usuario;
	}
	
	@SuppressWarnings("unchecked")
	public   Usuario getUsuarioPorSession(String key) {
		EntityManager em = HibernateUtil.getEntityManager();
		
		Usuario usuario = new Usuario();
		
		try {
			String query = "from Usuario usr LEFT JOIN FETCH usr.cidadaos cid LEFT JOIN FETCH usr.responsaveis resp where usr.sessionId = :key";
			
			List<Usuario> usuarioList = em.createQuery(query)
					.setParameter("key", key)
					.setMaxResults(1)
					.getResultList();
			
			if(!usuarioList.isEmpty()) {
				usuario = usuarioList.get(0);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			em.close();
		}
		
		return usuario;
	}

	public Long countNick(String nick) {
		EntityManager em = HibernateUtil.getEntityManager();
		
		Long count = (long) 0;
		
		try {
			String query = "Select count(usr.nick) from Usuario usr where usr.nick like :nick";
			
			count = (Long) em.createQuery(query)
					.setParameter("nick", nick+"%")
					.getSingleResult();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			em.close();
		}
		
		return count;
	}
	
	@SuppressWarnings("unchecked")
	public   List<String> listUsuariosPorNick(String nick) {
		EntityManager em = HibernateUtil.getEntityManager();
		
		List<String> result = new ArrayList<String>();
		
		try {
			String query = "Select usu.nick FROM Usuario usu "
					+ "WHERE usu.nick LIKE :nick "
					+ "AND (usu.perfil = 1 "
					+ "OR usu.nick NOT IN (SELECT resp.usuario.nick FROM Responsavel resp where resp.ativo = true)) "
					+ "ORDER BY usu.nick asc"
					;
			
			result = new ArrayList<String>();
			
			result = em.createQuery(query)
					.setParameter("nick", "%" + nick + "%")
					.getResultList();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			em.close();
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public   List<Usuario> listUsuariosPorEmailCidadao(String email) {
		EntityManager em = HibernateUtil.getEntityManager();

		List<Usuario> result = new ArrayList<Usuario>();
		
		try {
			String query = "Select usr FROM Usuario usr JOIN usr.cidadaos cid WHERE cid.email = :email AND usr.nick != 'cidAtendente'";
			
			result = em.createQuery(query)
					.setParameter("email", email)
					.getResultList();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			em.close();
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Usuario> listUsuariosPorEmailResponsavel(String email) {
		EntityManager em = HibernateUtil.getEntityManager();

		List<Usuario> result = new ArrayList<Usuario>();
		
		try {
			String query = "Select usr FROM Usuario usr JOIN usr.responsaveis resp WHERE resp.email = :email";
			
			result = em.createQuery(query)
					.setParameter("email", email)
					.getResultList();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			em.close();
		}
		
		return result;
	}
	
	public boolean existeEmail(String email) {
		EntityManager em = HibernateUtil.getEntityManager();

		try {
			
			String query = "Select EXISTS "
					+ "( Select resp.email FROM responsavel resp WHERE resp.email = :email  UNION  Select cid.email FROM cidadao cid INNER JOIN usuario usr ON usr.idUsuario = cid.idUsuario WHERE cid.email = :email AND usr.nick != 'cidAtendente' ) rst";
			
			Integer result = (Integer) em.createNativeQuery(query)
							.setParameter("email", email)
							.getSingleResult();
			
			boolean existe = (result.intValue() == 1);
			
			return existe;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			em.close();
		}
	}
	
	public boolean existeEmail(String emailNovo, String emailAtual) {
		EntityManager em = HibernateUtil.getEntityManager();

		try {
			
			String query = "Select case when (count(rst.email) > 0) then true else false end "
					+ "FROM ( Select resp.email FROM responsavel resp WHERE resp.email = :emailNovo and resp.email != :emailAtual  UNION  Select cid.email FROM cidadao cid INNER JOIN usuario usr ON usr.idUsuario = cid.idUsuario WHERE cid.email = :emailNovo and cid.email != :emailAtual AND usr.nick != 'cidAtendente' ) rst";
			
			BigInteger result = (BigInteger) em.createNativeQuery(query)
							.setParameter("emailNovo", emailNovo)
							.setParameter("emailAtual", emailAtual)
							.getSingleResult();
			
			boolean existe = (result.intValue() == 1);
			
			return existe;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			em.close();
		}
	}
	
	public boolean existeCPF(String cpf) {
		EntityManager em = HibernateUtil.getEntityManager();

		try {
			
			String query = "Select case when (count(cid) > 0) then true else false end FROM Cidadao cid WHERE cid.cpf = :cpf AND cid.usuario.nick != 'cidAtendente'";
			
			boolean existe = (boolean) em.createQuery(query)
					.setParameter("cpf", cpf)
					.getSingleResult();
			
			
			return existe;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			em.close();
		}
	}
	
	public boolean existeCPF(String cpfNovo, String cpfAtual) {
		EntityManager em = HibernateUtil.getEntityManager();

		try {
			
			String query = "Select case when (count(cid) > 0) then true else false end FROM Cidadao cid WHERE cid.cpf = :cpfNovo and cid.cpf != :cpfAtual AND cid.usuario.nick != 'cidAtendente'";
			
			boolean existe = (boolean) em.createQuery(query)
							.setParameter("cpfNovo", cpfNovo)
							.setParameter("cpfAtual", cpfAtual)
							.getSingleResult();
			
			
			return existe;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			em.close();
		}
	}
	
	public boolean existeRG(String rg) {
		EntityManager em = HibernateUtil.getEntityManager();

		try {
			
			String query = "Select case when (count(cid) > 0) then true else false end FROM Cidadao cid WHERE cid.rg = :rg AND cid.usuario.nick != 'cidAtendente'";
			
			boolean existe = (boolean) em.createQuery(query)
							.setParameter("rg", rg)
							.getSingleResult();
			
			return existe;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			em.close();
		}
	}
	
	public boolean existeRG(String rgNovo, String rgAtual) {
		EntityManager em = HibernateUtil.getEntityManager();

		try {
			
			String query = "Select case when (count(cid) > 0) then true else false end FROM Cidadao cid WHERE cid.rg = :rgNovo AND cid.rg = :rgAtual AND cid.usuario.nick != 'cidAtendente'";
			
			boolean existe = (boolean) em.createQuery(query)
					.setParameter("rgNovo", rgNovo)
					.setParameter("rgAtual", rgAtual)
					.getSingleResult();
			
			return existe;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			em.close();
		}
	}
}
