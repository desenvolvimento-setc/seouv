package br.gov.se.setc.DAO;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import br.gov.se.setc.model.Manifestacao;
import br.gov.se.setc.model.Mensagem;
import br.gov.se.setc.util.HibernateUtil;

public class MensagemDAO {
	
	private static MensagemDAO instance;

	private MensagemDAO(){}

	public static MensagemDAO getInstance(){
		if( instance == null )
			synchronized( MensagemDAO.class ){
				if( instance == null )
					instance = new MensagemDAO();
			}

		return instance;
	}

	private static EntityManager em = HibernateUtil.getEntityManager();

	public boolean saveOrUpdate(Mensagem mensagem) {

		try {
			if(!em.getTransaction().isActive()) em.getTransaction().begin();
        	if(mensagem.getIdMensagem() ==  null) {
        		em.persist(mensagem);
    		}else {
    			em.merge(mensagem);
    		}
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
        	em.getTransaction().rollback();
        	e.printStackTrace();
            return false;
        }
		
	}
	
	public Mensagem getMensagem(Integer id) {

		Mensagem mensagem = em.find(Mensagem.class, id);
		
		return mensagem;
	}
	
	@SuppressWarnings("unchecked")
	public List<Mensagem> listMensagensManifestacao(Integer idManifestacao) {

		List<Mensagem> result = new ArrayList<Mensagem>();
		
		String query = "Select msg FROM Mensagem as msg WHERE msg.manifestacao.idManifestacao = :idManifestacao AND (msg.tipo = 1 OR msg.tipo = 2 OR msg.tipo = 6 OR msg.tipo = 7) ORDER BY msg.data ASC";
		
		result = (List<Mensagem>) em.createQuery(query)
				.setParameter("idManifestacao", idManifestacao)
				.getResultList();
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Mensagem> listMensagensHistorico(Integer idManifestacao) {
		
		List<Mensagem> result = new ArrayList<Mensagem>();
		
		String query = "Select msg FROM Mensagem as msg WHERE msg.manifestacao.idManifestacao = :idManifestacao AND (msg.tipo = 3 OR msg.tipo = 4 OR msg.tipo = 8) ORDER BY msg.data ASC";
		
		result = (List<Mensagem>) em.createQuery(query)
				.setParameter("idManifestacao", idManifestacao)
				.getResultList();
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Mensagem> listMensagensTramite(Integer idManifestacao) {
		
		List<Mensagem> result = new ArrayList<Mensagem>();
		
		String query = "Select msg FROM Mensagem as msg WHERE msg.manifestacao.idManifestacao = :idManifestacao AND (msg.tipo = 5) ORDER BY msg.data ASC";
		
		result = (List<Mensagem>) em.createQuery(query)
				.setParameter("idManifestacao", idManifestacao)
				.getResultList();
		
		return result;
	}
	
}
