package br.gov.se.setc.DAO;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.primefaces.model.SortOrder;

import br.gov.se.setc.model.Anexo;
import br.gov.se.setc.model.Cidadao;
import br.gov.se.setc.model.DashboardInfo;
import br.gov.se.setc.model.DashboardInfo2;
import br.gov.se.setc.model.Entidade;
import br.gov.se.setc.model.Manifestacao;
import br.gov.se.setc.model.Mensagem;
import br.gov.se.setc.model.Responsavel;
import br.gov.se.setc.model.Tema;
import br.gov.se.setc.model.Usuario;
import br.gov.se.setc.util.GeralUtil;
import br.gov.se.setc.util.HibernateUtil;

public class ManifestacaoDAO {
	
	private static ManifestacaoDAO instance;

	private ManifestacaoDAO(){}

	public static ManifestacaoDAO getInstance(){
		if( instance == null )
			synchronized( ManifestacaoDAO.class ){
				if( instance == null )
					instance = new ManifestacaoDAO();
			}

		return instance;
	}

//	private static EntityManager em = HibernateUtil.getEntityManager();

	public boolean saveOrUpdate(Manifestacao manifestacao) {
		EntityManager em = HibernateUtil.getEntityManager();
		
		try {
			if(!em.getTransaction().isActive()) em.getTransaction().begin();
        	if(manifestacao.getIdManifestacao() ==  null) {
        		em.persist(manifestacao);
    		}else {
    			em.merge(manifestacao);
    		}
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
        	em.getTransaction().rollback();
        	e.printStackTrace();
            return false;
        }  finally {
			em.close();
		}
		
	}
	
	
	public Manifestacao getManifestacao(Integer id) {
		EntityManager em = HibernateUtil.getEntityManager();
		
		Manifestacao manifestacao = null;
		
		try {
			manifestacao = em.find(Manifestacao.class, id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			em.close();
		}
		
		return manifestacao;
	}
	
	@SuppressWarnings("unchecked")
	public Manifestacao getManifestacaoProtocolo(String protocolo) {
		EntityManager em = HibernateUtil.getEntityManager();
		
		List<Manifestacao> result = new ArrayList<Manifestacao>();
		
		try {
			result = new ArrayList<Manifestacao>();
			
			String query = "Select mani from Manifestacao mani LEFT JOIN FETCH mani.entidade LEFT JOIN FETCH mani.cidadao LEFT JOIN FETCH mani.mensagens msg LEFT JOIN FETCH msg.anexos LEFT JOIN FETCH mani.historico LEFT JOIN FETCH mani.tramites LEFT JOIN FETCH mani.tema where mani.protocolo = :protocolo ";
			
			result = (List<Manifestacao>) em.createQuery(query)
					.setParameter("protocolo", protocolo)
					.getResultList();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			em.close();
		}
		
		if(!result.isEmpty())
			return result.get(0);
		else
			return null;
		
	}
	
	public Integer getLastId() {
		EntityManager em = HibernateUtil.getEntityManager();
		
		Integer result = null;
		try {
			String query = "Select max(idManifestacao) from Manifestacao";
			
			result = (Integer) em.createQuery(query)
					.getSingleResult();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			em.close();
		}
		
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<Manifestacao> listManifestacoesTramitando() {
		EntityManager em = HibernateUtil.getEntityManager();
		
		List<Manifestacao> result = new ArrayList<Manifestacao>();
		
		try {
			String query = "Select mani from Manifestacao mani where mani.status NOT IN ('Finalizada', 'Sem Resposta')";
			
			result = (List<Manifestacao>) em.createQuery(query)
					.getResultList();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			em.close();
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Manifestacao> listManifestacoesTramitandoPorOrgao(Integer idEntidade) {
		EntityManager em = HibernateUtil.getEntityManager();
		
		List<Manifestacao> result = new ArrayList<Manifestacao>();
		
		try {
			String query = "Select distinct mani from Manifestacao mani LEFT JOIN FETCH mani.entidade LEFT JOIN FETCH mani.cidadao LEFT JOIN FETCH mani.mensagens msg LEFT JOIN FETCH msg.anexos LEFT JOIN FETCH mani.historico hist LEFT JOIN FETCH mani.tramites tra where mani.status NOT IN ('Finalizada', 'Sem Resposta') and mani.entidade.idEntidade = :idEntidade";
			
			result = (List<Manifestacao>) em.createQuery(query)
					.setParameter("idEntidade", idEntidade)
					.getResultList();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			em.close();
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Manifestacao> listPorProtocolo(String protocolo) {
		EntityManager em = HibernateUtil.getEntityManager();

		List<Manifestacao> result = new ArrayList<Manifestacao>();
		
		try {
			String query = "SELECT DISTINCT mani FROM Manifestacao mani LEFT JOIN FETCH mani.entidade LEFT JOIN FETCH mani.cidadao LEFT JOIN FETCH mani.mensagens msg LEFT JOIN FETCH msg.anexos LEFT JOIN FETCH mani.historico hist LEFT JOIN FETCH mani.tramites tra LEFT JOIN FETCH mani.tema WHERE replace(replace(mani.protocolo ,'-',''),'/','') like replace(replace(:protocolo,'-',''),'/','') ORDER BY mani.dataIni desc, msg.data asc, hist.data asc, tra.data asc";
			
			result = (List<Manifestacao>) em.createQuery(query)
					.setParameter("protocolo", "%"+protocolo)
					.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			 em.close();
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Manifestacao> listPorManifestante(String nome) {
		EntityManager em = HibernateUtil.getEntityManager();

		List<Manifestacao> result = new ArrayList<Manifestacao>();
		
		try {
			String query = "SELECT DISTINCT mani FROM Manifestacao mani LEFT JOIN FETCH mani.entidade LEFT JOIN FETCH mani.cidadao cid LEFT JOIN FETCH mani.mensagens msg LEFT JOIN FETCH msg.anexos LEFT JOIN FETCH mani.historico hist LEFT JOIN FETCH mani.tramites tra LEFT JOIN FETCH mani.tema WHERE cid.usuario.nome like :nome ORDER BY mani.dataIni desc, msg.data asc, hist.data asc, tra.data asc";
			
			result = (List<Manifestacao>) em.createQuery(query)
					.setParameter("nome", "%"+nome+"%")
					.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			 em.close();
		}
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<Manifestacao> listPorProtocoloEntidade(String protocolo, int idEntidade) {
		EntityManager em = HibernateUtil.getEntityManager();

		List<Manifestacao> result = new ArrayList<Manifestacao>();
		
		try {
			
			String query = "SELECT DISTINCT mani FROM Manifestacao mani LEFT JOIN FETCH mani.entidade ent LEFT JOIN FETCH mani.cidadao LEFT JOIN FETCH mani.mensagens msg LEFT JOIN FETCH msg.anexos LEFT JOIN FETCH mani.historico hist LEFT JOIN FETCH mani.tramites tra LEFT JOIN FETCH mani.tema WHERE replace(replace(mani.protocolo ,'-',''),'/','') like replace(replace(:protocolo,'-',''),'/','') and ent.idEntidade = :idEntidade  ORDER BY mani.dataIni desc, msg.data asc, hist.data asc, tra.data asc";
			
			result = (List<Manifestacao>) em.createQuery(query)
					.setParameter("protocolo", "%"+protocolo)
					.setParameter("idEntidade", idEntidade)
					.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			 em.close();
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<Manifestacao> listPorManifestanteEntidade(String nome, int idEntidade) {
		EntityManager em = HibernateUtil.getEntityManager();

		List<Manifestacao> result = new ArrayList<Manifestacao>();
		
		try {
			String query = "SELECT DISTINCT mani FROM Manifestacao mani LEFT JOIN FETCH mani.entidade ent LEFT JOIN FETCH mani.cidadao LEFT JOIN FETCH mani.mensagens msg LEFT JOIN FETCH msg.anexos LEFT JOIN FETCH mani.historico hist LEFT JOIN FETCH mani.tramites tra LEFT JOIN FETCH mani.tema WHERE cid.usuario.nome like :nome and ent.idEntidade = :idEntidade  ORDER BY mani.dataIni desc, msg.data asc, hist.data asc, tra.data asc";
			
			result = (List<Manifestacao>) em.createQuery(query)
					.setParameter("nome", "%"+nome+"%")
					.setParameter("idEntidade", idEntidade)
					.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			 em.close();
		}
		return result;
	}
	
//	==========================================================================
//	====================== CONSULTAS TELA MANIFESTAÇÕES ======================
//	==========================================================================
	public Date dataAtual() {
	    EntityManager em = HibernateUtil.getEntityManager();
	    Date result = null;

	    try {
	        String query = "SELECT CURRENT_TIMESTAMP";

	        Object singleResult = em.createNativeQuery(query).getSingleResult();

	        if (singleResult instanceof java.sql.Timestamp) {
	            result = new Date(((java.sql.Timestamp) singleResult).getTime());
	        } else if (singleResult instanceof java.util.Date) {
	            result = (Date) singleResult;
	        }
	    } catch (Exception e) {
	        e.printStackTrace(); 
	    } finally {
	        em.close();
	    }

	    return result;
	}
	// Ou
	public List<Manifestacao> listManifestacoes(Usuario usuario, int start, int size, String sortField, SortOrder sortOrder, Map<String, Object> filters, short filtro) {
		EntityManager em = HibernateUtil.getEntityManager();
		
		
		List<Integer> listIds = new ArrayList<>();
		List<Manifestacao> list = new ArrayList<Manifestacao>();
		
		try {
			CriteriaBuilder builder = em.getCriteriaBuilder();
			CriteriaQuery<Integer> criteria = builder.createQuery(Integer.class);
			
			Root<Manifestacao> manifestacao = criteria.from(Manifestacao.class);
			
			// Joins
			Join<Manifestacao,Entidade> entJoin = manifestacao.join("entidade");
			Join<Manifestacao,Tema> temaJoin = manifestacao.join("tema");
			Join<Manifestacao,Cidadao> cidadaoJoin = manifestacao.join("cidadao");
			Join<Cidadao,Usuario> usuarioJoin = cidadaoJoin.join("usuario");
			
			// Condições
			List<Predicate> predicates = new ArrayList<Predicate>();

			// Filtro por tipo de usuario
			if(usuario != null) {

				// Verifica se é cidadão
				if(usuario.getPerfil() == 3) {
					System.out.println("Filtrou usuário");
					predicates.add(builder.equal(usuarioJoin.get("idUsuario"), usuario.getIdUsuario()));
				}
				
				// Verifica se é responsável
				else if(usuario.getPerfil() == 2) {
					Set<Responsavel> respList = usuario.getResponsaveis();
					
					boolean isOGE = false;
					for (Responsavel resp : respList) {
						if (resp.getEntidade().getSigla().equals("OGE")) {
							// Sem filtro
							isOGE = true;
						}
					}

					if (!isOGE) {
						for (Responsavel resp : respList) {
							if (resp.isAtivo()) {
								if (resp.getEntidade().getIdEntidade() == 116) {
									Predicate agreseCondition = builder.or(builder.equal(entJoin.get("idEntidade"), resp.getEntidade().getIdEntidade()), builder.isNotNull(manifestacao.get("protocoloAgrese")));
		
									predicates.add(agreseCondition);
								} else {
									predicates.add(builder.equal(entJoin.get("idEntidade"), resp.getEntidade().getIdEntidade()));
								}
							}
						}
					}

				}

				// Verifica se é admin
				else if(usuario.getPerfil() == 5 || usuario.getPerfil() == 6) {
				}

			} else {
				System.out.println("Usuário não encontrado");
			}
			
			// Filtros dashbaord vencimento
			if(filtro == 1) { // Vencendo
				predicates.add(builder.lessThanOrEqualTo(manifestacao.<Date>get("dataLimite"), Date.from(LocalDate.now().plusDays(5).atStartOfDay(ZoneId.systemDefault()).toInstant())));
				predicates.add(builder.and(builder.notEqual(manifestacao.get("status"), "Finalizada"), builder.notEqual(manifestacao.get("status"), "Sem Resposta"), builder.notEqual(manifestacao.get("status"), "Atendida")));
			} else if(filtro == 2) { // Não Visualizadas
				predicates.add(builder.equal(manifestacao.get("status"), "Aberta"));
			} else if(filtro == 3) { // Inativas
				predicates.add(builder.equal(entJoin.get("ativa"), 0));
				predicates.add(builder.and(builder.notEqual(manifestacao.get("status"), "Finalizada"), builder.notEqual(manifestacao.get("status"), "Sem Resposta")));
			}

			
			List<Order> orderList = new ArrayList<Order>();

			Path<?> path = GeralUtil.getPath(sortField, manifestacao, entJoin, temaJoin, usuarioJoin);
			if (sortOrder == null){
				//just don't sort
			} else if (sortOrder.equals(SortOrder.ASCENDING)){
				orderList = new ArrayList<Order>();
				orderList.add(builder.asc(path));
			} else if (sortOrder.equals(SortOrder.DESCENDING)){
				orderList = new ArrayList<Order>();
				orderList.add(builder.desc(path));
			} else if (sortOrder.equals(SortOrder.UNSORTED)){
				//just don't sort
			} else{
				//just don't sort
			}
			
			criteria.orderBy(orderList);
			
			//    	// Filter
			Predicate filterCondition = builder.conjunction();
			for (Map.Entry<String, Object> filter : filters.entrySet()) {
				if (!filter.getValue().equals("")) {
					//try as string using like
					if (filter.getKey().equals("dataIni") || filter.getKey().equals("dataLimite") || filter.getKey().equals("datafim")) {
						//    				System.out.println(" --- Filtrou por data --- ");
						try {
							Predicate startDateCondition, endDateCondition = builder.conjunction();
							
							FastDateFormat fdt = FastDateFormat.getInstance("EEE MMM dd HH:mm:ss z yyyy", TimeZone.getTimeZone("America/Maceio"), Locale.US);
							
							Date data = fdt.parse(filter.getValue().toString());
							
							Date startDate = data;
							Date endDate = data;
							
							startDate = DateUtils.setHours(startDate, 0);
							startDate = DateUtils.setMinutes(startDate, 0);
							startDate = DateUtils.setSeconds(startDate, 0);
							
							endDate = DateUtils.setHours(endDate, 23);
							endDate = DateUtils.setMinutes(endDate, 59);
							endDate = DateUtils.setSeconds(endDate, 59);

							startDateCondition = builder.and(filterCondition, builder.greaterThanOrEqualTo(manifestacao.<Date>get(filter.getKey()), startDate));
							endDateCondition = builder.and(filterCondition, builder.lessThanOrEqualTo(manifestacao.<Date>get(filter.getKey()), endDate));

							predicates.add(startDateCondition);
							predicates.add(endDateCondition);
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {
						Path<String> pathFilter = GeralUtil.getStringPath(filter.getKey(), manifestacao, entJoin, temaJoin, usuarioJoin);
						if (pathFilter != null){
								    				System.out.println("-- Filtrou String:" + filter.getValue());
							filterCondition = builder.and(filterCondition, builder.like(pathFilter, "%"+filter.getValue()+"%"));
						}else{
							//	    				try as non-string using equal
								    				System.out.println("-- Filtrou Objeto:" + filter.getValue());
							Path<?> pathFilterNonString = GeralUtil.getPath(filter.getKey(), manifestacao, entJoin, temaJoin, usuarioJoin);
							filterCondition = builder.and(filterCondition, builder.equal(pathFilterNonString, filter.getValue()));
						}
					}
				} 
			}

			predicates.add(filterCondition);

			criteria.where(predicates.toArray(new Predicate[] {}));

			criteria.select(manifestacao.get("idManifestacao")).distinct(true);

			TypedQuery<Integer> query = em.createQuery(criteria);
			query.setFirstResult(start);
			query.setMaxResults(size);

			listIds = query.getResultList();
			
//			System.out.println("Quantidade de manifestações encontradas: " + listIds.size());
			
			if(listIds.size() > 0) {
				CriteriaQuery<Manifestacao> criteria2 = builder.createQuery(Manifestacao.class);
				
				Root<Manifestacao> manifestacao2 = criteria2.from(Manifestacao.class);
				
				Fetch<Manifestacao, Entidade> entFetch = manifestacao2.fetch("entidade", JoinType.LEFT);
				Fetch<Manifestacao, Tema> temaFetch = manifestacao2.fetch("tema", JoinType.LEFT);
				Fetch<Manifestacao, Cidadao> cidadaoFetch = manifestacao2.fetch("cidadao", JoinType.LEFT);
				Fetch<Cidadao, Usuario> usuarioFetch = cidadaoFetch.fetch("usuario", JoinType.LEFT);
				Fetch<Manifestacao, Responsavel> responsavelFetch = manifestacao2.fetch("responsavel", JoinType.LEFT);
				Fetch<Responsavel, Usuario> usuarioResposnavelFetch = responsavelFetch.fetch("usuario", JoinType.LEFT);
				
				Fetch<Manifestacao, Mensagem> mensagensFetch = manifestacao2.fetch("mensagens", JoinType.LEFT);
				Fetch<Mensagem, Anexo> anexosFetch = mensagensFetch.fetch("anexos", JoinType.LEFT);
				
				Fetch<Manifestacao, Mensagem> historicoFetch = manifestacao2.fetch("historico", JoinType.LEFT);
				Fetch<Manifestacao, Mensagem> tramitesFetch = manifestacao2.fetch("tramites", JoinType.LEFT);
				
				criteria2.orderBy(orderList);
				
				criteria2.select(manifestacao2).where(manifestacao2.get("idManifestacao").in(listIds)).distinct(true);
				
				TypedQuery<Manifestacao> query2 = em.createQuery(criteria2);
				
				list = query2.getResultList();
			} else {
				System.out.println("Manifestações não encontradas");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			em.close();
		}

		return list;
		
	}
	
	
	public int countManifestacoes(Usuario usuario, Map<String, Object> filters, short filtro) {
		EntityManager em = HibernateUtil.getEntityManager();
		
		int count = 0;
		
		try {
			CriteriaBuilder builder = em.getCriteriaBuilder();
			CriteriaQuery<Long> criteria = builder.createQuery(Long.class);

			// Consulta raiz
			Root<Manifestacao> solicitacao = criteria.from(Manifestacao.class);

			// Joins
			Join<Manifestacao,Entidade> entJoin = solicitacao.join("entidade");
			Join<Manifestacao,Tema> temaJoin = solicitacao.join("tema");
			Join<Manifestacao,Cidadao> cidadaoJoin = solicitacao.join("cidadao");
			Join<Cidadao,Usuario> usuarioJoin = cidadaoJoin.join("usuario");

			// Condições
			List<Predicate> predicates = new ArrayList<Predicate>();


			// Filtro por tipo de usuario
			if(usuario != null) {

				// Verifica se é cidadão
				if(usuario.getPerfil() == 3) {
					predicates.add(builder.equal(usuarioJoin.get("idUsuario"), usuario.getIdUsuario()));
				}

				// Verifica se é responsável
				else if(usuario.getPerfil() == 2) {
					Set<Responsavel> respList = usuario.getResponsaveis();

					boolean isOGE = false;
					
					for (Responsavel resp : respList) {
						if (resp.getEntidade().getIdEntidade() == 166) {
							isOGE = true;
						}
					}

					if (!isOGE) {
						for (Responsavel resp : respList) {
							if (resp.getEntidade().getIdEntidade() == 116) {
								Predicate agreseCondition = builder.or(builder.equal(entJoin.get("idEntidade"), resp.getEntidade().getIdEntidade()), builder.isNotNull(solicitacao.get("protocoloAgrese")));
	
								predicates.add(agreseCondition);
							} else {
								predicates.add(builder.equal(entJoin.get("idEntidade"), resp.getEntidade().getIdEntidade()));
							}
						}
					}

				}

				// Verifica se é admin
				else if(usuario.getPerfil() == 5 || usuario.getPerfil() == 6) {
					// Sem filtro
				}
			}
			
			// Filtros dashbaord vencimento
			if(filtro == 1) { // Vencendo
				predicates.add(builder.lessThanOrEqualTo(solicitacao.<Date>get("dataLimite"), Date.from(LocalDate.now().plusDays(5).atStartOfDay(ZoneId.systemDefault()).toInstant())));
				predicates.add(builder.and(builder.notEqual(solicitacao.get("status"), "Finalizada"), builder.notEqual(solicitacao.get("status"), "Sem Resposta"), builder.notEqual(solicitacao.get("status"), "Atendida")));
			} else if(filtro == 2) { // Não Visualizadas
				predicates.add(builder.equal(solicitacao.get("status"), "Aberta"));
			} else if(filtro == 3) { // Inativas
				predicates.add(builder.equal(entJoin.get("ativa"), 0));
				predicates.add(builder.and(builder.notEqual(solicitacao.get("status"), "Finalizada"), builder.notEqual(solicitacao.get("status"), "Sem Resposta")));
			}

			// Filter
			Predicate filterCondition = builder.conjunction();
			for (Map.Entry<String, Object> filter : filters.entrySet()) {
				if (!filter.getValue().equals("")) {
					if (filter.getKey().equals("dataIni") || filter.getKey().equals("dataLimite") || filter.getKey().equals("datafim")) {
						//    				System.out.println(" --- Filtrou por data --- ");
						try {
							Predicate startDateCondition, endDateCondition = builder.conjunction();
							
							FastDateFormat fdt = FastDateFormat.getInstance("EEE MMM dd HH:mm:ss z yyyy", TimeZone.getTimeZone("America/Maceio"), Locale.US);
							
							Date data = fdt.parse(filter.getValue().toString());
							
//							System.out.println("::::::::::: DATA: " + data);
							
							Date startDate = data;
							Date endDate = data;
							
							startDate = DateUtils.setHours(startDate, 0);
							startDate = DateUtils.setMinutes(startDate, 0);
							startDate = DateUtils.setSeconds(startDate, 0);
							
							endDate = DateUtils.setHours(endDate, 23);
							endDate = DateUtils.setMinutes(endDate, 59);
							endDate = DateUtils.setSeconds(endDate, 59);

							startDateCondition = builder.and(filterCondition, builder.greaterThanOrEqualTo(solicitacao.<Date>get(filter.getKey()), startDate));
							endDateCondition = builder.and(filterCondition, builder.lessThanOrEqualTo(solicitacao.<Date>get(filter.getKey()), endDate));

							predicates.add(startDateCondition);
							predicates.add(endDateCondition);
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} else {
						//try as string using like
						
						Path<String> pathFilter = GeralUtil.getStringPath(filter.getKey(), solicitacao, entJoin, temaJoin, usuarioJoin);
						if (pathFilter != null) {
							filterCondition = builder.and(filterCondition, builder.like(pathFilter, "%"+filter.getValue()+"%"));
						} else {
							//try as non-string using equal
							Path<?> pathFilterNonString = GeralUtil.getPath(filter.getKey(), solicitacao, entJoin, temaJoin, usuarioJoin);
							filterCondition = builder.and(filterCondition, builder.equal(pathFilterNonString, filter.getValue()));
						}
					}
				} 
			}

			predicates.add(filterCondition);

			criteria.where(predicates.toArray(new Predicate[0]));

			criteria.select(builder.count(solicitacao)).distinct(true);
			
			count = Math.toIntExact(em.createQuery(criteria).getResultList().get(0));
			
//			System.out.println("countManifestacoes: " + count);
			
			return count;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			em.close();
		}
		
		
		return count;
	}
	
	
//	=================================================================
//	====================== CONSULTAS DASHBOARD ======================
//	=================================================================
	
	// Cidadao
	public DashboardInfo contarManifestacoesCidadao(Integer idCidadao) {
		EntityManager em = HibernateUtil.getEntityManager();
		
		DashboardInfo result = new DashboardInfo();
		
		try {
			result = new DashboardInfo();
			
			String query = "SELECT "
					+ "(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE mani.idCidadao = :idCidadao) as total, "
					+ "(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE (mani.status like 'Atendida' or mani.status like 'Finalizada') AND mani.idCidadao = :idCidadao) as atendidas, "
					+ "(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE (mani.status like 'Sem resposta' or mani.status like 'Negada') AND mani.idCidadao = :idCidadao) as semResposta, "
					+ "(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE (mani.status like 'Tramitando' or mani.status like 'Reencaminhada' or mani.status like 'Prorrogada' or mani.status like 'Recurso' or mani.status like 'Reformulação') AND mani.idCidadao = :idCidadao) as emTramite, "
					+ "(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE mani.visualizada = 0 AND mani.status like 'Aberta' AND mani.idCidadao = :idCidadao) as naoVisualizadas ";
			
			result = (DashboardInfo) em.createNativeQuery(query, "DashboardInfo")
					.setParameter("idCidadao", idCidadao)
					.getSingleResult();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			em.close();
		}
		
		return result;
	}
	
	// Ouvidor
	public DashboardInfo contarManifestacoesOuvidor(Integer idEntidade) {
		EntityManager em = HibernateUtil.getEntityManager();
		
		DashboardInfo result = new DashboardInfo();
		try {
			result = new DashboardInfo();
			
			String query = "SELECT "
					+ "(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE mani.idEntidades = :idEntidade) as total, "
					+ "(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE (mani.status like 'Atendida' or mani.status like 'Finalizada') AND mani.idEntidades = :idEntidade) as atendidas, "
					+ "(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE (mani.status like 'Sem resposta' or mani.status like 'Negada') AND mani.idEntidades = :idEntidade) as semResposta, "
					+ "(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE (mani.status like 'Tramitando' or mani.status like 'Reencaminhada' or mani.status like 'Prorrogada' or mani.status like 'Recurso' or mani.status like 'Reformulação') AND mani.idEntidades = :idEntidade) as emTramite, "
					+ "(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE mani.visualizada = 0 AND mani.status like 'Aberta' AND mani.idEntidades = :idEntidade) as naoVisualizadas ";
			
			result = (DashboardInfo) em.createNativeQuery(query, "DashboardInfo")
					.setParameter("idEntidade", idEntidade)
					.getSingleResult();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			em.close();
		}
		
		return result;
	}
	
	// Por tipo
	public DashboardInfo contarManifestacoesPorTipo(String tipo) {
		EntityManager em = HibernateUtil.getEntityManager();
		
		DashboardInfo result = new DashboardInfo();
		
		try {
			result = new DashboardInfo();
			
			String query = "SELECT "
					+ "(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE mani.tipo = :tipo) as total, "
					+ "(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE (mani.status like 'Atendida' or mani.status like 'Finalizada') AND mani.tipo = :tipo) as atendidas, "
					+ "(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE (mani.status like 'Sem resposta' or mani.status like 'Negada') AND mani.tipo = :tipo) as semResposta, "
					+ "(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE (mani.status like 'Tramitando' or mani.status like 'Reencaminhada' or mani.status like 'Prorrogada' or mani.status like 'Recurso' or mani.status like 'Reformulação') AND mani.tipo = :tipo) as emTramite, "
					+ "(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE mani.visualizada = 0 AND mani.status like 'Aberta' AND mani.tipo = :tipo) as naoVisualizadas ";
			
			result = (DashboardInfo) em.createNativeQuery(query, "DashboardInfo")
					.setParameter("tipo", tipo)
					.getSingleResult();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			em.close();
		}
		
		return result;
	}
	
	public DashboardInfo contarManifestacoesPorTipo(String tipo, Integer idEntidade) {
		EntityManager em = HibernateUtil.getEntityManager();
		
		DashboardInfo result = new DashboardInfo();
		
		try {
			result = new DashboardInfo();
			
			String query = "SELECT "
					+ "(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE mani.tipo = :tipo AND mani.idEntidades = :idEntidade) as total, "
					+ "(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE (mani.status like 'Atendida' or mani.status like 'Finalizada') AND mani.tipo = :tipo AND mani.idEntidades = :idEntidade) as atendidas, "
					+ "(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE (mani.status like 'Sem resposta' or mani.status like 'Negada') AND mani.tipo = :tipo AND mani.idEntidades = :idEntidade) as semResposta, "
					+ "(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE (mani.status like 'Tramitando' or mani.status like 'Reencaminhada' or mani.status like 'Prorrogada' or mani.status like 'Recurso' or mani.status like 'Reformulação') AND mani.tipo = :tipo AND mani.idEntidades = :idEntidade) as emTramite, "
					+ "(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE mani.visualizada = 0 AND mani.status like 'Aberta' AND mani.tipo = :tipo AND mani.idEntidades = :idEntidade) as naoVisualizadas ";
			
			result = (DashboardInfo) em.createNativeQuery(query, "DashboardInfo")
					.setParameter("tipo", tipo)
					.setParameter("idEntidade", idEntidade)
					.getSingleResult();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			em.close();
		}
		
		return result;
	}
	
	public DashboardInfo2 contarManifestacoesSituacaoVencimento() throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		
		DashboardInfo2 result = new DashboardInfo2();
		
		try {
			String query = "SELECT " + 
					"(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE mani.idEntidades) as total, " + 
					"(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE DATE(mani.dataLimite) <= (NOW() + INTERVAL 5 DAY) and mani.status not in ('Sem Resposta', 'Finalizada', 'Atendida')) as vencendo, " + 
					"(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE mani.status = 'Aberta') as naoVisualizadas, " + 
					"(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani INNER JOIN entidades ent ON ent.idEntidades = mani.idEntidades WHERE mani.status not in ('Sem Resposta', 'Finalizada') AND ent.ativa = 0) as inativas; ";
			
			result = (DashboardInfo2) em.createNativeQuery(query, "DashboardInfo2")
					.getSingleResult();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			em.close();
		}
		
		return result;
	}
	
	public DashboardInfo2 contarManifestacoesSituacaoVencimentoOrgao(Integer idEntidade) throws Exception {
		EntityManager em = HibernateUtil.getEntityManager();
		
		DashboardInfo2 result = new DashboardInfo2();
		
		try {
			String query = "";
			if(idEntidade == 116) {
				query = "SELECT " + 
						"(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE mani.idEntidades = :idEntidade or mani.protocoloAgrese is not null) as total, " + 
						"(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE (DATE(mani.dataLimite) <= (NOW() + INTERVAL 5 DAY) and mani.status not in ('Sem Resposta', 'Finalizada', 'Atendida')) and (mani.idEntidades = :idEntidade or mani.protocoloAgrese is not null)) as vencendo, " + 
						"(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE mani.status = 'Aberta' AND (mani.idEntidades = :idEntidade or mani.protocoloAgrese is not null)) as naoVisualizadas, " + 
						"0 as inativas ";
			} else {
				query = "SELECT " + 
						"(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE mani.idEntidades = :idEntidade) as total, " + 
						"(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE DATE(mani.dataLimite) <= (NOW() + INTERVAL 5 DAY) and mani.status not in ('Sem Resposta', 'Finalizada', 'Atendida') and mani.idEntidades = :idEntidade) as vencendo, " + 
						"(SELECT COUNT(distinct mani.idSolicitacao) FROM solicitacao mani WHERE mani.status = 'Aberta' AND mani.idEntidades = :idEntidade) as naoVisualizadas, " +
						"0 as inativas";
			}
			
			result = (DashboardInfo2) em.createNativeQuery(query, "DashboardInfo2")
					.setParameter("idEntidade", idEntidade)
					.getSingleResult();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			em.close();
		}
		
		return result;
	}

}
