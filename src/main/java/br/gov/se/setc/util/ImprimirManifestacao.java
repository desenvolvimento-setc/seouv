package br.gov.se.setc.util;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import br.gov.se.setc.model.Manifestacao;
import br.gov.se.setc.model.Mensagem;

@ManagedBean(name = "printManifestacao")
@SessionScoped
public class ImprimirManifestacao implements Serializable {

	private static final long serialVersionUID = 1L;

	private StreamedContent file;

	private final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	private final SimpleDateFormat formatter2 = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	
	private String getFilePath(String fileName) {
		ClassLoader classLoader = getClass().getClassLoader();
        String path = classLoader.getResource(fileName).getPath().replaceAll("%23", "#");
        
        if (System.getProperty("os.name").startsWith("Windows"))
        	path = classLoader.getResource(fileName).getPath().replaceFirst("/", "").replaceAll("%23", "#");
        else 
        	path = classLoader.getResource(fileName).getPath().replaceAll("%23", "#");
        
        if (path == null) {
            throw new IllegalArgumentException("file not found! " + fileName);
        } else {
            return path;
        }
	}
	
	public void imprimirManifestacao(Manifestacao solicitacao) throws IOException {
		List<String> mensagens = listaMensagens(solicitacao);

		PDFont font = PDType1Font.HELVETICA;
		PDFont fontBold = PDType1Font.HELVETICA_BOLD;

		Color titulo = new Color(26, 86, 140);
		Color texto = Color.DARK_GRAY;

		int marginTop = 30;
		int fontSize = 12;

		PDDocument doc = new PDDocument();

		PDPage page = new PDPage(new PDRectangle(PDRectangle.A4.getWidth(), PDRectangle.A4.getHeight()));
		doc.addPage(page);
		PDPageContentStream content = new PDPageContentStream(doc, page);

		PDRectangle pageSize = page.getMediaBox();
		float pageWidth = pageSize.getWidth();
		float pageHeight = pageSize.getHeight();
		
		Dimension scaledDim = new Dimension();
		
		try {
			String path = ImprimirManifestacao.class.getResource("/images/header.png").getPath();


			// ================================= //
			// Imprime cabeçalho da manifestação //
			// ================================= //
			PDImageXObject pdImage = PDImageXObject.createFromFile(getFilePath("images/header.png"), doc);
			scaledDim = getScaledDimension(new Dimension(pdImage.getWidth(), pdImage.getHeight()), new Dimension((int) (pageWidth), (int) pageHeight));
			content.drawImage(pdImage, centralizar(pageWidth, scaledDim.width), pageHeight-scaledDim.height-10, scaledDim.width, scaledDim.height);
			
			content.beginText();
			content.setLeading(15f);
			content.newLineAtOffset(50, startY(font, fontSize, pageHeight, 100));

			// Protocolo
			content.setNonStrokingColor(titulo);
			content.setFont(fontBold, fontSize);
			content.showText("Protocolo: ");
			content.setNonStrokingColor(texto);
			content.setFont(font, fontSize);
			content.showText(solicitacao.getProtocolo());
			content.newLine();

			// Solicitante
			content.setNonStrokingColor(titulo);
			content.setFont(fontBold, fontSize);
			content.showText("Solicitante: ");
			content.setNonStrokingColor(texto);
			content.setFont(font, fontSize);
			if (solicitacao.getSigilo() == 1)
				content.showText("(Sigiloso)");
			else
				content.showText(removeLineBreak(solicitacao.getCidadao().getUsuario().getNome()));
			content.newLine();

			// Tema
			content.setNonStrokingColor(titulo);
			content.setFont(fontBold, fontSize);
			content.showText("Tema: ");
			content.setNonStrokingColor(texto);
			content.setFont(font, fontSize);
			content.showText(removeLineBreak(solicitacao.getTema().getTitulo()));
			content.newLine();

			// Assunto
			content.setNonStrokingColor(titulo);
			content.setFont(fontBold, fontSize);
			content.showText("Assunto: ");
			content.setNonStrokingColor(texto);
			content.setFont(font, fontSize);
			if (solicitacao.getTitulo().isEmpty())
				content.showText("");
			else
				content.showText(removeLineBreak(solicitacao.getTitulo()));
			content.newLine();

			// Órgão
			content.setNonStrokingColor(titulo);
			content.setFont(fontBold, fontSize);
			content.showText("Órgão / Entidade: ");
			content.setNonStrokingColor(texto);
			content.setFont(font, fontSize);
			content.showText(solicitacao.getEntidade().getSigla() + " - " + solicitacao.getEntidade().getNome());
			content.newLine();

			// Tipo de manifestação
			content.setNonStrokingColor(titulo);
			content.setFont(fontBold, fontSize);
			content.showText("Tipo de manifestação: ");
			content.setNonStrokingColor(texto);
			content.setFont(font, fontSize);
			if (solicitacao.getTipo().equals("Informação"))
				content.showText("Pedido de Informação (e-SIC)");
			else
				content.showText(solicitacao.getTipo());
			content.newLine();

			// Status
			content.setNonStrokingColor(titulo);
			content.setFont(fontBold, fontSize);
			content.showText("Status: ");
			content.setNonStrokingColor(texto);
			content.setFont(font, fontSize);
			content.showText(solicitacao.getStatus());
			content.newLine();

			// Data Inicial
			content.setNonStrokingColor(titulo);
			content.setFont(fontBold, fontSize);
			content.showText("Data Inicial: ");
			content.setNonStrokingColor(texto);
			content.setFont(font, fontSize);
			content.showText(formatter.format(solicitacao.getDataIni()));
			content.newLine();

			if (solicitacao.getDatafim() != null) {
				// Data Final
				content.setNonStrokingColor(titulo);
				content.setFont(fontBold, fontSize);
				content.showText("Data Final: ");
				content.setNonStrokingColor(texto);
				content.setFont(font, fontSize);
				content.showText(formatter.format(solicitacao.getDatafim()));
				content.newLine();
			} else if (solicitacao.getDataLimite() != null) {
				// Data Limite
				content.setNonStrokingColor(titulo);
				content.setFont(fontBold, fontSize);
				content.showText("Data Limite: ");
				content.setNonStrokingColor(texto);
				content.setFont(font, fontSize);
				content.showText(formatter.format(solicitacao.getDataLimite()));
				content.newLine();
			}

			content.newLine();

			int lineCount = 11;
			int idLinha = 0;
			int pageCount = 0;

			// Imprime primeira página
			for (int i = 0; i < mensagens.size(); i++) {
				content.showText(mensagens.get(i).replace('\u00A0', ' '));
				content.newLine();
				lineCount++;
				idLinha = i;
				if (lineCount >= 45) break;
			}

			idLinha++;
			pageCount++;

			content.endText();
			
			content.beginText();
			content.newLineAtOffset(300, 20);
			content.showText(new Integer(pageCount).toString());
			content.endText();
			
			content.close();

			// Imprime demais páginas
			while (idLinha < mensagens.size()) {
				page = new PDPage(new PDRectangle(PDRectangle.A4.getWidth(), PDRectangle.A4.getHeight()));
				doc.addPage(page);
				content = new PDPageContentStream(doc, page);
				
				// ================================= //
				// Imprime cabeçalho da manifestação //
				// ================================= //
				content.drawImage(pdImage, centralizar(pageWidth, scaledDim.width), pageHeight-scaledDim.height-10, scaledDim.width, scaledDim.height);
				
				content.beginText();
				content.setLeading(15f);
				content.newLineAtOffset(50, startY(font, fontSize, pageHeight, 100));
				
				content.setNonStrokingColor(texto);
				content.setFont(font, fontSize);

				lineCount = 0;

				for (int i = idLinha; i < mensagens.size(); i++) {
					content.showText(mensagens.get(i).replace('\u00A0', ' '));
					content.newLine();
					lineCount++;
					idLinha = i;
					if (lineCount >= 45) break;
				}
				

				idLinha++;
				pageCount++;

				content.endText();
				
				content.beginText();
				content.newLineAtOffset(300, 20);
				content.showText(new Integer(pageCount).toString());
				content.endText();
				
				content.close();
			}

			// Fazer download do PDF
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			doc.save(output);
			byte[] buf = output.toByteArray();
			InputStream in = new ByteArrayInputStream(buf);
			this.file = new DefaultStreamedContent(in, "application/pdf", solicitacao.getProtocolo() + ".pdf");
			doc.close();

		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Algo deu errado", "Falha ao imprimir manifestação"));
			e.printStackTrace();
			doc.close();
		}

	}

	public void imprimirProtocolo(Manifestacao manifestacao) throws IOException {
		PDDocument doc = null;
		PDPage page = null;

		PDFont font = PDType1Font.HELVETICA;
		PDFont fontBold = PDType1Font.HELVETICA_BOLD;

		Color titulo = new Color(26, 86, 140);
		Color texto = Color.DARK_GRAY;

		doc = new PDDocument();
		page = new PDPage();

		int marginTop = 30;
		int fontSize = 12;

		PDRectangle pageSize = page.getMediaBox();
		float pageHeight = pageSize.getHeight();
		float pageWidth = pageSize.getWidth();
		
		Dimension scaledDim = new Dimension();
		
		try {

			PDPageContentStream content = new PDPageContentStream(doc, page);
			
//			String path = "";
//
//			if (System.getProperty("os.name").startsWith("Windows"))
//				path = ImprimirManifestacao.class.getResource("/").getPath().replaceFirst("/", "");
//			if (System.getProperty("os.name").equals("Linux"))
//				path = ImprimirManifestacao.class.getResource("/").getPath();

			// System.out.println(path);

			// ================================= //
			// Imprime cabeçalho da manifestação //
			// ================================= //
			PDImageXObject pdImage = PDImageXObject.createFromFile(getFilePath("images/header.png"), doc);
			scaledDim = getScaledDimension(new Dimension(pdImage.getWidth(), pdImage.getHeight()), new Dimension((int) (pageWidth), (int) pageHeight));
			content.drawImage(pdImage, centralizar(pageWidth, scaledDim.width), pageHeight-scaledDim.height-10, scaledDim.width, scaledDim.height);
			
			content.beginText();
			content.setLeading(15f);
			content.newLineAtOffset(50, startY(font, fontSize, pageHeight, 100));
			
			// Solicitante
			content.setNonStrokingColor(titulo);
			content.setFont(fontBold, fontSize);
			content.showText("Solicitante: ");
			content.setNonStrokingColor(texto);
			content.setFont(font, fontSize);
			if (manifestacao.getSigilo() == 1)
				content.showText("(Sigiloso)");
			else
				content.showText(manifestacao.getCidadao().getUsuario().getNome());
			content.newLine();

			// Tema
			content.setNonStrokingColor(titulo);
			content.setFont(fontBold, fontSize);
			content.showText("Tema: ");
			content.setNonStrokingColor(texto);
			content.setFont(font, fontSize);
			content.showText(manifestacao.getTema().getTitulo());
			content.newLine();

			// Assunto
			content.setNonStrokingColor(titulo);
			content.setFont(fontBold, fontSize);
			content.showText("Assunto: ");
			content.setNonStrokingColor(texto);
			content.setFont(font, fontSize);
			if (manifestacao.getTitulo().isEmpty())
				content.showText("");
			else
				content.showText(removeLineBreak(manifestacao.getTitulo()));
			content.newLine();

			// Órgão
			content.setNonStrokingColor(titulo);
			content.setFont(fontBold, fontSize);
			content.showText("Órgão / Entidade: ");
			content.setNonStrokingColor(texto);
			content.setFont(font, fontSize);
			content.showText(manifestacao.getEntidade().getSigla() + " - " + manifestacao.getEntidade().getNome());
			content.newLine();

			// Tipo de manifestação
			content.setNonStrokingColor(titulo);
			content.setFont(fontBold, fontSize);
			content.showText("Tipo de manifestação: ");
			content.setNonStrokingColor(texto);
			content.setFont(font, fontSize);
			if (manifestacao.getTipo().equals("Informação"))
				content.showText("Pedido de Informação (e-SIC)");
			else
				content.showText(manifestacao.getTipo());
			content.newLine();

			// Status
			content.setNonStrokingColor(titulo);
			content.setFont(fontBold, fontSize);
			content.showText("Status: ");
			content.setNonStrokingColor(texto);
			content.setFont(font, fontSize);
			content.showText(manifestacao.getStatus());
			content.newLine();

			// Data Inicial
			content.setNonStrokingColor(titulo);
			content.setFont(fontBold, fontSize);
			content.showText("Data Inicial: ");
			content.setNonStrokingColor(texto);
			content.setFont(font, fontSize);
			content.showText(formatter.format(manifestacao.getDataIni()));
			content.newLine();

			// Data Limite
			content.setNonStrokingColor(titulo);
			content.setFont(fontBold, fontSize);
			content.showText("Data Limite: ");
			content.setNonStrokingColor(texto);
			content.setFont(font, fontSize);
			content.showText(formatter.format(manifestacao.getDataLimite()));
			content.newLine();

			content.newLine();
			content.newLine();

			content.setNonStrokingColor(texto);
			content.setFont(fontBold, fontSize);
			content.showText("Protocolo: ");
			content.newLine();
			// content.newLineAtOffset((float) (startX(fontBold, 50,
			// solicitacao.getProtocolo(), pageWidth)*0.7),
			// fontBold.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * 50);
			// content.newLine
			content.newLineAtOffset((float) (startX(fontBold, 50, manifestacao.getProtocolo(), pageWidth) * 0.7),
					fontBold.getFontDescriptor().getFontBoundingBox().getHeight() / 800);
			content.setNonStrokingColor(titulo);
			content.setFont(fontBold, 50);
			content.showText(manifestacao.getProtocolo());
			content.newLine();

			content.endText();
			content.close();

			doc.addPage(page);

			// Fazer download do PDF
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			doc.save(output);
			byte[] buf = output.toByteArray();
			InputStream in = new ByteArrayInputStream(buf);
			this.file = new DefaultStreamedContent(in, "application/pdf", manifestacao.getProtocolo() + ".pdf");
			doc.close();

		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro", "Falha ao imprimir manifestação"));
			e.printStackTrace();
			doc.close();
		}
	}

	private List<String> listaMensagens(Manifestacao sol) {
		formatter2.setTimeZone(TimeZone.getTimeZone("GMT+0"));
		
		List<String> mensagens = new ArrayList<>();
			try {
	
			Set<Mensagem> msgsManifestacao = sol.getMensagens();
			Set<Mensagem> msgsHistorico = sol.getHistorico();
	
			mensagens.add("Manifestação:");
			for (Mensagem msg : msgsManifestacao) {
				if (sol.getCidadao().getUsuario().getNome().equals(msg.getUsuario().getNome())) {
					if (sol.getSigilo() == 1)
						mensagens.add("Manifestante"  + " - " + formatter2.format(msg.getData()) + " :");
					else
						mensagens.add(msg.getUsuario().getNome() + " - " + formatter2.format(msg.getData()) + " :");
				} else {
					mensagens.add(msg.getUsuario().getNome() + " [ Ouvidor(a) ] - " + formatter2.format(msg.getData()) + " :");
				}
				
				List<String> mensagem = wrapText(msg.getTexto());
				mensagens.addAll(mensagem);
			}
	
			mensagens.add("Histórico:");
			for (Mensagem msg : msgsHistorico) {
				mensagens.add(msg.getUsuario().getNome() + " - " + formatter2.format(msg.getData()) + " :");
				List<String> mensagem = wrapText(msg.getTexto());
				mensagens.addAll(mensagem);
			}
		} catch (Exception e) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro", "Falha ao recuperar manifestação"));
			e.printStackTrace();
		}

		return mensagens;
	}

	private List<String> wrapText(String text) {
		List<String> linhas = new ArrayList<String>();
		
		try {

			text = removeLineBreak(text);

			String[] paragrafos = text.split(" -- ");

			for (String parag : paragrafos) {

				int tamanho = parag.length();
				int limite = 65;
				int limiteWidth = 450;

				if (tamanho > limite) {

					int i = 0;
					char endLine;
					int limiteLocal = limite;

					while (i < tamanho && (i + limiteLocal) < tamanho) {
//						System.out.println("Tamanho: " + getTextSize(parag.substring(i, i + limiteLocal + 1)));
						endLine = parag.charAt(i + limiteLocal);
						
						if (Character.isWhitespace(endLine) || isPunctuation(endLine)) {
							linhas.add(parag.substring(i, i + limiteLocal + 1));
							i = i + limiteLocal  + 1;
							limiteLocal = limite;
						} else {
							limiteLocal++;
						}
					}

					if (i < tamanho && (i + limiteLocal) >= tamanho) {
						linhas.add(parag.substring(i, tamanho));
					}
				} else {
					linhas.add(parag);
				}

				linhas.add("");

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		for (int i = 0; i < linhas.size() - 1; i++) {
			if (linhas.get(i).isEmpty() && linhas.get(i + 1).isEmpty()) {
				// System.out.println("Removeu");
				linhas.remove(i--);
			}

		}
		
//		for (String txt : linhas) {
//			System.out.println("Tamanho: " + getTextSize(txt));
//		}
		
		return linhas;
	}
	
	public int getTextSize(String text) {
		AffineTransform affinetransform = new AffineTransform();     
		FontRenderContext frc = new FontRenderContext(affinetransform,true,true);     
		Font font = new Font("Tahoma", Font.PLAIN, 12);
		return (int) font.getStringBounds(text, frc).getWidth();
	}
	
	
	public static boolean isPunctuation(char c) {
        return c == ','
            || c == '.'
            || c == '!'
            || c == '?'
            || c == ':'
            || c == ';'
            ;
    }
	
	private String removeLineBreak(String input) {
		
		String output = input.replaceAll("\n", " -- ").replaceAll("\r", " -- ").replaceAll("\t", "").replaceAll("\u00a0"," ").replaceAll("\u00B7", "-").replaceAll("\u0009"," ").replaceAll("\u0002"," ")
				.replaceAll("\u200B"," ").replaceAll("\u200C"," ").replaceAll("\u200D"," ").replaceAll("\u200E"," ").replaceAll("\u200F"," ");
		
		return output;
	}

	private float startX(PDFont font, int fontSize, String line, float pageWidth) throws IOException {
		float titleWidth = font.getStringWidth(line) / 1000 * fontSize;

		float startX = (pageWidth - titleWidth) / 2;

		return startX;
	}

	private float startY(PDFont font, int fontSize, float pageHeight, float marginTop) throws IOException {
		float titleHeight = font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * fontSize;

		float startY = pageHeight - marginTop - titleHeight;

		return startY;
	}
	
	private float centralizar(float tamanhoPagina, float tamanhoImagem) {
		return (tamanhoPagina - tamanhoImagem) / 2;
	}
	
	private static Dimension getScaledDimension(Dimension imgSize, Dimension boundary) {
		  int original_width = imgSize.width;
		  int original_height = imgSize.height;
		  int bound_width = boundary.width;
		  int bound_height = boundary.height;
		  int new_width = original_width;
		  int new_height = original_height;

		  // first check if we need to scale width
		  if (original_width > bound_width) {
		    //scale width to fit
		    new_width = bound_width;
		    //scale height to maintain aspect ratio
		    new_height = (new_width * original_height) / original_width;
		  }

		  // then check if we need to scale even with the new height
		  if (new_height > bound_height) {
		    //scale height to fit instead
		    new_height = bound_height;
		    //scale width to maintain aspect ratio
		    new_width = (new_height * original_width) / original_height;
		  }

		  return new Dimension(new_width, new_height);
	}
	
	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}

}
