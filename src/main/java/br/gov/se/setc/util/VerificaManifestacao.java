package br.gov.se.setc.util;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import br.gov.se.setc.DAO.EntidadeDAO;
import br.gov.se.setc.DAO.ManifestacaoDAO;
import br.gov.se.setc.DAO.ResponsavelDAO;
import br.gov.se.setc.model.Entidade;
import br.gov.se.setc.model.Manifestacao;
import br.gov.se.setc.model.Mensagem;
import br.gov.se.setc.model.Usuario;
 

public class VerificaManifestacao implements Job {

	private ManifestacaoDAO manifestacaoDAO = ManifestacaoDAO.getInstance();
	private ResponsavelDAO responsavelDAO = ResponsavelDAO.getInstance();
	private EntidadeDAO entidadeDAO = EntidadeDAO.getInstance();
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		System.out.println("Entrou em verificacoes");
		
		List<Entidade> entidades = entidadeDAO.listEntidadesVerificacao();
		
		try {
			for (Entidade ent : entidades) {
				System.out.println(":: Verificando manifestaçoes " + ent.getSigla());
				List<String> destinatarios = responsavelDAO.listEmailResponsaveisEntidade(ent.getIdEntidade());
				
				List<Manifestacao> manifestacoes = manifestacaoDAO.listManifestacoesTramitandoPorOrgao(ent.getIdEntidade());
				
				System.out.println("    :: Quantidade de manifestacoes: " + manifestacoes.size());
				for (Manifestacao mani : manifestacoes) {
					updateStatus(mani);
					notificacaoPrazo(mani, destinatarios);
				}
				
				System.out.println("");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		

	}
	
	/**
	 * Verifica o tempo restante em dias para a Data Prazo das manifestações pendentes, notificando os responsáveis nos seguintes períodos:
	 * 	- 	1 dia seguinte
	 * 	- 	Metade do prazo
	 * 	- 	Restando 2 dias
	 * 	- 	Restando 1 dia
	 * 
	 * @param solicitacao Manifestação a ser verificada
	 */
	
	private void notificacaoPrazo(Manifestacao manifestacao, List<String> destinatarios) {
		
		if (!manifestacao.getStatus().equals("Atendida")) {

			LocalDate now = LocalDate.now();
			
			int prazo;
			
			if (manifestacao.getTipo().equals("Informação"))
				prazo = PrazosUtil.prazoResposta("Esic",manifestacao.getStatus());
			else
				prazo = PrazosUtil.prazoResposta("Ouv",manifestacao.getStatus());
				
			//Datas dos prazos para comparações
			LocalDate inicioPrazo = manifestacao.getDataIni().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().plusDays(1);
			LocalDate metadePrazo = manifestacao.getDataLimite().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().minusDays((prazo)/2);
			LocalDate vesperaPrazo = manifestacao.getDataLimite().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().minusDays(1);
			LocalDate vesperaPrazoGestor = manifestacao.getDataLimite().toInstant().atZone(ZoneId.systemDefault()).toLocalDate().minusDays(2);
			
			
			if(now.isEqual(inicioPrazo)) {
				String mensagem = "Manifestação "+ manifestacao.getProtocolo()+" completou 1 dia.";
				EmailUtil.enviarEmailPrazo(manifestacao, destinatarios, mensagem);
			} else if (now.isEqual(metadePrazo)) {
				String mensagem = "Faltam "+(prazo/2)+" dias para a manifestação "+ manifestacao.getProtocolo() +" expirar.";
				EmailUtil.enviarEmailPrazo(manifestacao, destinatarios, mensagem);
			} else if(now.isEqual(vesperaPrazoGestor)) {
				String mensagem = "Faltam 2 dias para a manifestação "+ manifestacao.getProtocolo() +" expirar.";
				EmailUtil.enviarEmailPrazo(manifestacao, destinatarios, mensagem);
			} else if(now.isEqual(vesperaPrazo)) {
				String mensagem = "Falta 1 dia para a manifestação "+ manifestacao.getProtocolo()+" expirar.";
				EmailUtil.enviarEmailPrazo(manifestacao, destinatarios, mensagem);
			}
		}		
	} 
	
	public void updateStatus(Manifestacao manifestacao) {

		Calendar nowCal = Calendar.getInstance();
		Date now = nowCal.getTime();
		
		Calendar limiteCal = Calendar.getInstance();
		limiteCal.setTime(manifestacao.getDataLimite());
		limiteCal.set(Calendar.HOUR_OF_DAY,23);
		limiteCal.set(Calendar.MINUTE,59);
		limiteCal.set(Calendar.SECOND,59);
		limiteCal.set(Calendar.MILLISECOND,59);
		
		Date dataLimite = limiteCal.getTime();
		
		if (now.after(dataLimite)) {
			if (manifestacao.getStatus().equals("Atendida") || manifestacao.getStatus().equals("Negada")) {
				Mensagem mensagem = new Mensagem();
				mensagem.setTexto("Manifestação "+ manifestacao.getProtocolo() + " foi finalizada no sistema.");
				mensagem.setData(new Date());
				mensagem.setTipo((short) 4);
				mensagem.setUsuario(new Usuario(2));
				
				manifestacao.setDatafim(new Date());
				manifestacao.setStatus("Finalizada");
				manifestacao.addHistorico(mensagem);
				
				boolean salvou = manifestacaoDAO.saveOrUpdate(manifestacao);
				
				if(salvou) {
					EmailUtil.enviarNotificacao(manifestacao, "Manifestação "+ manifestacao.getProtocolo() + " foi finalizada no sistema.");
					System.out.println("Manfiestaçao encerrada com resposta - " + manifestacao.getProtocolo());
				} else 
					System.out.println("Falha ao atualizar manifestação "  + manifestacao.getProtocolo());
				
			} else if (manifestacao.getStatus().equals("Aberta")
						|| manifestacao.getStatus().equals("Tramitando")
						|| manifestacao.getStatus().equals("Prorrogada")
						|| manifestacao.getStatus().equals("Recurso")
						|| manifestacao.getStatus().equals("Reencaminhada")) {
				
				Mensagem mensagem = new Mensagem();
				mensagem.setTexto("Manifestação " + manifestacao.getProtocolo() + " não recebeu resposta no sistema.");
				mensagem.setData(new Date());
				mensagem.setTipo((short) 4);
				mensagem.setUsuario(new Usuario(2));
				
				manifestacao.setDatafim(new Date());
				manifestacao.setStatus("Sem Resposta");
				manifestacao.addHistorico(mensagem);
				
				boolean salvou = manifestacaoDAO.saveOrUpdate(manifestacao);
				
				if(salvou) {
					EmailUtil.enviarNotificacao(manifestacao, "Manifestação " + manifestacao.getProtocolo() + " não recebeu resposta no sistema.");
					System.out.println("Manfiestaçao encerrada por falta de resposta - " + manifestacao.getProtocolo());
				} else 
					System.out.println("Falha ao atualizar manifestação "  + manifestacao.getProtocolo());
				
				
			} else if (manifestacao.getStatus().equals("Reformulação")) {
				Mensagem mensagem = new Mensagem();
				mensagem.setTexto("Prazo de reformulação encerrado sem resposta do manifestante.");
				mensagem.setData(new Date());
				mensagem.setTipo((short) 4);
				mensagem.setUsuario(new Usuario(2));
				
				manifestacao.setDatafim(new Date());
				manifestacao.setStatus("Finalizada");
				manifestacao.addHistorico(mensagem);
				
				boolean salvou = manifestacaoDAO.saveOrUpdate(manifestacao);
				
				if(salvou) {
					EmailUtil.enviarNotificacao(manifestacao, "Prazo de reformulação da manifestação "+ manifestacao.getProtocolo() + " finalizado sem resposta do manifestante.");
					System.out.println("Manifestaçao encerrada por falta de reformulaçao - " + manifestacao.getProtocolo());
				} else 
					System.out.println("Falha ao atualizar manifestação "  + manifestacao.getProtocolo());
			}
		}
	}

	

}

