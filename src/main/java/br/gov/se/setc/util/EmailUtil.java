package br.gov.se.setc.util;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.ImageHtmlEmail;
import org.apache.commons.mail.SimpleEmail;

import br.gov.se.setc.DAO.ResponsavelDAO;
import br.gov.se.setc.model.Cidadao;
import br.gov.se.setc.model.Entidade;
import br.gov.se.setc.model.Manifestacao;
import br.gov.se.setc.model.Mensagem;
import br.gov.se.setc.model.Responsavel;
import br.gov.se.setc.model.Usuario;


public class EmailUtil {
	
	public static void enviarEmailAutomatico(Manifestacao manifestacao, String titulo, String mensagem) {
		String destinatario = (manifestacao.getCidadao()).getEmail();
				
		String msg = "<h4>Notificação do prazo de solicitação</h4>" + 
				"<table>" + 
				"	<tr>" + 
				"<h5 style=\"margin-bottom: 0\"> teste </h5>" + 
				"	</tr>" + 
				"</table>" + 
				"<h5 style=\"margin-bottom: 0\">Acesse o sistema para visualizar as solicitações:</h5>" + 
				"<a href=\"" + AutenticacaoUtil.getEndereco() + "\">" + AutenticacaoUtil.getEndereco() + "</a>";
		try {
			enviarEmailHTML(destinatario, titulo, msg);
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public static void enviarEmailTramites(Manifestacao manifestacao, String texto, Responsavel respRemetente) {
		String titulo = "SE-Ouv - Nova manifestação encaminhada para " + manifestacao.getEntidade().getSigla();
		
		String tipoSol1;
		if (manifestacao.getTipo().equals("Informação")) tipoSol1 = "Pedido de Informação";
		else tipoSol1 = manifestacao.getTipo();
		
		String msg = "			<h4> Nova manifestação encaminhada para " + manifestacao.getEntidade().getSigla() + " - " + manifestacao.getEntidade().getNome() + "</h4>" + 
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Protocolo: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + manifestacao.getProtocolo() + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Tipo: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + tipoSol1 + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Órgão: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + manifestacao.getEntidade().getSigla() + " - " + manifestacao.getEntidade().getNome() + "</p></td>" + 
				"				</tr>" + 
				"			</table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Manifestante: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + manifestacao.getCidadao().getUsuario().getNome() + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<br/>" +
				"			</table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Encaminhada por: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + respRemetente.getUsuario().getNome() + "(" + respRemetente.getEmail() + ")</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				
				"			</table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Justificativa: </h4></td>" + 
				"				</tr>" + 
				"				<tr>" + 
				"				<td><p style=\"margin: 0\"> \"" + texto + "\"</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<h5 style=\"margin-bottom: 0\">Acesse o sistema para visualizar as solicitações:</h5>" + 
				"			<a href=\"" + AutenticacaoUtil.getEndereco() + "\">" + AutenticacaoUtil.getEndereco() + "</a>";
				
				try {
					List<String> destinatarios = ResponsavelDAO.getInstance().listEmailResponsaveisEntidade(manifestacao.getEntidade().getIdEntidade());
					
					for (String email : destinatarios) {
						enviarEmailHTML(email, titulo, msg);
					}
				} catch (EmailException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	}
	
	public static void enviarEmailNovaManifestacaoCidadao(Manifestacao manifestacao) {
		Cidadao cidadao = manifestacao.getCidadao();
		
		String destinatario = cidadao.getEmail();
		String titulo = "SE-Ouv - Manifestação realizada com sucesso";
		
		String tipoSol;
		if (manifestacao.getTipo().equals("Informação")) tipoSol = "Seu Pedido de Informação foi realizado com sucesso!";
		else if (manifestacao.getTipo().equals("Elogio")) tipoSol = "Seu Elogio foi realizado com sucesso!";
		else tipoSol = "Sua " + manifestacao.getTipo() + " foi realizada com sucesso!";
		
		String msg = "<p style=\"font-size: 30px; margin-top: 0\">Olá, " + cidadao.getUsuario().getNome() + "</p>" + 
				"			<h4>" + tipoSol + "</h4>" + 
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Protocolo: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + manifestacao.getProtocolo() + "</p></td>" + 
				"				</tr>" + 
				"			</table>" + 
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Órgão: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + manifestacao.getEntidade().getSigla() + " - " + manifestacao.getEntidade().getNome() + "</p></td>" + 
				"				</tr>" + 
				"			</table>" + 
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Título: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + manifestacao.getTitulo() + "</p></td>" + 
				"				</tr>" + 
				"			</table>" + 
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Data de envio: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + DateFormat.getDateInstance(DateFormat.SHORT, Locale.CANADA).format(manifestacao.getDataIni()) + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<table>" +
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Data limite: </h4></td>" + 
//				"				<td><p style=\"margin: 0\">" + DateFormat.getDateInstance(DateFormat.SHORT, Locale.CANADA).format(manifestacao.getDataLimite()) + "</p></td>" + 
				"				</tr>" + 
				"			</table>" + 
				"			<h5>Por favor, aguarde a resposta do órgão competente dentro do prazo estipulado</h5>" + 
				"			<h5 style=\"margin-bottom: 0\">Acesse o sistema para visualizar suas solicitações:</h5>" + 
				"			<a href=\"" + AutenticacaoUtil.getEndereco() + "\">" + AutenticacaoUtil.getEndereco() + "</a>";
				
				try {
					enviarEmailHTML(destinatario, titulo, msg);
				} catch (EmailException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	}
	
	public static void enviarEmailNovaManifestacaoResp(Manifestacao manifestacao) {
		String titulo = "SE-Ouv - Nova manifestação para " + manifestacao.getEntidade().getSigla();
		
		String tipoSol1;
		if (manifestacao.getTipo().equals("Informação")) tipoSol1 = "Pedido de Informação";
		else tipoSol1 = manifestacao.getTipo();
		
		String msg = "			<h4> Nova manifestação realizada para " + manifestacao.getEntidade().getSigla() + " - " + manifestacao.getEntidade().getNome() + "</h4>" + 
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Protocolo: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + manifestacao.getProtocolo() + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Tipo: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + tipoSol1 + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Órgão: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + manifestacao.getEntidade().getSigla() + " - " + manifestacao.getEntidade().getNome() + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Título: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + manifestacao.getTitulo() + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<table>" +
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Manifestante: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + manifestacao.getCidadao().getUsuario().getNome() + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<h5 style=\"margin-bottom: 0\">Acesse o sistema para visualizar as solicitações:</h5>" + 
				"			<a href=\"" + AutenticacaoUtil.getEndereco() + "\">" + AutenticacaoUtil.getEndereco() + "</a>";
				
				try {
					List<String> destinatarios = ResponsavelDAO.getInstance().listEmailResponsaveisEntidade(manifestacao.getEntidade().getIdEntidade());
					
					for (String email : destinatarios) {
						enviarEmailHTML(email, titulo, msg);
					}
				} catch (EmailException e) {
					e.printStackTrace();
				}
	}

	public static void enviarEmailNotificacaoCidadao(Manifestacao manifestacao, Mensagem mensagem) {
		String destinatario = manifestacao.getCidadao().getEmail();
		String titulo = "SE-Ouv - Atualização na manifestação "+ manifestacao.getProtocolo();
		
		String msg = 	"<h4>Nova atualização na manifestação " + manifestacao.getProtocolo() + " - " + manifestacao.getTitulo() + ".</h4>" +
						"<table>" +
						"	<tr>" +
						"	<td><h4 style=\"margin: 0\">Protocolo: </h4></td>" +
						"	<td><p style=\"margin: 0\">" + manifestacao.getProtocolo() + "</p></td>" +
						"	</tr>" +
						"</table>" +
						"<table>" +
						"	<tr>" +
						"	<td><h4 style=\"margin: 0\">Título: </h4></td>" +
						"	<td><p style=\"margin: 0\">" + manifestacao.getTitulo() + "</p></td>" +
						"	</tr>" +
						"</table>" +
						"<table>" +
						"	<tr>" +
						"	<td><h4 style=\"margin: 0\">Órgão: </h4></td>" +
						"	<td><p style=\"margin: 0\">" + manifestacao.getEntidade().getSigla() + " - "  + manifestacao.getEntidade().getNome() + "</p></td>" +
						"	</tr>" +
						"</table>" +
						"<br/>" +
						"<table>" +
						"	<tr>" +
						"	<td><h4 style=\"margin: 0\">Atualização: </h4></td>" +
						"	<td><p style=\"margin: 0\">" + manifestacao.getStatus() + "</p></td>" +
						"	</tr>" +
						"</table>" +
						"<table>" +
						"	<tr>" +
						"	<td><h4 style=\"margin: 0\">Data: </h4></td>" +
						"	<td><p style=\"margin: 0\">" + DateFormat.getDateInstance(DateFormat.SHORT, Locale.CANADA).format(mensagem.getData()) + "</p></td>" +
						"	</tr>" +
						"</table>" +
						"<table>" +
						"	<tr>" +
						"	<td><h4 style=\"margin: 0\"> Mensagem: </h4></td>" +
						"	</tr>" +
						"	<tr>" +
						"	<td><p style=\"margin: 0\">\" " + mensagem.getTexto() + " \"</p></td>" +
						"	</tr>" +
						"</table>" +
						"<h5 style=\"margin-bottom: 0\">Acesse o sistema para visualização completa:</h5>" +
						"<a href=\"" + AutenticacaoUtil.getEndereco() + "\">" + AutenticacaoUtil.getEndereco() + "</a>";

		try {
			enviarEmailHTML(destinatario, titulo, msg);
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void enviarEmailNotificacaoRecurso(Manifestacao manifestacao) {
		
		String titulo = "SE-Ouv - Manifestação para " + manifestacao.getEntidade().getSigla() + " entrou em recurso.";
		
		String tipoSol1;
		if (manifestacao.getTipo().equals("Informação")) tipoSol1 = "Pedido de Informação";
		else tipoSol1 = manifestacao.getTipo();
		
		String msg = "			<h4> Manifestação realizada para " + manifestacao.getEntidade().getSigla() + " - " + manifestacao.getEntidade().getNome() + " entrou em recuso.</h4>" + 
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Protocolo: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + manifestacao.getProtocolo() + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Tipo: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + tipoSol1 + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Órgão: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + manifestacao.getEntidade().getSigla() + " - " + manifestacao.getEntidade().getNome() + "</p></td>" + 
				"				</tr>" + 
				"			</table>" + 
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Data de envio: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + DateFormat.getDateInstance(DateFormat.SHORT, Locale.CANADA).format(manifestacao.getDataIni()) + "</p></td>" + 
				"				</tr>" + 
				"			</table>" + 
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Data limite: </h4></td>" + 
//				"				<td><p style=\"margin: 0\">" + DateFormat.getDateInstance(DateFormat.SHORT, Locale.CANADA).format(manifestacao.getDataLimite()) + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Manifestante: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + manifestacao.getCidadao().getUsuario().getNome() + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<h5 style=\"margin-bottom: 0\">Acesse o sistema para visualizar as solicitações:</h5>" + 
				"			<a href=\"" + AutenticacaoUtil.getEndereco() + "\">" + AutenticacaoUtil.getEndereco() + "</a>";
				
				try {
					List<String> destinatarios = ResponsavelDAO.getInstance().listEmailResponsaveisEntidade(manifestacao.getEntidade().getIdEntidade());
					
					for (String email : destinatarios) {
						enviarEmailHTML(email, titulo, msg);
					}
				} catch (EmailException e) {
					e.printStackTrace();
				}
	}
	
	
	public static void enviarEmailRedefinicaoSenha(String link, String destinatario, String nomeUser, String nick) {
		String titulo = "SE-Ouv - Redefinição de senha";
		
		String msg = "<h1 style=\"margin-bottom:50px\">Olá, "+ nomeUser +"</h1>" + 
				"	<table align=\"center\">" + 
				"		<tr align=\"center\">" + 
				"		<td>" + 
				"			<h3> Nome de usuário: " + nick + "</h3>" + 
				"			<p>Se você esqueceu sua senha ou deseja redefini-la, utilize o botão abaixo para fazê-lo.</p>" +
				"			<a href=\"" + link + "\" style=\"color: white; border-radius: 3px; border: 1px solid #1d4a74; padding: 10px; font-size: 20px; background-color: #296099; background: linear-gradient(to bottom right, #040c31, #4087dc); text-decoration: none;\">" + 
				"			Redefinir Senha" + 
				"			</a>" + 
				"		</td>" + 
				"		</tr>" + 
				"	</table>" + 
				"<h5 style=\"margin-bottom: 0; margin-top: 50px; font-weight: normal\">Caso o botão não funcione, clique ou copie o link a seguir no navegador:</h5>" + 
				"<a href=\"" + link + "\">" + link + "</a>" + 
				"<h5 style=\"margin-bottom: 0; font-weight: normal\">Se você não requisitou a mudança de senha, ignore este e-mail. Apenas uma pessoa com acesso ao seu e-mail pode redefinir sua senha.</h5>";
		
		try {
			enviarEmailHTML(destinatario, titulo, msg);
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void enviarEmailPrazo(Manifestacao manifestacao, List<String> destinatarios, String mensagem) {
		String titulo = "SE-Ouv - Notificação de prazo da manifestação " + manifestacao.getProtocolo();
		
		String tipoSol1;
		if (manifestacao.getTipo().equals("Informação")) tipoSol1 = "Pedido de Informação";
		else tipoSol1 = manifestacao.getTipo();
		
		String msg = "			<h4>" + mensagem + "</h4>" + 
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Protocolo: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + manifestacao.getProtocolo() + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Tipo: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + tipoSol1 + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Órgão: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + manifestacao.getEntidade().getSigla() + " - " + manifestacao.getEntidade().getNome() + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Título: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + manifestacao.getTitulo() + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<table>" +
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Manifestante: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + manifestacao.getCidadao().getUsuario().getNome() + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
					"			<h5 style=\"margin-bottom: 0\">Acesse o sistema para visualizar as solicitações:</h5>" + 
					"			<a href=\"" + AutenticacaoUtil.getEndereco() + "\">" + AutenticacaoUtil.getEndereco() + "</a>";
		
		try {
			for (String destinatario : destinatarios) {
				enviarEmailHTML(destinatario, titulo, msg);
			}
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	
	public static void enviarNotificacao(Manifestacao manifestacao, String mensagem) {
//		ArrayList<String> destinatarios = destinatarioResp(manifestacao.getEntidade(), nivel);
		String destinatario = manifestacao.getCidadao().getEmail();
				
		String titulo = "SE-Ouv - Atualização na manifestação " + manifestacao.getProtocolo();
		
		String tipoSol1;
		if (manifestacao.getTipo().equals("Informação")) tipoSol1 = "Pedido de Informação";
		else tipoSol1 = manifestacao.getTipo();
		
		String msg = "			<h4>" + mensagem + "</h4>" + 
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Protocolo: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + manifestacao.getProtocolo() + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Tipo: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + tipoSol1 + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Órgão: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + manifestacao.getEntidade().getSigla() + " - " + manifestacao.getEntidade().getNome() + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Título: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + manifestacao.getTitulo() + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<h5 style=\"margin-bottom: 0\">Acesse o sistema para visualizar as solicitações:</h5>" + 
				"			<a href=\"" + AutenticacaoUtil.getEndereco() + "\">" + AutenticacaoUtil.getEndereco() + "</a>";
		
		try {
			enviarEmailHTML(destinatario, titulo, msg);
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	
	public static void enviarEmailCadastroCid (Cidadao cidadao) {
		String destinatario = cidadao.getEmail();
		
		String titulo = "SE-Ouv - Bem vindo ao SE-Ouv";
		
		String msg = 	"<p style=\"font-size: 30px; margin-top: 0\">Olá, " + cidadao.getUsuario().getNome() + "</p>" + 
						"			<h4>Bem vindo ao SE-Ouv</h4>" + 
						"			<br/>" +
						"			<h3>Lembre-se do seu nome de usuário (" + cidadao.getUsuario().getNick() + ") e senha para acessar o sistema.</h3>" +
						"			<h3 style=\"margin:0\">Acesse o sistema para realizar e visualizar manifestações:</h3>" + 
						"			<a style=\"font-size: 15px\" href=\"" + AutenticacaoUtil.getEndereco() + "\">" + AutenticacaoUtil.getEndereco() + "</a>";
		
		try {
			enviarEmailHTML(destinatario, titulo, msg);
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void enviarEmailCadastroResp (Responsavel responsavel) {
		String destinatario = responsavel.getEmail();
		String nivel;
		
		if (responsavel.getNivel() == 1)
			nivel = "Ouvidor Setorial";
		else if (responsavel.getNivel() == 3)
			nivel = "Gestor";
		else 
			nivel = "responsável";
		
		String titulo = "SE-Ouv - Cadastro como responsável";
		
		String msg = 	"<p style=\"font-size: 30px; margin-top: 0\">Olá, " + responsavel.getUsuario().getNome() + "</p>" + 
				"			<h2>Você foi cadastrado no sistema como + "+ nivel +" do órgão " + responsavel.getEntidade().getSigla() + " - " + responsavel.getEntidade().getNome() + ".</h2>" + 
				"			<h3>Lembre-se do seu nome de usuário (" + responsavel.getUsuario().getNick() + ") e senha para acessar o sistema.</h3>" +
				"			<h3 style=\"margin:0\">Acesse o sistema para visualizar e responder manifestações:</h3>" + 
				"			<a style=\"font-size: 15px\" href=\"" + AutenticacaoUtil.getEndereco() + "\">" + AutenticacaoUtil.getEndereco() + "</a>";
		
		try {
			enviarEmailHTML(destinatario, titulo, msg);
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void enviarEmailRespostaReformulacao (Manifestacao manifestacao) {
		String titulo = "SE-Ouv - Reformulação realizada na manifestação " + manifestacao.getProtocolo();
		
		String tipoSol1;
		if (manifestacao.getTipo().equals("Informação")) tipoSol1 = "Pedido de Informação";
		else tipoSol1 = manifestacao.getTipo();
		
		String msg = "			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Protocolo: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + manifestacao.getProtocolo() + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Tipo: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + tipoSol1 + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Órgão: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + manifestacao.getEntidade().getSigla() + " - " + manifestacao.getEntidade().getNome() + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Título: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + manifestacao.getTitulo() + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<table>" +
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Manifestante: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + manifestacao.getCidadao().getUsuario().getNome() + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
					"			<h5 style=\"margin-bottom: 0\">Acesse o sistema para visualizar as solicitações:</h5>" + 
					"			<a href=\"" + AutenticacaoUtil.getEndereco() + "\">" + AutenticacaoUtil.getEndereco() + "</a>";
		
		try {
			List<String> destinatarios = ResponsavelDAO.getInstance().listEmailResponsaveisEntidade(manifestacao.getEntidade().getIdEntidade());
			
			for (String email : destinatarios) {
				enviarEmailHTML(email, titulo, msg);
			}
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	public static void enviarNotificacaoEntidade(Manifestacao manifestacao, String mensagem) {
//		ArrayList<String> destinatarios = destinatarioResp(manifestacao.getEntidade(), nivel);
		List<String> destinatarios = new ArrayList<String>();
				
		String titulo = "SE-Ouv - Atualização na manifestação " + manifestacao.getProtocolo();
		
		String tipoSol1;
		if (manifestacao.getTipo().equals("Informação")) tipoSol1 = "Pedido de Informação";
		else tipoSol1 = manifestacao.getTipo();
		
		String msg = "			<h4>" + mensagem + "</h4>" + 
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Protocolo: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + manifestacao.getProtocolo() + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Tipo: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + tipoSol1 + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Órgão: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + manifestacao.getEntidade().getSigla() + " - " + manifestacao.getEntidade().getNome() + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<table>" + 
				"				<tr>" + 
				"				<td><h4 style=\"margin: 0\">Título: </h4></td>" + 
				"				<td><p style=\"margin: 0\">" + manifestacao.getTitulo() + "</p></td>" + 
				"				</tr>" + 
				"			</table>" +
				"			<h5 style=\"margin-bottom: 0\">Acesse o sistema para visualizar as solicitações:</h5>" + 
				"			<a href=\"" + AutenticacaoUtil.getEndereco() + "\">" + AutenticacaoUtil.getEndereco() + "</a>";
		
		try {
			for (String destinatario : destinatarios) {
				enviarEmailHTML(destinatario, titulo, msg);
			}
		} catch (EmailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	//+++++++++++++++++++ Email HTML Template
	public static void enviarEmailHTML(String destinatario, String titulo, String mensagem) throws EmailException {
		boolean envioAtivo = true;
		
		if (envioAtivo) {
			Pattern pattern = Pattern.compile("^(.+)@(.+)$");
			Matcher matcher = pattern.matcher(destinatario);
			
			if(matcher.matches()) {
			
				// Create the email message
				ImageHtmlEmail emailHtml = new ImageHtmlEmail();
				emailHtml.setDebug(true);
				emailHtml.setHostName(AutenticacaoUtil.getHostNameEmail());
	//			emailHtml.setSmtpPort(DadosAutenticacao.getPort());
	//			emailHtml.setSSLOnConnect(true);
				emailHtml.setAuthentication(AutenticacaoUtil.getUserLoginEmailAuthentication(),AutenticacaoUtil.getSenhaUserLoginEmailAuthentication());  
				emailHtml.addTo(destinatario);
				emailHtml.setFrom("nao_responder@setc.se.gov.br"); //será passado o email que você fará a autenticação
				emailHtml.setSubject(titulo);
				
				StringBuffer msg = new StringBuffer();
				msg.append("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
				msg.append("<html xmlns=\"http://www.w3.org/1999/xhtml\" >");
				msg.append("<head>");
				msg.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\" />");
				msg.append("<title>" + titulo + "</title>");
				msg.append("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\" />");
				msg.append("<style>");
				msg.append("html, body {height:100%}");
				msg.append("a:hover {text-decoration: none !important; color: #2196F3 !important}");
				msg.append("</style>");
				msg.append("</head>");
				msg.append("<body style=\"padding: 0; margin: 0; font-family: lato, Sans-serif; font-weight: normal;\">");
				msg.append("	<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" style=\"width:100%; height:100%; background-color: #F2F2F2; padding: 20px\">");
				msg.append("		<tr>");
				msg.append("		<td style=\"padding:10px; border:none\">");
				msg.append("	<table align=\"center\" border=\"1\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\" style=\"border: none; background: white; border-collapse: collapse;\">");
				msg.append("		<tr  style=\"color:white; border: none; background: rgb(8, 72, 130); background: -webkit-gradient(linear, left top, right top, from(rgba(10,46,78,1)), color-stop(24%, rgba(27,99,158,0.9)), color-stop(50%, rgba(27,99,158,0.8)), to(rgba(27,99,158,0)));background: -webkit-linear-gradient(left, rgba(10,46,78,1) 0%, rgba(27,99,158,0.9) 24%, rgba(27,99,158,0.8) 50%, rgba(27,99,158,0) 100%);background: -o-linear-gradient(left, rgba(10,46,78,1) 0%, rgba(27,99,158,0.9) 24%, rgba(27,99,158,0.8) 50%, rgba(27,99,158,0) 100%);background: linear-gradient(to right, rgba(10,46,78,1) 0%, rgba(27,99,158,0.9) 24%, rgba(27,99,158,0.8) 50%, rgba(27,99,158,0) 100%);\">");
				msg.append("			<td style=\"padding:10px; border:none\">");
				msg.append("				<p style=\"font-size: 28px; font-weight: bold; margin: 0\">SE-Ouv</p>");
				msg.append("				<p style=\"font-size: 12px; font-weight: bold;  margin: 0\">SISTEMA DE OUVIDORIAS</p>");
				msg.append("				<p style=\"font-size: 12px; font-weight: bold;  margin: 0\">DO ESTADO DE SERGIPE</p>");
				msg.append("			</td>");
				msg.append("		</tr>");
				msg.append("		<tr>");
				
				msg.append("			<td bgcolor=\"#ffffff\" style=\"padding: 40px; text-align: left; border: none; color: #606060\">");
				
				msg.append(mensagem);
				
				msg.append("			<h5 style=\"margin-bottom: 0; font-weight: normal\">Em caso de dúvida, entre em contato com a Ouvidoria Geral do Estado pelo telefone 0800 079 0162</h5>");
				msg.append("			<h5 style=\"margin-bottom: 0; font-weight: normal\">E-mail automático, favor não responder.</h5>");
				msg.append("			</td>");
				
				msg.append("		</tr>");
				msg.append("		<tr>");
				msg.append("			<td style=\"padding: 20px; border: none; text-align: center; font-size: 12px; color: #757575;\">");
				msg.append("				<p style=\"margin: 0\">OGE - Ouvidoria Geral do Estado</p>");
				msg.append("				<p style=\"margin: 0\">SETC - Secretaria do Estado da Transparência e Controle</p>");
				msg.append("			</td>");
				msg.append("		</tr>");
				msg.append("	</table>");
				msg.append("		</td>");
				msg.append("		</tr>");
				msg.append("	</table>");
				msg.append("</body>");
				msg.append("</html>");
				
				// set the html message
				emailHtml.setHtmlMsg(msg.toString());
				
				// set the alternative message
				emailHtml.setTextMsg("Seu provedor de email não suporta este tipo de mensagens.");
				
				emailHtml.setDebug(false);
				
				// send the email
				emailHtml.send();
				
				System.out.println("Email enviado para: " + destinatario + "	Em: " + emailHtml.getSentDate());
				System.out.println("Título: " + titulo);
			} else {
				System.out.println("Formato de email do destinatário '"+ destinatario +"' inválido" );
			}
		} else {
			System.out.println("Envio de email desativado. Mensagem '"+ titulo +"' para " + destinatario + " não enviada." );
		}
	}
	
	//+++++++++++++++++++ Email simples
		public static void enviarEmail(String remetente, String destinatario, String titulo, String mensagem) throws EmailException {
			Email email = new SimpleEmail();
			
			email.setDebug(true);  
			email.setHostName(AutenticacaoUtil.getHostNameEmail());
			email.setSmtpPort(AutenticacaoUtil.getPort());
			email.setSSLOnConnect(true);
//			email.setSslSmtpPort(sslSmtpPort);
			email.setAuthentication(AutenticacaoUtil.getUserLoginEmailAuthentication(),AutenticacaoUtil.getSenhaUserLoginEmailAuthentication());  
			email.addTo(destinatario); //pode ser qualquer email  
			email.setFrom(remetente);//será passado o email que você fará a autenticação 
			email.setSubject(titulo);  
			email.setMsg(mensagem);  // só para teste  
			email.send();  
			
		}
		
	//+++++++++++++++++++ Destinatário todos responsáveis por nível (1 = Representante; 2 = Autoridade; 3 = Gestor)
		public static ArrayList<String> destinatarioResp(Entidade entidade, Short nivel) {
//			List<Responsavel> listResp = ResponsavelDAO.findResponsavelEntidadeNivel(entidade.getIdEntidade());
			List<Responsavel> listResp = new ArrayList<Responsavel>();
			ArrayList<String> destinatarios = new ArrayList<String>();
			
			for (Responsavel resp : listResp) {
				destinatarios.add(resp.getEmail());
			}
			
			return destinatarios;
		}
		
		
}
