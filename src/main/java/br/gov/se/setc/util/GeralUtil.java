package br.gov.se.setc.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;
import java.util.regex.Pattern;

import javax.faces.context.FacesContext;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;


import br.gov.se.setc.DAO.ManifestacaoDAO;
import br.gov.se.setc.model.Cidadao;
import br.gov.se.setc.model.Entidade;
import br.gov.se.setc.model.Manifestacao;
import br.gov.se.setc.model.Mensagem;
import br.gov.se.setc.model.Tema;
import br.gov.se.setc.model.Usuario;

public class GeralUtil {

	private static String[] palavrasReservadas = {"admin", "administrador", "sistema", "gestor", "gestorsistema", "gestor.sistema", "anonimo", "teste", "administrator", "sistema.gestor","sistemagestor", "usuario", "sudo", "sudo.admin"};
	
    public static String[] meses = {"Vazio", //ajuste para indexar o nome do mes diretamente pelo numero    
            "Janeiro", 
            "Fevereiro",
            "Março",
            "Abril",
            "Maio",
            "Junho",
            "Julho",
            "Agosto",
            "Setembro",
            "Outubro",
            "Novembro",
            "Dezembro"};
	
	public static void salvarNaSessao(String key, Object obj) {
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(key, obj);
	}
	
	public static Object recuperarDaSessao(String key) {
		return FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(key);
	}
	
//	public static String caminhoAnexos = "anexosHOMO";
	public static String caminhoAnexos = "anexos";
	
	/**
	 * Gera a junção do primeiro e ultimo nome o usuário
	 * do usuário.
	 * @param nomeCompleto
	 */
	public static String nomeSobrenome(String nomeCompleto) {
		if (nomeCompleto != null) {
			String[] nomeSobrenome = nomeCompleto.split(" ");
			if (nomeSobrenome.length > 1) {
				String nome = nomeSobrenome[0] + " " + nomeSobrenome[nomeSobrenome.length - 1];
				return nome;
			} else {
				String nome = nomeSobrenome[0];
				return nome;
			}
		}
		
		return null;
	}
	
	
	/**
	 * Função para retirar o acento dos textos
	 * @param str
	 * @return
	 */
	public static String removerAcentos(String str) {
		String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
		Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
		return pattern.matcher(nfdNormalizedString).replaceAll("");
	}

	
	 public static String[]  getCurrentUrl() {
		 	String[] currentUrl = {"","","","","","","","",""};
	        FacesContext facesContext = FacesContext.getCurrentInstance();
	        if (facesContext == null) {
	            return null;
	        }

	        HttpServletRequest request = (HttpServletRequest) facesContext.getExternalContext().getRequest();
	        StringBuilder url = new StringBuilder();
	        url.append(request.getScheme()) 
	           .append("://")
	           .append(request.getServerName()) 
	           .append(":")
	           .append(request.getServerPort()) 
	           .append(request.getContextPath()) 
	           .append(request.getServletPath());
	        
	        currentUrl[0] = request.getScheme();
	        currentUrl[1] = "://";
	        currentUrl[2] = request.getServerName();
	        currentUrl[3] = ":";
	        currentUrl[4] =  ""+request.getServerPort();
	        currentUrl[5] = request.getContextPath();
	        currentUrl[7] = request.getServletPath();
	       
	        
	        
	        
	        if (request.getQueryString() != null) {
	            url.append("?").append(request.getQueryString()); 
	        }
	        String urlFinal = url.toString();
	       
	        System.out.println(urlFinal);
         
	        return currentUrl;
	    }
	
	/**
	 * Função checarPalavrasReservadas
	 * 
	 * Verifica se o texto possui alguma palavra reservada pelo sistema
	 * 
	 * @param str
	 * @return
	 */
	public static boolean checarPalavrasReservadas(String str) {
		for (String string : palavrasReservadas) {
			if(str.contains(string)) {
				return true;
			}
		}
		return false;
	}
	
	
	public static Mensagem gerarMensagem(String texto, Usuario usuario, Short tipo) {
		Mensagem mensagemSistema = new Mensagem();
		
		mensagemSistema.setTexto(texto);
		mensagemSistema.setTipo(tipo);
		mensagemSistema.setUsuario(usuario);
		mensagemSistema.setData(getDataAtual());
		
		return mensagemSistema;
	}
	
	
	public static Date filterDate(String dataString) throws ParseException{
		System.out.println("Data string: " + dataString);
		
		Date data = new SimpleDateFormat("EEE MMM dd HH:mm:ss z YYYY").parse(dataString);
		
		System.out.println("Start data: " + data);
		
		Date dataConvertida = new SimpleDateFormat("dd/mm/YYYY").parse(dataString);
		
		System.out.println("End data: " + dataConvertida);
		
		return data;
	}
	
	public static Path<?> getPath(String field, Root<Manifestacao> solicitacao, Join<Manifestacao,Entidade> entidade, Join<Manifestacao,Tema> tema,  Join<Cidadao,Usuario> cidadao) {
		//sort
		Path<?> path = null;
		if (field == null){
			path = solicitacao.get("dataIni");
		}else{
			switch(field){
			case "idSolicitacao":
				path = solicitacao.get("idSolicitacao");
				break;
			case "protocolo":
				path = solicitacao.get("protocolo");
				break;
			case "tipo":
				path = solicitacao.get("tipo");
				break;
			case "entidade.sigla":
				path = entidade.get("sigla");
				break;
			case "tema.titulo":
				path = tema.get("titulo");
				break;
			case "titulo":
				path = solicitacao.get("titulo");
				break;
			case "dataIni":
				path = solicitacao.get("dataIni");
				break;
			case "dataLimite":
				path = solicitacao.get("dataLimite");
				break;
			case "datafim":
				path = solicitacao.get("datafim");
				break;
			case "cidadao.usuario.nome":
				path = cidadao.get("nome");
				break;
			case "status":
				path = solicitacao.get("status");
				break;
			case "protocoloAgrese":
				path = solicitacao.get("protocoloAgrese");
				break;
			}
		}
		return path;
	}
	
	public static Path<String> getStringPath(String field, Root<Manifestacao> solicitacao, Join<Manifestacao,Entidade> entidade, Join<Manifestacao,Tema> tema,  Join<Cidadao,Usuario> cidadao) {
		//sort
		Path<String> path = null;
		if (field == null){
			path = solicitacao.get("dataIni");
		}else{
			switch(field){
			case "idSolicitacao":
				path = solicitacao.get("idSolicitacao");
				break;
			case "protocolo":
				path = solicitacao.get("protocolo");
				break;
			case "tipo":
				path = solicitacao.get("tipo");
				break;
			case "entidade.sigla":
				path = entidade.get("sigla");
				break;
			case "tema.titulo":
				path = tema.get("titulo");
				break;
			case "titulo":
				path = solicitacao.get("titulo");
				break;
			case "dataIni":
				path = solicitacao.get("dataIni");
				break;
			case "dataLimite":
				path = solicitacao.get("dataLimite");
				break;
			case "datafim":
				path = solicitacao.get("datafim");
				break;
			case "cidadao.usuario.nome":
				path = cidadao.get("nome");
				break;
			case "status":
				path = solicitacao.get("status");
				break;
			case "protocoloAgrese":
				path = solicitacao.get("protocoloAgrese");
				break;
			}
		}
		return path;
	}
	
	
	public static Date getDataAtual() {
     	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:s");
 		//sdf.setTimeZone(TimeZone.getTimeZone("GMT-3"));
		ManifestacaoDAO maDao = ManifestacaoDAO.getInstance();
	    String server = getCurrentUrl()[2];

	    Date date = maDao.dataAtual();
		
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:s");
			//formatter.setTimeZone(TimeZone.getTimeZone("GMT-3"));
			
			SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:s");
			
			String sDate = formatter.format(date);
			
			Date newDate = formatter2.parse(sDate);
			/*if (!server.contains("localhost")) {
	            long threeHoursInMillis = 3 * 60 * 60 * 1000;
	            newDate = new Date(newDate.getTime() + threeHoursInMillis);
	            System.out.println("Adjusted for localhost: " + newDate);
	        }*/
			System.out.println("Data atual: " + date + " => " + sDate + " => " + newDate);
			return newDate;
		} catch (Exception e) {
			System.out.println(":: Falha ao recuperar data atual");
		}
		
		System.out.println("Data atual: " + date);
		return date;
	}

	
	public static Date getDataAtual(ManifestacaoDAO maDao) {

	    Date date = maDao.dataAtual();
	    String server = getCurrentUrl()[2];
		
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:s");
			//formatter.setTimeZone(TimeZone.getTimeZone("GMT-3"));
			
			SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:s");
			
			String sDate = formatter.format(date);
			
			Date newDate = formatter2.parse(sDate);
		 /* if (!server.contains("localhost")) {
	            long threeHoursInMillis = 3 * 60 * 60 * 1000;
	            newDate = new Date(newDate.getTime() + threeHoursInMillis);
	            System.out.println("Adjusted for localhost: " + newDate);
	        }*/
			System.out.println("Data atual: " + date + " => " + sDate + " => " + newDate);
			
			return newDate;
		} catch (Exception e) {
			System.out.println(":: Falha ao recuperar data atual");
		}
		
		System.out.println("Data atual: " + date);
		return date;
	}
	
	
	public static String generateSessionId() {
		int len = 45;
		String charsCaps = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		String Chars = "abcdefghijklmnopqrstuvwxyz";
		String nums = "0123456789";
		String passSymbols = charsCaps + Chars + nums;
		Random rnd = new Random();
		String sessionId = "";

		for (int i = 0; i < len; i++) {
			sessionId += passSymbols.charAt(rnd.nextInt(passSymbols.length()));
		}
		return sessionId;
	}
	
}
