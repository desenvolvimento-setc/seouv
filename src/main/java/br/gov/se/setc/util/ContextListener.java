package br.gov.se.setc.util;

import javax.servlet.ServletContextEvent;  
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

@WebListener
public class ContextListener implements ServletContextListener {
	
	private SchedulerFactory shedFact = null;
	
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        System.out.println("Starting up!");
        
        shedFact = new StdSchedulerFactory();
		try {
			
//			Scheduler sched = shedFact.getScheduler();
//			sched.start();
//			
////			 Criar trigger programado para 00:01;
//			  JobDetail job = JobBuilder.newJob(VerificaManifestacao.class)
//			      .withIdentity("myJob", "group1") // name "myJob", group "group1"
//			      .build();
//
//			  Trigger trigger = TriggerBuilder.newTrigger()
//			      .withIdentity("myTrigger", "group1")
//			      .withSchedule(CronScheduleBuilder.cronSchedule("0 1 0 ? * *"))
//			      .build();
//			  
//			  sched.scheduleJob(job, trigger);
//			  System.out.println(":: Criou trigger por tempo programado para " + trigger.getNextFireTime());
			  
			// Criar trigger na execução;
//				JobDetail job2 = JobBuilder.newJob(VerificaManifestacao.class)
//					.withIdentity("myJob2", "group1")
//					.build();
//			  
//				Trigger trigger2 = TriggerBuilder.newTrigger()
//						.withIdentity("runtimeTrigger", "group1")
//						.startNow()
//						.build();
//				
//			  sched.scheduleJob(job2, trigger2);
//			  System.out.println(":: Criou trigger por execução");
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        System.out.println("Shutting down!");
        try {
			shedFact.getScheduler().shutdown();
			Thread.sleep(1000);
		} catch (SchedulerException | InterruptedException ex) {
        	System.out.println("Quartz falhou em desligar.");
        	ex.printStackTrace();
        }
    }
}