package br.gov.se.setc.util;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.imageio.ImageIO;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.LosslessFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

@ManagedBean(name = "chartExport")
@SessionScoped
public class ChartExport {
	
	private String base64Str;
	private StreamedContent file;
	
	// Graficos
	private String chart_lai_mensal;
	private String chart_lai_tema;
	private String chart_lai_situacao;
	private String chart_lai_orgao;
	
	private String chart_lai_faixa_etaria;
	private String chart_lai_renda;
	private String chart_lai_sexo;
	private String chart_lai_escolaridade;
	private String chart_lai_estado;
	
	private String chart_ouvidoria_mensal;
	private String chart_ouvidoria_tipo;
	private String chart_ouvidoria_tema;
	private String chart_ouvidoria_situacao;
	private String chart_ouvidoria_orgao;
	private String chart_ouvidoria_canal;
	
	private String chart_ouvidoria_faixa_etaria;
	private String chart_ouvidoria_renda;
	private String chart_ouvidoria_sexo;
	private String chart_ouvidoria_escolaridade;
	private String chart_ouvidoria_estado;
	
	private String chart_atendimento_mensal;
	private String chart_atendimento_orgao;
	private String chart_atendimento_canal;
	private String chart_atendimento_situacao;
	
	// Composicao vinda da tela com Período e Órgão da pesquisa
	private String periodo_impressao = null;
	private String orgao_impressao = null;
	
	public void submittedBase64Str(ActionEvent event) throws IOException{
	    if(base64Str.split(",").length > 1){
	        String encoded = base64Str.split(",")[1];
	        byte[] decoded = org.apache.commons.codec.binary.Base64.decodeBase64(encoded);
	            InputStream in = new ByteArrayInputStream(decoded);
	            file = new DefaultStreamedContent(in, "image/png", "chart.png");
	    }
	}
	
	public void exportChartImage(String chart) throws IOException{
	    if(base64Str.split(",").length > 1){
	        String encoded = base64Str.split(",")[1];
	        byte[] decoded = org.apache.commons.codec.binary.Base64.decodeBase64(encoded);
	            InputStream in = new ByteArrayInputStream(decoded);
	            file = new DefaultStreamedContent(in, "image/png", "chart.png");
	    }
	}
	
	
	public void gerarRelatorioLAI() throws IOException {
		PDDocument document = new PDDocument();  
		
		try {
			// Criar Imagens
			
			PDImageXObject imgChart;
			imgChart = LosslessFactory.createFromImage(document, criarImagem(chart_lai_mensal));
			document = criarPagina("Relatório de Acesso à Informação", document, imgChart);
			
			imgChart = LosslessFactory.createFromImage(document, criarImagem(chart_lai_tema));
			document = criarPagina("Relatório de Acesso à Informação", document, imgChart);
			
			imgChart = LosslessFactory.createFromImage(document, criarImagem(chart_lai_situacao));
			document = criarPagina("Relatório de Acesso à Informação", document, imgChart);
			
			imgChart = LosslessFactory.createFromImage(document, criarImagem(chart_lai_orgao));
			document = criarPagina("Relatório de Acesso à Informação", document, imgChart);
			
			imgChart = LosslessFactory.createFromImage(document, criarImagem(chart_lai_faixa_etaria));
			document = criarPagina("Relatório de Acesso à Informação", document, imgChart);
			
			imgChart = LosslessFactory.createFromImage(document, criarImagem(chart_lai_renda));
			document = criarPagina("Relatório de Acesso à Informação", document, imgChart);
			
			imgChart = LosslessFactory.createFromImage(document, criarImagem(chart_lai_sexo));
			document = criarPagina("Relatório de Acesso à Informação", document, imgChart);
			
			imgChart = LosslessFactory.createFromImage(document, criarImagem(chart_lai_escolaridade));
			document = criarPagina("Relatório de Acesso à Informação", document, imgChart);
			
			imgChart = LosslessFactory.createFromImage(document, criarImagem(chart_lai_estado));
			document = criarPagina("Relatório de Acesso à Informação", document, imgChart);
			
			// Converter página Byte Array para possibilitar download
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			document.save(output);
			byte[] buf = output.toByteArray();
			
			String texto = "Geral_" + periodo_impressao;
			if(periodo_impressao != null)
				if(!orgao_impressao.equals(""))
					texto = orgao_impressao + "_" + periodo_impressao;
			
			InputStream in = new ByteArrayInputStream(buf);
			file = new DefaultStreamedContent(in, "application/pdf", "Relatorio LAI-" +texto+ ".pdf");

			document.close();
		} catch (Exception e) {
			e.printStackTrace();
			document.close();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Erro ao gerar relatório"));
		}
	}
	
	public void gerarRelatorioOuvidoria() throws IOException {
		PDDocument document = new PDDocument();  
		
		try {
			// Criar Imagens
			
			PDImageXObject imgChart;
			imgChart = LosslessFactory.createFromImage(document, criarImagem(chart_ouvidoria_mensal));
			document = criarPagina("Relatório de Ouvidoria", document, imgChart);
			
			imgChart = LosslessFactory.createFromImage(document, criarImagem(chart_ouvidoria_tipo));
			document = criarPagina("Relatório de Ouvidoria", document, imgChart);
			
			imgChart = LosslessFactory.createFromImage(document, criarImagem(chart_ouvidoria_tema));
			document = criarPagina("Relatório de Ouvidoria", document, imgChart);
			
			imgChart = LosslessFactory.createFromImage(document, criarImagem(chart_ouvidoria_situacao));
			document = criarPagina("Relatório de Ouvidoria", document, imgChart);
			
			imgChart = LosslessFactory.createFromImage(document, criarImagem(chart_ouvidoria_orgao));
			document = criarPagina("Relatório de Ouvidoria", document, imgChart);
			
			imgChart = LosslessFactory.createFromImage(document, criarImagem(chart_ouvidoria_canal));
			document = criarPagina("Relatório de Ouvidoria", document, imgChart);
			
			imgChart = LosslessFactory.createFromImage(document, criarImagem(chart_ouvidoria_faixa_etaria));
			document = criarPagina("Relatório de Ouvidoria", document, imgChart);
			
			imgChart = LosslessFactory.createFromImage(document, criarImagem(chart_ouvidoria_renda));
			document = criarPagina("Relatório de Ouvidoria", document, imgChart);
			
			imgChart = LosslessFactory.createFromImage(document, criarImagem(chart_ouvidoria_sexo));
			document = criarPagina("Relatório de Ouvidoria", document, imgChart);
			
			imgChart = LosslessFactory.createFromImage(document, criarImagem(chart_ouvidoria_escolaridade));
			document = criarPagina("Relatório de Ouvidoria", document, imgChart);
			
			imgChart = LosslessFactory.createFromImage(document, criarImagem(chart_ouvidoria_estado));
			document = criarPagina("Relatório de Ouvidoria", document, imgChart);
			
			// Converter página Byte Array para possibilitar download
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			document.save(output);
			byte[] buf = output.toByteArray();
			
			String texto = "Geral_" + periodo_impressao;
			if(periodo_impressao != null)
				if(!orgao_impressao.equals(""))
					texto = orgao_impressao + "_" + periodo_impressao;
			

			InputStream in = new ByteArrayInputStream(buf);
			file = new DefaultStreamedContent(in, "application/pdf", "Relatorio Ouvidoria_" + texto + ".pdf");

			document.close();
		} catch (Exception e) {
			e.printStackTrace();
			document.close();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Erro ao gerar relatório"));
		}
	}
	
	public void gerarRelatorioAtendimento() throws IOException {
		PDDocument document = new PDDocument();  
		
		try {
			// Criar Imagens
			
			PDImageXObject imgChart;
			imgChart = LosslessFactory.createFromImage(document, criarImagem(chart_atendimento_mensal));
			document = criarPagina("Relatório de Atendimento", document, imgChart);
			
			imgChart = LosslessFactory.createFromImage(document, criarImagem(chart_atendimento_orgao));
			document = criarPagina("Relatório de Atendimento", document, imgChart);
			
			imgChart = LosslessFactory.createFromImage(document, criarImagem(chart_atendimento_canal));
			document = criarPagina("Relatório de Atendimento", document, imgChart);
			
			imgChart = LosslessFactory.createFromImage(document, criarImagem(chart_atendimento_situacao));
			document = criarPagina("Relatório de Atendimento", document, imgChart);
			
			// Converter página Byte Array para possibilitar download
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			document.save(output);
			byte[] buf = output.toByteArray();
			
			String texto = "Geral_" + periodo_impressao;
			if(periodo_impressao != null)
				if(!orgao_impressao.equals(""))
					texto = orgao_impressao + "_" + periodo_impressao;
			

			InputStream in = new ByteArrayInputStream(buf);
			file = new DefaultStreamedContent(in, "application/pdf", "Relatorio Atendimento_" + texto + ".pdf");

			document.close();
		} catch (Exception e) {
			e.printStackTrace();
			document.close();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Erro ao gerar relatório"));
		}
	}
	
	private PDDocument criarPagina(String titulo, PDDocument document, PDImageXObject imgChart) throws IOException {
		PDPageContentStream contentStream;
		
		PDPage page = new PDPage(new PDRectangle(PDRectangle.A4.getHeight(), PDRectangle.A4.getWidth()));
		
		PDRectangle pageSize = page.getMediaBox();
		float pageWidth = pageSize.getWidth();
		float pageHeight = pageSize.getHeight();

		Dimension scaledDim = new Dimension();
		
		PDFont font = PDType1Font.HELVETICA;
		int marginTop = 30;
		int fontSize = 20;

		Color color = new Color(52, 105, 170);

		String line1 = "Acesso à Informação Pública - Transparência Passiva";
		String line2 = titulo;
		String line3 = "Geral - " + periodo_impressao;
		if(orgao_impressao != null)
			if(!orgao_impressao.equals(""))
			line3 = orgao_impressao  + periodo_impressao;
		String fonte = "Fonte: ouvidoria.se.gov.br";
		String data = "Gerado em " + getDataAtual() ;
		
		try {
			contentStream = new PDPageContentStream(document, page);
	
			contentStream.setNonStrokingColor(color);
			contentStream.addRect(170, 500, 700, 70);
			contentStream.fill();
	
			contentStream.beginText();
			contentStream.setNonStrokingColor(Color.WHITE);
			contentStream.setLeading(15f);
			contentStream.setFont(font, fontSize);
			contentStream.newLineAtOffset(startX(font, fontSize, line1, pageWidth), startY(font, fontSize, pageHeight, marginTop));
			contentStream.showText(line1);
			contentStream.newLine();
			contentStream.setFont(font, 14);
			contentStream.showText(line2);
			contentStream.newLine();
			contentStream.showText(line3);
			contentStream.endText();
	
			scaledDim = getScaledDimension(new Dimension(imgChart.getWidth(), imgChart.getHeight()), new Dimension((int) (pageWidth*0.85), (int) (pageHeight*0.66)));
			contentStream.drawImage(imgChart, centralizar(pageWidth, scaledDim.width), centralizar(pageHeight, scaledDim.height), scaledDim.width, scaledDim.height);
	
			contentStream.beginText();
			contentStream.setNonStrokingColor(Color.DARK_GRAY);
			contentStream.setFont(font, 10);
			contentStream.newLineAtOffset(startX(font, 10, fonte, pageWidth), 30);
			contentStream.showText(fonte);
			contentStream.endText();
	
			contentStream.beginText();
			contentStream.setNonStrokingColor(Color.DARK_GRAY);
			contentStream.setFont(font, 7);
			contentStream.newLineAtOffset(startX(font, 7, data, pageWidth), 20);
			contentStream.showText(data);
			contentStream.endText();
	
			contentStream.close();
			document.addPage(page);
		
		} catch (Exception e) {
			e.printStackTrace();
			document.close();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Erro ao gerar relatório"));
		}
		
		return document;
	}
	
	
	private String getMesFinal() {
		return GeralUtil.meses[Calendar.getInstance().get(Calendar.MONTH)+1];
	}
	
	private static Dimension getScaledDimension(Dimension imgSize, Dimension boundary) {
		  int original_width = imgSize.width;
		  int original_height = imgSize.height;
		  int bound_width = boundary.width;
		  int bound_height = boundary.height;
		  int new_width = original_width;
		  int new_height = original_height;

		  // first check if we need to scale width
		  if (original_width > bound_width) {
		    //scale width to fit
		    new_width = bound_width;
		    //scale height to maintain aspect ratio
		    new_height = (new_width * original_height) / original_width;
		  }

		  // then check if we need to scale even with the new height
		  if (new_height > bound_height) {
		    //scale height to fit instead
		    new_height = bound_height;
		    //scale width to maintain aspect ratio
		    new_width = (new_height * original_width) / original_height;
		  }

		  return new Dimension(new_width, new_height);
	}
	
	private float centralizar(float tamanhoPagina, float tamanhoImagem) {
		return (tamanhoPagina - tamanhoImagem) / 2;
	}
	
	private String getDataAtual() {
		String data;
		Calendar c = Calendar.getInstance();
		int diaAtual = c.get(Calendar.DAY_OF_MONTH);
		int mesAtual = c.get(Calendar.MONTH) + 1;
		int anoAtual = c.get(Calendar.YEAR);
		int horaAtual = c.get(Calendar.HOUR_OF_DAY);
		int minAtual = c.get(Calendar.MINUTE);
		
		if (mesAtual >= 12) mesAtual = 12;
		else if (mesAtual <= 0) mesAtual = 1;
		
		data = diaAtual + "/" + mesAtual + "/" + anoAtual + " às " + horaAtual + ":" + minAtual;
		
 		return data;
	}
	
	private float startX(PDFont font, int fontSize, String line, float pageWidth) throws IOException {
		float titleWidth = font.getStringWidth(line) / 1000 * fontSize;
		
        float startX = (pageWidth - titleWidth) / 2;
        
		return startX;
	}
	
	private float startY(PDFont font, int fontSize, float pageHeight, float marginTop) throws IOException {
        float titleHeight = font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * fontSize;
		
		float startY = pageHeight - marginTop - titleHeight;
		
		return startY;
	}
	
	public BufferedImage criarImagem(String imagem) throws IOException {
		if (imagem != null) {
			String encoded = imagem.split(",")[1];
			byte[] decoded = org.apache.commons.codec.binary.Base64.decodeBase64(encoded);
			ByteArrayInputStream bais = new ByteArrayInputStream(decoded);
			BufferedImage bim = ImageIO.read(bais);
			return bim;
		}else {
			return null;
		}
		
		
	}
	
	
    public StreamedContent getFile() {
    	return file;
    }
	
	public String getBase64Str() {
		return base64Str;
	}

	public void setBase64Str(String base64Str) {
		this.base64Str = base64Str;
	}


//	public String getChartGeral() {
//		return chartGeral;
//	}
//
//
//	public void setChartGeral(String chartGeral) {
//		this.chartGeral = chartGeral;
//	}
//
//
//	public String getChartMensal() {
//		return chartMensal;
//	}
//
//
//	public void setChartMensal(String chartMensal) {
//		this.chartMensal = chartMensal;
//	}
//
//
//	public String getChartAnual() {
//		return chartAnual;
//	}
//
//
//	public void setChartAnual(String chartAnual) {
//		this.chartAnual = chartAnual;
//	}
//
//
//	public String getChartOrgao() {
//		return chartOrgao;
//	}
//
//
//	public void setChartOrgao(String chartOrgao) {
//		this.chartOrgao = chartOrgao;
//	}
//
//
//	public String getChartAnualAcu() {
//		return chartAnualAcu;
//	}
//
//
//	public void setChartAnualAcu(String chartAnualAcu) {
//		this.chartAnualAcu = chartAnualAcu;
//	}
//
//
//	public String getChartEntidade() {
//		return chartEntidade;
//	}
//
//
//	public void setChartEntidade(String chartEntidade) {
//		this.chartEntidade = chartEntidade;
//	}
//
//
//	public String getChartAssunto() {
//		return chartAssunto;
//	}
//
//
//	public void setChartAssunto(String chartAssunto) {
//		this.chartAssunto = chartAssunto;
//	}
//
//
//	public String getChartPessoa() {
//		return chartPessoa;
//	}
//
//
//	public void setChartPessoa(String chartPessoa) {
//		this.chartPessoa = chartPessoa;
//	}
//
//
//	public String getChartEstado() {
//		return chartEstado;
//	}
//
//
//	public void setChartEstado(String chartEstado) {
//		this.chartEstado = chartEstado;
//	}
//
//	public String getChartGeralOuvidoria() {
//		return chartGeralOuvidoria;
//	}
//
//	public void setChartGeralOuvidoria(String chartGeralOuvidoria) {
//		this.chartGeralOuvidoria = chartGeralOuvidoria;
//	}
//
//	public String getChartMensalOuvidoria() {
//		return chartMensalOuvidoria;
//	}
//
//	public void setChartMensalOuvidoria(String chartMensalOuvidoria) {
//		this.chartMensalOuvidoria = chartMensalOuvidoria;
//	}
//
//	public String getChartPorCanalOuvidoria() {
//		return chartPorCanalOuvidoria;
//	}
//
//	public void setChartPorCanalOuvidoria(String chartPorCanalOuvidoria) {
//		this.chartPorCanalOuvidoria = chartPorCanalOuvidoria;
//	}
//
//	public String getChartPorOrgaoDiretaOuvidoria() {
//		return chartPorOrgaoDiretaOuvidoria;
//	}
//
//	public void setChartPorOrgaoDiretaOuvidoria(String chartPorOrgaoDiretaOuvidoria) {
//		this.chartPorOrgaoDiretaOuvidoria = chartPorOrgaoDiretaOuvidoria;
//	}
//
//	public String getChartPorOrgaoIniretaOuvidoria() {
//		return chartPorOrgaoIndiretaOuvidoria;
//	}
//
//	public void setChartPorOrgaoIniretaOuvidoria(String chartPorOrgaoIniretaOuvidoria) {
//		this.chartPorOrgaoIndiretaOuvidoria = chartPorOrgaoIniretaOuvidoria;
//	}
//
//	public String getChartPorTipoOuvidoria() {
//		return chartPorTipoOuvidoria;
//	}
//
//	public void setChartPorTipoOuvidoria(String chartPorTipoOuvidoria) {
//		this.chartPorTipoOuvidoria = chartPorTipoOuvidoria;
//	}
//
//	public String getChartPorIdentificacaoOuvidoria() {
//		return chartPorIdentificacaoOuvidoria;
//	}
//
//	public void setChartPorIdentificacaoOuvidoria(String chartPorIdentificacaoOuvidoria) {
//		this.chartPorIdentificacaoOuvidoria = chartPorIdentificacaoOuvidoria;
//	}
//
//	public String getChartPorStatusOuvidoria() {
//		return chartPorStatusOuvidoria;
//	}
//
//	public void setChartPorStatusOuvidoria(String chartPorStatusOuvidoria) {
//		this.chartPorStatusOuvidoria = chartPorStatusOuvidoria;
//	}
//
//	public String getChartPorPessoaOuvidoria() {
//		return chartPorPessoaOuvidoria;
//	}
//
//	public void setChartPorPessoaOuvidoria(String chartPorPessoaOuvidoria) {
//		this.chartPorPessoaOuvidoria = chartPorPessoaOuvidoria;
//	}
//
//	public String getChartPorTemaOuvidoria() {
//		return chartPorTemaOuvidoria;
//	}
//
//	public void setChartPorTemaOuvidoria(String chartPorTemaOuvidoria) {
//		this.chartPorTemaOuvidoria = chartPorTemaOuvidoria;
//	}
	
	//Novos Graficos

	public String getChart_lai_mensal() {
		return chart_lai_mensal;
	}

	public void setChart_lai_mensal(String chart_lai_mensal) {
		this.chart_lai_mensal = chart_lai_mensal;
	}

	public String getChart_lai_tema() {
		return chart_lai_tema;
	}

	public void setChart_lai_tema(String chart_lai_tema) {
		this.chart_lai_tema = chart_lai_tema;
	}

	public String getChart_lai_situacao() {
		return chart_lai_situacao;
	}

	public void setChart_lai_situacao(String chart_lai_situacao) {
		this.chart_lai_situacao = chart_lai_situacao;
	}

	public String getChart_lai_orgao() {
		return chart_lai_orgao;
	}

	public void setChart_lai_orgao(String chart_lai_orgao) {
		this.chart_lai_orgao = chart_lai_orgao;
	}

	public String getChart_ouvidoria_mensal() {
		return chart_ouvidoria_mensal;
	}

	public void setChart_ouvidoria_mensal(String chart_ouvidoria_mensal) {
		this.chart_ouvidoria_mensal = chart_ouvidoria_mensal;
	}

	public String getChart_ouvidoria_tipo() {
		return chart_ouvidoria_tipo;
	}

	public void setChart_ouvidoria_tipo(String chart_ouvidoria_tipo) {
		this.chart_ouvidoria_tipo = chart_ouvidoria_tipo;
	}

	public String getChart_ouvidoria_tema() {
		return chart_ouvidoria_tema;
	}

	public void setChart_ouvidoria_tema(String chart_ouvidoria_tema) {
		this.chart_ouvidoria_tema = chart_ouvidoria_tema;
	}

	public String getChart_ouvidoria_situacao() {
		return chart_ouvidoria_situacao;
	}

	public void setChart_ouvidoria_situacao(String chart_ouvidoria_situacao) {
		this.chart_ouvidoria_situacao = chart_ouvidoria_situacao;
	}

	public String getChart_ouvidoria_orgao() {
		return chart_ouvidoria_orgao;
	}

	public void setChart_ouvidoria_orgao(String chart_ouvidoria_orgao) {
		this.chart_ouvidoria_orgao = chart_ouvidoria_orgao;
	}

	public String getPeriodo_impressao() {
		return periodo_impressao;
	}

	public void setPeriodo_impressao(String periodo_impressao) {
		this.periodo_impressao = periodo_impressao;
	}

	public String getOrgao_impressao() {
		return orgao_impressao;
	}

	public void setOrgao_impressao(String orgao_impressao) {
		this.orgao_impressao = orgao_impressao;
	}

	public String getChart_ouvidoria_faixa_etaria() {
		return chart_ouvidoria_faixa_etaria;
	}

	public void setChart_ouvidoria_faixa_etaria(String chart_ouvidoria_faixa_etaria) {
		this.chart_ouvidoria_faixa_etaria = chart_ouvidoria_faixa_etaria;
	}

	public String getChart_ouvidoria_renda() {
		return chart_ouvidoria_renda;
	}

	public void setChart_ouvidoria_renda(String chart_ouvidoria_renda) {
		this.chart_ouvidoria_renda = chart_ouvidoria_renda;
	}

	public String getChart_ouvidoria_sexo() {
		return chart_ouvidoria_sexo;
	}

	public void setChart_ouvidoria_sexo(String chart_ouvidoria_sexo) {
		this.chart_ouvidoria_sexo = chart_ouvidoria_sexo;
	}

	public String getChart_ouvidoria_escolaridade() {
		return chart_ouvidoria_escolaridade;
	}

	public void setChart_ouvidoria_escolaridade(String chart_ouvidoria_escolaridade) {
		this.chart_ouvidoria_escolaridade = chart_ouvidoria_escolaridade;
	}

	public String getChart_lai_faixa_etaria() {
		return chart_lai_faixa_etaria;
	}

	public void setChart_lai_faixa_etaria(String chart_lai_faixa_etaria) {
		this.chart_lai_faixa_etaria = chart_lai_faixa_etaria;
	}

	public String getChart_lai_renda() {
		return chart_lai_renda;
	}

	public void setChart_lai_renda(String chart_lai_renda) {
		this.chart_lai_renda = chart_lai_renda;
	}

	public String getChart_lai_sexo() {
		return chart_lai_sexo;
	}

	public void setChart_lai_sexo(String chart_lai_sexo) {
		this.chart_lai_sexo = chart_lai_sexo;
	}

	public String getChart_lai_escolaridade() {
		return chart_lai_escolaridade;
	}

	public void setChart_lai_escolaridade(String chart_lai_escolaridade) {
		this.chart_lai_escolaridade = chart_lai_escolaridade;
	}

	public String getChart_lai_estado() {
		return chart_lai_estado;
	}

	public void setChart_lai_estado(String chart_lai_estado) {
		this.chart_lai_estado = chart_lai_estado;
	}

	public String getChart_ouvidoria_estado() {
		return chart_ouvidoria_estado;
	}

	public void setChart_ouvidoria_estado(String chart_ouvidoria_estado) {
		this.chart_ouvidoria_estado = chart_ouvidoria_estado;
	}

	public String getChart_ouvidoria_canal() {
		return chart_ouvidoria_canal;
	}

	public void setChart_ouvidoria_canal(String chart_ouvidoria_canal) {
		this.chart_ouvidoria_canal = chart_ouvidoria_canal;
	}

	public String getChart_atendimento_mensal() {
		return chart_atendimento_mensal;
	}

	public void setChart_atendimento_mensal(String chart_atendimento_mensal) {
		this.chart_atendimento_mensal = chart_atendimento_mensal;
	}

	public String getChart_atendimento_orgao() {
		return chart_atendimento_orgao;
	}

	public void setChart_atendimento_orgao(String chart_atendimento_orgao) {
		this.chart_atendimento_orgao = chart_atendimento_orgao;
	}

	public String getChart_atendimento_canal() {
		return chart_atendimento_canal;
	}

	public void setChart_atendimento_canal(String chart_atendimento_canal) {
		this.chart_atendimento_canal = chart_atendimento_canal;
	}

	public String getChart_atendimento_situacao() {
		return chart_atendimento_situacao;
	}

	public void setChart_atendimento_situacao(String chart_atendimento_situacao) {
		this.chart_atendimento_situacao = chart_atendimento_situacao;
	}
}
