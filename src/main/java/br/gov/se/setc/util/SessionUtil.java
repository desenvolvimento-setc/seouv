package br.gov.se.setc.util;

import javax.faces.context.FacesContext;

public class SessionUtil {

	public static void salvarNaSessao(String key, Object obj) {
		FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put(key, obj);
	}
	
	public static Object recuperarDaSessao(String key) {
		return FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get(key);
	}
	
}
