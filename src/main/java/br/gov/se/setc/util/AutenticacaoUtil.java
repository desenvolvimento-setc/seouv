package br.gov.se.setc.util;

public final class  AutenticacaoUtil {
	
//	private final static String hostNameEmail = "smtp.gmail.com";
	private final static int port = 587;
	private final static String hostNameEmail = "smtp.expresso.se.gov.br";
	private final static String userLoginEmailAuthentication = "desenvolvimento.cge";
	private final static String senhaUserLoginEmailAuthentication = "setc2019";
	private final static String emailFrom = "nao_responder@setc.se.gov.br";
	private final static String enderecolocal = "http://localhost:9090/esic/";
	private final static String enderecohomologacao = "http://172.22.21.120:8080/esic/";
	private final static String enderecoProducao = "https://ouvidoria.se.gov.br/";

	public static String getHostNameEmail() {
		return hostNameEmail;
	}
	
	public static String getUserLoginEmailAuthentication() {
		return userLoginEmailAuthentication;
	}
	
	public static String getSenhaUserLoginEmailAuthentication() {
		return senhaUserLoginEmailAuthentication;
	}
	
	public static String getEmailFrom() {
		return emailFrom;
	}
	
	public static String getEndereco() {
		//return enderecolocal;
//		return enderecohomologacao;
		return enderecoProducao;
	}
	
	public static int getPort() {
		return port;
	}
	
}
