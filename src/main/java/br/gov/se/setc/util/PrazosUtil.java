package br.gov.se.setc.util;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class PrazosUtil {
	
	private static final int prazoEsicResposta = 20; // Produção: 20
	private static final int prazoEsicProrrogada = 10; // Produção: 10
	private static final int prazoEsicRecurso = 10; // Produção: 10
	private static final int prazoEsicEncaminhamento = 5; // Produção: 5
	
	private static final int prazoOuvResposta = 30; // Produção: 30
	private static final int prazoOuvProrrogada = 30; // Produção: 30
	private static final int prazoOuvRecurso = 10; // Produção: 15
	private static final int prazoOuvEncaminhamento = 5; // Produção: 5
	

	public static int prazoResposta(String tipo, String status) {
		
//		System.out.println(":: Entrou em prazoResposta | Tipo: " + tipo + " | Status: " + status);
		
		if (tipo.equals("Esic")) {
			if (status.equals("Aberta"))
				return prazoEsicResposta;
			else if (status.equals("Prorrogada"))
				return prazoEsicProrrogada;
			else if (status.equals("Atendida") || status.equals("Recurso") || status.equals("Reformulação"))
				return prazoEsicRecurso;
			else if(status.equals("Encaminhar") || status.equals("Reabrir"))
				return prazoEsicEncaminhamento;
			else
				return prazoEsicResposta;
		} 
		
		else if (tipo.equals("Ouv")) {
			if (status.equals("Aberta"))
				return prazoOuvResposta;
			else if (status.equals("Prorrogada"))
				return prazoOuvProrrogada;
			else if (status.equals("Atendida") || status.equals("Recurso") || status.equals("Reformulação"))
				return prazoOuvRecurso;
			else if(status.equals("Encaminhar") || status.equals("Reabrir"))
				return prazoOuvEncaminhamento;
			else
				return prazoOuvResposta;
		}
		
		return prazoEsicResposta;
	}
	
	public static Date gerarPrazoDiaUtilLimite(Date data, String tipo, String status) {
		
		int prazo = prazoResposta(tipo, status);
		
		Calendar dataInicial = Calendar.getInstance();
		
		dataInicial.setTime(data);
		
		dataInicial = pularFimDeSemana(dataInicial);
		
		dataInicial.add(Calendar.DAY_OF_MONTH, prazo);
		
		dataInicial = pularFimDeSemana(dataInicial);
		
		return dataInicial.getTime();
	}
	
	public static Calendar pularFimDeSemana(Calendar data) {
		
		if (data.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
			data.add(Calendar.DAY_OF_MONTH, +2);
		} else if (data.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
			data.add(Calendar.DAY_OF_MONTH, +1);
		}
		
		return data;
	}
	
	
	
}
