package br.gov.se.setc.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import br.gov.se.setc.model.Anexo;

public class AnexoUtil {

	public static StreamedContent download(Anexo anexo) throws IOException {
		StreamedContent file;
		
		byte[] buf = anexo.getConteudo();
		
		InputStream inputStream = new ByteArrayInputStream(buf);
        
        file = new DefaultStreamedContent(inputStream, anexo.getTipo(), anexo.getNome());
        
        inputStream.close();
    
        return file;
	}
	
}
