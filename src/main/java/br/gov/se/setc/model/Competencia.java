package br.gov.se.setc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "competencias")
public class Competencia implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idCompetencias", unique = true, nullable = false)
	private Integer idCompetencia;
	
	@ManyToOne(optional=false, fetch = FetchType.EAGER)
	@JoinColumn(name="idAcoes", nullable=false)
	private Tema tema;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idEntidades", nullable = false)
	private Entidade entidade;
	
	@Column(name = "Descricao", nullable = true)
	private String descricao;
	
	@Column(name = "ativa", nullable = false)
	private boolean ativa;
	
	@ManyToOne(optional=true, fetch = FetchType.LAZY)
	@JoinColumn(name="atualizado_por", nullable=true)
	private Responsavel atualizado_por;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "atualizado_em", length = 19)
	private Date atualizado_em;

	public Competencia() {
		
	}
	
	public Integer getIdCompetencia() {
		return this.idCompetencia;
	}

	public void setIdCompetencia(Integer idCompetencias) {
		this.idCompetencia = idCompetencias;
	}

	public Tema getTema() {
		return this.tema;
	}

	public void setTema(Tema tema) {
		this.tema = tema;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public boolean isAtiva() {
		return this.ativa;
	}

	public void setAtiva(boolean ativa) {
		this.ativa = ativa;
	}

	public Entidade getEntidade() {
		return entidade;
	}

	public void setEntidade(Entidade entidade) {
		this.entidade = entidade;
	}
	
	public Responsavel getAtualizado_por() {
		return atualizado_por;
	}

	public void setAtualizado_por(Responsavel atualizado_por) {
		this.atualizado_por = atualizado_por;
	}

	public Date getAtualizado_em() {
		return atualizado_em;
	}

	public void setAtualizado_em(Date atualizado_em) {
		this.atualizado_em = atualizado_em;
	}

}
