package br.gov.se.setc.model;

import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;


@SqlResultSetMapping(
    name = "DashboardInfo",
    classes={
      @ConstructorResult(
        targetClass = DashboardInfo.class,
        columns={
          @ColumnResult(name="total", type=Integer.class),
          @ColumnResult(name="atendidas", type=Integer.class),
          @ColumnResult(name="semResposta", type=Integer.class),
          @ColumnResult(name="emTramite", type=Integer.class),
          @ColumnResult(name="naoVisualizadas", type=Integer.class)
        })
    }
)


@Entity
@Table(name = "anexo")
public class Anexo implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idAnexo", unique = true, nullable = false)
	private Integer idAnexo;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "Mensagem_idMensagem", nullable = false)
	private Mensagem mensagem;
	
	@Column(name = "nome", length = 100)
	private String nome;
	
	@Column(name = "tipo", length = 800)
	private String tipo;
	
	@Column(name = "tamanho")
	private Long tamanho;
	
	@Column(name = "local", length = 100)
	private String local;
	
	@Column(name = "conteudo")
	private byte[] conteudo;

	
	public Anexo() {
		
	}

	public Anexo(Mensagem mensagem) {
		this.mensagem = mensagem;
	}

	public Anexo(Mensagem mensagem, String nome, String tipo, Long tamanho, byte[] conteudo) {
		this.mensagem = mensagem;
		this.nome = nome;
		this.tipo = tipo;
		this.tamanho = tamanho;
		this.conteudo = conteudo;
	}


	public Integer getIdAnexo() {
		return idAnexo;
	}


	public void setIdAnexo(Integer idAnexo) {
		this.idAnexo = idAnexo;
	}


	public Mensagem getMensagem() {
		return mensagem;
	}


	public void setMensagem(Mensagem mensagem) {
		this.mensagem = mensagem;
	}


	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getTipo() {
		return tipo;
	}


	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	public Long getTamanho() {
		return tamanho;
	}


	public void setTamanho(Long tamanho) {
		this.tamanho = tamanho;
	}


	public byte[] getConteudo() {
		return conteudo;
	}


	public void setConteudo(byte[] conteudo) {
		this.conteudo = conteudo;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}


}


