package br.gov.se.setc.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.OrderBy;

@Entity
@Table(name = "mensagem")
public class Mensagem implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "idMensagem", unique = true, nullable = false)
	private Integer idMensagem;
	
	@ManyToOne(optional=false, fetch = FetchType.LAZY)
	@JoinColumn(name = "idSolicitacao", nullable = false)
	private Manifestacao manifestacao;
	
	@Column(name = "texto", length = 65535)
	private String texto;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data", length = 19)
	private Date data;
	
	@Column(name = "tipo")
	private short tipo;
	
	@Column(name = "nota")
	private Integer nota;
	
	@ManyToOne(optional=false, fetch = FetchType.EAGER)
	@JoinColumn(name="idUsuario", nullable=false)
	private Usuario usuario;
	
	@OneToMany(mappedBy="mensagem", cascade = CascadeType.ALL)
	@OrderBy(clause = "LENGTH(nome), nome ASC")
	private Set<Anexo> anexos = new HashSet<>(0);

	
	public Mensagem() {
		
	}

	
	public Mensagem(String texto, Date data, short tipo, Integer nota,Usuario usuario) {
		this.texto = texto;
		this.data = data;
		this.tipo = tipo;
		this.nota = nota;
		this.usuario = usuario;
	}


	public Integer getIdMensagem() {
		return idMensagem;
	}

	public void setIdMensagem(Integer idMensagem) {
		this.idMensagem = idMensagem;
	}

	public Manifestacao getManifestacao() {
		return manifestacao;
	}

	public void setManifestacao(Manifestacao manifestacao) {
		this.manifestacao = manifestacao;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public Date getData() {

	    return data; 
	    
	}
	public void setData(Date data) {
		this.data = data;
	}

	public short getTipo() {
		return tipo;
	}

	public void setTipo(short tipo) {
		this.tipo = tipo;
	}

	public Integer getNota() {
		return nota;
	}

	public void setNota(Integer nota) {
		this.nota = nota;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Set<Anexo> getAnexos() {
		return anexos;
	}

	public void setAnexos(List<Anexo> anexos) {
		for(Anexo anx : anexos) 
			anx.setMensagem(this);
		
		this.anexos = new HashSet<>(anexos);
	}
	
	public void addAnexo(Anexo anexo) {
		anexo.setMensagem(this);
		this.anexos.add(anexo);
	}

}
