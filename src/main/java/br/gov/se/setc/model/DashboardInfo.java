package br.gov.se.setc.model;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.SqlResultSetMapping;

public class DashboardInfo implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private int total;
	private int atendidas;
	private int semResposta;
	private int emTramite;
	private int naoVisualizada;
	
	
	public DashboardInfo() {
		this.total = 0;
		this.atendidas = 0;
		this.semResposta = 0;
		this.emTramite = 0;
		this.naoVisualizada = 0;
	}
	
	
	public DashboardInfo(int total, int atendidas, int semResposta, int emTramite, int naoVisualizada) {
		this.total = total;
		this.atendidas = atendidas;
		this.semResposta = semResposta;
		this.emTramite = emTramite;
		this.naoVisualizada = naoVisualizada;
	}


	public int getTotal() {
		return total;
	}
	
	public void setTotal(int total) {
		this.total = total;
	}
	
	public int getAtendidas() {
		return atendidas;
	}
	
	public void setAtendidas(int atendidas) {
		this.atendidas = atendidas;
	}
	
	public int getSemResposta() {
		return semResposta;
	}
	
	public void setSemResposta(int semResposta) {
		this.semResposta = semResposta;
	}
	
	public int getEmTramite() {
		return emTramite;
	}
	
	public void setEmTramite(int emTramite) {
		this.emTramite = emTramite;
	}
	
	public int getNaoVisualizada() {
		return naoVisualizada;
	}
	
	public void setNaoVisualizada(int naoVisualizada) {
		this.naoVisualizada = naoVisualizada;
	}

	
}
