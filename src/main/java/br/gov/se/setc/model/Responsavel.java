package br.gov.se.setc.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "responsavel")
public class Responsavel implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idResponsavel;
	
	@ManyToOne(optional=false, fetch = FetchType.EAGER)
	@JoinColumn(name="idUsuario", nullable=false)
	private Usuario usuario;
	
	@ManyToOne(optional=false, fetch = FetchType.EAGER)
	@JoinColumn(name="idEntidades", nullable=false)
	private Entidade entidade;
	
	private Short nivel;
	private String email;
	private String telefone1;
	private String telefone2;
	private boolean ativo;
	
	public Responsavel() {
		
	}
	
	public Responsavel(Integer idResponsavel) {
		this.idResponsavel = idResponsavel;
	}

	public Integer getIdResponsavel() {
		return idResponsavel;
	}

	public void setIdResponsavel(Integer idResponsavel) {
		this.idResponsavel = idResponsavel;
	}

	public Short getNivel() {
		return nivel;
	}

	public void setNivel(Short nivel) {
		this.nivel = nivel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone1() {
		return telefone1;
	}

	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}

	public String getTelefone2() {
		return telefone2;
	}

	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Entidade getEntidade() {
		return entidade;
	}

	public void setEntidade(Entidade entidade) {
		this.entidade = entidade;
	}

	@Override
	public String toString() {
		return "Responsavel [idResponsavel=" + idResponsavel + ", usuario=" + usuario + ", entidade=" + entidade
				+ ", nivel=" + nivel + ", email=" + email + ", telefone1=" + telefone1 + ", telefone2=" + telefone2
				+ ", ativo=" + ativo + "]";
	}
	
}
