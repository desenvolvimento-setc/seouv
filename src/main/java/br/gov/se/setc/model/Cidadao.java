package br.gov.se.setc.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

@SqlResultSetMapping(
    name = "DashboardInfo2",
    classes={
      @ConstructorResult(
        targetClass = DashboardInfo2.class,
        columns={
          @ColumnResult(name="total", type=Integer.class),
          @ColumnResult(name="vencendo", type=Integer.class),
          @ColumnResult(name="naoVisualizadas", type=Integer.class),
          @ColumnResult(name="inativas", type=Integer.class),
        })
    }
)

@Entity
@Table(name = "cidadao")
public class Cidadao implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idCidadao", unique = true, nullable = false)
	private Integer idCidadao;
	
	@ManyToOne(optional=false, fetch = FetchType.EAGER)
	@JoinColumn(name="idUsuario", nullable=false)
	private Usuario usuario;
	
	private String email;
	private String cpf;
	private Boolean tipo;
	private String rg;
	private String orgaexp;
	private Date datanasc;
	private String sexo;
	private Short escolaridade;
	private String profissao;
	private String endereco;
	private String estado;
	private String cidade;
	private String cep;
	private String bairro;
	private String complemento;
	private short renda;
	private String numero;
	private String telefone;
	private String celular;
	
	
	public Cidadao() {
	
	}
	
	public Cidadao(Integer idCidadao) {
		this.idCidadao = idCidadao;
	}
	
	public Cidadao(Usuario usuario, String email) {
		this.usuario = usuario;
		this.email = email;
	}

	public Cidadao(String email, String cpf, Boolean tipo, String rg, String orgaexp, Date datanasc,
			String sexo, Short escolaridade, String profissao, String endereco, String estado, String cidade,
			String cep, String bairro, String complemento, short renda, String numero, String telefone,
			String celular) {
		this.email = email;
		this.cpf = cpf;
		this.tipo = tipo;
		this.rg = rg;
		this.orgaexp = orgaexp;
		this.datanasc = datanasc;
		this.sexo = sexo;
		this.escolaridade = escolaridade;
		this.profissao = profissao;
		this.endereco = endereco;
		this.estado = estado;
		this.cidade = cidade;
		this.cep = cep;
		this.bairro = bairro;
		this.complemento = complemento;
		this.renda = renda;
		this.numero = numero;
		this.telefone = telefone;
		this.celular = celular;
	}


	public Integer getIdCidadao() {
		return idCidadao;
	}

	public void setIdCidadao(Integer idCidadao) {
		this.idCidadao = idCidadao;
	}
	
	public Usuario getUsuario() {
		return usuario;
	}
	
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getCpf() {
		return cpf;
	}
	
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public Boolean getTipo() {
		return tipo;
	}
	
	public void setTipo(Boolean tipo) {
		this.tipo = tipo;
	}
	
	public String getRg() {
		return rg;
	}
	
	public void setRg(String rg) {
		this.rg = rg;
	}
	
	public String getOrgaexp() {
		return orgaexp;
	}
	
	public void setOrgaexp(String orgaexp) {
		this.orgaexp = orgaexp;
	}
	
	public Date getDatanasc() {
		return datanasc;
	}
	
	public void setDatanasc(Date datanasc) {
		this.datanasc = datanasc;
	}
	
	public String getSexo() {
		return sexo;
	}
	
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public Short getEscolaridade() {
		return escolaridade;
	}
	public void setEscolaridade(Short escolaridade) {
		this.escolaridade = escolaridade;
	}
	public String getProfissao() {
		return profissao;
	}
	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public short getRenda() {
		return renda;
	}
	public void setRenda(short renda) {
		this.renda = renda;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}

	@Override
	public String toString() {
		return "Cidadao [idCidadao=" + idCidadao + ", usuario=" + usuario + ", email=" + email + ", cpf=" + cpf
				+ ", tipo=" + tipo + ", rg=" + rg + ", orgaexp=" + orgaexp + ", datanasc=" + datanasc + ", sexo=" + sexo
				+ ", escolaridade=" + escolaridade + ", profissao=" + profissao + ", endereco=" + endereco + ", estado="
				+ estado + ", cidade=" + cidade + ", cep=" + cep + ", bairro=" + bairro + ", complemento=" + complemento
				+ ", renda=" + renda + ", numero=" + numero + ", telefone=" + telefone + ", celular=" + celular + "]";
	}
	
}
