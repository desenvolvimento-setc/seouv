package br.gov.se.setc.model;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.SqlResultSetMapping;

public class DashboardInfo2 implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private int total;
	private int vencendo;
	private int naoVisualizadas;
	private int inativas;
	
	
	public DashboardInfo2() {
		this.total = 0;
		this.vencendo = 0;
		this.naoVisualizadas = 0;
		this.inativas = 0;
	}
	
	
	public DashboardInfo2(int total, int vencendo, int naoVisualizadas, int inativas) {
		this.total = total;
		this.vencendo = vencendo;
		this.naoVisualizadas = naoVisualizadas;
		this.inativas = inativas;
	}


	public int getTotal() {
		return total;
	}
	
	public void setTotal(int total) {
		this.total = total;
	}
	
	public int getVencendo() {
		return vencendo;
	}
	
	public void setVencendo(int vencendo) {
		this.vencendo = vencendo;
	}
	
	public int getNaoVisualizadas() {
		return naoVisualizadas;
	}
	
	public void setNaoVisualizadas(int naoVisualizadas) {
		this.naoVisualizadas = naoVisualizadas;
	}
	
	public int getInativas() {
		return inativas;
	}
	
	public void setInativas(int inativas) {
		this.inativas = inativas;
	}
	
	
}
