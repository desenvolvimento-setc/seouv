package br.gov.se.setc.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "acoes")
public class Tema implements java.io.Serializable, Comparable<Tema> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idAcoes", unique = true, nullable = false)
	private Integer idTema;
	
	@Column(name = "titulo", length = 60)
	private String titulo;
	
	@Column(name = "descricao", length = 65535)
	private String descricao;
	
	@Column(name = "status", length = 45)
	private String status;
	
	@OneToMany(mappedBy="tema", cascade = CascadeType.PERSIST)
	private List<Competencia> competencias = new ArrayList<>();

	public Tema(Integer idTema) {
		this.idTema = idTema;
	}
	
	public Tema() {
	}

	public Tema(String titulo, String descricao, List<Competencia> competencias, String status) {
		this.titulo = titulo;
		this.descricao = descricao;
		this.setCompetencias(competencias);
		this.status = status;
	}
	
	public Tema(Integer idTema, String titulo, String descricao, String status) {
		this.idTema = idTema;
		this.titulo = titulo;
		this.descricao = descricao;
		this.status = status;
	}
	
	@Override
	public int compareTo(Tema t) {
		if (getTitulo() == null || t.getTitulo() == null) {
	      return 0;
	    }
		
	    return getTitulo().compareTo(t.getTitulo());
	}
	
	public Tema copy(Tema this) {
		Tema t = new Tema(this.idTema, this.titulo, this.descricao, this.status);
		return t;
	}
	
	
	public Integer getIdTema() {
		return this.idTema;
	}

	public void setIdTema(Integer idTema) {
		this.idTema = idTema;
	}

	public String getTitulo() {
		return this.titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public void addCompetencia(Competencia competencia) {
		competencia.setTema(this);
		competencias.add(competencia);
	}

	public List<Competencia> getCompetencias() {
		return competencias;
	}

	public void setCompetencias(List<Competencia> competencias) {
		this.competencias = competencias;
	}

	@Override
	public String toString() {
		return "Tema [idTema=" + idTema + ", titulo=" + titulo + ", descricao=" + descricao + ", status=" + status
				+ ", competencias=" + competencias + "]";
	}
	

}
