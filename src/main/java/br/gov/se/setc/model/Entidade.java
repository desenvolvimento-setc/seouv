package br.gov.se.setc.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Where;

@Entity
@Table(name = "entidades")
public class Entidade {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idEntidades", unique = true, nullable = false)
	private Integer idEntidade;
	@Column(name = "idOrgaos", nullable = false)
	private Integer idSuperior;
	private String nome;
	private String sigla;
	private boolean orgao;
	private boolean ativa;
	private String site;
	private String telefone;
	private String endereco;
	private String horario;
	
	@OneToMany(mappedBy="entidade", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	private List<Competencia> competencias = new ArrayList<>();
	
	@OneToMany(mappedBy="entidade", fetch = FetchType.LAZY)
	@Where(clause = "ativo = true and nivel = 1")
	private List<Responsavel> ouvidores = new ArrayList<>();
	
	@ManyToOne(optional=true, fetch = FetchType.LAZY)
	@JoinColumn(name="atualizado_por", nullable=true)
	private Responsavel atualizado_por;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "atualizado_em", length = 19)
	private Date atualizado_em;
	
	
	public Entidade() {
		
	}
	
	public Entidade(Integer idEntidade) {
		this.idEntidade = idEntidade;
	}
	
	public Integer getIdEntidade() {
		return idEntidade;
	}
	
	public void setIdEntidade(Integer idEntidades) {
		this.idEntidade = idEntidades;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getSigla() {
		return sigla;
	}
	
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	
	public boolean isOrgao() {
		return orgao;
	}
	
	public void setOrgao(boolean orgao) {
		this.orgao = orgao;
	}
	
	public boolean isAtiva() {
		return ativa;
	}
	
	public void setAtiva(boolean ativa) {
		this.ativa = ativa;
	}

	@Override
	public String toString() {
		return "Entidade [idEntidade=" + idEntidade + ", nome=" + nome + ", sigla=" + sigla
				+ ", orgao=" + orgao + ", ativa=" + ativa + "]";
	}

	public List<Competencia> getCompetencias() {
		return competencias;
	}

	public void setCompetencias(List<Competencia> competencias) {
		this.competencias = competencias;
	}
	
	public void addCompetencia(Competencia competencia) {
		competencia.setEntidade(this);
		competencias.add(competencia);
	}

	public Integer getIdSuperior() {
		return idSuperior;
	}

	public void setIdSuperior(Integer idSuperior) {
		this.idSuperior = idSuperior;
	}

	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getHorario() {
		return horario;
	}

	public void setHorario(String horario) {
		this.horario = horario;
	}

	public List<Responsavel> getOuvidores() {
		return ouvidores;
	}

	public void setOuvidores(List<Responsavel> ouvidores) {
		this.ouvidores = ouvidores;
	}

	public Responsavel getAtualizado_por() {
		return atualizado_por;
	}

	public void setAtualizado_por(Responsavel atualizado_por) {
		this.atualizado_por = atualizado_por;
	}

	public Date getAtualizado_em() {
		return atualizado_em;
	}

	public void setAtualizado_em(Date atualizado_em) {
		this.atualizado_em = atualizado_em;
	}
	
	
}
