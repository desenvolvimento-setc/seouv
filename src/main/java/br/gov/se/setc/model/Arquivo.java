package br.gov.se.setc.model;

import java.io.Serializable;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Date;

public class Arquivo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String pasta;
	private String nome;
	private String formato;
	private String tamanho;
	private Date dtAtualizacao;
	private String endereco;
	
	public Arquivo(String pasta, String nome, String formato, String tamanho, Date dtAtualizacao, String endereco) {
		super();
		this.pasta = pasta;
		this.nome = nome;
		this.formato = formato;
		this.tamanho = tamanho;
		this.dtAtualizacao = dtAtualizacao;
		this.endereco = endereco;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getPasta() {
		return pasta;
	}
	public void setPasta(String pasta) {
		this.pasta = pasta;
	}
	
	public Date getDtAtualizacao() {
		return  dtAtualizacao;
	}
	public void setDtAtualizacao(Date dtAtualizacao) {
		this.dtAtualizacao = dtAtualizacao;
	}

	public String getTamanho() {
		DecimalFormat df = new DecimalFormat("#.###");
		df.setRoundingMode(RoundingMode.CEILING);
		
		Double tamanhoNum = Double.parseDouble(tamanho) / (1024L * 1024L);
		return String.format(df.format(tamanhoNum), tamanhoNum) + " MB";
	}
	public void setTamanho(String tamanho) {
		this.tamanho = tamanho;
	}

	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}


	public String getFormato() {
		return formato;
	}


	public void setFormato(String formato) {
		this.formato = formato;
	}
	
	
}
