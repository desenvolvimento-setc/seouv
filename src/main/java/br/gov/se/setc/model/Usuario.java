package br.gov.se.setc.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import org.hibernate.annotations.Where;

@Entity
@Table(name="usuario")
public class Usuario implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idUsuario", unique = true, nullable = false)
	private Integer idUsuario;
	
	@Column(name = "nome", nullable = false, length = 100)
	private String nome;
	
	@Column(name = "nick", nullable = false, length = 100)
	private String nick;
	
	@Column(name = "senha", nullable = false, length = 64)
	private String senha;
	
	@Column(name = "perfil", nullable = false)
	private short perfil;
	
	@Column(name = "sessionId", length = 45)
	private String sessionId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "lastLogged", length = 19)
	private Date lastLogged;
	
	@Column(name = "migrado", nullable = false)
	private boolean migrado;

	@OneToMany(mappedBy="usuario", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Cidadao> cidadaos = new HashSet<>(0);
	
	@OneToMany(mappedBy="usuario", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@Where(clause = "ativo = true")
	private Set<Responsavel> responsaveis = new HashSet<>(0);
	
	
	public Usuario() {
		
	}
	
	public Usuario(String nome, String nick, String senha, String sessionId, short perfil, Date lastLogged, boolean migrado) {
		this.nome = nome;
		this.nick = nick;
		this.senha = senha;
		this.sessionId = sessionId;
		this.perfil = perfil;
		this.lastLogged = lastLogged;
		this.migrado = migrado;
	}
	
	public Usuario(String nome, String nick, String senha, short perfil, boolean migrado) {
		this.nome = nome;
		this.nick = nick;
		this.senha = senha;
		this.perfil = perfil;
		this.migrado = migrado;
	}
	
	public boolean isEmpty() {
		if(this.idUsuario == null)
			return true;
		else
			return false;
	}

	public Usuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	
	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public short getPerfil() {
		return perfil;
	}

	public void setPerfil(short perfil) {
		this.perfil = perfil;
	}

	public Date getLastLogged() {
		return lastLogged;
	}

	public void setLastLogged(Date lastLogged) {
		this.lastLogged = lastLogged;
	}

	public boolean isMigrado() {
		return migrado;
	}

	public void setMigrado(boolean migrado) {
		this.migrado = migrado;
	}
	
	public Set<Cidadao> getCidadaos() {
		return cidadaos;
	}

	public void setCidadaos(Set<Cidadao> cidadaos) {
		this.cidadaos = cidadaos;
	}
	
	public void addCidadao(Cidadao cidadao) {
		cidadao.setUsuario(this);
		cidadaos.add(cidadao);
	}

	public Set<Responsavel> getResponsaveis() {
		return responsaveis;
	}

	public void setResponsaveis(Set<Responsavel> responsaveis) {
		this.responsaveis = responsaveis;
	}
	
	public void addResponsavel(Responsavel responsavel) {
		responsavel.setUsuario(this);
		responsaveis.add(responsavel);
	}

	@Override
	public String toString() {
		return "Usuario [idUsuario=" + idUsuario + ", nome=" + nome + ", nick=" + nick + ", senha=" + senha
				+ ", sessionId=" + sessionId + ", perfil=" + perfil + ", lastLogged=" + lastLogged + ", migrado="
				+ migrado + "]";
	}
	
}
