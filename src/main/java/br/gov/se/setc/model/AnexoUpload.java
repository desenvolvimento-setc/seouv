package br.gov.se.setc.model;

import java.io.InputStream;
import java.io.Serializable;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Date;

public class AnexoUpload implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String nome;
	private String formato;
	private long tamanho;
	private InputStream stream;
	
	public AnexoUpload(String nome, String formato, long tamanho, InputStream stream) {
		super();
		this.nome = nome;
		this.formato = formato;
		this.tamanho = tamanho;
		this.stream = stream;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getFormato() {
		return formato;
	}
	public void setFormato(String formato) {
		this.formato = formato;
	}
	public long getTamanho() {
		return tamanho;
	}
	public void setTamanho(long tamanho) {
		this.tamanho = tamanho;
	}
	public InputStream getStream() {
		return stream;
	}
	public void setStream(InputStream stream) {
		this.stream = stream;
	}
	
	
	
	
}
