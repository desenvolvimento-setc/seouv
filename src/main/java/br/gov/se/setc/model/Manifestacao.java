package br.gov.se.setc.model;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.OrderBy;
import org.hibernate.annotations.Where;

@Entity
@Table(name = "solicitacao")
public class Manifestacao implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idSolicitacao", unique = true, nullable = false)
	private Integer idManifestacao;
	
	@Column(name = "instancia", length = 1)
	private short instancia;
	
	@Column(name = "status", length = 10)
	private String status;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "dataIni", length = 19)
	private Date dataIni;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "dataLimite", length = 19)
	private Date dataLimite;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "datafim", length = 10)
	private Date datafim;
	
	@Column(name = "titulo", length = 100)
	private String titulo;
	
	@Column(name = "protocolo", length = 46)
	private String protocolo;
	
	@Column(name = "tipo", length = 30)
	private String tipo;
	
	@Column(name = "encaminhada", length = 1)
	private boolean encaminhada;
	
	@Column(name = "visualizada", length = 1)
	private boolean visualizada;
	
	@Column(name = "sigilo", length = 1)
	private short sigilo;
	
	@Column(name = "formaRecebimento")
	private Integer formaRecebimento;
	
	@Column(name = "avaliacao")
	private Float avaliacao;
	
	@Column(name = "liberaDenuncia")
	private boolean liberaDenuncia;
	
	@Column(name = "canalEntrada", length = 1)
	private short canalEntrada;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idCidadao", nullable = false)
	private Cidadao cidadao;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idEntidades", nullable = false)
	private Entidade entidade;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idAcao", nullable = false)
	private Tema tema;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idResponsavel")
	private Responsavel responsavel;
	
	@OneToMany(mappedBy = "manifestacao", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@OrderBy(clause = "data ASC")
	@Where(clause = "tipo = 1 OR tipo = 2 OR tipo = 6 OR tipo = 7")
	private Set<Mensagem> mensagens = new LinkedHashSet<>(0);
	
	@OneToMany(mappedBy = "manifestacao", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@OrderBy(clause = "data ASC")
	@Where(clause = "tipo = 3 OR tipo = 4 OR tipo = 8")
	private Set<Mensagem> historico = new LinkedHashSet<>(0);
	
	@OneToMany(mappedBy = "manifestacao", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@OrderBy(clause = "data ASC")
	@Where(clause = "tipo = 5")
	private Set<Mensagem> tramites = new LinkedHashSet<>(0);
	
	@Column(name = "protocoloAgrese", length = 45)
	private String protocoloAgrese;

	
	public Manifestacao() {
		
	}
	
	
	public Manifestacao(String titulo, String protocolo, String tipo, String status, Date dataIni, Date dataLimite,
			Date datafim, Cidadao cidadao, Entidade entidade, Tema tema, short instancia, boolean encaminhada,
			boolean visualizada, short sigilo, Integer formaRecebimento, short canalEntrada, Responsavel responsavel) {
		this.titulo = titulo;
		this.protocolo = protocolo;
		this.tipo = tipo;
		this.status = status;
		this.dataIni = dataIni;
		this.dataLimite = dataLimite;
		this.datafim = datafim;
		this.cidadao = cidadao;
		this.entidade = entidade;
		this.tema = tema;
		this.instancia = instancia;
		this.encaminhada = encaminhada;
		this.visualizada = visualizada;
		this.sigilo = sigilo;
		this.formaRecebimento = formaRecebimento;
		this.canalEntrada = canalEntrada;
		this.responsavel = responsavel;
	}
	
	
	public Manifestacao(String titulo, String protocolo, String tipo, String status, Date dataIni, Date dataLimite,
			Date datafim, Cidadao cidadao, Entidade entidade, Tema tema, short instancia, boolean encaminhada,
			boolean visualizada, short sigilo, Integer formaRecebimento, short canalEntrada) {
		this.titulo = titulo;
		this.protocolo = protocolo;
		this.tipo = tipo;
		this.status = status;
		this.dataIni = dataIni;
		this.dataLimite = dataLimite;
		this.datafim = datafim;
		this.cidadao = cidadao;
		this.entidade = entidade;
		this.tema = tema;
		this.instancia = instancia;
		this.encaminhada = encaminhada;
		this.visualizada = visualizada;
		this.sigilo = sigilo;
		this.formaRecebimento = formaRecebimento;
		this.canalEntrada = canalEntrada;
	}


	public Integer getIdManifestacao() {
		return idManifestacao;
	}

	public void setIdManifestacao(Integer idManifestacao) {
		this.idManifestacao = idManifestacao;
	}

	public short getInstancia() {
		return instancia;
	}

	public void setInstancia(short instancia) {
		this.instancia = instancia;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getDataIni() {
		return dataIni;
	}

	public void setDataIni(Date dataIni) {
		this.dataIni = dataIni;
	}

	public Date getDataLimite() {
		return dataLimite;
	}

	public void setDataLimite(Date dataLimite) {
		this.dataLimite = dataLimite;
	}

	public Date getDatafim() {
		return datafim;
	}

	public void setDatafim(Date datafim) {
		this.datafim = datafim;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getProtocolo() {
		return protocolo;
	}

	public void setProtocolo(String protocolo) {
		this.protocolo = protocolo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public boolean isEncaminhada() {
		return encaminhada;
	}

	public void setEncaminhada(boolean encaminhada) {
		this.encaminhada = encaminhada;
	}

	public boolean isVisualizada() {
		return visualizada;
	}

	public void setVisualizada(boolean visualizada) {
		this.visualizada = visualizada;
	}

	public short getSigilo() {
		return sigilo;
	}

	public void setSigilo(short sigilo) {
		this.sigilo = sigilo;
	}

	public Integer getFormaRecebimento() {
		return formaRecebimento;
	}

	public void setFormaRecebimento(Integer formaRecebimento) {
		this.formaRecebimento = formaRecebimento;
	}

	public Float getAvaliacao() {
		return avaliacao;
	}

	public void setAvaliacao(Float avaliacao) {
		this.avaliacao = avaliacao;
	}

	public boolean isLiberaDenuncia() {
		return liberaDenuncia;
	}

	public void setLiberaDenuncia(boolean liberaDenuncia) {
		this.liberaDenuncia = liberaDenuncia;
	}

	public short getCanalEntrada() {
		return canalEntrada;
	}

	public void setCanalEntrada(short canalEntrada) {
		this.canalEntrada = canalEntrada;
	}

	public Cidadao getCidadao() {
		return cidadao;
	}

	public void setCidadao(Cidadao cidadao) {
		this.cidadao = cidadao;
	}

	public Entidade getEntidade() {
		return entidade;
	}

	public void setEntidade(Entidade entidade) {
		this.entidade = entidade;
	}

	public Tema getTema() {
		return tema;
	}

	public void setTema(Tema tema) {
		this.tema = tema;
	}

	public Responsavel getResponsavel() {
		return responsavel;
	}

	public void setResponsavel(Responsavel responsavel) {
		this.responsavel = responsavel;
	}

	public Set<Mensagem> getMensagens() {
		return mensagens;
	}

	public void setMensagens(Set<Mensagem> mensagens) {
		this.mensagens = mensagens;
	}

	public void addMensagem(Mensagem mensagem) {
		mensagem.setManifestacao(this);
		mensagens.add(mensagem);
	}
	
    public void addHistorico(Mensagem mensagem) {
        mensagem.setManifestacao(this);
        historico.add(mensagem);
    }

    public void addTramites(Mensagem mensagem) {
        mensagem.setManifestacao(this);
        tramites.add(mensagem);
    }


	@Override
	public String toString() {
		return "Manifestacao [idManifestacao=" + idManifestacao + ", instancia=" + instancia + ", status=" + status
				+ ", dataIni=" + dataIni + ", dataLimite=" + dataLimite + ", datafim=" + datafim + ", titulo=" + titulo
				+ ", protocolo=" + protocolo + ", tipo=" + tipo + ", encaminhada=" + encaminhada + ", visualizada="
				+ visualizada + ", sigilo=" + sigilo + ", formaRecebimento=" + formaRecebimento + ", avaliacao="
				+ avaliacao + ", liberaDenuncia=" + liberaDenuncia + ", canalEntrada=" + canalEntrada + ", entidade="
				+ entidade + ", tema=" + tema + "]";
	}


	public Set<Mensagem> getHistorico() {
		return historico;
	}


	public void setHistorico(Set<Mensagem> historico) {
		this.historico = historico;
	}


	public Set<Mensagem> getTramites() {
		return tramites;
	}


	public void setTramites(Set<Mensagem> tramites) {
		this.tramites = tramites;
	}


	public String getProtocoloAgrese() {
		return protocoloAgrese;
	}


	public void setProtocoloAgrese(String protocoloAgrese) {
		this.protocoloAgrese = protocoloAgrese;
	}
	
	
}
